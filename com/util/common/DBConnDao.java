package com.util.common;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

//获取注入之后的Dao连接 https://blog.csdn.net/pengyu1801/article/details/60143810 在spring-common中创建模板并需要注入数据源，配置dao，然后执行数据

public class DBConnDao {
    
    private JdbcTemplate jdbcTemplate;
    
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }
    
    /*
    String xmlPath = "config/spring-common.xml";
    BeanFactory context = new ClassPathXmlApplicationContext(xmlPath);//读取xml文件
    DBConnDao userDao = (DBConnDao) context.getBean("personDaoId");//获取personDaoId
    userDao.insert();//执行数据
    */
    
    public int insert() {
        String sql = "INSERT INTO userconn(VARVALUE1, VARVALUE2) VALUES ('test2', '123456')";
        return jdbcTemplate.update(sql);
    }
    
}
