package com.util.common;

import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.FileOutputStream;
import java.net.HttpURLConnection;
import java.io.IOException;
import java.net.URL;

public class FileEdit {
	
	public boolean booleanValue = false;
	
	public boolean isFile(String path){
		booleanValue = false;
		File f = new File(path);
		try{
			if(f.isFile()){
				booleanValue = true;
			}	
		}catch(Exception e){
			System.out.println("FileEdit isFile:" + e.getLocalizedMessage());
		}
		
		return booleanValue;
	}
	
	public void deleteFile(String path){
		File f = new File(path);  //absolute path
		try{
			if(f.isFile()){
				f.delete();
			}
		}catch(Exception e){
			System.out.println("FileEdit deleteFile:" + e.getLocalizedMessage());
		}
	}
	
	public boolean deleteFile1(String path){
		booleanValue = false;
		File f = new File(path);
		try{
			if(f.isFile()){
				if(f.delete())
					booleanValue = true;
				
			}
		}catch(Exception e){
			System.out.println("FileEdit deleteFile1:" + e.getLocalizedMessage());
		}
		
		return booleanValue;
	}
	
	public boolean createFolders(String _path) {
		boolean success = false;
		String filePath = _path;
		try{
			File fileDir = new File(filePath);
			if(!fileDir.isDirectory()){
				fileDir.mkdirs();
			}
			success = fileDir.isDirectory();
		} catch (Exception e) {
			System.out.println("FileEdit createFolders:" + e.getLocalizedMessage());
		}
		
		return  success;
	}
	
	public boolean checkFolder(String path) {
		boolean success = false;
		try{
			File fileDir = new File(path);
			
			if(fileDir.isDirectory()){
				success = true;
			}
			
		} catch (Exception e) {
			System.out.println("FileEdit checkFolder:" + e.getLocalizedMessage());
		}
		
		return success;
	}
	
	public boolean renameFolders(String oldName,String newName) {
		boolean success = false;
		try{
			File oldDir = new File(oldName);
			
			if(oldDir.isDirectory()){
				File newDir = new File(newName);
				success = oldDir.renameTo(newDir);
			}
			
		} catch (Exception e) {
			System.out.println("FileEdit renameFolders:" + e.getLocalizedMessage());
		}
		
		return success;
	}
	
	public boolean createFile(String _path,String _fileName) throws IOException {
		boolean success = false;
		String fileName = _fileName;
		String filePath = _path;
		try{
			File file = new File(filePath);
			if(!file.isDirectory()){
				file.mkdirs();
			}
			
			File f = new File(filePath + File.separator +  fileName  +  ".txt");
			success  =  f.createNewFile();
		} catch (IOException e) {
			System.out.println("FileEdit createFile:" + e.getLocalizedMessage());
		}
		
		return  success;
	}
	
	public void writeFile(String _path,String _fileName,String insertString,boolean isLast) throws IOException {
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(_path + File.separator + _fileName + ".txt"));
			out.write(insertString);
			if(!isLast)
				out.newLine();
			out.flush();
			out.close();
		} catch (IOException e) {
			System.out.println("FileEdit writeFile:" + e.getLocalizedMessage());
		}
	}
	
	public boolean saveUrlFile(String fileUrl, String fileDir, String fileName) {  //此方法只能用于HTTP协议
		
		String fileAddr = fileDir + fileName;
		File dirPath = new File(fileDir);
		if (!dirPath.exists()) {
			dirPath.mkdirs();
		}
		
		try {
			URL url = new URL(fileUrl);
		    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		    DataInputStream in = new DataInputStream(connection.getInputStream());
		    DataOutputStream out = new DataOutputStream(new FileOutputStream(fileAddr));
		    byte[] buffer = new byte[4096];
		    int count = 0;
		    while ((count = in.read(buffer)) > 0) {
		    	out.write(buffer, 0, count);
		    }
		    out.close();
		    in.close();
		    return true;
		} catch (Exception e) {
			System.out.println("FileEdit saveUrlFile:" + e.getLocalizedMessage());
			return false;
		}
	}
	
	
	
}
