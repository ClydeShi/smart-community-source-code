package com.util.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class BaseUpload {
	
	public static String GetImageStr(String imgFile){//将图片文件转化为字节数组字符串，并对其进行Base64编码处理
        InputStream in = null;  
        byte[] data = null;  
        //读取图片字节数组  
        try   
        {  
            in = new FileInputStream(imgFile);          
            data = new byte[in.available()];  
            in.read(data);  
            in.close();  
        }   
        catch (IOException e)   
        {  
            e.printStackTrace();  
        }  
        //对字节数组Base64编码  
        BASE64Encoder encoder = new BASE64Encoder();  
        return encoder.encode(data);//返回Base64编码过的字节数组字符串  
    }
	
	public static boolean GenerateImage(String base64Data,String savePath){//对字节数组字符串进行Base64解码并生成图片
        if (base64Data == null) //图像数据为空  
            return false;
        
        BASE64Decoder decoder = new BASE64Decoder();  
        try {  
            //Base64解码  
            byte[] b = decoder.decodeBuffer(base64Data);  
          //  System.out.println("解码完成");
            for(int i=0;i<b.length;++i)  
            {  
                if(b[i]<0)  
                {//调整异常数据  
                    b[i]+=256;  
                }  
            }
           // System.out.println("开始生成图片");
            //生成jpeg图片  
            OutputStream out = new FileOutputStream(savePath);
            out.write(b);  
            out.flush();  
            out.close();
            return true;
        } catch (Exception e) {
        	System.out.println("Base64 GenerateImage error");
            return false;  
        }
        /*
        try{ 
            String dataPrix = "";
            String data = "";
            if(base64Data == null || "".equals(base64Data)){
                throw new Exception("上传失败，上传图片数据为空");
            }else{
                String [] d = base64Data.split("base64,");
                if(d != null && d.length == 2){
                    dataPrix = d[0];
                    data = d[1];
                }else{
                    throw new Exception("上传失败，数据不合法");
                }
            }
            String suffix = "";
            if("data:image/jpeg;".equalsIgnoreCase(dataPrix)){//data:image/jpeg;base64,base64编码的jpeg图片数据
                suffix = ".jpg";
            } else if("data:image/x-icon;".equalsIgnoreCase(dataPrix)){//data:image/x-icon;base64,base64编码的icon图片数据
                suffix = ".ico";
            } else if("data:image/gif;".equalsIgnoreCase(dataPrix)){//data:image/gif;base64,base64编码的gif图片数据
                suffix = ".gif";
            } else if("data:image/png;".equalsIgnoreCase(dataPrix)){//data:image/png;base64,base64编码的png图片数据
                suffix = ".png";
            }else{
                throw new Exception("上传图片格式不合法");
            }
           
            File fImg = buildYuLanFolder(request);
            File fileUploadurl = new File(fImg.getCanonicalPath() + "/"
                    + Math.random() + suffix);
                    
             //进行解密
            byte[] result = Base64.decodeBase64(data);
            FileCopyUtils.copy(result, fileUploadurl);
            
            String strURL = fileUploadurl.getCanonicalPath();
            System.out.println(strURL);
            strURL = strURL.substring(request.getSession()
                    .getServletContext().getRealPath("/").length() - 1);
            strURL = strURL.replace("\\", "/");// 转换正反斜杠
            System.out.println("////" + strURL);
            
            return renderData(true, "ok", strURL);
        }catch (Exception e) {
            System.out.print(e.getMessage());
            return renderMsg(false, "ng");
        }*/
        
    }
	
}
