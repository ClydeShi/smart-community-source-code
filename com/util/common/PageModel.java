package com.util.common;

import java.util.List;


public class PageModel<T> {

    private Integer pageNumber; //当前页数
    private Integer pageSize;   //一页显示数量
    private Integer startRow;   //查询起始行
    private Integer endRow;   //查询起始行
    private Integer total;      //总记录行数
    private Integer curPages;
    private Integer totalPages;
    private List<T> rows;       //查询结果数据
    private T queryObj;         //查询对象
    
    public void setPages(int total, int pageNumber) {
		this.total = total;
		this.pageNumber = pageNumber;
	}
    
    //用于mysql分页
    public Integer getStartRow2() {
        if(pageNumber!=null && pageSize!=null) {
            return (pageNumber - 1) * pageSize;
        } else {
            return 0;
        }
    }
    
    //用于oracle分页
    public Integer getStartRow() {
        if(pageNumber!=null && pageSize!=null) {
            return (pageNumber - 1) * pageSize + 1;
        } else {
            return 0;
        }
    }
    
    //用于oracle分页, xml里面parameterType="com.util.common.PageModel", 通过#{endRow}获取
    public Integer getEndRow() {
		return getStartRow() + pageSize -1;
	}
    
	public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<T> getRows() {
        return rows;
    }

    public void setRows(List<T> rows) {
        this.rows = rows;
    }

    public void setQueryObj(T queryObj) {
        this.queryObj = queryObj;
    }

    public T getQueryObj() {
        return queryObj;
    }
    
    public Integer getTotalPages() {
    	//return totalPages;
    	totalPages = getTotal() / getPageSize();
		return (total % pageSize == 0) ? totalPages
				: totalPages + 1;
	}

	public void setTotalPages(Integer totalPages) {
		this.totalPages = totalPages;
	}
	
    public int getCurrentPage(String strPage){
        try {
            if (strPage == null || strPage.equals("")) {
                curPages = 1;
            } else {
                curPages = Integer.parseInt(strPage);
                if (curPages < 1)
                    curPages = 1;
            }
        } catch (Exception e) {
            System.out.println("curPages parameter exception");
        }
        return curPages;
    }

    public int getTotolPage(int totalRecords,int pageSize){
        int test;
        test = totalRecords % pageSize;
        if (test == 0)
        	totalPages = totalRecords / pageSize;
        else
        	totalPages = totalRecords / pageSize + 1;
        
        return totalPages;
    }
    
    public String BuilderPage(int totalRecords,int totalPages,int currentPage,int pageSize,String link){
    	
        int alter = 4 ;
        int startPage = 1 ;
        int endPage = currentPage + alter ;

        if(currentPage > alter)
        {
            startPage = currentPage - alter;
        }

        if(endPage > totalPages)
        {
            endPage = totalPages;
        }

        StringBuffer sb = new StringBuffer("");
        if(totalRecords > 0){
        	sb.append("<li><a href='javascript:void(0);'>共"+totalRecords+"条记录</a></li>");
        }
        if(currentPage != startPage && totalPages != 0)
        {
            int lastPage = currentPage-1;
            sb.append("<li><a href='"+link+"&page="+lastPage+"'><i class='fa fa-chevron-left'></i></a></li>");
        }

        for( int i = startPage ; i <= endPage ; i++ )
        {
            if (currentPage == i) {
                sb.append("<li class='active'><a href='"+link+"&page="+i+"'>" + i + "</a></li>");
            } else {
                sb.append("<li><a href='"+link+"&page="+i+"'>"+i+"</a></li>");
            }
        }

        if(currentPage != endPage && totalPages != 0)
        {
            int nextPage = currentPage+1;
            sb.append("<li><a href='"+link+"&page="+nextPage+"'><i class='fa fa-chevron-right'></i></a></li>");
        }

        return sb+"";

    }
}