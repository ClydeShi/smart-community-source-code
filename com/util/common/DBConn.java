package com.util.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.alibaba.druid.pool.DruidDataSource;

public class DBConn {
	
	private static DruidDataSource dataSource = null;//建立连接池，并获取数据源
	
	private Connection conn = null;
	private PreparedStatement prepstmt = null;
	private Statement stmt;
    private ResultSet rs = null;
    private int i = 0;
    
    static {
        try {
            String driver = "com.mysql.jdbc.Driver";
            String url = "jdbc:mysql://localhost:3306/mybatis?useUnicode=true&characterEncoding=utf-8";
            String user = "root";
            String password = "admin88";
            
            dataSource = new DruidDataSource();
            dataSource.setDriverClassName(driver);
            dataSource.setUrl(url);
            dataSource.setUsername(user);
            dataSource.setPassword(password);
            dataSource.setInitialSize(5);
            dataSource.setMinIdle(1);
            dataSource.setMaxActive(200);
            
            //连接泄漏监测
            dataSource.setRemoveAbandoned(true);
            dataSource.setRemoveAbandonedTimeout(30);//秒
            //配置获取连接等待超时的时间
            dataSource.setMaxWait(20000);
            //配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒
            dataSource.setTimeBetweenEvictionRunsMillis(20000);
            //防止过期
            dataSource.setValidationQuery("SELECT 'x'");
            dataSource.setTestWhileIdle(true);
            dataSource.setTestOnBorrow(true);
            
            dataSource.setPoolPreparedStatements(false);
            System.out.println("启用DruidDataSource连接池");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static synchronized Connection getConnection() {
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conn;
    }
    
    public ResultSet executeQuery(String sql) {
    	rs = null;
    	try {
    		if(conn==null || conn.isClosed())
    			conn = getConnection();
    		stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rs = stmt.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("DBUtil executeQuery: " + e.getMessage());
		}
    	
    	return rs;
    }
    
    public int executeUpdate(String sql) {
    	Statement stmt;
		try {
			if(conn==null || conn.isClosed())
				conn = getConnection();			
			stmt = conn.createStatement();
			i = stmt.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("DBUtil executeUpdate: " + e.getMessage());
		}
        
        return i;
    }
    
    public int executeUpdateAutoCommit(String sql) {
    	int intValue = 0;
    	try {
    		//Connection conn = null;
    		//Statement stmt = null;
    		conn = getConnection();
    		conn.setAutoCommit(false);
    		stmt = conn.createStatement();
    		
    		stmt.execute(sql);
    		conn.commit();  //数据库操作最终提交给数据库
			conn.setAutoCommit(true);
			intValue = 1;
    		
        } catch (SQLException ex) {
        	intValue = 0;
    		try {
				conn.rollback();
			} catch (SQLException e) {
				e.printStackTrace();
			}
    		System.out.println("DBUtil executeUpdateAutoCommit:" + ex.getMessage());
        }
        return intValue;
    }
    
    public void prepareStatement(String sql){
        try{
            prepstmt = this.conn.prepareStatement(sql);
        }
        catch(Exception e){
        	System.out.println("DBUtil prepareStatement:" + e.getMessage());
        }
   }
	
	public void setString(int index,String value){
       try{
           //value = new String(value.getBytes("GBK"), "ISO8859_1");
           prepstmt.setString(index, value);
       }
       catch(Exception e){
    	   System.out.println("DBUtil setString:" + e.getMessage());
       }
   }
	
	public void setInt(int index,int value){
       try{
           prepstmt.setInt(index, value);
       }
       catch(Exception e){
    	   System.out.println("DBUtil setInt:" + e.getMessage());
       }
   }
	
	public void clearParameters(){
       try{
           prepstmt.clearParameters();
           prepstmt=null;
       }
       catch(Exception e){
    	   System.out.println("DBUtil clearParameters:" + e.getMessage());
       }
   }
	
	public PreparedStatement getPreparedStatement(){
       return prepstmt;
   }
	
	public ResultSet executeQuery(){
		ResultSet rs=null;
		try{
			if(prepstmt!= null){
				rs = prepstmt.executeQuery();
			}
		}catch(Exception e){
			System.out.println("DBUtil executeQuery:" + e.getMessage());
		}
		
		return rs;
	}
	
	public int executeUpdate(){
		try{
			if(prepstmt!=null){
				i=prepstmt.executeUpdate();
			}
		}catch(Exception e){
			System.out.println("DBUtil executeUpdate:" + e.getMessage());
		}
		
		return i;
	}
    
    public void close() {
    	if (rs != null) {
    		try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
    	}
    	if (stmt != null) {
    		try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
    	}
    	if (conn != null) {
    		try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
    	}
    }
    
}
