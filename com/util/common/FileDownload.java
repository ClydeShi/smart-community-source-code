package com.util.common;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

public class FileDownload extends HttpServlet {	
	private static final long serialVersionUID = -8817483716216321263L;
	private static final String CONTENT_TYPE = "text/html; charset=utf-8";
    
    //Initialize global variables
    public FileDownload() {
		super();
	}

    //Process the HTTP Get request
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {

		String filePath = request.getParameter("file");
		
		String sPath = request.getSession().getServletContext().getRealPath("/");		

		File dfile = new File(sPath+filePath);
        //String fileName = dfile.getName();
        
        String fileName = request.getParameter("name");
        fileName = new String(fileName.getBytes(), "ISO-8859-1");
        //fileName = java.net.URLDecoder.decode(fileName,"gbk");

		response.setContentType("application/x-msdownload");
		response.setHeader("Content-Disposition", "attachment; filename=" + fileName);

		ServletOutputStream sos = null;
		FileInputStream fis = null;
		
		try {
			
			sos = response.getOutputStream();
			fis = new FileInputStream(dfile);
			byte[] b = new byte[(int) dfile.length()];

			int i = 0;
			while ((i = fis.read(b)) > 0) {
				sos.write(b, 0, i);
			}

			sos.flush();
		} catch (Exception e) {
			
            System.out.println(e.toString());
            
        } finally {
        	
            if (fis != null) {
                fis.close();
                fis = null;
            }
            if (sos != null) {
            	sos.close();
            	sos = null;
            }
            
        }
        
        
    }
    
    //Process the HTTP Post request
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {
        doGet(request, response);

	}

    //Clean up resources
    public void destroy() {
    }
}
