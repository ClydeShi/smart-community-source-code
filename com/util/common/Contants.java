package com.util.common;

public class Contants {
	public static final String SESSION_SECURITY_CODE = "sessionSecCode";
	public static final String SESSION_USER = "sessionUser";
	public static final String SESSION_AUTHORITY = "authority";

    public Contants() {
    }
}
