package com.util.weixin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import com.alibaba.fastjson.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.ConnectException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class WeixinUtil {
	private static Logger log = LoggerFactory.getLogger(WeixinUtil.class);
	
	private String AppID = "wx6ed44d2601d37947";
	private String AppSecret = "70969d07ee0b23fe8b8f9a80360750c7";
	/*
	https://www.cnblogs.com/fengli9998/p/7402507.html
	使用net.sf.json.JSONObject新增了以下jar包
	commons-beanutils-1.8.3
	commons-codec-1.10
	commons-collections-3.2.1
	commons-configuration-1.10
	commons-httpclient-3.0.1
	json-lib-2.3-jdk15
	*/
	
	/**
	 * URL编码（utf-8）
	 * 
	 * @param source
	 * @return
	 */
	public static String urlEncodeUTF8(String source) {
		String result = source;
		try {
			result = java.net.URLEncoder.encode(source, "utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public static String httpGet(String url){
		String strResult = null;
		CloseableHttpClient httpClient = HttpClients.createDefault();
		CloseableHttpResponse response = null;
        try {
            HttpGet httpGet = new HttpGet(url);
            response = httpClient.execute(httpGet);
            //System.out.println("========接口返回getStatusCode=======" + response.getStatusLine().getStatusCode());
            //System.out.println("========接口返回SC_OK=======" + org.apache.http.HttpStatus.SC_OK);
            HttpEntity entity1 = response.getEntity();
            if(entity1 != null){
            	//strResult = EntityUtils.toString(entity1);
            	strResult = EntityUtils.toString(entity1,"UTF-8");
            	System.out.println("========接口返回=======" + strResult);
    		}
            
            /*
            if (response.getStatusLine().getStatusCode() == org.apache.http.HttpStatus.SC_OK) {//也可以使用此判断，值为200
                  strResult = EntityUtils.toString(response.getEntity());  
            } else {
            	System.out.println("get请求提交失败");
            }
            */
            
            EntityUtils.consume(entity1);
            
        } catch (IOException e) {
        	System.out.println("get请求提交失败:" + e.getMessage());
        } finally {
        	if (response != null) {
                try {
                	response.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (httpClient != null) {
                try {
                	httpClient.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return strResult;
    }
	
	public static JSONObject strToJson(String rlt){
		JSONObject obj = new JSONObject();
		obj = (JSONObject) JSONObject.parse(rlt);
		return obj;
	}
	
	/*
	 * 微信小程序获取openid
	 */
	public String getOpenId(String code) {
		log.info("code：{}", code);
		String url = "https://api.weixin.qq.com/sns/jscode2session?appid=APPID&secret=SECRET&js_code=CODE&grant_type=authorization_code";
		String requestUrl = url.replace("APPID", AppID).replace("SECRET", AppSecret).replace("CODE", code);
		
		JSONObject jsonObject = strToJson(httpGet(requestUrl));
		System.out.println(jsonObject.toString());
		System.out.println(jsonObject.getString("openid"));
		System.out.println(jsonObject.getString("session_key"));
		String Openid =String.valueOf(jsonObject.get("openid"));
		
		return Openid;
	}
	
}
