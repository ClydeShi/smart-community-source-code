package com.util.weixin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import net.sf.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.ConnectException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * 使用net.sf.json.JSONObject实现
 */

public class WeixinUtil2 {
	private static Logger log = LoggerFactory.getLogger(WeixinUtil2.class);
	
	private String AppID = "wx6ed44d2601d37947";
	private String AppSecret = "70969d07ee0b23fe8b8f9a80360750c7";
	/*
	https://www.cnblogs.com/fengli9998/p/7402507.html
	使用net.sf.json.JSONObject新增了以下jar包
	commons-beanutils-1.8.3
	commons-codec-1.10
	commons-collections-3.2.1
	commons-configuration-1.10
	commons-httpclient-3.0.1
	json-lib-2.3-jdk15
	*/

	/**
	 * 发送https请求
	 * 
	 * @param requestUrl
	 *            请求地址
	 * @param requestMethod
	 *            请求方式（GET、POST）
	 * @param outputStr
	 *            提交的数据
	 * @return JSONObject(通过JSONObject.get(key)的方式获取json对象的属性值)
	 */
	public static JSONObject httpsRequest(String requestUrl, String requestMethod, String outputStr) {
		JSONObject jsonObject = null;
		try {
			// 创建SSLContext对象，并使用我们指定的信任管理器初始化
			TrustManager[] tm = { new MyX509TrustManager() };
			SSLContext sslContext = SSLContext.getInstance("SSL", "SunJSSE");
			sslContext.init(null, tm, new java.security.SecureRandom());
			// 从上述SSLContext对象中得到SSLSocketFactory对象
			SSLSocketFactory ssf = sslContext.getSocketFactory();

			URL url = new URL(requestUrl);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			conn.setSSLSocketFactory(ssf);

			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setUseCaches(false);
			// 设置请求方式（GET/POST）
			conn.setRequestMethod(requestMethod);

			// 当outputStr不为null时向输出流写数据
			if (null != outputStr) {
				OutputStream outputStream = conn.getOutputStream();
				// 注意编码格式
				outputStream.write(outputStr.getBytes("UTF-8"));
				outputStream.close();
			}

			// 从输入流读取返回内容
			InputStream inputStream = conn.getInputStream();
			InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "utf-8");
			BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
			String str = null;
			StringBuffer buffer = new StringBuffer();
			while ((str = bufferedReader.readLine()) != null) {
				buffer.append(str);
			}

			// 释放资源
			bufferedReader.close();
			inputStreamReader.close();
			inputStream.close();
			inputStream = null;
			conn.disconnect();
			jsonObject = JSONObject.fromObject(buffer.toString());
		} catch (ConnectException ce) {
			log.error("连接超时：{}", ce);
		} catch (Exception e) {
			log.error("https请求异常：{}", e);
		}
		return jsonObject;
	}
	
	/**
	 * URL编码（utf-8）
	 * 
	 * @param source
	 * @return
	 */
	public static String urlEncodeUTF8(String source) {
		String result = source;
		try {
			result = java.net.URLEncoder.encode(source, "utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public static String httpGet(String url){
		String strResult = null;
		CloseableHttpClient httpClient = HttpClients.createDefault();
		CloseableHttpResponse response = null;
        try {
            HttpGet httpGet = new HttpGet(url);
            response = httpClient.execute(httpGet);
            //System.out.println("========接口返回getStatusCode=======" + response.getStatusLine().getStatusCode());
            //System.out.println("========接口返回SC_OK=======" + org.apache.http.HttpStatus.SC_OK);
            HttpEntity entity1 = response.getEntity();
            if(entity1 != null){
            	//strResult = EntityUtils.toString(entity1);
            	strResult = EntityUtils.toString(entity1,"UTF-8");
            	System.out.println("========接口返回=======" + strResult);
    		}
            
            /*
            if (response.getStatusLine().getStatusCode() == org.apache.http.HttpStatus.SC_OK) {//也可以使用此判断，值为200
                  strResult = EntityUtils.toString(response.getEntity());  
            } else {
            	System.out.println("get请求提交失败");
            }
            */
            
            EntityUtils.consume(entity1);
            
        } catch (IOException e) {
        	System.out.println("get请求提交失败:" + e.getMessage());
        } finally {
        	if (response != null) {
                try {
                	response.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (httpClient != null) {
                try {
                	httpClient.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return strResult;
    }
	
	public String getOpenId(String code) {
		log.info("code：{}", code);
		String url = "https://api.weixin.qq.com/sns/jscode2session?appid=APPID&secret=SECRET&js_code=CODE&grant_type=authorization_code";
		String requestUrl = url.replace("APPID", AppID).replace("SECRET", AppSecret).replace("CODE", code);
		
		JSONObject jsonObject = JSONObject.fromObject(httpGet(requestUrl));
		System.out.println(jsonObject.toString());
		String Openid =String.valueOf(jsonObject.get("openid"));
		
		/*if (null != jsonObject) {
			int errorCode = jsonObject.getInt("errcode");
			String errorMsg = jsonObject.getString("errmsg");
			if (errorCode == 0) {
				result = true;
				log.info("客服消息发送成功 errcode:{} errmsg:{}", errorCode, errorMsg);
			} else if (errorCode == 43004) {
				log.info("客服消息发送失败 errcode:{} errmsg:{}", errorCode, errorMsg);//未关注
			} else {
				log.error("客服消息发送失败 errcode:{} errmsg:{}", errorCode, errorMsg);
			}
		}*/
		
		return Openid;
	}
	
}
