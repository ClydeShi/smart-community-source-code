package com.luktel.service;

import java.util.List;
import java.util.Map;

import com.luktel.model.Meeting;

/*
 * ArchiveMapper和ArchiveService代码基本一样
 */

public interface MeetingService {
	
	List<Meeting> pageList(Map<String, Object> paramMap);
	Integer totalRecord(Map<String, Object> paramMap);
	
	List<Map<String,Object>> selectMapList(int typeId);
	List<String> selectList(int typeId);
	
	List<Meeting> findAll();
	Meeting findById(int id);
	void save(Meeting data);
	boolean update(Meeting data);
	boolean updateByTask(Meeting data);
	boolean updateByMap(Map<String, Object> paramMap);
	boolean delete(int id);
	boolean deleteByMap(Map<String, Object> paramMap);
	Integer getIntValue(Map<String, Object> paramMap);
	String getStringValue(Map<String, Object> paramMap);
	List<Meeting> getListByMap(Map<String, Object> paramMap);
	Meeting getByMap(Map<String, Object> paramMap);

}
