package com.luktel.service;

import java.util.List;
import java.util.Map;

import com.luktel.model.Clean;

/*
 * ArchiveMapper和ArchiveService代码基本一样
 */

public interface CleanService {
	
	List<Clean> pageList(Map<String, Object> paramMap);
	Integer totalRecord(Map<String, Object> paramMap);
	
	List<Map<String,Object>> selectMapList(int typeId);
	List<String> selectList(int typeId);
	
	List<Clean> findAll();
	Clean findById(int id);
	void save(Clean data);
	boolean update(Clean data);
	boolean updateByTask(Clean data);
	boolean updateByMap(Map<String, Object> paramMap);
	boolean delete(int id);
	boolean deleteByMap(Map<String, Object> paramMap);
	Integer getIntValue(Map<String, Object> paramMap);
	String getStringValue(Map<String, Object> paramMap);
	List<Clean> getListByMap(Map<String, Object> paramMap);
	Clean getByMap(Map<String, Object> paramMap);

}
