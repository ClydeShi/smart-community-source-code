package com.luktel.service;

import java.util.List;
import java.util.Map;

import com.luktel.model.Decorate;

/*
 * ArchiveMapper和ArchiveService代码基本一样
 */

public interface DecorateService {
	
	List<Decorate> pageList(Map<String, Object> paramMap);
	Integer totalRecord(Map<String, Object> paramMap);
	
	List<Map<String,Object>> selectMapList(int typeId);
	List<String> selectList(int typeId);
	
	List<Decorate> findAll();
	Decorate findById(int id);
	void save(Decorate data);
	boolean update(Decorate data);
	boolean updateByTask(Decorate data);
	boolean updateByMap(Map<String, Object> paramMap);
	boolean delete(int id);
	boolean deleteByMap(Map<String, Object> paramMap);
	Integer getIntValue(Map<String, Object> paramMap);
	String getStringValue(Map<String, Object> paramMap);
	List<Decorate> getListByMap(Map<String, Object> paramMap);
	Decorate getByMap(Map<String, Object> paramMap);

}
