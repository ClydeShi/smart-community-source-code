package com.luktel.service;

import java.util.List;
import java.util.Map;

import com.luktel.model.Menu;

/*
 * ArchiveMapper和ArchiveService代码基本一样
 */

public interface MenuService {
	
	List<Menu> pageList(Map<String, Object> paramMap);
	Integer totalRecord(Map<String, Object> paramMap);
	
	List<Map<String,Object>> selectMapList(int typeId);
	List<String> selectList(int typeId);
	
	List<Menu> findAll();
	Menu findById(int id);
	void save(Menu data);
	boolean update(Menu data);
	boolean updateByTask(Menu data);
	boolean updateByMap(Map<String, Object> paramMap);
	boolean delete(int id);
	boolean deleteByMap(Map<String, Object> paramMap);
	Integer getIntValue(Map<String, Object> paramMap);
	String getStringValue(Map<String, Object> paramMap);
	List<Menu> getListByMap(Map<String, Object> paramMap);
	Menu getByMap(Map<String, Object> paramMap);

}
