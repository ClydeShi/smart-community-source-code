package com.luktel.service;

import java.util.List;
import java.util.Map;

import com.luktel.model.MeetingRoom;

/*
 * ArchiveMapper和ArchiveService代码基本一样
 */

public interface MeetingRoomService {
	
	List<MeetingRoom> pageList(Map<String, Object> paramMap);
	Integer totalRecord(Map<String, Object> paramMap);
	
	List<Map<String,Object>> selectMapList(int typeId);
	List<String> selectList(int typeId);
	
	List<MeetingRoom> findAll();
	MeetingRoom findById(int id);
	void save(MeetingRoom data);
	boolean update(MeetingRoom data);
	boolean updateByTask(MeetingRoom data);
	boolean updateByMap(Map<String, Object> paramMap);
	boolean delete(int id);
	boolean deleteByMap(Map<String, Object> paramMap);
	Integer getIntValue(Map<String, Object> paramMap);
	String getStringValue(Map<String, Object> paramMap);
	List<MeetingRoom> getListByMap(Map<String, Object> paramMap);
	MeetingRoom getByMap(Map<String, Object> paramMap);

}
