package com.luktel.service;

import java.util.List;
import java.util.Map;

import com.luktel.model.Category;

/*
 * ArchiveMapper和ArchiveService代码基本一样
 */

public interface CategoryService {
	
	List<Category> pageList(Map<String, Object> paramMap);
	Integer totalRecord(Map<String, Object> paramMap);
	
	List<Map<String,Object>> selectMapList(int typeId);
	List<String> selectList(int typeId);
	
	List<Category> findAll();
	Category findById(int id);
	void save(Category data);
	boolean update(Category data);
	boolean updateByTask(Category data);
	boolean updateByMap(Map<String, Object> paramMap);
	boolean delete(int id);
	boolean deleteByMap(Map<String, Object> paramMap);
	Integer getIntValue(Map<String, Object> paramMap);
	String getStringValue(Map<String, Object> paramMap);
	List<Category> getListByMap(Map<String, Object> paramMap);
	Category getByMap(Map<String, Object> paramMap);

}
