package com.luktel.service;

import java.util.List;
import java.util.Map;

import com.luktel.model.Deptment;

/*
 * ArchiveMapper和ArchiveService代码基本一样
 */

public interface DeptmentService {
	
	List<Deptment> pageList(Map<String, Object> paramMap);
	Integer totalRecord(Map<String, Object> paramMap);
	
	List<Map<String,Object>> selectMapList(int typeId);
	List<String> selectList(int typeId);
	
	List<Deptment> findAll();
	Deptment findById(int id);
	void save(Deptment data);
	boolean update(Deptment data);
	boolean updateByTask(Deptment data);
	boolean updateByMap(Map<String, Object> paramMap);
	boolean delete(int id);
	boolean deleteByMap(Map<String, Object> paramMap);
	Integer getIntValue(Map<String, Object> paramMap);
	String getStringValue(Map<String, Object> paramMap);
	Deptment getByMap(Map<String, Object> paramMap);

}
