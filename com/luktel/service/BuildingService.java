package com.luktel.service;

import java.util.List;
import java.util.Map;

import com.luktel.model.Building;

/*
 * ArchiveMapper和ArchiveService代码基本一样
 */

public interface BuildingService {
	
	List<Building> pageList(Map<String, Object> paramMap);
	Integer totalRecord(Map<String, Object> paramMap);
	
	List<Map<String,Object>> selectMapList(int typeId);
	List<String> selectList(int typeId);
	
	List<Building> findAll();
	Building findById(int id);
	void save(Building data);
	boolean update(Building data);
	boolean updateByTask(Building data);
	boolean updateByMap(Map<String, Object> paramMap);
	boolean delete(int id);
	boolean deleteByMap(Map<String, Object> paramMap);
	Integer getIntValue(Map<String, Object> paramMap);
	String getStringValue(Map<String, Object> paramMap);
	List<Building> getListByMap(Map<String, Object> paramMap);
	Building getByMap(Map<String, Object> paramMap);

}
