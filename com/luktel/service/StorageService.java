package com.luktel.service;

import java.util.List;
import java.util.Map;

import com.luktel.model.Storage;

/*
 * ArchiveMapper和ArchiveService代码基本一样
 */

public interface StorageService {
	
	List<Storage> pageList(Map<String, Object> paramMap);
	Integer totalRecord(Map<String, Object> paramMap);
	
	List<Map<String,Object>> selectMapList(int typeId);
	List<String> selectList(int typeId);
	
	List<Storage> findAll();
	Storage findById(int id);
	void save(Storage data);
	boolean update(Storage data);
	boolean updateByTask(Storage data);
	boolean updateByMap(Map<String, Object> paramMap);
	boolean delete(int id);
	boolean deleteByMap(Map<String, Object> paramMap);
	Integer getIntValue(Map<String, Object> paramMap);
	String getStringValue(Map<String, Object> paramMap);
	List<Storage> getListByMap(Map<String, Object> paramMap);
	Storage getByMap(Map<String, Object> paramMap);

}
