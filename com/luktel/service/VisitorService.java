package com.luktel.service;

import java.util.List;
import java.util.Map;

import com.luktel.model.Visitor;

/*
 * ArchiveMapper和ArchiveService代码基本一样
 */

public interface VisitorService {
	
	List<Visitor> pageList(Map<String, Object> paramMap);
	Integer totalRecord(Map<String, Object> paramMap);
	
	List<Map<String,Object>> selectMapList(int typeId);
	List<String> selectList(int typeId);
	
	List<Visitor> findAll();
	Visitor findById(int id);
	void save(Visitor data);
	boolean update(Visitor data);
	boolean updateByTask(Visitor data);
	boolean updateByMap(Map<String, Object> paramMap);
	boolean delete(int id);
	boolean deleteByMap(Map<String, Object> paramMap);
	Integer getIntValue(Map<String, Object> paramMap);
	String getStringValue(Map<String, Object> paramMap);
	List<Visitor> getListByMap(Map<String, Object> paramMap);
	Visitor getByMap(Map<String, Object> paramMap);

}
