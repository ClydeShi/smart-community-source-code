package com.luktel.service;

import java.util.List;
import java.util.Map;

import com.luktel.model.GoodsName;

/*
 * ArchiveMapper和ArchiveService代码基本一样
 */

public interface GoodsNameService {
	
	List<GoodsName> pageList(Map<String, Object> paramMap);
	Integer totalRecord(Map<String, Object> paramMap);
	
	List<Map<String,Object>> selectMapList(int typeId);
	List<String> selectList(int typeId);
	
	List<GoodsName> findAll();
	GoodsName findById(int id);
	void save(GoodsName data);
	boolean update(GoodsName data);
	boolean updateByTask(GoodsName data);
	boolean updateByMap(Map<String, Object> paramMap);
	boolean delete(int id);
	boolean deleteByMap(Map<String, Object> paramMap);
	Integer getIntValue(Map<String, Object> paramMap);
	String getStringValue(Map<String, Object> paramMap);
	List<GoodsName> getListByMap(Map<String, Object> paramMap);
	GoodsName getByMap(Map<String, Object> paramMap);

}
