package com.luktel.service;

import java.util.List;
import java.util.Map;

import com.luktel.model.StorageIn;

/*
 * ArchiveMapper和ArchiveService代码基本一样
 */

public interface StorageInService {
	
	List<StorageIn> pageList(Map<String, Object> paramMap);
	Integer totalRecord(Map<String, Object> paramMap);
	
	List<Map<String,Object>> selectMapList(int typeId);
	List<String> selectList(int typeId);
	
	List<StorageIn> findAll();
	StorageIn findById(int id);
	void save(StorageIn data);
	boolean update(StorageIn data);
	boolean updateByTask(StorageIn data);
	boolean updateByMap(Map<String, Object> paramMap);
	boolean delete(int id);
	boolean deleteByMap(Map<String, Object> paramMap);
	Integer getIntValue(Map<String, Object> paramMap);
	String getStringValue(Map<String, Object> paramMap);
	List<StorageIn> getListByMap(Map<String, Object> paramMap);
	StorageIn getByMap(Map<String, Object> paramMap);

}
