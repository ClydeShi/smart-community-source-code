package com.luktel.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.luktel.model.Attachment;

/*
 * AttachmentMapper和AttachmentService代码基本一样
 */

public interface AttachmentService {
	
	List<Attachment> pageList(Map<String, Object> paramMap);
	Integer totalRecord(Map<String, Object> paramMap);
	
	List<Attachment> list(@Param("classId") int classId, @Param("infoId") int infoId);
	List<Attachment> findAll();
	String getFilePath(int id);
	void save(Attachment data);
	boolean update(@Param("conId") int conId, @Param("infoId") int infoId);
	boolean delete(int id);
	boolean deleteByFile(String filePath);
	List<Attachment> getListByMap(Map<String, Object> paramMap);

}
