package com.luktel.service;

import java.util.List;
import java.util.Map;

import com.luktel.model.Company;

/*
 * ArchiveMapper和ArchiveService代码基本一样
 */

public interface CompanyService {
	
	List<Company> pageList(Map<String, Object> paramMap);
	Integer totalRecord(Map<String, Object> paramMap);
	
	List<Map<String,Object>> selectMapList(int typeId);
	List<String> selectList(int typeId);
	
	List<Company> findAll();
	Company findById(int id);
	void save(Company data);
	boolean update(Company data);
	boolean updateByTask(Company data);
	boolean updateByMap(Map<String, Object> paramMap);
	boolean delete(int id);
	boolean deleteByMap(Map<String, Object> paramMap);
	Integer getIntValue(Map<String, Object> paramMap);
	String getStringValue(Map<String, Object> paramMap);
	List<Company> getListByMap(Map<String, Object> paramMap);
	Company getByMap(Map<String, Object> paramMap);

}
