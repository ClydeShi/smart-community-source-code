package com.luktel.service;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

import com.luktel.model.User;
import com.util.common.PageModel;

/*
 * UserMapper和UserService代码基本一样
 */

public interface UserService {
	
	List<User> pageList(Map<String, Object> paramMap);
	Integer totalRecord(Map<String, Object> paramMap);
	
	List<User> findAll();
	User findById(int id);
	void save(User user);
	boolean update(User user);
	boolean updateSpecialTask(User data);
	boolean delete(int id);
	
	public User getUserByNameAndPwd(String loginname, String password);
	Integer getIntValue(Map<String, Object> paramMap);
	String getStringValue(Map<String, Object> paramMap);
}
