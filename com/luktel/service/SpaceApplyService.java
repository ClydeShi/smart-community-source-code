package com.luktel.service;

import java.util.List;
import java.util.Map;

import com.luktel.model.SpaceApply;

/*
 * ArchiveMapper和ArchiveService代码基本一样
 */

public interface SpaceApplyService {
	
	List<SpaceApply> pageList(Map<String, Object> paramMap);
	Integer totalRecord(Map<String, Object> paramMap);
	
	List<Map<String,Object>> selectMapList(int typeId);
	List<String> selectList(int typeId);
	
	List<SpaceApply> findAll();
	SpaceApply findById(int id);
	void save(SpaceApply data);
	boolean update(SpaceApply data);
	boolean updateByTask(SpaceApply data);
	boolean updateByMap(Map<String, Object> paramMap);
	boolean delete(int id);
	boolean deleteByMap(Map<String, Object> paramMap);
	Integer getIntValue(Map<String, Object> paramMap);
	String getStringValue(Map<String, Object> paramMap);
	List<SpaceApply> getListByMap(Map<String, Object> paramMap);
	SpaceApply getByMap(Map<String, Object> paramMap);

}
