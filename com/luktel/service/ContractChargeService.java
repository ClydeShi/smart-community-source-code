package com.luktel.service;

import java.util.List;
import java.util.Map;

import com.luktel.model.ContractCharge;

/*
 * ArchiveMapper和ArchiveService代码基本一样
 */

public interface ContractChargeService {
	
	List<ContractCharge> pageList(Map<String, Object> paramMap);
	Integer totalRecord(Map<String, Object> paramMap);
	
	List<Map<String,Object>> selectMapList(int typeId);
	List<String> selectList(int typeId);
	
	List<ContractCharge> findAll();
	ContractCharge findById(int id);
	void save(ContractCharge data);
	boolean update(ContractCharge data);
	boolean updateByTask(ContractCharge data);
	boolean updateByMap(Map<String, Object> paramMap);
	boolean delete(int id);
	boolean deleteByMap(Map<String, Object> paramMap);
	Integer getIntValue(Map<String, Object> paramMap);
	String getStringValue(Map<String, Object> paramMap);
	List<ContractCharge> getListByMap(Map<String, Object> paramMap);
	ContractCharge getByMap(Map<String, Object> paramMap);

}
