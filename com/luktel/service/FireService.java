package com.luktel.service;

import java.util.List;
import java.util.Map;

import com.luktel.model.Fire;

/*
 * ArchiveMapper和ArchiveService代码基本一样
 */

public interface FireService {
	
	List<Fire> pageList(Map<String, Object> paramMap);
	Integer totalRecord(Map<String, Object> paramMap);
	
	List<Map<String,Object>> selectMapList(int typeId);
	List<String> selectList(int typeId);
	
	List<Fire> findAll();
	Fire findById(int id);
	void save(Fire data);
	boolean update(Fire data);
	boolean updateByTask(Fire data);
	boolean updateByMap(Map<String, Object> paramMap);
	boolean delete(int id);
	boolean deleteByMap(Map<String, Object> paramMap);
	Integer getIntValue(Map<String, Object> paramMap);
	String getStringValue(Map<String, Object> paramMap);
	List<Fire> getListByMap(Map<String, Object> paramMap);
	Fire getByMap(Map<String, Object> paramMap);

}
