package com.luktel.service.impl;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import javax.annotation.Resource;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.luktel.mapper.DictionaryMapper;
import com.luktel.model.Dictionary;
import com.luktel.service.DictionaryService;
import com.util.common.PageModel;


@Service
@Transactional  //此处不再进行创建SqlSession和提交事务，都已交由spring去管理了。
public class DictionaryServiceImpl implements DictionaryService {
	
	@Resource
	private DictionaryMapper mapper;
	
	public List<Dictionary> pageList(Map<String,Object> paramMap) {
		List<Dictionary> findAllList = mapper.pageList(paramMap);
		return findAllList;
	}
	public Integer totalRecord(Map<String,Object> paramMap) {
		Integer total = mapper.totalRecord(paramMap);
		return total;
	}
	
	public List<Map<String,Object>> selectDictionaryMapList(int typeId) {
		List<Map<String,Object>> findAllList = mapper.selectDictionaryMapList(typeId);
		return findAllList;
	}
	
	public List<String> selectDictionaryList(int typeId) {
		List<String> findAllList = mapper.selectDictionaryList(typeId);
		return findAllList;
	}
	
	public List<Dictionary> findAll() {
		List<Dictionary> findAllList = mapper.findAll();
		return findAllList;
	}
	
	public Dictionary findById(int id) {
		Dictionary data = mapper.findById(id);
		return data;
	}

	public void save(Dictionary data) {
		mapper.save(data);
	}

	public boolean update(Dictionary data) {
		return mapper.update(data);
	}
	
	public boolean delete(int id) {
		return mapper.delete(id);
	}
	
	public boolean deleteByMap(Map<String,Object> paramMap) {
		return mapper.deleteByMap(paramMap);
	}
	
	public Integer getIntValue(Map<String,Object> paramMap) {
		Integer total = mapper.getIntValue(paramMap);
		return total;
	}
	
	public String getStringValue(Map<String,Object> paramMap) {
		String strValue = mapper.getStringValue(paramMap);
		return strValue;
	}
	
	public List<Dictionary> getListByMap(Map<String,Object> paramMap) {
		List<Dictionary> findAllList = mapper.getListByMap(paramMap);
		return findAllList;
	}
	
	public Dictionary getByMap(Map<String, Object> paramMap) {
		Dictionary data = mapper.getByMap(paramMap);
		return data;
	}
	
	

}
