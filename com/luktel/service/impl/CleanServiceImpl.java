package com.luktel.service.impl;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import javax.annotation.Resource;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.luktel.mapper.CleanMapper;
import com.luktel.model.Clean;
import com.luktel.service.CleanService;
import com.util.common.PageModel;


@Service
@Transactional  //此处不再进行创建SqlSession和提交事务，都已交由spring去管理了。
public class CleanServiceImpl implements CleanService {
	
	@Resource
	private CleanMapper mapper;
	
	public List<Clean> pageList(Map<String,Object> paramMap) {
		List<Clean> findAllList = mapper.pageList(paramMap);
		return findAllList;
	}
	public Integer totalRecord(Map<String,Object> paramMap) {
		Integer total = mapper.totalRecord(paramMap);
		return total;
	}
	
	public List<Map<String,Object>> selectMapList(int typeId) {
		List<Map<String,Object>> findAllList = mapper.selectMapList(typeId);
		return findAllList;
	}
	
	public List<String> selectList(int typeId) {
		List<String> findAllList = mapper.selectList(typeId);
		return findAllList;
	}
	
	public List<Clean> findAll() {
		List<Clean> findAllList = mapper.findAll();
		return findAllList;
	}
	
	public Clean findById(int id) {
		Clean data = mapper.findById(id);
		return data;
	}

	public void save(Clean data) {
		mapper.save(data);
	}

	public boolean update(Clean data) {
		return mapper.update(data);
	}
	
	public boolean updateByTask(Clean data) {
		return mapper.updateByTask(data);
	}
	
	public boolean updateByMap(Map<String,Object> paramMap) {
		return mapper.updateByMap(paramMap);
	}
	
	public boolean delete(int id) {
		return mapper.delete(id);
	}
	
	public boolean deleteByMap(Map<String,Object> paramMap) {
		return mapper.deleteByMap(paramMap);
	}
	
	public Integer getIntValue(Map<String,Object> paramMap) {
		Integer total = mapper.getIntValue(paramMap);
		return total;
	}
	
	public String getStringValue(Map<String,Object> paramMap) {
		String strValue = mapper.getStringValue(paramMap);
		return strValue;
	}
	
	public List<Clean> getListByMap(Map<String,Object> paramMap) {
		List<Clean> findAllList = mapper.getListByMap(paramMap);
		return findAllList;
	}
	
	public Clean getByMap(Map<String, Object> paramMap) {
		Clean data = mapper.getByMap(paramMap);
		return data;
	}
	
	

}
