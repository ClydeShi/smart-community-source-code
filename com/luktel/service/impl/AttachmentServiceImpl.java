package com.luktel.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.luktel.mapper.AttachmentMapper;
import com.luktel.model.Attachment;
import com.luktel.service.AttachmentService;


@Service
@Transactional  //此处不再进行创建SqlSession和提交事务，都已交由spring去管理了。
public class AttachmentServiceImpl implements AttachmentService {
	
	@Resource
	private AttachmentMapper mapper;
	
	public List<Attachment> pageList(Map<String,Object> paramMap) {
		List<Attachment> findAllList = mapper.pageList(paramMap);
		return findAllList;
	}
	public Integer totalRecord(Map<String,Object> paramMap) {
		Integer total = mapper.totalRecord(paramMap);
		return total;
	}
	
	public List<Attachment> list(@Param("classId") int classId, @Param("infoId") int infoId) {
		List<Attachment> findAllList = mapper.list(classId,infoId);
		return findAllList;
	}
	
	public List<Attachment> findAll() {
		List<Attachment> findAllList = mapper.findAll();
		return findAllList;
	}
	
	public String getFilePath(int id) {
		return mapper.getFilePath(id);
	}

	public void save(Attachment data) {
		mapper.save(data);
	}
	
	public boolean update(@Param("conId") int conId, @Param("infoId") int infoId) {
		return mapper.update(conId,infoId);
	}
	
	public boolean delete(int id) {
		return mapper.delete(id);
	}
	
	public boolean deleteByFile(String filePath) {
		return mapper.deleteByFile(filePath);
	}
	
	public List<Attachment> getListByMap(Map<String,Object> paramMap) {
		List<Attachment> findAllList = mapper.getListByMap(paramMap);
		return findAllList;
	}
	
	

}
