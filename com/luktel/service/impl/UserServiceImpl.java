package com.luktel.service.impl;

import java.util.List;
import java.util.Map;
import javax.annotation.Resource;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.luktel.mapper.UserMapper;
import com.luktel.model.User;
import com.luktel.service.UserService;
import com.util.common.PageModel;


@Service
@Transactional  //此处不再进行创建SqlSession和提交事务，都已交由spring去管理了。
public class UserServiceImpl implements UserService {
	
	@Resource
	private UserMapper mapper;
	
	public List<User> pageList(Map<String,Object> paramMap) {
		List<User> findAllList = mapper.pageList(paramMap);
		return findAllList;
	}
	public Integer totalRecord(Map<String,Object> paramMap) {
		Integer total = mapper.totalRecord(paramMap);
		return total;
	}
	
	public List<User> findAll() {
		List<User> findAllList = mapper.findAll();
		return findAllList;
	}
	
	public User findById(int id) {
		User user = mapper.findById(id);
		return user;
	}

	public void save(User user) {
		mapper.save(user);
	}

	public boolean update(User user) {
		return mapper.update(user);
	}
	
	public boolean updateSpecialTask(User data) {
		return mapper.updateSpecialTask(data);
	}
	
	public boolean delete(int id) {
		return mapper.delete(id);
	}
	
	public User getUserByNameAndPwd(String varValue1, String varValue2) {
		User user = new User();
		user.setVarValue1(varValue1);
		user.setVarValue2(varValue2);
		return mapper.getUserInfo(user);
	}
	
	public Integer getIntValue(Map<String,Object> paramMap) {
		Integer total = mapper.getIntValue(paramMap);
		return total;
	}
	
	public String getStringValue(Map<String,Object> paramMap) {
		String strValue = mapper.getStringValue(paramMap);
		return strValue;
	}
	
	

}
