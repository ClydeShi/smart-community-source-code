package com.luktel.service.impl;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import javax.annotation.Resource;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.luktel.mapper.NewsMapper;
import com.luktel.model.News;
import com.luktel.service.NewsService;
import com.util.common.PageModel;


@Service
@Transactional  //此处不再进行创建SqlSession和提交事务，都已交由spring去管理了。
public class NewsServiceImpl implements NewsService {
	
	@Resource
	private NewsMapper mapper;
	
	public List<News> pageList(Map<String,Object> paramMap) {
		List<News> findAllList = mapper.pageList(paramMap);
		return findAllList;
	}
	public Integer totalRecord(Map<String,Object> paramMap) {
		Integer total = mapper.totalRecord(paramMap);
		return total;
	}
	
	public List<Map<String,Object>> selectMapList(int typeId) {
		List<Map<String,Object>> findAllList = mapper.selectMapList(typeId);
		return findAllList;
	}
	
	public List<String> selectList(int typeId) {
		List<String> findAllList = mapper.selectList(typeId);
		return findAllList;
	}
	
	public List<News> findAll() {
		List<News> findAllList = mapper.findAll();
		return findAllList;
	}
	
	public News findById(int id) {
		News data = mapper.findById(id);
		return data;
	}

	public void save(News data) {
		mapper.save(data);
	}

	public boolean update(News data) {
		return mapper.update(data);
	}
	
	public boolean updateByTask(News data) {
		return mapper.updateByTask(data);
	}
	
	public boolean updateByMap(Map<String,Object> paramMap) {
		return mapper.updateByMap(paramMap);
	}
	
	public boolean delete(int id) {
		return mapper.delete(id);
	}
	
	public boolean deleteByMap(Map<String,Object> paramMap) {
		return mapper.deleteByMap(paramMap);
	}
	
	public Integer getIntValue(Map<String,Object> paramMap) {
		Integer total = mapper.getIntValue(paramMap);
		return total;
	}
	
	public String getStringValue(Map<String,Object> paramMap) {
		String strValue = mapper.getStringValue(paramMap);
		return strValue;
	}
	
	public List<News> getListByMap(Map<String,Object> paramMap) {
		List<News> findAllList = mapper.getListByMap(paramMap);
		return findAllList;
	}
	
	public News getByMap(Map<String, Object> paramMap) {
		News data = mapper.getByMap(paramMap);
		return data;
	}
	
	

}
