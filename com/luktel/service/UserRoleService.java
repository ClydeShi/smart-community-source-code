package com.luktel.service;

import java.util.List;
import java.util.Map;

import com.luktel.model.UserRole;

/*
 * ArchiveMapper和ArchiveService代码基本一样
 */

public interface UserRoleService {
	
	List<UserRole> pageList(Map<String, Object> paramMap);
	Integer totalRecord(Map<String, Object> paramMap);
	
	List<Map<String,Object>> selectMapList(int typeId);
	List<String> selectList(int typeId);
	
	List<UserRole> findAll();
	UserRole findById(int id);
	void save(UserRole data);
	boolean update(UserRole data);
	boolean updateByTask(UserRole data);
	boolean updateByMap(Map<String, Object> paramMap);
	boolean delete(int id);
	boolean deleteByMap(Map<String, Object> paramMap);
	Integer getIntValue(Map<String, Object> paramMap);
	String getStringValue(Map<String, Object> paramMap);

}
