package com.luktel.service;

import java.util.List;
import java.util.Map;

import com.luktel.model.StorageOut;

/*
 * ArchiveMapper和ArchiveService代码基本一样
 */

public interface StorageOutService {
	
	List<StorageOut> pageList(Map<String, Object> paramMap);
	Integer totalRecord(Map<String, Object> paramMap);
	
	List<Map<String,Object>> selectMapList(int typeId);
	List<String> selectList(int typeId);
	
	List<StorageOut> findAll();
	StorageOut findById(int id);
	void save(StorageOut data);
	boolean update(StorageOut data);
	boolean updateByTask(StorageOut data);
	boolean updateByMap(Map<String, Object> paramMap);
	boolean delete(int id);
	boolean deleteByMap(Map<String, Object> paramMap);
	Integer getIntValue(Map<String, Object> paramMap);
	String getStringValue(Map<String, Object> paramMap);
	List<StorageOut> getListByMap(Map<String, Object> paramMap);
	StorageOut getByMap(Map<String, Object> paramMap);

}
