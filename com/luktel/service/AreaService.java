package com.luktel.service;

import java.util.List;
import java.util.Map;

import com.luktel.model.Area;

/*
 * ArchiveMapper和ArchiveService代码基本一样
 */

public interface AreaService {
	
	List<Area> pageList(Map<String, Object> paramMap);
	Integer totalRecord(Map<String, Object> paramMap);
	
	List<Map<String,Object>> selectMapList(int typeId);
	List<String> selectList(int typeId);
	
	List<Area> findAll();
	Area findById(int id);
	void save(Area data);
	boolean update(Area data);
	boolean updateByTask(Area data);
	boolean updateByMap(Map<String, Object> paramMap);
	boolean delete(int id);
	boolean deleteByMap(Map<String, Object> paramMap);
	Integer getIntValue(Map<String, Object> paramMap);
	String getStringValue(Map<String, Object> paramMap);
	List<Area> getListByMap(Map<String, Object> paramMap);
	Area getByMap(Map<String, Object> paramMap);

}
