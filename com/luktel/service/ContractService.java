package com.luktel.service;

import java.util.List;
import java.util.Map;

import com.luktel.model.Contract;

/*
 * ArchiveMapper和ArchiveService代码基本一样
 */

public interface ContractService {
	
	List<Contract> pageList(Map<String, Object> paramMap);
	Integer totalRecord(Map<String, Object> paramMap);
	
	List<Map<String,Object>> selectMapList(int typeId);
	List<String> selectList(int typeId);
	
	List<Contract> findAll();
	Contract findById(int id);
	void save(Contract data);
	boolean update(Contract data);
	boolean updateByTask(Contract data);
	boolean updateByMap(Map<String, Object> paramMap);
	boolean delete(int id);
	boolean deleteByMap(Map<String, Object> paramMap);
	Integer getIntValue(Map<String, Object> paramMap);
	String getStringValue(Map<String, Object> paramMap);
	List<Contract> getListByMap(Map<String, Object> paramMap);
	Contract getByMap(Map<String, Object> paramMap);

}
