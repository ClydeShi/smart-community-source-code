package com.luktel.mapper;

import java.util.List;
import java.util.Map;

import com.luktel.model.Deptments;

/*
 * ArchiveMapper和ArchiveService代码基本一样
 */

public interface DeptmentsMapper {
	
	List<Deptments> pageList(Map<String, Object> paramMap);
	Integer totalRecord(Map<String, Object> paramMap);
	
	List<Map<String,Object>> selectMapList(int typeId);
	List<String> selectList(int typeId);
	
	List<Deptments> findAll();
	Deptments findById(int id);
	void save(Deptments data);
	boolean update(Deptments data);
	boolean updateByTask(Deptments data);
	boolean updateByMap(Map<String, Object> paramMap);
	boolean delete(int id);
	boolean deleteByMap(Map<String, Object> paramMap);
	Integer getIntValue(Map<String, Object> paramMap);
	String getStringValue(Map<String, Object> paramMap);
	List<Deptments> getListByMap(Map<String, Object> paramMap);
	Deptments getByMap(Map<String, Object> paramMap);

}
