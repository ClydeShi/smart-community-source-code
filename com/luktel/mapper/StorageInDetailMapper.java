package com.luktel.mapper;

import java.util.List;
import java.util.Map;

import com.luktel.model.StorageInDetail;

/*
 * ArchiveMapper和ArchiveService代码基本一样
 */

public interface StorageInDetailMapper {
	
	List<StorageInDetail> pageList(Map<String, Object> paramMap);
	Integer totalRecord(Map<String, Object> paramMap);
	
	List<Map<String,Object>> selectMapList(int typeId);
	List<String> selectList(int typeId);
	
	List<StorageInDetail> findAll();
	StorageInDetail findById(int id);
	void save(StorageInDetail data);
	boolean update(StorageInDetail data);
	boolean updateByTask(StorageInDetail data);
	boolean updateByMap(Map<String, Object> paramMap);
	boolean delete(int id);
	boolean deleteByMap(Map<String, Object> paramMap);
	Integer getIntValue(Map<String, Object> paramMap);
	String getStringValue(Map<String, Object> paramMap);
	List<StorageInDetail> getListByMap(Map<String, Object> paramMap);
	StorageInDetail getByMap(Map<String, Object> paramMap);

}
