package com.luktel.mapper;

import java.util.List;
import java.util.Map;

import com.luktel.model.Bill;

/*
 * ArchiveMapper和ArchiveService代码基本一样
 */

public interface BillMapper {
	
	List<Bill> pageList(Map<String, Object> paramMap);
	Integer totalRecord(Map<String, Object> paramMap);
	
	List<Map<String,Object>> selectMapList(int typeId);
	List<String> selectList(int typeId);
	
	List<Bill> findAll();
	Bill findById(int id);
	void save(Bill data);
	boolean update(Bill data);
	boolean updateByTask(Bill data);
	boolean updateByMap(Map<String, Object> paramMap);
	boolean delete(int id);
	boolean deleteByMap(Map<String, Object> paramMap);
	Integer getIntValue(Map<String, Object> paramMap);
	String getStringValue(Map<String, Object> paramMap);
	List<Bill> getListByMap(Map<String, Object> paramMap);
	Bill getByMap(Map<String, Object> paramMap);

}
