package com.luktel.mapper;

import java.util.List;
import java.util.Map;

import com.luktel.model.DecorateAccept;

/*
 * ArchiveMapper和ArchiveService代码基本一样
 */

public interface DecorateAcceptMapper {
	
	List<DecorateAccept> pageList(Map<String, Object> paramMap);
	Integer totalRecord(Map<String, Object> paramMap);
	
	List<Map<String,Object>> selectMapList(int typeId);
	List<String> selectList(int typeId);
	
	List<DecorateAccept> findAll();
	DecorateAccept findById(int id);
	void save(DecorateAccept data);
	boolean update(DecorateAccept data);
	boolean updateByTask(DecorateAccept data);
	boolean updateByMap(Map<String, Object> paramMap);
	boolean delete(int id);
	boolean deleteByMap(Map<String, Object> paramMap);
	Integer getIntValue(Map<String, Object> paramMap);
	String getStringValue(Map<String, Object> paramMap);
	List<DecorateAccept> getListByMap(Map<String, Object> paramMap);
	DecorateAccept getByMap(Map<String, Object> paramMap);

}
