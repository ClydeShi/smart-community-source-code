package com.luktel.mapper;

import java.util.List;
import java.util.Map;

import com.luktel.model.User;

/*
 * UserMapper和UserService代码基本一样
 */

public interface UserMapper {
	
	List<User> pageList(Map<String, Object> paramMap);
	Integer totalRecord(Map<String, Object> paramMap);
	
	List<User> findAll();
	User findById(int id);
	void save(User user);
	boolean update(User user);
	boolean updateSpecialTask(User data);
	boolean delete(int id);
	
	public User getUserInfo(User user);
	
	Integer getIntValue(Map<String, Object> paramMap);
	String getStringValue(Map<String, Object> paramMap);
}
