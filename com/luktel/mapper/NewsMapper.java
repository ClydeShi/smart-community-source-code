package com.luktel.mapper;

import java.util.List;
import java.util.Map;

import com.luktel.model.News;

/*
 * ArchiveMapper和ArchiveService代码基本一样
 */

public interface NewsMapper {
	
	List<News> pageList(Map<String, Object> paramMap);
	Integer totalRecord(Map<String, Object> paramMap);
	
	List<Map<String,Object>> selectMapList(int typeId);
	List<String> selectList(int typeId);
	
	List<News> findAll();
	News findById(int id);
	void save(News data);
	boolean update(News data);
	boolean updateByTask(News data);
	boolean updateByMap(Map<String, Object> paramMap);
	boolean delete(int id);
	boolean deleteByMap(Map<String, Object> paramMap);
	Integer getIntValue(Map<String, Object> paramMap);
	String getStringValue(Map<String, Object> paramMap);
	List<News> getListByMap(Map<String, Object> paramMap);
	News getByMap(Map<String, Object> paramMap);

}
