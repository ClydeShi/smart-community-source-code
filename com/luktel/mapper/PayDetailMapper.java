package com.luktel.mapper;

import java.util.List;
import java.util.Map;

import com.luktel.model.PayDetail;

/*
 * ArchiveMapper和ArchiveService代码基本一样
 */

public interface PayDetailMapper {
	
	List<PayDetail> pageList(Map<String, Object> paramMap);
	Integer totalRecord(Map<String, Object> paramMap);
	
	List<Map<String,Object>> selectMapList(int typeId);
	List<String> selectList(int typeId);
	
	List<PayDetail> findAll();
	PayDetail findById(int id);
	void save(PayDetail data);
	boolean update(PayDetail data);
	boolean updateByTask(PayDetail data);
	boolean updateByMap(Map<String, Object> paramMap);
	boolean delete(int id);
	boolean deleteByMap(Map<String, Object> paramMap);
	Integer getIntValue(Map<String, Object> paramMap);
	String getStringValue(Map<String, Object> paramMap);
	List<PayDetail> getListByMap(Map<String, Object> paramMap);
	PayDetail getByMap(Map<String, Object> paramMap);

}
