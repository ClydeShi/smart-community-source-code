package com.luktel.mapper;

import java.util.List;
import java.util.Map;

import com.luktel.model.Space;

/*
 * ArchiveMapper和ArchiveService代码基本一样
 */

public interface SpaceMapper {
	
	List<Space> pageList(Map<String, Object> paramMap);
	Integer totalRecord(Map<String, Object> paramMap);
	
	List<Map<String,Object>> selectMapList(int typeId);
	List<String> selectList(int typeId);
	
	List<Space> findAll();
	Space findById(int id);
	void save(Space data);
	boolean update(Space data);
	boolean updateByTask(Space data);
	boolean updateByMap(Map<String, Object> paramMap);
	boolean delete(int id);
	boolean deleteByMap(Map<String, Object> paramMap);
	Integer getIntValue(Map<String, Object> paramMap);
	String getStringValue(Map<String, Object> paramMap);
	List<Space> getListByMap(Map<String, Object> paramMap);
	Space getByMap(Map<String, Object> paramMap);

}
