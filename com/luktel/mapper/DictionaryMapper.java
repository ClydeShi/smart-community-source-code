package com.luktel.mapper;

import java.util.List;
import java.util.Map;

import com.luktel.model.Dictionary;

/*
 * ArchiveMapper和ArchiveService代码基本一样
 */

public interface DictionaryMapper {
	
	List<Dictionary> pageList(Map<String, Object> paramMap);
	Integer totalRecord(Map<String, Object> paramMap);
	
	List<Map<String,Object>> selectDictionaryMapList(int typeId);
	List<String> selectDictionaryList(int typeId);
	
	List<Dictionary> findAll();
	Dictionary findById(int id);
	void save(Dictionary data);
	boolean update(Dictionary data);
	boolean delete(int id);
	boolean deleteByMap(Map<String, Object> paramMap);
	Integer getIntValue(Map<String, Object> paramMap);
	String getStringValue(Map<String, Object> paramMap);
	List<Dictionary> getListByMap(Map<String, Object> paramMap);
	Dictionary getByMap(Map<String, Object> paramMap);

}
