package com.luktel.mapper;

import java.util.List;
import java.util.Map;

import com.luktel.model.StorageOutDetail;

/*
 * ArchiveMapper和ArchiveService代码基本一样
 */

public interface StorageOutDetailMapper {
	
	List<StorageOutDetail> pageList(Map<String, Object> paramMap);
	Integer totalRecord(Map<String, Object> paramMap);
	
	List<Map<String,Object>> selectMapList(int typeId);
	List<String> selectList(int typeId);
	
	List<StorageOutDetail> findAll();
	StorageOutDetail findById(int id);
	void save(StorageOutDetail data);
	boolean update(StorageOutDetail data);
	boolean updateByTask(StorageOutDetail data);
	boolean updateByMap(Map<String, Object> paramMap);
	boolean delete(int id);
	boolean deleteByMap(Map<String, Object> paramMap);
	Integer getIntValue(Map<String, Object> paramMap);
	String getStringValue(Map<String, Object> paramMap);
	List<StorageOutDetail> getListByMap(Map<String, Object> paramMap);
	StorageOutDetail getByMap(Map<String, Object> paramMap);

}
