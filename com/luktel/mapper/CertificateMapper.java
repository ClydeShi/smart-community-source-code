package com.luktel.mapper;

import java.util.List;
import java.util.Map;

import com.luktel.model.Certificate;

/*
 * ArchiveMapper和ArchiveService代码基本一样
 */

public interface CertificateMapper {
	
	List<Certificate> pageList(Map<String, Object> paramMap);
	Integer totalRecord(Map<String, Object> paramMap);
	
	List<Map<String,Object>> selectMapList(int typeId);
	List<String> selectList(int typeId);
	
	List<Certificate> findAll();
	Certificate findById(int id);
	void save(Certificate data);
	boolean update(Certificate data);
	boolean updateByTask(Certificate data);
	boolean updateByMap(Map<String, Object> paramMap);
	boolean delete(int id);
	boolean deleteByMap(Map<String, Object> paramMap);
	Integer getIntValue(Map<String, Object> paramMap);
	String getStringValue(Map<String, Object> paramMap);
	List<Certificate> getListByMap(Map<String, Object> paramMap);
	Certificate getByMap(Map<String, Object> paramMap);

}
