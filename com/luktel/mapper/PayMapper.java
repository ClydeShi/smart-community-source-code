package com.luktel.mapper;

import java.util.List;
import java.util.Map;

import com.luktel.model.Pay;

/*
 * ArchiveMapper和ArchiveService代码基本一样
 */

public interface PayMapper {
	
	List<Pay> pageList(Map<String, Object> paramMap);
	Integer totalRecord(Map<String, Object> paramMap);
	
	List<Map<String,Object>> selectMapList(int typeId);
	List<String> selectList(int typeId);
	
	List<Pay> findAll();
	Pay findById(int id);
	void save(Pay data);
	boolean update(Pay data);
	boolean updateByTask(Pay data);
	boolean updateByMap(Map<String, Object> paramMap);
	boolean delete(int id);
	boolean deleteByMap(Map<String, Object> paramMap);
	Integer getIntValue(Map<String, Object> paramMap);
	String getStringValue(Map<String, Object> paramMap);
	List<Pay> getListByMap(Map<String, Object> paramMap);
	Pay getByMap(Map<String, Object> paramMap);

}
