package com.luktel.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 用户
 */
public class Clean {
	
	private int conId = 0;
	private int channelId = 0;
	private int classId = 0;
	private String task = "";
	private String varValue1 = "";
	private String varValue2 = "";
	private String varValue3 = "";
	private String varValue4 = "";
	private String varValue5 = "";
	private String varValue6 = "";
	private String varValue7 = "";
	private String varValue8 = "";
	private String varValue9 = "";
	private String varValue10 = "";
	private String varValue11 = "";
	private String varValue12 = "";
	private String varValue13 = "";
	private String varValue14 = "";
	private String varValue15 = "";
	private String varValue16 = "";
	private String varValue17 = "";
	private String varValue18 = "";
	private String varValue19 = "";
	private String varValue20 = "";
	private String varValue21 = "";
	private String varValue22 = "";
	private String varValue23 = "";
	private String varValue24 = "";
	private String varValue25 = "";
	private String varValue26 = "";
	private String varValue27 = "";
	private String varValue28 = "";
	private String varValue29 = "";
	private String varValue30 = "";
	private String varValue31 = "";
	private String varValue32 = "";
	private String varValue33 = "";
	private String varValue34 = "";
	private String varValue35 = "";
	private String varValue36 = "";
	private String varValue37 = "";
	private String varValue38 = "";
	private String varValue39 = "";
	private String varValue40 = "";
	private String varValue41 = "";
	private String varValue42 = "";
	private String varValue43 = "";
	private String varValue44 = "";
	private String varValue45 = "";
	private String varValue46 = "";
	private String varValue47 = "";
	private String varValue48 = "";
	private String varValue49 = "";
	private String varValue50 = "";
	private String decValue1 = "0.00";
	private String decValue2 = "0.00";
	private String decValue3 = "0.00";
	private String decValue4 = "0.00";
	private String decValue5 = "0.00";
	private String decValue6 = "0.00";
	private String decValue7 = "0.00";
	private String decValue8 = "0.00";
	private String decValue9 = "0.00";
	private String decValue10 = "0.00";
	private String text1 = "";
	private String text2 = "";
	private String text3 = "";
	private String userName = "";
	private String keywords = "";
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date addTime;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date updateTime;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date dateValue1;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date dateValue2;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date dateValue3;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date dateValue4;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date dateValue5;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date dateValue6;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date dateValue7;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date dateValue8;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date dateValue9;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date dateValue10;
	private int intValue1 = 0;
	private int intValue2 = 0;
	private int intValue3 = 0;
	private int intValue4 = 0;
	private int intValue5 = 0;
	private int intValue6 = 0;
	private int intValue7 = 0;
	private int intValue8 = 0;
	private int intValue9 = 0;
	private int intValue10 = 0;
	private int userId = 0;
	private int status = 0;
	
	public int getConId() {
		return conId;
	}
	public void setConId(int conId) {
		this.conId = conId;
	}
	public int getChannelId() {
		return channelId;
	}
	public void setChannelId(int channelId) {
		this.channelId = channelId;
	}
	public int getClassId() {
		return classId;
	}
	public void setClassId(int classId) {
		this.classId = classId;
	}
	public String getTask() {
		return task;
	}
	public void setTask(String task) {
		this.task = task;
	}
	public String getVarValue1() {
		return varValue1;
	}
	public void setVarValue1(String varValue1) {
		if(varValue1 == null){varValue1 = "";}//通过这个方法解决null
		this.varValue1 = varValue1;
	}
	public String getVarValue2() {
		return varValue2;
	}
	public void setVarValue2(String varValue2) {
		if(varValue2 == null){varValue2 = "";}
		this.varValue2 = varValue2;
	}
	public String getVarValue3() {
		return varValue3;
	}
	public void setVarValue3(String varValue3) {
		if(varValue3 == null){varValue3 = "";}
		this.varValue3 = varValue3;
	}
	public String getVarValue4() {
		return varValue4;
	}
	public void setVarValue4(String varValue4) {
		if(varValue4 == null){varValue4 = "";}
		this.varValue4 = varValue4;
	}
	public String getVarValue5() {
		return varValue5;
	}
	public void setVarValue5(String varValue5) {
		if(varValue5 == null){varValue5 = "";}
		this.varValue5 = varValue5;
	}
	public String getVarValue6() {
		return varValue6;
	}
	public void setVarValue6(String varValue6) {
		if(varValue6 == null){varValue6 = "";}
		this.varValue6 = varValue6;
	}
	public String getVarValue7() {
		return varValue7;
	}
	public void setVarValue7(String varValue7) {
		if(varValue7 == null){varValue7 = "";}
		this.varValue7 = varValue7;
	}
	public String getVarValue8() {
		return varValue8;
	}
	public void setVarValue8(String varValue8) {
		if(varValue8 == null){varValue8 = "";}
		this.varValue8 = varValue8;
	}
	public String getVarValue9() {
		return varValue9;
	}
	public void setVarValue9(String varValue9) {
		if(varValue9 == null){varValue9 = "";}
		this.varValue9 = varValue9;
	}
	public String getVarValue10() {
		return varValue10;
	}
	public void setVarValue10(String varValue10) {
		if(varValue10 == null){varValue10 = "";}
		this.varValue10 = varValue10;
	}
	public String getVarValue11() {
		return varValue11;
	}
	public void setVarValue11(String varValue11) {
		if(varValue11 == null){varValue11 = "";}
		this.varValue11 = varValue11;
	}
	public String getVarValue12() {
		return varValue12;
	}
	public void setVarValue12(String varValue12) {
		if(varValue12 == null){varValue12 = "";}
		this.varValue12 = varValue12;
	}
	public String getVarValue13() {
		return varValue13;
	}
	public void setVarValue13(String varValue13) {
		if(varValue13 == null){varValue13 = "";}
		this.varValue13 = varValue13;
	}
	public String getVarValue14() {
		return varValue14;
	}
	public void setVarValue14(String varValue14) {
		if(varValue14 == null){varValue14 = "";}
		this.varValue14 = varValue14;
	}
	public String getVarValue15() {
		return varValue15;
	}
	public void setVarValue15(String varValue15) {
		if(varValue15 == null){varValue15 = "";}
		this.varValue15 = varValue15;
	}
	public String getVarValue16() {
		return varValue16;
	}
	public void setVarValue16(String varValue16) {
		if(varValue16 == null){varValue16 = "";}
		this.varValue16 = varValue16;
	}
	public String getVarValue17() {
		return varValue17;
	}
	public void setVarValue17(String varValue17) {
		if(varValue17 == null){varValue17 = "";}
		this.varValue17 = varValue17;
	}
	public String getVarValue18() {
		return varValue18;
	}
	public void setVarValue18(String varValue18) {
		if(varValue18 == null){varValue18 = "";}
		this.varValue18 = varValue18;
	}
	public String getVarValue19() {
		return varValue19;
	}
	public void setVarValue19(String varValue19) {
		if(varValue19 == null){varValue19 = "";}
		this.varValue19 = varValue19;
	}
	public String getVarValue20() {
		return varValue20;
	}
	public void setVarValue20(String varValue20) {
		if(varValue20 == null){varValue20 = "";}
		this.varValue20 = varValue20;
	}
	public String getVarValue21() {
		return varValue21;
	}
	public void setVarValue21(String varValue21) {
		if(varValue21 == null){varValue21 = "";}
		this.varValue21 = varValue21;
	}
	public String getVarValue22() {
		return varValue22;
	}
	public void setVarValue22(String varValue22) {
		if(varValue22 == null){varValue22 = "";}
		this.varValue22 = varValue22;
	}
	public String getVarValue23() {
		return varValue23;
	}
	public void setVarValue23(String varValue23) {
		if(varValue23 == null){varValue23 = "";}
		this.varValue23 = varValue23;
	}
	public String getVarValue24() {
		return varValue24;
	}
	public void setVarValue24(String varValue24) {
		if(varValue24 == null){varValue24 = "";}
		this.varValue24 = varValue24;
	}
	public String getVarValue25() {
		return varValue25;
	}
	public void setVarValue25(String varValue25) {
		if(varValue25 == null){varValue25 = "";}
		this.varValue25 = varValue25;
	}
	public String getVarValue26() {
		return varValue26;
	}
	public void setVarValue26(String varValue26) {
		if(varValue26 == null){varValue26 = "";}
		this.varValue26 = varValue26;
	}
	public String getVarValue27() {
		return varValue27;
	}
	public void setVarValue27(String varValue27) {
		if(varValue27 == null){varValue27 = "";}
		this.varValue27 = varValue27;
	}
	public String getVarValue28() {
		return varValue28;
	}
	public void setVarValue28(String varValue28) {
		if(varValue28 == null){varValue28 = "";}
		this.varValue28 = varValue28;
	}
	public String getVarValue29() {
		return varValue29;
	}
	public void setVarValue29(String varValue29) {
		if(varValue29 == null){varValue29 = "";}
		this.varValue29 = varValue29;
	}
	public String getVarValue30() {
		return varValue30;
	}
	public void setVarValue30(String varValue30) {
		if(varValue30 == null){varValue30 = "";}
		this.varValue30 = varValue30;
	}
	public String getVarValue31() {
		return varValue31;
	}
	public void setVarValue31(String varValue31) {
		if(varValue31 == null){varValue31 = "";}
		this.varValue31 = varValue31;
	}
	public String getVarValue32() {
		return varValue32;
	}
	public void setVarValue32(String varValue32) {
		if(varValue32 == null){varValue32 = "";}
		this.varValue32 = varValue32;
	}
	public String getVarValue33() {
		return varValue33;
	}
	public void setVarValue33(String varValue33) {
		if(varValue33 == null){varValue33 = "";}
		this.varValue33 = varValue33;
	}
	public String getVarValue34() {
		return varValue34;
	}
	public void setVarValue34(String varValue34) {
		if(varValue34 == null){varValue34 = "";}
		this.varValue34 = varValue34;
	}
	public String getVarValue35() {
		return varValue35;
	}
	public void setVarValue35(String varValue35) {
		if(varValue35 == null){varValue35 = "";}
		this.varValue35 = varValue35;
	}
	public String getVarValue36() {
		return varValue36;
	}
	public void setVarValue36(String varValue36) {
		if(varValue36 == null){varValue36 = "";}
		this.varValue36 = varValue36;
	}
	public String getVarValue37() {
		return varValue37;
	}
	public void setVarValue37(String varValue37) {
		if(varValue37 == null){varValue37 = "";}
		this.varValue37 = varValue37;
	}
	public String getVarValue38() {
		return varValue38;
	}
	public void setVarValue38(String varValue38) {
		if(varValue38 == null){varValue38 = "";}
		this.varValue38 = varValue38;
	}
	public String getVarValue39() {
		return varValue39;
	}
	public void setVarValue39(String varValue39) {
		if(varValue39 == null){varValue39 = "";}
		this.varValue39 = varValue39;
	}
	public String getVarValue40() {
		return varValue40;
	}
	public void setVarValue40(String varValue40) {
		if(varValue40 == null){varValue40 = "";}
		this.varValue40 = varValue40;
	}
	public String getVarValue41() {
		return varValue41;
	}
	public void setVarValue41(String varValue41) {
		this.varValue41 = varValue41;
	}
	public String getVarValue42() {
		return varValue42;
	}
	public void setVarValue42(String varValue42) {
		this.varValue42 = varValue42;
	}
	public String getVarValue43() {
		return varValue43;
	}
	public void setVarValue43(String varValue43) {
		this.varValue43 = varValue43;
	}
	public String getVarValue44() {
		return varValue44;
	}
	public void setVarValue44(String varValue44) {
		this.varValue44 = varValue44;
	}
	public String getVarValue45() {
		return varValue45;
	}
	public void setVarValue45(String varValue45) {
		this.varValue45 = varValue45;
	}
	public String getVarValue46() {
		return varValue46;
	}
	public void setVarValue46(String varValue46) {
		this.varValue46 = varValue46;
	}
	public String getVarValue47() {
		return varValue47;
	}
	public void setVarValue47(String varValue47) {
		this.varValue47 = varValue47;
	}
	public String getVarValue48() {
		return varValue48;
	}
	public void setVarValue48(String varValue48) {
		this.varValue48 = varValue48;
	}
	public String getVarValue49() {
		return varValue49;
	}
	public void setVarValue49(String varValue49) {
		this.varValue49 = varValue49;
	}
	public String getVarValue50() {
		return varValue50;
	}
	public void setVarValue50(String varValue50) {
		this.varValue50 = varValue50;
	}
	public String getDecValue1() {
		return decValue1;
	}
	public void setDecValue1(String decValue1) {
		this.decValue1 = decValue1;
	}
	public String getDecValue2() {
		return decValue2;
	}
	public void setDecValue2(String decValue2) {
		this.decValue2 = decValue2;
	}
	public String getDecValue3() {
		return decValue3;
	}
	public void setDecValue3(String decValue3) {
		this.decValue3 = decValue3;
	}
	public String getDecValue4() {
		return decValue4;
	}
	public void setDecValue4(String decValue4) {
		this.decValue4 = decValue4;
	}
	public String getDecValue5() {
		return decValue5;
	}
	public void setDecValue5(String decValue5) {
		this.decValue5 = decValue5;
	}
	public String getDecValue6() {
		return decValue6;
	}
	public void setDecValue6(String decValue6) {
		this.decValue6 = decValue6;
	}
	public String getDecValue7() {
		return decValue7;
	}
	public void setDecValue7(String decValue7) {
		this.decValue7 = decValue7;
	}
	public String getDecValue8() {
		return decValue8;
	}
	public void setDecValue8(String decValue8) {
		this.decValue8 = decValue8;
	}
	public String getDecValue9() {
		return decValue9;
	}
	public void setDecValue9(String decValue9) {
		this.decValue9 = decValue9;
	}
	public String getDecValue10() {
		return decValue10;
	}
	public void setDecValue10(String decValue10) {
		this.decValue10 = decValue10;
	}
	public String getText1() {
		return text1;
	}
	public void setText1(String text1) {
		if(text1 == null){text1 = "";}
		this.text1 = text1;
	}
	public String getText2() {
		return text2;
	}
	public void setText2(String text2) {
		if(text2 == null){text2 = "";}
		this.text2 = text2;
	}
	public String getText3() {
		return text3;
	}
	public void setText3(String text3) {
		if(text3 == null){text3 = "";}
		this.text3 = text3;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getKeywords() {
		return keywords;
	}
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	public Date getAddTime() {
		return addTime;
	}
	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}

	@JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	public Date getDateValue1() {
		return dateValue1;
	}
	public void setDateValue1(Date dateValue1) {
		this.dateValue1 = dateValue1;
	}
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	public Date getDateValue2() {
		return dateValue2;
	}
	public void setDateValue2(Date dateValue2) {
		this.dateValue2 = dateValue2;
	}
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	public Date getDateValue3() {
		return dateValue3;
	}
	public void setDateValue3(Date dateValue3) {
		this.dateValue3 = dateValue3;
	}
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	public Date getDateValue4() {
		return dateValue4;
	}
	public void setDateValue4(Date dateValue4) {
		this.dateValue4 = dateValue4;
	}
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	public Date getDateValue5() {
		return dateValue5;
	}
	public void setDateValue5(Date dateValue5) {
		this.dateValue5 = dateValue5;
	}
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	public Date getDateValue6() {
		return dateValue6;
	}
	public void setDateValue6(Date dateValue6) {
		this.dateValue6 = dateValue6;
	}
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	public Date getDateValue7() {
		return dateValue7;
	}
	public void setDateValue7(Date dateValue7) {
		this.dateValue7 = dateValue7;
	}
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	public Date getDateValue8() {
		return dateValue8;
	}
	public void setDateValue8(Date dateValue8) {
		this.dateValue8 = dateValue8;
	}
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	public Date getDateValue9() {
		return dateValue9;
	}
	public void setDateValue9(Date dateValue9) {
		this.dateValue9 = dateValue9;
	}
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	public Date getDateValue10() {
		return dateValue10;
	}
	public void setDateValue10(Date dateValue10) {
		this.dateValue10 = dateValue10;
	}
	public int getIntValue1() {
		return intValue1;
	}
	public void setIntValue1(int intValue1) {
		this.intValue1 = intValue1;
	}
	public int getIntValue2() {
		return intValue2;
	}
	public void setIntValue2(int intValue2) {
		this.intValue2 = intValue2;
	}
	public int getIntValue3() {
		return intValue3;
	}
	public void setIntValue3(int intValue3) {
		this.intValue3 = intValue3;
	}
	public int getIntValue4() {
		return intValue4;
	}
	public void setIntValue4(int intValue4) {
		this.intValue4 = intValue4;
	}
	public int getIntValue5() {
		return intValue5;
	}
	public void setIntValue5(int intValue5) {
		this.intValue5 = intValue5;
	}
	public int getIntValue6() {
		return intValue6;
	}
	public void setIntValue6(int intValue6) {
		this.intValue6 = intValue6;
	}
	public int getIntValue7() {
		return intValue7;
	}
	public void setIntValue7(int intValue7) {
		this.intValue7 = intValue7;
	}
	public int getIntValue8() {
		return intValue8;
	}
	public void setIntValue8(int intValue8) {
		this.intValue8 = intValue8;
	}
	public int getIntValue9() {
		return intValue9;
	}
	public void setIntValue9(int intValue9) {
		this.intValue9 = intValue9;
	}
	public int getIntValue10() {
		return intValue10;
	}
	public void setIntValue10(int intValue10) {
		this.intValue10 = intValue10;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Clean(){
		super();
	}
	public Clean(int conId, String varValue1, String varValue2, String varValue3, String varValue4, String varValue5, String varValue6, String varValue7, String varValue8, String varValue9, String varValue10) {
		super();
		this.conId = conId;
		this.varValue1 = varValue1;
		this.varValue2 = varValue2;
		this.varValue3 = varValue3;
		this.varValue4 = varValue4;
		this.varValue5 = varValue5;
		this.varValue6 = varValue6;
		this.varValue7 = varValue7;
		this.varValue8 = varValue8;
		this.varValue9 = varValue9;
		this.varValue10 = varValue10;
	}
}
