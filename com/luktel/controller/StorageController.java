package com.luktel.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.luktel.model.Attachment;
import com.luktel.model.Storage;
import com.luktel.model.User;
import com.luktel.service.AttachmentService;
import com.luktel.service.BuildingService;
import com.luktel.service.StorageService;
import com.luktel.service.DictionaryService;
import com.util.common.Contants;
import com.util.common.PageModel;
import com.util.common.StrEdit;

/*
 * @Controller是标记在类UserController上面的，所以类UserController就是一个SpringMVC Controller对象了
 * 使用@RequestMapping("/getAllUser")标记在Controller方法上，表示当请求/getAllUser的时候访问的是LoginController的getAllUser方法，
 * 该方法返回了一个包括Model和View的ModelAndView对象113323
 */

@Controller//@Controller注解标识一个控制器
@RequestMapping("/storage")//如果@RequestMapping注解在类级别上，则表示相对路径，在方法级别上，则标记访问的路径
public class StorageController{
	
	@Autowired
	private StorageService service;
	
	@Autowired
	private BuildingService building;
	
	@Autowired
	private AttachmentService attach;
	
	@Autowired
	private DictionaryService dictionary;
	
	@RequestMapping("/list")
	public String list(HttpServletRequest request) {
		String modelTitle = "", modelName = "", redirect = "";
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		if(channelId==1){
			modelName = "工具耗材库存";
			redirect = "front/storage/list";
		}else if(channelId==5){
			modelTitle = "资产";
			modelName = "固定资产查看";
			redirect = "front/storage/listzc";
		}else if(channelId==15){
			modelTitle = "设备";
			modelName = "设备查看";
			redirect = "front/storage/listzc";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelTitle", modelTitle);
		request.setAttribute("modelName", modelName);
		return redirect;
	}
	
	@RequestMapping("/listzcselect")
	public String listzcselect(HttpServletRequest request) {
		String modelTitle = "", modelName = "", redirect = "";
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		String varValue39 = StrEdit.StrChkNull(request.getParameter("varValue39"));
		if(channelId==1){
			modelName = "工具耗材库存";
			redirect = "front/storage/list";
		}else if(channelId==5){
			modelTitle = "资产";
			modelName = "固定资产查看";
			request.setAttribute("varValue39", varValue39);
			redirect = "front/storage/listzcselect";
		}else if(channelId==15){
			modelTitle = "设备";
			modelName = "设备查看";
			request.setAttribute("varValue39", varValue39);
			redirect = "front/storage/listzcselect";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelTitle", modelTitle);
		request.setAttribute("modelName", modelName);
		return redirect;
	}
	
	@RequestMapping("/auditlist")
	public String auditlist(HttpServletRequest request) {
		String modelName = "";
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		if(channelId==1){
			modelName = "工具耗材库存";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelName", modelName);
		return "front/storage/auditlist";
	}
	
	@RequestMapping("/getJsonById")
	@ResponseBody
	public String getJsonById(int id){
		StringBuffer strBuff = new StringBuffer("");
		Storage data = service.findById(id);
		if(data!=null){
			String dateValue1 = StrEdit.dateToStr(data.getDateValue1());
			strBuff.append("{");
			strBuff.append("\"status\": \"1\",");
			strBuff.append("\"id\": \""+data.getConId()+"\",");
			strBuff.append("\"varValue1\": \""+data.getVarValue1()+"\",");
			strBuff.append("\"varValue2\": \""+data.getVarValue2()+"\",");
			strBuff.append("\"varValue3\": \""+data.getVarValue3()+"\",");
			strBuff.append("\"varValue4\": \""+data.getVarValue4()+"\",");
			strBuff.append("\"varValue5\": \""+data.getVarValue5()+"\",");
			strBuff.append("\"varValue6\": \""+data.getVarValue6()+"\",");
			strBuff.append("\"varValue7\": \""+data.getVarValue7()+"\",");
			strBuff.append("\"varValue8\": \""+data.getVarValue8()+"\",");
			strBuff.append("\"varValue9\": \""+data.getVarValue9()+"\",");
			strBuff.append("\"varValue10\": \""+data.getVarValue10()+"\",");
			strBuff.append("\"varValue11\": \""+data.getVarValue11()+"\",");
			strBuff.append("\"varValue12\": \""+data.getVarValue12()+"\",");
			strBuff.append("\"varValue13\": \""+data.getVarValue13()+"\",");
			strBuff.append("\"varValue14\": \""+data.getVarValue14()+"\",");
			strBuff.append("\"varValue15\": \""+data.getVarValue15()+"\",");
			strBuff.append("\"varValue16\": \""+data.getVarValue16()+"\",");
			strBuff.append("\"varValue17\": \""+data.getVarValue17()+"\",");
			strBuff.append("\"varValue18\": \""+data.getVarValue18()+"\",");
			strBuff.append("\"varValue19\": \""+data.getVarValue19()+"\",");
			strBuff.append("\"varValue20\": \""+data.getVarValue20()+"\",");
			strBuff.append("\"varValue21\": \""+data.getVarValue21()+"\",");
			strBuff.append("\"varValue22\": \""+data.getVarValue22()+"\",");
			strBuff.append("\"varValue23\": \""+data.getVarValue23()+"\",");
			strBuff.append("\"varValue24\": \""+data.getVarValue24()+"\",");
			strBuff.append("\"varValue25\": \""+data.getVarValue25()+"\",");
			strBuff.append("\"varValue26\": \""+data.getVarValue26()+"\",");
			strBuff.append("\"decValue1\": \""+data.getDecValue1()+"\",");
			strBuff.append("\"decValue2\": \""+data.getDecValue2()+"\",");
			strBuff.append("\"intValue1\": \""+data.getIntValue1()+"\",");
			strBuff.append("\"intValue2\": \""+data.getIntValue2()+"\",");
			strBuff.append("\"intValue3\": \""+data.getIntValue3()+"\",");
			strBuff.append("\"intValue4\": \""+data.getIntValue4()+"\",");
			strBuff.append("\"intValue5\": \""+data.getIntValue5()+"\",");
			strBuff.append("\"dateValue1\": \""+dateValue1+"\",");
			strBuff.append("\"text1\": \""+data.getText1()+"\"");
			strBuff.append("}");
		}else{
			strBuff.append("{");
			strBuff.append("\"status\": \"0\"");
			strBuff.append("}");
		}
		
		//String strTemp = "";
		//Storage data = service.findById(id);
		//strTemp = "{\"status\":\"1\",\"id\":\""+id+"\",\"varValue1\":\""+data.getVarValue1()+"\"}";
		return strBuff.toString();
	}
	
	@RequestMapping("/get")
	@ResponseBody
	public Map<String, Object> getAll(PageModel<Storage> pageModel, Storage data, HttpServletRequest request, Model model) {
		
		//System.out.println("------------------get------------------");
		String keywords = StrEdit.StrChkNull(data.getKeywords());
		
		Map<String,Object> newMap = new HashMap<String,Object>();
		
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		int classId = StrEdit.StrToInt(request.getParameter("classId"));
		int intValue2 = StrEdit.StrToInt(request.getParameter("intValue2"));
		
		int categoryId = StrEdit.StrToInt(request.getParameter("categoryId"));
		int personId = StrEdit.StrToInt(request.getParameter("personId"));
		int officeId = StrEdit.StrToInt(request.getParameter("officeId"));
		
		String varValue1 = StrEdit.StrChkNull(request.getParameter("varValue1"));
		String varValue2 = StrEdit.StrChkNull(request.getParameter("varValue2"));
		String varValue3 = StrEdit.StrChkNull(request.getParameter("varValue3"));
		String varValue4 = StrEdit.StrChkNull(request.getParameter("varValue4"));
		String varValue6 = StrEdit.StrChkNull(request.getParameter("varValue6"));
		String varValue8 = StrEdit.StrChkNull(request.getParameter("varValue8"));
		String varValue12 = StrEdit.StrChkNull(request.getParameter("varValue12"));
		String varValue17 = StrEdit.StrChkNull(request.getParameter("varValue17"));
		String varValue39 = StrEdit.StrChkNull(request.getParameter("varValue39"));
		String startDate = StrEdit.StrChkNull(request.getParameter("startDate"));
		String endDate = StrEdit.StrChkNull(request.getParameter("endDate"));
		if(!startDate.equals("")){
			startDate = startDate + " 00:00:00";
		}
		if(!endDate.equals("")){
			endDate = endDate + " 23:59:59";
		}
		
		int pageNumber = StrEdit.StrToInt(request.getParameter("pageNumber"));
		int pageSize = StrEdit.StrToInt(request.getParameter("pageSize"));
		int startRow = 0;
        if(pageNumber!=0) {
        	startRow = (pageNumber - 1) * pageSize;
        }
        newMap.put("startRow", startRow);
        newMap.put("pageSize", pageSize);
        newMap.put("channelId", channelId);
        newMap.put("classId", classId);
        newMap.put("intValue2", intValue2);
        newMap.put("keywords", keywords);
        newMap.put("varValue1", varValue1);
        newMap.put("varValue2", varValue2);
        newMap.put("varValue3", varValue3);
        newMap.put("varValue4", varValue4);
        newMap.put("varValue6", varValue6);
        newMap.put("varValue8", varValue8);
        newMap.put("varValue12", varValue12);
        newMap.put("varValue17", varValue17);
        newMap.put("varValue39", varValue39);
        newMap.put("categoryId", categoryId);
        newMap.put("personId", personId);
        newMap.put("officeId", officeId);
        newMap.put("startDate", startDate);
        newMap.put("endDate", endDate);
		List<Storage> list = service.pageList(newMap);
		int totalRecords = service.totalRecord(newMap);
		pageModel.setTotal(totalRecords);
		
		//Object jsonObject = JSONObject.toJSON(list);
		//System.out.println("jsonObject:"+jsonObject);
		//System.out.println("totalRecords1:"+totalRecords);
		
		int pn = 1;
		if (pageNumber != 0) {
			pn = new Integer(pageNumber);
		}
		pageModel.setPages(totalRecords, pn);//设置总记录数和当前页面
		Map<String, Object> map = new HashMap<>();
		map.put("page", pageModel);
		map.put("list", list);
		return map;
	}
	
	@RequestMapping("/getconnect")
	@ResponseBody
	public Map<String, Object> getconnect(PageModel<Storage> pageModel, Storage data, HttpServletRequest request, Model model) {
		
		//System.out.println("------------------get------------------");
		String keywords = StrEdit.StrChkNull(data.getKeywords());
		
		Map<String,Object> newMap = new HashMap<String,Object>();
		
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		int classId = StrEdit.StrToInt(request.getParameter("classId"));
		int intValue2 = StrEdit.StrToInt(request.getParameter("intValue2"));
		String varValue1 = StrEdit.StrChkNull(request.getParameter("varValue1"));
		String varValue2 = StrEdit.StrChkNull(request.getParameter("varValue2"));
		String varValue3 = StrEdit.StrChkNull(request.getParameter("varValue3"));
		String varValue4 = StrEdit.StrChkNull(request.getParameter("varValue4"));
		String varValue6 = StrEdit.StrChkNull(request.getParameter("varValue6"));
		String varValue8 = StrEdit.StrChkNull(request.getParameter("varValue8"));
		String date = StrEdit.StrChkNull(request.getParameter("date"));
		String startDate = "";
		String endDate = "";
		if(!date.equals("")){
			startDate = date + " 00:00:00";
			endDate = date + " 23:59:59";
		}
		
		int pageNumber = StrEdit.StrToInt(request.getParameter("pageNumber"));
		int pageSize = StrEdit.StrToInt(request.getParameter("pageSize"));
		int startRow = 0;
        if(pageNumber!=0) {
        	startRow = (pageNumber - 1) * pageSize;
        }
        newMap.put("startRow", startRow);
        newMap.put("pageSize", pageSize);
		/*int pageStart = (pageNumber -1) * pageSize+1;//oracle分页
        int pageEnd = pageStart + pageSize -1;
        newMap.put("pageStart", pageStart);
        newMap.put("pageEnd", pageEnd);*/
        newMap.put("task", "getList");
        newMap.put("channelId", channelId);
        newMap.put("classId", classId);
        newMap.put("intValue2", intValue2);
        newMap.put("keywords", keywords);
        newMap.put("varValue1", varValue1);
        newMap.put("varValue2", varValue2);
        newMap.put("varValue3", varValue3);
        newMap.put("varValue4", varValue4);
        newMap.put("varValue6", varValue6);
        newMap.put("varValue8", varValue8);
        newMap.put("startDate", startDate);
        newMap.put("endDate", endDate);
		List<Storage> list = service.getListByMap(newMap);
		int totalRecords = service.getIntValue(newMap);
		pageModel.setTotal(totalRecords);
		
		//Object jsonObject = JSONObject.toJSON(list);
		//System.out.println("jsonObject:"+jsonObject);
		//System.out.println("totalRecords1:"+totalRecords);
		
		int pn = 1;
		if (pageNumber != 0) {
			pn = new Integer(pageNumber);
		}
		pageModel.setPages(totalRecords, pn);//设置总记录数和当前页面
		Map<String, Object> map = new HashMap<>();
		map.put("page", pageModel);
		map.put("list", list);
		return map;
	}
	
	@RequestMapping("/add")
	public String toAdd(Storage data,HttpServletRequest request,Model model){
		String modelName = "";
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		if(channelId==1){
			modelName = "工具耗材库存";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelName", modelName);
		
		List<String> dictionery5 = dictionary.selectDictionaryList(5);
		model.addAttribute("dictionery5", dictionery5);
		
		Map<Long, String> typeMap = new HashMap<Long, String>();
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
		return "/front/storage/add";
	}
	
	@RequestMapping("/save")
	@ResponseBody
	public String save(Storage data,HttpServletRequest request,HttpServletResponse response){
		int conId = 0;
		String strTemp = "";
		
		int userId = 0;
		String userName = "";
		User sessionUser = ((User)request.getSession().getAttribute(Contants.SESSION_USER));
		if(sessionUser!=null){
			userId = sessionUser.getConId();
			userName = sessionUser.getVarValue1();
		}
		
		String autoNo = "";
		int intValue2 = data.getIntValue2();
		if(intValue2==1){
			autoNo = StrEdit.getSN("BJGJ");
		}else if(intValue2==2){
			autoNo = StrEdit.getSN("BJHC");
		}else if(intValue2==3){
			autoNo = StrEdit.getSN("WXGJ");
		}else if(intValue2==4){
			autoNo = StrEdit.getSN("WXHC");
		}
		data.setVarValue2(autoNo);
		
		try{
			service.save(data);
			conId = data.getConId();
		}catch(Exception e){
			conId = 0;
			System.out.println("save插入数据失败" + e.getLocalizedMessage());
		}
		
		if(conId>0){
			
			String attachIds=StrEdit.StrChkNull(request.getParameter("attachIds"));//附件
			if(!attachIds.equals("")){
				String[] arrAttachIds = attachIds.split("\\,");
				for(int m=0;m<arrAttachIds.length;m++){
					boolean boolValue = attach.update(StrEdit.StrToInt(arrAttachIds[m]),conId);
					if(!boolValue){
						System.out.println("附件信息ID更新失败："+arrAttachIds[m]);
					}
				}
			}
			
			strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"添加失败\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/update")
	@ResponseBody
	public String update(Storage data,HttpServletRequest request){
		int conId = data.getConId();
		String strTemp = "";
		
		int userId = 0;
		String userName = "";
		User sessionUser = ((User)request.getSession().getAttribute(Contants.SESSION_USER));
		if(sessionUser!=null){
			userId = sessionUser.getConId();
			userName = sessionUser.getVarValue1();
		}
		
		boolean boolValue = service.update(data);
		if(boolValue){
			
			strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"更新失败\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/getById")
	public String getById(int id,HttpServletRequest request,Model model){
		Map<Long, String> typeMap = new HashMap<Long, String>();
		Storage data = service.findById(id);
		String modelTitle = "", modelName = "", redirect = "";
		int channelId = data.getChannelId();
		if(channelId==1){
			modelName = "工具耗材库存";
			redirect = "front/storage/add";
		}else if(channelId==5){
			modelTitle = "固定资产";
			modelName = "固定资产查看";
			redirect = "front/storage/view";
			
			Map<String,Object> newMap1 = new HashMap<String,Object>();
			newMap1.put("task", "getStorageIn");
			newMap1.put("conId", id);
	        List<Storage> list1 = service.getListByMap(newMap1);
	        request.setAttribute("list1", list1);
	        request.setAttribute("zynumber2", list1.size());
			
			Map<String,Object> newMap2 = new HashMap<String,Object>();
			newMap2.put("task", "getStorageMove");
			newMap2.put("channelId", channelId);
			newMap2.put("intValue3", id);
	        List<Storage> list2 = service.getListByMap(newMap2);
	        request.setAttribute("list2", list2);
	        request.setAttribute("zynumber2", list2.size());
	        
	        Map<String,Object> newMap3 = new HashMap<String,Object>();
	        newMap3.put("task", "getStorageRepair");
	        newMap3.put("channelId", 6);
	        newMap3.put("intValue3", id);
	        List<Storage> list3 = service.getListByMap(newMap3);
	        request.setAttribute("list3", list3);
	        request.setAttribute("zynumber3", list3.size());
	        
	        Map<String,Object> newMap4 = new HashMap<String,Object>();
	        newMap4.put("task", "getStorageBack");
	        newMap4.put("channelId", 8);
	        newMap4.put("intValue3", id);
	        List<Storage> list4 = service.getListByMap(newMap4);
	        request.setAttribute("list4", list4);
	        request.setAttribute("zynumber4", list4.size());
	        
	        Map<String,Object> newMap5 = new HashMap<String,Object>();
	        newMap5.put("task", "getStorageExit");
	        newMap5.put("channelId", 7);
	        newMap5.put("intValue3", id);
	        List<Storage> list5 = service.getListByMap(newMap5);
	        request.setAttribute("list5", list5);
	        request.setAttribute("zynumber5", list5.size());
	        
		}else if(channelId==15){
			modelTitle = "设备";
			modelName = "设备查看";
			redirect = "front/storage/view";
			
			Map<String,Object> newMap1 = new HashMap<String,Object>();
			newMap1.put("task", "getStorageIn");
			newMap1.put("conId", id);
	        List<Storage> list1 = service.getListByMap(newMap1);
	        request.setAttribute("list1", list1);
	        request.setAttribute("zynumber2", list1.size());
			
			Map<String,Object> newMap2 = new HashMap<String,Object>();
			newMap2.put("task", "getStorageMove");
			newMap2.put("channelId", channelId);
			newMap2.put("intValue3", id);
	        List<Storage> list2 = service.getListByMap(newMap2);
	        request.setAttribute("list2", list2);
	        request.setAttribute("zynumber2", list2.size());
	        
	        Map<String,Object> newMap3 = new HashMap<String,Object>();
	        newMap3.put("task", "getStorageRepair");
	        newMap3.put("channelId", 16);
	        newMap3.put("intValue3", id);
	        List<Storage> list3 = service.getListByMap(newMap3);
	        request.setAttribute("list3", list3);
	        request.setAttribute("zynumber3", list3.size());
	        
	        Map<String,Object> newMap4 = new HashMap<String,Object>();
	        newMap4.put("task", "getStorageBack");
	        newMap4.put("channelId", 18);
	        newMap4.put("intValue3", id);
	        List<Storage> list4 = service.getListByMap(newMap4);
	        request.setAttribute("list4", list4);
	        request.setAttribute("zynumber4", list4.size());
	        
	        Map<String,Object> newMap5 = new HashMap<String,Object>();
	        newMap5.put("task", "getStorageExit");
	        newMap5.put("channelId", 17);
	        newMap5.put("intValue3", id);
	        List<Storage> list5 = service.getListByMap(newMap5);
	        request.setAttribute("list5", list5);
	        request.setAttribute("zynumber5", list5.size());
	        
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelTitle", modelTitle);
		request.setAttribute("modelName", modelName);
		
		List<String> dictionery5 = dictionary.selectDictionaryList(5);
		model.addAttribute("dictionery5", dictionery5);
        
        List<Attachment> attachList1011 = attach.list(1011,id);
        
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
        model.addAttribute("attachList1011", attachList1011);
        
		return redirect;
	}
	
	@RequestMapping("/getByIdAudit")
	public String getByIdAudit(int id,HttpServletRequest request,Model model){
		Map<Long, String> typeMap = new HashMap<Long, String>();
		Storage data = service.findById(id);
		String modelName = "";
		int channelId = data.getChannelId();
		if(channelId==1){
			modelName = "工具耗材库存";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelName", modelName);
		
		List<String> dictionery5 = dictionary.selectDictionaryList(5);
		model.addAttribute("dictionery5", dictionery5);
        
        List<Attachment> attachList1001 = attach.list(1001,id);
        List<String> dictionery4 = dictionary.selectDictionaryList(4);
        
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
        model.addAttribute("attachList1001", attachList1001);
        model.addAttribute("dictionery4", dictionery4);
        
		return "/front/storage/auditadd";
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public String delete(int id,HttpServletRequest request){
		String strTemp = "", strValue = "";
		boolean boolValue = false;
		String authority = request.getSession().getAttribute(Contants.SESSION_AUTHORITY).toString();
		if(authority.contains("goodsnamedel")){//authority.indexOf("goodsnamedel")!=0
			boolValue = service.delete(id);
			if(boolValue){
				strTemp = "{\"status\":\"1\"}";
			}else{
				strValue = "删除失败";
				strTemp = "{\"status\":\"0\",\"errors\":\""+strValue+"\"}";
			}
		}else{
			strValue = "您没有此操作权限";
			strTemp = "{\"status\":\"2\",\"errors\":\""+strValue+"\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/audit")
	@ResponseBody
	public String audit(HttpServletRequest request){
		String strTemp = "";
		boolean boolValue = false;
		String task = StrEdit.StrChkNull(request.getParameter("task"));
		Date currTime = new Date();
		
		if(task.equals("auditresult")){
			int status = StrEdit.StrToInt(request.getParameter("status"));
			String reason = StrEdit.StrChkNull(request.getParameter("reason"));
			String memo = StrEdit.StrChkNull(request.getParameter("memo"));
			String audituserid = StrEdit.StrChkNull(request.getParameter("audituserid"));
			String auditname = StrEdit.StrChkNull(request.getParameter("auditname"));
			String auditdate = StrEdit.StrChkNull(request.getParameter("auditdate"));
			String selectids = StrEdit.StrChkNull(request.getParameter("selectids"));
			String[] arrParameter = selectids.split("\\,");//如果不包含,则直接返回原字符的一维数组
	    	if (arrParameter!=null){
				for(int i=0;i<arrParameter.length;i++){
					Map<String,Object> newMap = new HashMap<String,Object>();
					newMap.put("task", task);
					newMap.put("varValue6", status);
					newMap.put("varValue7", reason);
					newMap.put("varValue8", memo);
					newMap.put("varValue12", audituserid);
					newMap.put("varValue13", auditname);
					newMap.put("updateTime", currTime);
					newMap.put("dateValue3", auditdate);
			        newMap.put("conId", StrEdit.StrToInt(arrParameter[i]));
			        boolValue = service.updateByMap(newMap);
				}
			}
		}
		
		return strTemp;
	}
	
}
