package com.luktel.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.luktel.model.Attachment;
import com.luktel.model.News;
import com.luktel.model.User;
import com.luktel.service.AttachmentService;
import com.luktel.service.NewsService;
import com.luktel.service.DictionaryService;
import com.util.common.Contants;
import com.util.common.PageModel;
import com.util.common.StrEdit;


@Controller
@RequestMapping("/news")
public class NewsController{
	
	@Autowired
	private NewsService service;
	
	@Autowired
	private AttachmentService attach;
	
	@Autowired
	private DictionaryService dictionary;
	
	@RequestMapping("/list")
	public String list(HttpServletRequest request) {
		String modelTitle = "", modelName = "", redirect = "";
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		if(channelId==1){
			modelTitle = "通知";
			modelName = "通知列表";
			redirect = "front/article/news/list";
		}else if(channelId==2){
			modelTitle = "公告";
			modelName = "公告列表";
			redirect = "front/article/news/list";
		}else if(channelId==3){
			modelTitle = "信息";
			modelName = "信息列表";
			redirect = "front/article/news/list";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelTitle", modelTitle);
		request.setAttribute("modelName", modelName);
		return redirect;
	}
	
	@RequestMapping("/auditlist")
	public String auditlist(HttpServletRequest request) {
		String modelTitle = "", modelName = "", redirect = "";
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		if(channelId==1){
			modelTitle = "通知";
			modelName = "通知列表";
			redirect = "front/article/news/auditlist";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelTitle", modelTitle);
		request.setAttribute("modelName", modelName);
		return redirect;
	}
	
	@RequestMapping("/getJsonById")
	@ResponseBody
	public String getJsonById(int id){
		StringBuffer strBuff = new StringBuffer("");
		News data = service.findById(id);
		if(data!=null){
			String dateValue1 = StrEdit.dateToStr(data.getDateValue1());
			strBuff.append("{");
			strBuff.append("\"status\": \"1\",");
			strBuff.append("\"id\": \""+data.getConId()+"\",");
			strBuff.append("\"varValue1\": \""+data.getVarValue1()+"\",");
			strBuff.append("\"varValue2\": \""+data.getVarValue2()+"\",");
			strBuff.append("\"varValue3\": \""+data.getVarValue3()+"\",");
			strBuff.append("\"varValue4\": \""+data.getVarValue4()+"\",");
			strBuff.append("\"varValue5\": \""+data.getVarValue5()+"\",");
			strBuff.append("\"varValue6\": \""+data.getVarValue6()+"\",");
			strBuff.append("\"varValue7\": \""+data.getVarValue7()+"\",");
			strBuff.append("\"varValue8\": \""+data.getVarValue8()+"\",");
			strBuff.append("\"varValue9\": \""+data.getVarValue9()+"\",");
			strBuff.append("\"varValue10\": \""+data.getVarValue10()+"\",");
			strBuff.append("\"varValue11\": \""+data.getVarValue11()+"\",");
			strBuff.append("\"varValue12\": \""+data.getVarValue12()+"\",");
			strBuff.append("\"varValue13\": \""+data.getVarValue13()+"\",");
			strBuff.append("\"varValue14\": \""+data.getVarValue14()+"\",");
			strBuff.append("\"varValue15\": \""+data.getVarValue15()+"\",");
			strBuff.append("\"varValue16\": \""+data.getVarValue16()+"\",");
			strBuff.append("\"varValue17\": \""+data.getVarValue17()+"\",");
			strBuff.append("\"varValue18\": \""+data.getVarValue18()+"\",");
			strBuff.append("\"varValue19\": \""+data.getVarValue19()+"\",");
			strBuff.append("\"varValue20\": \""+data.getVarValue20()+"\",");
			strBuff.append("\"varValue21\": \""+data.getVarValue21()+"\",");
			strBuff.append("\"varValue22\": \""+data.getVarValue22()+"\",");
			strBuff.append("\"varValue23\": \""+data.getVarValue23()+"\",");
			strBuff.append("\"varValue24\": \""+data.getVarValue24()+"\",");
			strBuff.append("\"varValue25\": \""+data.getVarValue25()+"\",");
			strBuff.append("\"varValue26\": \""+data.getVarValue26()+"\",");
			strBuff.append("\"decValue1\": \""+data.getDecValue1()+"\",");
			strBuff.append("\"decValue2\": \""+data.getDecValue2()+"\",");
			strBuff.append("\"intValue1\": \""+data.getIntValue1()+"\",");
			strBuff.append("\"intValue2\": \""+data.getIntValue2()+"\",");
			strBuff.append("\"intValue3\": \""+data.getIntValue3()+"\",");
			strBuff.append("\"intValue4\": \""+data.getIntValue4()+"\",");
			strBuff.append("\"intValue5\": \""+data.getIntValue5()+"\",");
			strBuff.append("\"dateValue1\": \""+dateValue1+"\",");
			strBuff.append("\"text1\": \""+data.getText1()+"\"");
			strBuff.append("}");
		}else{
			strBuff.append("{");
			strBuff.append("\"status\": \"0\"");
			strBuff.append("}");
		}
		
		//String strTemp = "";
		//strTemp = "{\"status\":\"1\",\"id\":\""+id+"\",\"varValue1\":\""+data.getVarValue1()+"\"}";
		return strBuff.toString();
	}
	
	@RequestMapping("/get")
	@ResponseBody
	public Map<String, Object> getAll(PageModel<News> pageModel, News data, HttpServletRequest request, Model model) {
		
		//System.out.println("------------------get------------------");
		String keywords = StrEdit.StrChkNull(data.getKeywords());
		
		Map<String,Object> newMap = new HashMap<String,Object>();
		
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		int classId = StrEdit.StrToInt(request.getParameter("classId"));
		int intValue1 = StrEdit.StrToInt(request.getParameter("intValue1"));
		int intValue2 = StrEdit.StrToInt(request.getParameter("intValue2"));
		int intValue3 = StrEdit.StrToInt(request.getParameter("intValue3"));
		String varValue1 = StrEdit.StrChkNull(request.getParameter("varValue1"));
		String varValue2 = StrEdit.StrChkNull(request.getParameter("varValue2"));
		String varValue3 = StrEdit.StrChkNull(request.getParameter("varValue3"));
		String varValue4 = StrEdit.StrChkNull(request.getParameter("varValue4"));
		String varValue5 = StrEdit.StrChkNull(request.getParameter("varValue5"));
		String varValue6 = StrEdit.StrChkNull(request.getParameter("varValue6"));
		String varValue7 = StrEdit.StrChkNull(request.getParameter("varValue7"));
		String varValue8 = StrEdit.StrChkNull(request.getParameter("varValue8"));
		String varValue9 = StrEdit.StrChkNull(request.getParameter("varValue9"));
		String varValue10 = StrEdit.StrChkNull(request.getParameter("varValue10"));
		String varValue21 = StrEdit.StrChkNull(request.getParameter("varValue21"));
		String varValue23 = StrEdit.StrChkNull(request.getParameter("varValue23"));
		String varValue39 = StrEdit.StrChkNull(request.getParameter("varValue39"));
		String varValue40 = StrEdit.StrChkNull(request.getParameter("varValue40"));
		String startDate = StrEdit.StrChkNull(request.getParameter("startDate"));
		String endDate = StrEdit.StrChkNull(request.getParameter("endDate"));
		if(!startDate.equals("")){
			startDate = startDate + " 00:00:00";
		}
		if(!endDate.equals("")){
			endDate = endDate + " 23:59:59";
		}
		String startDate1 = StrEdit.StrChkNull(request.getParameter("startDate1"));
		String endDate1 = StrEdit.StrChkNull(request.getParameter("endDate1"));
		if(!startDate1.equals("")){
			startDate1 = startDate1 + " 00:00:00";
		}
		if(!endDate1.equals("")){
			endDate1 = endDate1 + " 23:59:59";
		}
		
		int pageNumber = StrEdit.StrToInt(request.getParameter("pageNumber"));
		int pageSize = StrEdit.StrToInt(request.getParameter("pageSize"));
		int startRow = 0;
        if(pageNumber!=0) {
        	startRow = (pageNumber - 1) * pageSize;
        }
        newMap.put("startRow", startRow);
        newMap.put("pageSize", pageSize);
        newMap.put("channelId", channelId);
        newMap.put("classId", classId);
        newMap.put("intValue1", intValue1);
        newMap.put("intValue2", intValue2);
        newMap.put("intValue3", intValue3);
        newMap.put("keywords", keywords);
        newMap.put("varValue1", varValue1);
        newMap.put("varValue2", varValue2);
        newMap.put("varValue3", varValue3);
        newMap.put("varValue4", varValue4);
        newMap.put("varValue5", varValue5);
        newMap.put("varValue6", varValue6);
        newMap.put("varValue7", varValue7);
        newMap.put("varValue8", varValue8);
        newMap.put("varValue9", varValue9);
        newMap.put("varValue10", varValue10);
        newMap.put("varValue21", varValue21);
        newMap.put("varValue23", varValue23);
        newMap.put("varValue39", varValue39);
        newMap.put("varValue40", varValue40);
        newMap.put("startDate", startDate);
        newMap.put("endDate", endDate);
        newMap.put("startDate1", startDate1);
        newMap.put("endDate1", endDate1);
		List<News> list = service.pageList(newMap);
		int totalRecords = service.totalRecord(newMap);
		pageModel.setTotal(totalRecords);
		
		//Object jsonObject = JSONObject.toJSON(list);
		//System.out.println("jsonObject:"+jsonObject);
		//System.out.println("totalRecords1:"+totalRecords);
		
		int pn = 1;
		if (pageNumber != 0) {
			pn = new Integer(pageNumber);
		}
		pageModel.setPages(totalRecords, pn);//设置总记录数和当前页面
		Map<String, Object> map = new HashMap<>();
		map.put("page", pageModel);
		map.put("list", list);
		return map;
	}
	
	@RequestMapping("/add")
	public String toAdd(News data,HttpServletRequest request,Model model){
		String modelTitle = "", modelName = "", redirect = "";
		Date currTime = new Date();
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		if(channelId==1){
			modelTitle = "通知";
			modelName = "通知增加";
			redirect = "front/article/news/add";
		}else if(channelId==2){
			modelTitle = "公告";
			modelName = "公告增加";
			redirect = "front/article/news/add";
		}else if(channelId==3){
			modelTitle = "信息";
			modelName = "信息发布";
			redirect = "front/article/news/add";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelTitle", modelTitle);
		request.setAttribute("modelName", modelName);
		
		List<String> dictionery8 = dictionary.selectDictionaryList(8);
		model.addAttribute("dictionery8", dictionery8);
		
		Map<Long, String> typeMap = new HashMap<Long, String>();
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
        return redirect;
	}
	
	@RequestMapping("/save")
	@ResponseBody
	public String save(News data,HttpServletRequest request,HttpServletResponse response){
		int conId = 0;
		String strTemp = "";
		
		int userId = 0;
		String userName = "";
		User sessionUser = ((User)request.getSession().getAttribute(Contants.SESSION_USER));
		if(sessionUser!=null){
			userId = sessionUser.getConId();
			userName = sessionUser.getVarValue1();
		}
        
		try{
			service.save(data);
			conId = data.getConId();
		}catch(Exception e){
			conId = 0;
			System.out.println("save插入数据失败" + e.getLocalizedMessage());
		}
		
		if(conId>0){
			
			String attachIds=StrEdit.StrChkNull(request.getParameter("attachIds"));//附件
			if(!attachIds.equals("")){
				String[] arrAttachIds = attachIds.split("\\,");
				for(int m=0;m<arrAttachIds.length;m++){
					boolean boolValue = attach.update(StrEdit.StrToInt(arrAttachIds[m]),conId);
					if(!boolValue){
						System.out.println("附件信息ID更新失败："+arrAttachIds[m]);
					}
				}
			}
			
			String attachId1002=StrEdit.StrChkNull(request.getParameter("attachId1002"));
			if(!attachId1002.equals("")){
				String[] arrAttachIds = attachId1002.split("\\,");
				for(int m=0;m<arrAttachIds.length;m++){
					boolean boolValue = attach.update(StrEdit.StrToInt(arrAttachIds[m]),conId);
					if(!boolValue){
						System.out.println("附件信息ID更新失败："+arrAttachIds[m]);
					}
				}
			}
			
			strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"添加失败\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/update")
	@ResponseBody
	public String update(News data,HttpServletRequest request){
		int conId = data.getConId();
		String strTemp = "";
		
		int userId = 0;
		String userName = "";
		User sessionUser = ((User)request.getSession().getAttribute(Contants.SESSION_USER));
		if(sessionUser!=null){
			userId = sessionUser.getConId();
			userName = sessionUser.getVarValue1();
		}
		
		boolean boolValue = service.update(data);
		if(boolValue){
			
			strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"更新失败\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/getById")
	public String getById(int id,HttpServletRequest request,Model model){
		Map<Long, String> typeMap = new HashMap<Long, String>();
		News data = service.findById(id);
		String modelTitle = "", modelName = "", redirect = "";
		int channelId = data.getChannelId();
		if(channelId==1){
			modelTitle = "通知";
			modelName = "通知编辑";
			redirect = "front/article/news/add";
		}else if(channelId==2){
			modelTitle = "公告";
			modelName = "公告编辑";
			List<Attachment> attachList1 = attach.list(1201,id);
			List<Attachment> attachList2 = attach.list(1202,id);
			model.addAttribute("attachList1", attachList1);
			model.addAttribute("attachList2", attachList2);
			redirect = "front/article/news/add";
		}else if(channelId==3){
			modelTitle = "信息";
			modelName = "信息编辑";
			List<Attachment> attachList1 = attach.list(1201,id);
			List<Attachment> attachList2 = attach.list(1202,id);
			model.addAttribute("attachList1", attachList1);
			model.addAttribute("attachList2", attachList2);
			redirect = "front/article/news/add";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelTitle", modelTitle);
		request.setAttribute("modelName", modelName);
		
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
        
        return redirect;
	}
	
	@RequestMapping("/getByIdAudit")
	public String getByIdAudit(int id,HttpServletRequest request,Model model){
		Map<Long, String> typeMap = new HashMap<Long, String>();
		News data = service.findById(id);
		String modelTitle = "", modelName = "", redirect = "";
		int channelId = data.getChannelId();
		if(channelId==1){
			modelTitle = "通知";
			modelName = "通知查看";
			redirect = "front/article/news/auditadd";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelTitle", modelTitle);
		request.setAttribute("modelName", modelName);
		
		List<String> dictionery8 = dictionary.selectDictionaryList(8);
		model.addAttribute("dictionery8", dictionery8);
        
        List<Attachment> attachList1011 = attach.list(1011,id);
        
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
        model.addAttribute("attachList1011", attachList1011);
        
        return redirect;
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public String delete(int id,HttpServletRequest request){
		String strTemp = "";
		boolean boolValue = service.delete(id);
		if(boolValue){
			strTemp = "{\"status\":\"1\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"删除失败\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/audit")
	@ResponseBody
	public String audit(News data,HttpServletRequest request){
		String strTemp = "";
		boolean boolValue = false;
		String task = StrEdit.StrChkNull(request.getParameter("task"));
		Date currTime = new Date();
		
		if(task.equals("audit")){
			int status = StrEdit.StrToInt(request.getParameter("status"));
			String selectids = StrEdit.StrChkNull(request.getParameter("selectids"));
			String[] arrParameter = selectids.split("\\,");//如果不包含,则直接返回原字符的一维数组
	    	if (arrParameter!=null){
				for(int i=0;i<arrParameter.length;i++){
					Map<String,Object> newMap = new HashMap<String,Object>();
					newMap.put("task", task);
					newMap.put("varValue6", status);
					newMap.put("updateTime", currTime);
			        newMap.put("conId", StrEdit.StrToInt(arrParameter[i]));
			        boolValue = service.updateByMap(newMap);
				}
			}
		}
		if(task.equals("auditresult")){
			int status = StrEdit.StrToInt(request.getParameter("status"));
			String reason = StrEdit.StrChkNull(request.getParameter("reason"));
			String memo = StrEdit.StrChkNull(request.getParameter("memo"));
			String audituserid = StrEdit.StrChkNull(request.getParameter("audituserid"));
			String auditname = StrEdit.StrChkNull(request.getParameter("auditname"));
			String auditdate = StrEdit.StrChkNull(request.getParameter("auditdate"));
			String selectids = StrEdit.StrChkNull(request.getParameter("selectids"));
			String[] arrParameter = selectids.split("\\,");//如果不包含,则直接返回原字符的一维数组
	    	if (arrParameter!=null){
				for(int i=0;i<arrParameter.length;i++){
					Map<String,Object> newMap = new HashMap<String,Object>();
					newMap.put("task", task);
					newMap.put("varValue6", status);
					newMap.put("varValue7", reason);
					newMap.put("varValue8", memo);
					newMap.put("varValue12", audituserid);
					newMap.put("varValue13", auditname);
					newMap.put("updateTime", currTime);
					newMap.put("dateValue3", auditdate);
			        newMap.put("conId", StrEdit.StrToInt(arrParameter[i]));
			        boolValue = service.updateByMap(newMap);
				}
			}
		}
		if(boolValue){
			strTemp = "{\"status\":\"1\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"更新失败\"}";
		}
		
		return strTemp;
	}
	
}
