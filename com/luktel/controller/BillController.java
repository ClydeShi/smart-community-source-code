package com.luktel.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.luktel.model.Attachment;
import com.luktel.model.Bill;
import com.luktel.model.StorageIn;
import com.luktel.model.User;
import com.luktel.service.AttachmentService;
import com.luktel.service.BillService;
import com.luktel.service.DictionaryService;
import com.util.common.Contants;
import com.util.common.DBConn2;
import com.util.common.PageModel;
import com.util.common.StrEdit;


@Controller
@RequestMapping("/bill")
public class BillController{
	
	private String sql = "";
    private DBConn2 mdb = new DBConn2();
    private ResultSet rs = null;
	private static Logger log = LoggerFactory.getLogger(BillController.class);
	
	@Autowired
	private BillService service;
	
	@Autowired
	private AttachmentService attach;
	
	@Autowired
	private DictionaryService dictionary;
	
	@RequestMapping("/list")
	public String list(HttpServletRequest request) {
		String modelTitle = "", modelName = "", redirect = "";
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		if(channelId==1){
			modelTitle = "账单";
			modelName = "账单列表";
			redirect = "front/bill/contract/list";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelTitle", modelTitle);
		request.setAttribute("modelName", modelName);
		return redirect;
	}
	
	@RequestMapping("/createbill")
	@ResponseBody
	public String createbill(StorageIn data,HttpServletRequest request,HttpServletResponse response){
		int conId = 0;
		String strTemp = "";
		System.out.println("createbill-------------------------------------------------");
		
		int year = StrEdit.StrToInt(request.getParameter("year"));
		int month = StrEdit.StrToInt(request.getParameter("month"));
		String firstDay = StrEdit.getFirstDayOfMonth(year, month);
		String lastDay = StrEdit.getLastDayOfMonth(year, month);
		//String firstDay = StrEdit.getFirstDayOfMonth(year, month) + " 00:00:00";
		//String lastDay = StrEdit.getLastDayOfMonth(year, month) + " 23:59:59";
		int days = StrEdit.daysBetween(firstDay, lastDay) + 1;
		System.out.println("firstDay:"+firstDay);
		System.out.println("lastDay:"+lastDay);
		System.out.println("days:"+days);
		
		int userId = 0;
		String userName = "";
		User sessionUser = ((User)request.getSession().getAttribute(Contants.SESSION_USER));
		if(sessionUser!=null){
			userId = sessionUser.getConId();
			userName = sessionUser.getVarValue1();
		}
		
		int channelId = data.getChannelId();
		String createDate = StrEdit.getSimDateForDB();
		
		/*
		 * 
		要生成账单月份第一天a，比如2019-02-01
		要生成账单月份最后一天e，比如2019-02-28
		当月天数d=e-a
		
		合同开始日期c1，比如2019-02-10
		合同结束日期c2，比如2019-03-10，2020-02-09
		
		如果 c1>=a 且 c2<=e 比如2019-02-20<2019-02-28 在这个月份之内的合同，c2-c1=租赁具体天数，总价/当月天数=每天租金*租赁具体天数=实际账单租金
		
		如果 c1>=a 且 c1<=e 且 c2>e 在该月份开始的合同，e-c1=租赁具体天数，总价/当月天数=每天租金*租赁具体天数=实际账单租金
		
		如果 c1<a 且 c2<=e 且 c2>a 在最后一个月到期的合同，c2-a=租赁具体天数，总价/当月天数=每天租金*租赁具体天数=实际账单租金
		
		如果 c1<a 且 c2>e 在自然月的合同，总价=实际账单租金
		 */
		
		Date a = null;
		Date e = null;
		
		java.text.DateFormat df=new java.text.SimpleDateFormat("yyyy-MM-dd");
		try {
            a = df.parse(firstDay);
            e = df.parse(lastDay);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
		
		Date c1 = null;
		Date c2 = null;
		
		sql = "select CONID,VARVALUE1,VARVALUE2,VARVALUE3,DECVALUE1,DECVALUE2,DECVALUE3,INTVALUE1,INTVALUE2,TEXT1,ADDTIME,UPDATETIME,DATEVALUE1,DATEVALUE2 from CONTRACTCHARGE where DATEVALUE2>='"+firstDay+"'";
		//sql = "select CONID,VARVALUE1,VARVALUE2,VARVALUE3,DECVALUE1,DECVALUE2,DECVALUE3,INTVALUE1,INTVALUE2,TEXT1,ADDTIME,UPDATETIME,DATEVALUE1,DATEVALUE2 from CONTRACTCHARGE where DATEVALUE1<='"+firstDay+"' and DATEVALUE2>='"+lastDay+"'";
		ResultSet rs = mdb.executeQuery(sql);
		try {
			while(rs.next()){
				int id = rs.getInt("CONID");
				int contractId = rs.getInt("INTVALUE2");
				String name = rs.getString("VARVALUE1");
				String standard = rs.getString("VARVALUE3");
				String amount = rs.getString("DECVALUE3");
				String sdateValue1 = StrEdit.StringLeft(rs.getString("DATEVALUE1"), 10);
				String sdateValue2 = StrEdit.StringLeft(rs.getString("DATEVALUE2"), 10);
				System.out.println("id:"+id);
				System.out.println("name:"+name);
				System.out.println("standard:"+standard);
				System.out.println("amount:"+amount);
				double damount = StrEdit.StrToDouble(amount);
				double average = damount/days;//月平均每天租金
				System.out.println("average:"+average);
				
				int companyId = 0;
				String contractName = "";
				String sql2 = "select CONID,VARVALUE1,INTVALUE2 from CONTRACT where CONID='"+contractId+"'";
				ResultSet rs2 = mdb.executeQuery(sql2);
				if(rs2.next()) {
					contractName = rs2.getString("VARVALUE1");
					companyId = rs2.getInt("INTVALUE2");
				}
				rs2.close();
				String billName = contractName + "-" + name;
				String billDate = year + "-" + month + "-" + "01";
				
				try {
		            c1 = df.parse(sdateValue1);
		            c2 = df.parse(sdateValue2);
		        } catch (Exception e2) {
		            e2.printStackTrace();
		        }
				
				if (c1.getTime() < a.getTime() && c2.getTime() > e.getTime()) {
					System.out.println("生成自然月账单");
					double billAmount = damount;
					//System.out.println("billAmount:"+billAmount);
					boolean result = this.createBill(1,id,billName,billAmount,companyId,year,month,days,userName,createDate,billDate);
					if(result) {
						System.out.println("生成自然月账单成功");
					}
				}
				
				if (c1.getTime() < a.getTime() && c2.getTime() <= e.getTime() && c2.getTime() > a.getTime()) {
					System.out.println("生成在最后一个月到期的合同账单");
					int rentDays = StrEdit.daysBetween(firstDay, sdateValue2) + 1;
					double billAmount = average*rentDays;
					//System.out.println("rentDays:"+rentDays);
					//System.out.println("billAmount:"+billAmount);
					boolean result = this.createBill(1,id,billName,billAmount,companyId,year,month,rentDays,userName,createDate,billDate);
					if(result) {
						System.out.println("生成自然月账单成功");
					}
				}
				
				if (c1.getTime() >= a.getTime() && c1.getTime() <= e.getTime() && c2.getTime() > e.getTime()) {
					System.out.println("生成在该月份开始的合同账单");
					int rentDays = StrEdit.daysBetween(sdateValue1, lastDay) + 1;
					double billAmount = average*rentDays;
					//System.out.println("rentDays:"+rentDays);
					//System.out.println("billAmount:"+billAmount);
					boolean result = this.createBill(1,id,billName,billAmount,companyId,year,month,rentDays,userName,createDate,billDate);
					if(result) {
						System.out.println("生成自然月账单成功");
					}
				}
				
				if (c1.getTime() >= a.getTime() && c2.getTime() <= e.getTime()) {
					System.out.println("生成在这个月份之内的合同账单");
					int rentDays = StrEdit.daysBetween(sdateValue1, sdateValue2) + 1;
					double billAmount = average*rentDays;
					//System.out.println("rentDays:"+rentDays);
					//System.out.println("billAmount:"+billAmount);
					boolean result = this.createBill(1,id,billName,billAmount,companyId,year,month,rentDays,userName,createDate,billDate);
					if(result) {
						System.out.println("生成自然月账单成功");
					}
				}
				
				
			}
			rs.close();
		} catch (java.sql.SQLException e3) {
			e3.printStackTrace();
		} finally {
			mdb.close();
		}
		
		strTemp = "{\"status\":\"1\"}";
		return strTemp;
	}
	
	private boolean createBill(int channelId,int id,String name,Double amount,int companyId,int year,int month,int days,String userName,String createDate,String billDate) {
		boolean boolValue = false;
		int total = 0;
		sql = "select count(*) as total from BILL where ChannelID="+channelId+" and INTVALUE2="+year+" and INTVALUE3="+month+" and INTVALUE4="+id+"";
		ResultSet rs4 = mdb.executeQuery(sql);
		try {
			if(rs4.next()){
				total = rs4.getInt("total");
			}
			rs4.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if(total==0){
			sql = "insert into BILL(ChannelID,VARVALUE1,VARVALUE2,VARVALUE39,VARVALUE40,DECVALUE1,INTVALUE2,INTVALUE3,INTVALUE4,INTVALUE5,ADDTIME,UPDATETIME,DATEVALUE1)";
			sql = sql + "values("+channelId+",'"+name+"','"+days+"','1','"+userName+"','"+amount+"','"+year+"','"+month+"','"+id+"','"+companyId+"','"+createDate+"','"+createDate+"','"+billDate+"')";
			int i = mdb.executeUpdate(sql);
			if(i>0) {
				boolValue = true;
			}
		}
		
		//mdb.close();
		
		return boolValue;
	}
	
	@RequestMapping("/getJsonById")
	@ResponseBody
	public String getJsonById(int id){
		StringBuffer strBuff = new StringBuffer("");
		Bill data = service.findById(id);
		if(data!=null){
			String dateValue1 = StrEdit.dateToStr(data.getDateValue1());
			strBuff.append("{");
			strBuff.append("\"status\": \"1\",");
			strBuff.append("\"id\": \""+data.getConId()+"\",");
			strBuff.append("\"varValue1\": \""+data.getVarValue1()+"\",");
			strBuff.append("\"varValue2\": \""+data.getVarValue2()+"\",");
			strBuff.append("\"varValue3\": \""+data.getVarValue3()+"\",");
			strBuff.append("\"varValue4\": \""+data.getVarValue4()+"\",");
			strBuff.append("\"varValue5\": \""+data.getVarValue5()+"\",");
			strBuff.append("\"varValue6\": \""+data.getVarValue6()+"\",");
			strBuff.append("\"varValue7\": \""+data.getVarValue7()+"\",");
			strBuff.append("\"varValue8\": \""+data.getVarValue8()+"\",");
			strBuff.append("\"varValue9\": \""+data.getVarValue9()+"\",");
			strBuff.append("\"varValue10\": \""+data.getVarValue10()+"\",");
			strBuff.append("\"varValue11\": \""+data.getVarValue11()+"\",");
			strBuff.append("\"varValue12\": \""+data.getVarValue12()+"\",");
			strBuff.append("\"varValue13\": \""+data.getVarValue13()+"\",");
			strBuff.append("\"varValue14\": \""+data.getVarValue14()+"\",");
			strBuff.append("\"varValue15\": \""+data.getVarValue15()+"\",");
			strBuff.append("\"varValue16\": \""+data.getVarValue16()+"\",");
			strBuff.append("\"varValue17\": \""+data.getVarValue17()+"\",");
			strBuff.append("\"varValue18\": \""+data.getVarValue18()+"\",");
			strBuff.append("\"varValue19\": \""+data.getVarValue19()+"\",");
			strBuff.append("\"varValue20\": \""+data.getVarValue20()+"\",");
			strBuff.append("\"varValue21\": \""+data.getVarValue21()+"\",");
			strBuff.append("\"varValue22\": \""+data.getVarValue22()+"\",");
			strBuff.append("\"varValue23\": \""+data.getVarValue23()+"\",");
			strBuff.append("\"varValue24\": \""+data.getVarValue24()+"\",");
			strBuff.append("\"varValue25\": \""+data.getVarValue25()+"\",");
			strBuff.append("\"varValue26\": \""+data.getVarValue26()+"\",");
			strBuff.append("\"decValue1\": \""+data.getDecValue1()+"\",");
			strBuff.append("\"decValue2\": \""+data.getDecValue2()+"\",");
			strBuff.append("\"intValue1\": \""+data.getIntValue1()+"\",");
			strBuff.append("\"intValue2\": \""+data.getIntValue2()+"\",");
			strBuff.append("\"intValue3\": \""+data.getIntValue3()+"\",");
			strBuff.append("\"intValue4\": \""+data.getIntValue4()+"\",");
			strBuff.append("\"intValue5\": \""+data.getIntValue5()+"\",");
			strBuff.append("\"dateValue1\": \""+dateValue1+"\",");
			strBuff.append("\"text1\": \""+data.getText1()+"\"");
			strBuff.append("}");
		}else{
			strBuff.append("{");
			strBuff.append("\"status\": \"0\"");
			strBuff.append("}");
		}
		
		//String strTemp = "";
		//strTemp = "{\"status\":\"1\",\"id\":\""+id+"\",\"varValue1\":\""+data.getVarValue1()+"\"}";
		return strBuff.toString();
	}
	
	@RequestMapping("/get")
	@ResponseBody
	public Map<String, Object> getAll(PageModel<Bill> pageModel, Bill data, HttpServletRequest request, Model model) {
		
		//System.out.println("------------------get------------------");
		String keywords = StrEdit.StrChkNull(data.getKeywords());
		
		Map<String,Object> newMap = new HashMap<String,Object>();
		
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		int classId = StrEdit.StrToInt(request.getParameter("classId"));
		int intValue1 = StrEdit.StrToInt(request.getParameter("intValue1"));
		int intValue2 = StrEdit.StrToInt(request.getParameter("intValue2"));
		int intValue3 = StrEdit.StrToInt(request.getParameter("intValue3"));
		String varValue1 = StrEdit.StrChkNull(request.getParameter("varValue1"));
		String varValue2 = StrEdit.StrChkNull(request.getParameter("varValue2"));
		String varValue3 = StrEdit.StrChkNull(request.getParameter("varValue3"));
		String varValue4 = StrEdit.StrChkNull(request.getParameter("varValue4"));
		String varValue5 = StrEdit.StrChkNull(request.getParameter("varValue5"));
		String varValue6 = StrEdit.StrChkNull(request.getParameter("varValue6"));
		String varValue7 = StrEdit.StrChkNull(request.getParameter("varValue7"));
		String varValue8 = StrEdit.StrChkNull(request.getParameter("varValue8"));
		String varValue9 = StrEdit.StrChkNull(request.getParameter("varValue9"));
		String varValue10 = StrEdit.StrChkNull(request.getParameter("varValue10"));
		String varValue21 = StrEdit.StrChkNull(request.getParameter("varValue21"));
		String varValue23 = StrEdit.StrChkNull(request.getParameter("varValue23"));
		String varValue39 = StrEdit.StrChkNull(request.getParameter("varValue39"));
		String varValue40 = StrEdit.StrChkNull(request.getParameter("varValue40"));
		String startDate = StrEdit.StrChkNull(request.getParameter("startDate"));
		String endDate = StrEdit.StrChkNull(request.getParameter("endDate"));
		if(!startDate.equals("")){
			startDate = startDate + " 00:00:00";
		}
		if(!endDate.equals("")){
			endDate = endDate + " 23:59:59";
		}
		String startDate1 = StrEdit.StrChkNull(request.getParameter("startDate1"));
		String endDate1 = StrEdit.StrChkNull(request.getParameter("endDate1"));
		if(!startDate1.equals("")){
			startDate1 = startDate1 + " 00:00:00";
		}
		if(!endDate1.equals("")){
			endDate1 = endDate1 + " 23:59:59";
		}
		
		int pageNumber = StrEdit.StrToInt(request.getParameter("pageNumber"));
		int pageSize = StrEdit.StrToInt(request.getParameter("pageSize"));
		int startRow = 0;
        if(pageNumber!=0) {
        	startRow = (pageNumber - 1) * pageSize;
        }
        newMap.put("startRow", startRow);
        newMap.put("pageSize", pageSize);
        newMap.put("channelId", channelId);
        newMap.put("classId", classId);
        newMap.put("intValue1", intValue1);
        newMap.put("intValue2", intValue2);
        newMap.put("intValue3", intValue3);
        newMap.put("keywords", keywords);
        newMap.put("varValue1", varValue1);
        newMap.put("varValue2", varValue2);
        newMap.put("varValue3", varValue3);
        newMap.put("varValue4", varValue4);
        newMap.put("varValue5", varValue5);
        newMap.put("varValue6", varValue6);
        newMap.put("varValue7", varValue7);
        newMap.put("varValue8", varValue8);
        newMap.put("varValue9", varValue9);
        newMap.put("varValue10", varValue10);
        newMap.put("varValue21", varValue21);
        newMap.put("varValue23", varValue23);
        newMap.put("varValue39", varValue39);
        newMap.put("varValue40", varValue40);
        newMap.put("startDate", startDate);
        newMap.put("endDate", endDate);
        newMap.put("startDate1", startDate1);
        newMap.put("endDate1", endDate1);
		List<Bill> list = service.pageList(newMap);
		int totalRecords = service.totalRecord(newMap);
		pageModel.setTotal(totalRecords);
		
		//Object jsonObject = JSONObject.toJSON(list);
		//System.out.println("jsonObject:"+jsonObject);
		//System.out.println("totalRecords1:"+totalRecords);
		
		int pn = 1;
		if (pageNumber != 0) {
			pn = new Integer(pageNumber);
		}
		pageModel.setPages(totalRecords, pn);//设置总记录数和当前页面
		Map<String, Object> map = new HashMap<>();
		map.put("page", pageModel);
		map.put("list", list);
		return map;
	}
	
	@RequestMapping("/add")
	public String toAdd(Bill data,HttpServletRequest request,Model model){
		String modelTitle = "", modelName = "", redirect = "";
		Date currTime = new Date();
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		if(channelId==1){
			modelTitle = "账单";
			modelName = "账单增加";
			redirect = "front/bill/contract/add";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelTitle", modelTitle);
		request.setAttribute("modelName", modelName);
		
		List<String> dictionery8 = dictionary.selectDictionaryList(8);
		model.addAttribute("dictionery8", dictionery8);
		
		Map<Long, String> typeMap = new HashMap<Long, String>();
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
        return redirect;
	}
	
	@RequestMapping("/save")
	@ResponseBody
	public String save(Bill data,HttpServletRequest request,HttpServletResponse response){
		int conId = 0;
		String strTemp = "";
		
		int userId = 0;
		String userName = "";
		User sessionUser = ((User)request.getSession().getAttribute(Contants.SESSION_USER));
		if(sessionUser!=null){
			userId = sessionUser.getConId();
			userName = sessionUser.getVarValue1();
		}
        
		try{
			service.save(data);
			conId = data.getConId();
		}catch(Exception e){
			conId = 0;
			System.out.println("save插入数据失败" + e.getLocalizedMessage());
		}
		
		if(conId>0){
			
			String attachIds=StrEdit.StrChkNull(request.getParameter("attachIds"));//附件
			if(!attachIds.equals("")){
				String[] arrAttachIds = attachIds.split("\\,");
				for(int m=0;m<arrAttachIds.length;m++){
					boolean boolValue = attach.update(StrEdit.StrToInt(arrAttachIds[m]),conId);
					if(!boolValue){
						System.out.println("附件信息ID更新失败："+arrAttachIds[m]);
					}
				}
			}
			
			String attachId1002=StrEdit.StrChkNull(request.getParameter("attachId1002"));
			if(!attachId1002.equals("")){
				String[] arrAttachIds = attachId1002.split("\\,");
				for(int m=0;m<arrAttachIds.length;m++){
					boolean boolValue = attach.update(StrEdit.StrToInt(arrAttachIds[m]),conId);
					if(!boolValue){
						System.out.println("附件信息ID更新失败："+arrAttachIds[m]);
					}
				}
			}
			
			strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"添加失败\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/update")
	@ResponseBody
	public String update(Bill data,HttpServletRequest request){
		int conId = data.getConId();
		String strTemp = "";
		
		int userId = 0;
		String userName = "";
		User sessionUser = ((User)request.getSession().getAttribute(Contants.SESSION_USER));
		if(sessionUser!=null){
			userId = sessionUser.getConId();
			userName = sessionUser.getVarValue1();
		}
		
		boolean boolValue = service.update(data);
		if(boolValue){
			
			strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"更新失败\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/getById")
	public String getById(int id,HttpServletRequest request,Model model){
		Map<Long, String> typeMap = new HashMap<Long, String>();
		Bill data = service.findById(id);
		String modelTitle = "", modelName = "", redirect = "";
		int channelId = data.getChannelId();
		if(channelId==1){
			modelTitle = "账单";
			modelName = "账单详情";
			redirect = "front/bill/contract/add";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelTitle", modelTitle);
		request.setAttribute("modelName", modelName);
		
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
        
        return redirect;
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public String delete(int id,HttpServletRequest request){
		String strTemp = "";
		boolean boolValue = service.delete(id);
		if(boolValue){
			strTemp = "{\"status\":\"1\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"删除失败\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/audit")
	@ResponseBody
	public String audit(Bill data,HttpServletRequest request){
		String strTemp = "";
		boolean boolValue = false;
		String task = StrEdit.StrChkNull(request.getParameter("task"));
		Date currTime = new Date();
		
		if(task.equals("audit")){
			int status = StrEdit.StrToInt(request.getParameter("status"));
			String selectids = StrEdit.StrChkNull(request.getParameter("selectids"));
			String[] arrParameter = selectids.split("\\,");//如果不包含,则直接返回原字符的一维数组
	    	if (arrParameter!=null){
				for(int i=0;i<arrParameter.length;i++){
					Map<String,Object> newMap = new HashMap<String,Object>();
					newMap.put("task", task);
					newMap.put("varValue6", status);
					newMap.put("updateTime", currTime);
			        newMap.put("conId", StrEdit.StrToInt(arrParameter[i]));
			        boolValue = service.updateByMap(newMap);
				}
			}
		}
		if(task.equals("auditresult")){
			int status = StrEdit.StrToInt(request.getParameter("status"));
			String reason = StrEdit.StrChkNull(request.getParameter("reason"));
			String memo = StrEdit.StrChkNull(request.getParameter("memo"));
			String audituserid = StrEdit.StrChkNull(request.getParameter("audituserid"));
			String auditname = StrEdit.StrChkNull(request.getParameter("auditname"));
			String auditdate = StrEdit.StrChkNull(request.getParameter("auditdate"));
			String selectids = StrEdit.StrChkNull(request.getParameter("selectids"));
			String[] arrParameter = selectids.split("\\,");//如果不包含,则直接返回原字符的一维数组
	    	if (arrParameter!=null){
				for(int i=0;i<arrParameter.length;i++){
					Map<String,Object> newMap = new HashMap<String,Object>();
					newMap.put("task", task);
					newMap.put("varValue6", status);
					newMap.put("varValue7", reason);
					newMap.put("varValue8", memo);
					newMap.put("varValue12", audituserid);
					newMap.put("varValue13", auditname);
					newMap.put("updateTime", currTime);
					newMap.put("dateValue3", auditdate);
			        newMap.put("conId", StrEdit.StrToInt(arrParameter[i]));
			        boolValue = service.updateByMap(newMap);
				}
			}
		}
		if(task.equals("auditpay")){
			int status = StrEdit.StrToInt(request.getParameter("status"));
			String payType = StrEdit.StrChkNull(request.getParameter("payType"));
			String id = StrEdit.StrChkNull(request.getParameter("id"));
			Map<String,Object> newMap = new HashMap<String,Object>();
			newMap.put("task", task);
			newMap.put("varValue3", payType);
			newMap.put("varValue39", status);
			newMap.put("updateTime", currTime);
	        newMap.put("conId", id);
	        boolValue = service.updateByMap(newMap);
		}
		if(boolValue){
			strTemp = "{\"status\":\"1\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"更新失败\"}";
		}
		
		return strTemp;
	}
	
}
