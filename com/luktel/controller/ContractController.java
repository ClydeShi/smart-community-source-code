package com.luktel.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.luktel.model.Attachment;
import com.luktel.model.Contract;
import com.luktel.model.ContractCharge;
import com.luktel.model.User;
import com.luktel.service.AttachmentService;
import com.luktel.service.ContractChargeService;
import com.luktel.service.ContractService;
import com.luktel.service.DictionaryService;
import com.util.common.Contants;
import com.util.common.PageModel;
import com.util.common.StrEdit;

/*
 * @Controller是标记在类UserController上面的，所以类UserController就是一个SpringMVC Controller对象了
 * 使用@RequestMapping("/getAllUser")标记在Controller方法上，表示当请求/getAllUser的时候访问的是LoginController的getAllUser方法，
 * 该方法返回了一个包括Model和View的ModelAndView对象113323
 */

@Controller//@Controller注解标识一个控制器
@RequestMapping("/contract")//如果@RequestMapping注解在类级别上，则表示相对路径，在方法级别上，则标记访问的路径
public class ContractController{
	
	@Autowired
	private ContractService service;
	
	@Autowired
	private ContractChargeService contractservice;
	
	@Autowired
	private AttachmentService attach;
	
	@Autowired
	private DictionaryService dictionary;
	
	@RequestMapping("/list")
	public String list() {
		return "front/contract/list";
	}
	
	@RequestMapping("/getJsonById")
	@ResponseBody
	public String getJsonById(int id){
		String strTemp = "";
		Contract data = service.findById(id);
		strTemp = "{\"status\":\"1\",\"id\":\""+id+"\",\"name\":\""+data.getVarValue1()+"\"}";
		return strTemp;
	}
	
	@RequestMapping("/get")
	@ResponseBody
	public Map<String, Object> getAll(PageModel<Contract> pageModel, Contract data, HttpServletRequest request, Model model) {
		
		//System.out.println("------------------get------------------");
		String keywords = StrEdit.StrChkNull(data.getKeywords());
		
		Map<String,Object> newMap = new HashMap<String,Object>();
		
		String varValue1 = StrEdit.StrChkNull(request.getParameter("varValue1"));
		String varValue2 = StrEdit.StrChkNull(request.getParameter("varValue2"));
		String varValue4 = StrEdit.StrChkNull(request.getParameter("varValue4"));
		String varValue8 = StrEdit.StrChkNull(request.getParameter("varValue8"));
		
		int pageNumber = StrEdit.StrToInt(request.getParameter("pageNumber"));
		int pageSize = StrEdit.StrToInt(request.getParameter("pageSize"));
		int startRow = 0;
        if(pageNumber!=0) {
        	startRow = (pageNumber - 1) * pageSize;
        }
        newMap.put("startRow", startRow);
        newMap.put("pageSize", pageSize);
		/*int pageStart = (pageNumber -1) * pageSize+1;//oracle分页
        int pageEnd = pageStart + pageSize -1;
        newMap.put("pageStart", pageStart);
        newMap.put("pageEnd", pageEnd);*/
        newMap.put("keywords", keywords);
        newMap.put("varValue1", varValue1);
        newMap.put("varValue2", varValue2);
        newMap.put("varValue4", varValue4);
        newMap.put("varValue8", varValue8);
		List<Contract> list = service.pageList(newMap);
		int totalRecords = service.totalRecord(newMap);
		pageModel.setTotal(totalRecords);
		
		//Object jsonObject = JSONObject.toJSON(list);
		//System.out.println("jsonObject:"+jsonObject);
		//System.out.println("totalRecords1:"+totalRecords);
		
		int pn = 1;
		if (pageNumber != 0) {
			pn = new Integer(pageNumber);
		}
		pageModel.setPages(totalRecords, pn);//设置总记录数和当前页面
		Map<String, Object> map = new HashMap<>();
		map.put("page", pageModel);
		map.put("list", list);
		return map;
	}
	
	@RequestMapping("/add")
	public String toAdd(Contract data,HttpServletRequest request,Model model){
		/*int channelID = StrEdit.StrToInt(request.getParameter("channelID"));
		int parentId = StrEdit.StrToInt(request.getParameter("parentId"));
		
		String parentName = "";
		if(parentId>0){
			Map<String,Object> newMap1 = new HashMap<String,Object>();
			newMap1.put("task", "className");
	        newMap1.put("conId", parentId);
	        parentName = service.getStringValue(newMap1);
		}
        
		request.setAttribute("channelID", channelID);
		request.setAttribute("parentId", parentId);
		request.setAttribute("parentName", parentName);
		
		data.setVarValue7("无");*/
		
		List<String> dictionery2 = dictionary.selectDictionaryList(2);
		List<String> dictionery9 = dictionary.selectDictionaryList(9);
		model.addAttribute("dictionery2", dictionery2);
		model.addAttribute("dictionery9", dictionery9);
		
		Map<Long, String> typeMap = new HashMap<Long, String>();
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
		return "/front/contract/add";
	}
	
	@RequestMapping("/save")
	@ResponseBody
	public String save(Contract data,HttpServletRequest request,HttpServletResponse response){
		int conId = 0;
		String strTemp = "";
		
		int userId = 0;
		String userName = "";
		User sessionUser = ((User)request.getSession().getAttribute(Contants.SESSION_USER));
		if(sessionUser!=null){
			userId = sessionUser.getConId();
			userName = sessionUser.getVarValue1();
		}
		
		try{
			service.save(data);
			conId = data.getConId();
		}catch(Exception e){
			conId = 0;
			System.out.println("save插入数据失败" + e.getLocalizedMessage());
		}
		
		if(conId>0){
			
			String field_scope=StrEdit.StrChkNull(request.getParameter("field_scope"));
			String field_scope1=StrEdit.StrChkNull(request.getParameter("field_scope1"));
			String field_scope2=StrEdit.StrChkNull(request.getParameter("field_scope2"));
			String field_scope3=StrEdit.StrChkNull(request.getParameter("field_scope3"));
			String field_scope4=StrEdit.StrChkNull(request.getParameter("field_scope4"));
			String field_scope5=StrEdit.StrChkNull(request.getParameter("field_scope5"));
			String field_scope6=StrEdit.StrChkNull(request.getParameter("field_scope6"));
			String field_scope7=StrEdit.StrChkNull(request.getParameter("field_scope7"));
			String field_scope8=StrEdit.StrChkNull(request.getParameter("field_scope8"));
			this.updateDetail(conId,userName,field_scope,field_scope1,field_scope2,field_scope3,field_scope4,field_scope5,field_scope6,field_scope7,field_scope8);
			
			String attachIds=StrEdit.StrChkNull(request.getParameter("attachIds"));//附件
			if(!attachIds.equals("")){
				String[] arrAttachIds = attachIds.split("\\,");
				for(int m=0;m<arrAttachIds.length;m++){
					boolean boolValue = attach.update(StrEdit.StrToInt(arrAttachIds[m]),conId);
					if(!boolValue){
						System.out.println("附件信息ID更新失败："+arrAttachIds[m]);
					}
				}
			}
			
			strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"添加失败\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/update")
	@ResponseBody
	public String update(Contract data,HttpServletRequest request){
		int conId = data.getConId();
		String strTemp = "";
		
		int userId = 0;
		String userName = "";
		User sessionUser = ((User)request.getSession().getAttribute(Contants.SESSION_USER));
		if(sessionUser!=null){
			userId = sessionUser.getConId();
			userName = sessionUser.getVarValue1();
		}
		
		boolean boolValue = service.update(data);
		if(boolValue){
			
			String field_scope=StrEdit.StrChkNull(request.getParameter("field_scope"));
			String field_scope1=StrEdit.StrChkNull(request.getParameter("field_scope1"));
			String field_scope2=StrEdit.StrChkNull(request.getParameter("field_scope2"));
			String field_scope3=StrEdit.StrChkNull(request.getParameter("field_scope3"));
			String field_scope4=StrEdit.StrChkNull(request.getParameter("field_scope4"));
			String field_scope5=StrEdit.StrChkNull(request.getParameter("field_scope5"));
			String field_scope6=StrEdit.StrChkNull(request.getParameter("field_scope6"));
			String field_scope7=StrEdit.StrChkNull(request.getParameter("field_scope7"));
			String field_scope8=StrEdit.StrChkNull(request.getParameter("field_scope8"));
			this.updateDetail(conId,userName,field_scope,field_scope1,field_scope2,field_scope3,field_scope4,field_scope5,field_scope6,field_scope7,field_scope8);
			
			strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"更新失败\"}";
		}
		
		return strTemp;
	}
	
	public String updateDetail(int id,String userName,String field_scope,String field_scope1,String field_scope2,String field_scope3,String field_scope4,String field_scope5,String field_scope6,String field_scope7,String field_scope8){
		
		String strTemp = "";
		Date currTime = new Date();
		
		if(field_scope.equals("")){
			field_scope = "0";
		}
		Map<String,Object> newMap1 = new HashMap<String,Object>();
		newMap1.put("task", "deleteByDetailIds");
		newMap1.put("intValue2", id);
		newMap1.put("detailIds", field_scope);
		contractservice.deleteByMap(newMap1);//删除已经去掉的id
		
		if(!field_scope1.equals("")){
			String[] arrField = field_scope.split("\\,");
			String[] arrField1 = field_scope1.split("\\,");
			String[] arrField2 = field_scope2.split("\\,");
			String[] arrField3 = field_scope3.split("\\,");
			String[] arrField4 = field_scope4.split("\\,");
			String[] arrField5 = field_scope5.split("\\,");
			String[] arrField6 = field_scope6.split("\\,");
			String[] arrField7 = field_scope7.split("\\,");
			String[] arrField8 = field_scope8.split("\\,");
			for(int m=0;m<arrField1.length;m++){
				int detailId = StrEdit.StrToInt(arrField[m]);
				String strValue1 = arrField1[m];
				String strValue2 = arrField2[m];
				String strValue3 = arrField3[m];
				String strValue4 = arrField4[m];
				String strValue5 = arrField5[m];
				String strValue6 = arrField6[m];
				String strValue7 = arrField7[m];
				String strValue8 = arrField8[m];
				
				if(strValue1.equals("NOINPUT")){
					strValue1 = "";
				}
				if(strValue2.equals("NOVALUE")){
					strValue2 = "";
				}
				if(strValue3.equals("NOVALUE")){
					strValue3 = "";
				}
				if(strValue4.equals("NOVALUE")){//数字字段
					strValue4 = "0";
				}
				if(strValue5.equals("NOVALUE")){
					strValue5 = "0";
				}
				if(strValue6.equals("NOVALUE")){
					strValue6 = "0";
				}
				if(strValue7.equals("NOVALUE")){
					strValue7 = "";
				}
				if(strValue8.equals("NOVALUE")){
					strValue8 = "";
				}System.out.println("detailId"+detailId);
				
				if(detailId>0){//更新
					ContractCharge data1 = new ContractCharge();
					data1.setConId(detailId);
					data1.setVarValue1(strValue1);
					data1.setVarValue2("");
					data1.setVarValue3(strValue8);
					data1.setVarValue4("");
					data1.setVarValue40(userName);
					data1.setDecValue1(strValue4);
					data1.setDecValue2(strValue5);
					data1.setDecValue3(strValue6);
					data1.setIntValue1(m+1);
					data1.setIntValue2(id);
					data1.setIntValue3(0);
					data1.setIntValue4(0);
					data1.setText1(strValue7);
					data1.setAddTime(currTime);
					data1.setUpdateTime(currTime);
					data1.setDateValue1(StrEdit.strToDate(strValue2));
					data1.setDateValue2(StrEdit.strToDate(strValue3));
					contractservice.update(data1);
				}else{//新增
					ContractCharge data1 = new ContractCharge();
					if(!strValue1.equals("") || !strValue8.equals("")){
						data1.setVarValue1(strValue1);
						data1.setVarValue2("");
						data1.setVarValue3(strValue8);
						data1.setVarValue4("");
						data1.setVarValue40(userName);
						data1.setDecValue1(strValue4);
						data1.setDecValue2(strValue5);
						data1.setDecValue3(strValue6);
						data1.setIntValue1(m+1);
						data1.setIntValue2(id);
						data1.setIntValue3(0);
						data1.setIntValue4(0);
						data1.setText1(strValue7);
						data1.setAddTime(currTime);
						data1.setUpdateTime(currTime);
						data1.setDateValue1(StrEdit.strToDate(strValue2));
						data1.setDateValue2(StrEdit.strToDate(strValue3));
						contractservice.save(data1);
						int conId = data1.getConId();
					}
				}
				
			}
		}
		
		return strTemp;
		
	}
	
	@RequestMapping("/getById")
	public String getById(int id,HttpServletRequest request,Model model){
		Map<Long, String> typeMap = new HashMap<Long, String>();
		
		Contract data = service.findById(id);
		
		List<String> dictionery2 = dictionary.selectDictionaryList(2);
		List<String> dictionery9 = dictionary.selectDictionaryList(9);
		model.addAttribute("dictionery2", dictionery2);
		model.addAttribute("dictionery9", dictionery9);
		
		Map<String,Object> newMap1 = new HashMap<String,Object>();
		newMap1.put("task", "getDetailByIntValue2");
        newMap1.put("intValue2", id);
        List<ContractCharge> list1 = contractservice.getListByMap(newMap1);
        request.setAttribute("list1", list1);
        request.setAttribute("roomnumber", list1.size());
        
        List<Attachment> attachList1001 = attach.list(1001,id);
        
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
        model.addAttribute("attachList1001", attachList1001);
        
		return "/front/contract/add";
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public String delete(int id,HttpServletRequest request){
		String strTemp = "";
		boolean boolValue = service.delete(id);
		if(boolValue){
			
			Map<String,Object> newMap1 = new HashMap<String,Object>();
			newMap1.put("task", "deleteByContractId");
	        newMap1.put("intValue2", id);
	        contractservice.deleteByMap(newMap1);
	        
			strTemp = "{\"status\":\"1\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"删除失败\"}";
		}
		
		return strTemp;
	}
	
}
