package com.luktel.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.luktel.model.Dictionary;
import com.luktel.model.Attachment;
import com.luktel.service.DictionaryService;
import com.luktel.service.AttachmentService;
import com.util.common.PageModel;
import com.util.common.StrEdit;

/*
 * @Controller是标记在类UserController上面的，所以类UserController就是一个SpringMVC Controller对象了
 * 使用@RequestMapping("/getAllUser")标记在Controller方法上，表示当请求/getAllUser的时候访问的是LoginController的getAllUser方法，
 * 该方法返回了一个包括Model和View的ModelAndView对象113323
 */

@Controller//@Controller注解标识一个控制器
@RequestMapping("/dictionary")//如果@RequestMapping注解在类级别上，则表示相对路径，在方法级别上，则标记访问的路径
public class DictionaryController{
	
	@Autowired
	private DictionaryService service;
	
	@Autowired
	private AttachmentService attach;
	
	@RequestMapping("/index")
	public String index() {
		return "front/dictionary/index";
	}
	
	@RequestMapping("/list")
	public String list(HttpServletRequest request) {
		String modelTitle = "数据字典";
		int typeId = StrEdit.StrToInt(request.getParameter("typeId"));
		if(typeId==1) {
			modelTitle = "岗位类别";
		}else if(typeId==2) {
			modelTitle = "合同类型";
		}else if(typeId==3) {
			modelTitle = "会议室收费标准";
		}else if(typeId==4) {
			modelTitle = "退回原因";
		}else if(typeId==5) {
			modelTitle = "装修类型";
		}else if(typeId==6) {
			modelTitle = "收费标准";
		}else if(typeId==7) {
			modelTitle = "来源";
		}else if(typeId==8) {
			modelTitle = "通道等级";
		}else if(typeId==9) {
			modelTitle = "支付方式";
		}else if(typeId==10) {
			modelTitle = "合同收费标准";
		}else if(typeId==11) {
			modelTitle = "故障类型";
		}else if(typeId==12) {
			modelTitle = "产权";
		}else if(typeId==13) {
			modelTitle = "供应商";
		}
		request.setAttribute("typeId", typeId);
		request.setAttribute("modelTitle", modelTitle);
		return "front/dictionary/list";
	}
	
	@RequestMapping("/treeselect")
	public String treecheck(HttpServletRequest request) {
		int type = StrEdit.StrToInt(request.getParameter("type"));
		int typeId = StrEdit.StrToInt(request.getParameter("typeId"));
		String selectIds = StrEdit.StrChkNull(request.getParameter("selectIds"));
		request.setAttribute("type", type);
		request.setAttribute("typeId", typeId);
		request.setAttribute("selectIds", selectIds);
		return "front/dictionary/treeselect";
	}
	
	@RequestMapping("/treeData")
	@ResponseBody
	public String treeData(HttpServletRequest request) {
		StringBuffer strBuff = new StringBuffer("");
		strBuff.append("[");
		
		String name = "";
		int typeId = StrEdit.StrToInt(request.getParameter("typeId"));
		int type = StrEdit.StrToInt(request.getParameter("type"));
		if(typeId==1){
			name = "岗位";
		}else if(typeId==10){
			name = "合同收费标准";
		}
		
		strBuff.append("{");
    	if(type==3 || type==4){//多选才显示isParent，才会加载treeDataSelect信息
    		strBuff.append("\"isParent\": \"true\",");
    	}
		strBuff.append("\"id\": \"1\",");
		strBuff.append("\"pId\": \"0\",");
		strBuff.append("\"name\": \""+name+"\"");
		//strBuff.append("\"open\": \"true\"");
		strBuff.append("}");
		
        strBuff.append("]");
		
		return strBuff.toString();
	}
	
	@RequestMapping("/treeDataSelect")
	@ResponseBody
	public String treeDataPerson(HttpServletRequest request) {
		StringBuffer strBuff = new StringBuffer("");
		strBuff.append("[");
		
		int typeId = StrEdit.StrToInt(request.getParameter("typeId"));
		
		List<Dictionary> list1 = new ArrayList<Dictionary>();
		Map<String,Object> newMap1 = new HashMap<String,Object>();
		newMap1.put("task", "getDictionaryByTypeId");
		newMap1.put("typeId", typeId);
        list1 = service.getListByMap(newMap1);
        for(int m=0; m<list1.size(); m++) {
        	if(m>0){strBuff.append(",");}
        	Dictionary temp = list1.get(m);
        	strBuff.append("{");
			strBuff.append("\"id\": \"u_"+temp.getConId()+"\",");
			strBuff.append("\"pId\": \""+temp.getIntValue2()+"\",");
			strBuff.append("\"name\": \""+temp.getVarValue1()+"\",");
			strBuff.append("\"price\": \""+temp.getVarValue3()+"\"");
			strBuff.append("}");
        }
        strBuff.append("]");
		//System.out.println("list:" + strBuff.toString());
		
		return strBuff.toString();
	}
	
	@RequestMapping("/menu")
	public String projectMenu(Model model){
		Map<Long, String> typeMap = new HashMap<Long, String>();
		typeMap.put(Long.valueOf("1"), "岗位类别");
		typeMap.put(Long.valueOf("2"), "合同类型");
		typeMap.put(Long.valueOf("3"), "会议室收费标准");
		typeMap.put(Long.valueOf("4"), "退回原因");
		typeMap.put(Long.valueOf("5"), "装修类型");
		typeMap.put(Long.valueOf("6"), "收费标准");
		typeMap.put(Long.valueOf("7"), "来源");
		typeMap.put(Long.valueOf("8"), "通道等级");
		typeMap.put(Long.valueOf("9"), "支付方式");
		typeMap.put(Long.valueOf("10"), "合同收费标准");
		typeMap.put(Long.valueOf("11"), "故障类型");
		typeMap.put(Long.valueOf("12"), "产权");
		typeMap.put(Long.valueOf("13"), "供应商");
        model.addAttribute("typeMap", typeMap);
		return "/front/dictionary/menu";
	}
	
	@RequestMapping("/get")
	@ResponseBody
	public Map<String, Object> getAll(PageModel<Dictionary> pageModel, Dictionary data, HttpServletRequest request, Model model) {
		
		//System.out.println("------------------get------------------");
		String keywords = StrEdit.StrChkNull(data.getKeywords());
		
		Map<String,Object> newMap = new HashMap<String,Object>();
		
		int pageNumber = StrEdit.StrToInt(request.getParameter("pageNumber"));
		int pageSize = StrEdit.StrToInt(request.getParameter("pageSize"));
		int typeId = StrEdit.StrToInt(request.getParameter("typeId"));
		//System.out.println("pageSize:"+pageSize);
		//System.out.println("pageSize:"+pageModel.getPageSize());
		int startRow = 0;
        if(pageNumber!=0) {
        	startRow = (pageNumber - 1) * pageSize;
        }
        newMap.put("startRow", startRow);
        newMap.put("pageSize", pageSize);
        newMap.put("typeId", typeId);
        newMap.put("keywords", keywords);
		List<Dictionary> list = service.pageList(newMap);
		int totalRecords = service.totalRecord(newMap);
		pageModel.setTotal(totalRecords);
		
		//Object jsonObject = JSONObject.toJSON(list);
		//System.out.println("jsonObject:"+jsonObject);
		//System.out.println("totalRecords1:"+totalRecords);
		
		int pn = 1;
		if (pageNumber != 0) {
			pn = new Integer(pageNumber);
		}
		pageModel.setPages(totalRecords, pn);//设置总记录数和当前页面
		Map<String, Object> map = new HashMap<>();
		map.put("page", pageModel);
		map.put("list", list);
		return map;
	}
	
	@RequestMapping("/add")
	public String toAdd(Dictionary data,HttpServletRequest request,Model model){
		String modelTitle = "数据字典";
		Map<Long, String> typeMap = new HashMap<Long, String>();
		/*List<Map<String,Object>> typeMaps = archive.selectArchiveTypeMapList();
        for (Map<String, Object> map : typeMaps) {
        	Long ids = null;
            String name = null;
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                if ("CONID".equals(entry.getKey())) {
                	ids = ((java.math.BigDecimal) entry.getValue()).longValue();
                } else if ("VARVALUE2".equals(entry.getKey())) {
                	name = (String) entry.getValue();
                }
            }
            typeMap.put(ids, name);
        }*/
		int typeId = StrEdit.StrToInt(request.getParameter("typeId"));
		if(typeId==1) {
			modelTitle = "岗位类别";
		}else if(typeId==2) {
			modelTitle = "合同类型";
		}else if(typeId==3) {
			modelTitle = "会议室收费标准";
		}else if(typeId==4) {
			modelTitle = "退回原因";
		}else if(typeId==5) {
			modelTitle = "装修类型";
		}else if(typeId==6) {
			modelTitle = "收费标准";
		}else if(typeId==7) {
			modelTitle = "来源";
		}else if(typeId==8) {
			modelTitle = "通道等级";
		}else if(typeId==9) {
			modelTitle = "支付方式";
		}else if(typeId==10) {
			modelTitle = "合同收费标准";
		}else if(typeId==11) {
			modelTitle = "故障类型";
		}else if(typeId==12) {
			modelTitle = "产权";
		}else if(typeId==13) {
			modelTitle = "供应商";
		}
		request.setAttribute("typeId", typeId);
		request.setAttribute("modelTitle", modelTitle);
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
		return "/front/dictionary/add";
	}
	
	@RequestMapping("/add2")
	public String add(HttpServletRequest request){
		//int typeId = StrEdit.StrToInt(request.getParameter("typeId"));
		return "/front/dictionary/add";
	}
	
	@RequestMapping("/save")
	@ResponseBody
	public String save(Dictionary data,HttpServletRequest request,HttpServletResponse response){
		int conId = 0;
		String strTemp = "";
		try{
			service.save(data);
			conId = data.getConId();
		}catch(Exception e){
			conId = 0;
			System.out.println("save插入数据失败" + e.getLocalizedMessage());
		}
		
		if(conId>0){
			
			String attachIds=StrEdit.StrChkNull(request.getParameter("attachIds"));//附件
			if(!attachIds.equals("")){
				String[] arrAttachIds = attachIds.split("\\,");
				for(int m=0;m<arrAttachIds.length;m++){
					boolean boolValue = attach.update(StrEdit.StrToInt(arrAttachIds[m]),conId);
					if(!boolValue){
						System.out.println("附件信息ID更新失败："+arrAttachIds[m]);
					}
				}
			}
			
			strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"添加失败\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/update")
	@ResponseBody
	public String update(Dictionary data,HttpServletRequest request){
		int conId = data.getConId();
		String strTemp = "";
		//Date currTime = new Date();
		//data.setUpdateTime(currTime);
		boolean boolValue = service.update(data);
		if(boolValue){
			strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"更新失败\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/getById")
	public String getById(int id,HttpServletRequest request,Model model){
		String modelTitle = "数据字典";
		Map<Long, String> typeMap = new HashMap<Long, String>();
        List<Attachment> attachList = attach.list(181,id);
        Dictionary data = service.findById(id);
        int typeId = data.getIntValue2();
		if(typeId==1) {
			modelTitle = "岗位类别";
		}else if(typeId==2) {
			modelTitle = "合同类型";
		}else if(typeId==3) {
			modelTitle = "会议室收费标准";
		}else if(typeId==4) {
			modelTitle = "退回原因";
		}else if(typeId==5) {
			modelTitle = "装修类型";
		}else if(typeId==6) {
			modelTitle = "收费标准";
		}else if(typeId==7) {
			modelTitle = "来源";
		}else if(typeId==8) {
			modelTitle = "通道等级";
		}else if(typeId==9) {
			modelTitle = "支付方式";
		}else if(typeId==10) {
			modelTitle = "合同收费标准";
		}else if(typeId==11) {
			modelTitle = "故障类型";
		}else if(typeId==12) {
			modelTitle = "产权";
		}else if(typeId==13) {
			modelTitle = "供应商";
		}
		request.setAttribute("typeId", typeId);
		request.setAttribute("modelTitle", modelTitle);
        
        request.setAttribute("typeId", typeId);
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
        model.addAttribute("attachList", attachList);
        
		return "/front/dictionary/add";
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public String delete(int id,HttpServletRequest request){
		String strTemp = "";
		boolean boolValue = service.delete(id);
		if(boolValue){
			strTemp = "{\"status\":\"1\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"删除失败\"}";
		}
		
		return strTemp;
	}
	
}
