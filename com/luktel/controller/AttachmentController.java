package com.luktel.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.luktel.model.Attachment;
import com.luktel.model.User;
import com.luktel.service.AttachmentService;
import com.util.common.FileEdit;
import com.util.common.PageModel;
import com.util.common.StrEdit;

/*
 * @Controller是标记在类UserController上面的，所以类UserController就是一个SpringMVC Controller对象了
 * 使用@RequestMapping("/getAllUser")标记在Controller方法上，表示当请求/getAllUser的时候访问的是LoginController的getAllUser方法，
 * 该方法返回了一个包括Model和View的ModelAndView对象113323
 */

@Controller//@Controller注解标识一个控制器
@RequestMapping("/attachment")//如果@RequestMapping注解在类级别上，则表示相对路径，在方法级别上，则标记访问的路径
public class AttachmentController{
	
	@Autowired
	private AttachmentService service;
	
	/*
	 * 在使用@RequestMapping后，返回值通常解析为跳转路径
	 */
	@RequestMapping("/list")
	public String list() {
		//return "userList";//访问userList.jsp
		return "front/attachment/list";
	}
	
	@RequestMapping("/get")
	@ResponseBody
	public Map<String, Object> getAll(PageModel<Attachment> pageModel, Attachment data, HttpServletRequest request, Model model) {
		
		//System.out.println("------------------get------------------");
		String keywords = StrEdit.StrChkNull(data.getKeywords());
		
		Map<String,Object> newMap = new HashMap<String,Object>();
		
		int pageNumber = StrEdit.StrToInt(request.getParameter("pageNumber"));
		int pageSize = StrEdit.StrToInt(request.getParameter("pageSize"));//pageSize传递过来的时候就已经赋值给PageModel了
		//System.out.println("pageSize:"+pageSize);
		//System.out.println("pageSize:"+pageModel.getPageSize());
		int startRow = 0;
        if(pageNumber!=0) {
        	startRow = (pageNumber - 1) * pageSize;
        }
        newMap.put("startRow", startRow);
        newMap.put("pageSize", pageSize);
        newMap.put("keywords", keywords);
		List<Attachment> list = service.pageList(newMap);
		int totalRecords = service.totalRecord(newMap);
		pageModel.setTotal(totalRecords);
		
		Object jsonObject = JSONObject.toJSON(list);
		//System.out.println("jsonObject:"+jsonObject);
		//System.out.println("totalRecords1:"+totalRecords);
		
		int pn = 1;
		if (pageNumber != 0) {
			pn = new Integer(pageNumber);
		}
		pageModel.setPages(totalRecords, pn);//设置总记录数和当前页面
		Map<String, Object> map = new HashMap<>();
		map.put("page", pageModel);
		map.put("list", list);
		return map;
	}
	
	/*
	 * 添加附件数据
	 * mybatis insert、update 、delete默认返回值解释与如何设置返回表主键 https://www.cnblogs.com/xuanaiwu/articles/6036682.html
	 * https://segmentfault.com/q/1010000002480348/a-1020000002480446
	 * http://blog.csdn.net/qq877507054/article/details/52725892
	 */
	@RequestMapping("/save")
	@ResponseBody
	public String save(Attachment data,HttpServletRequest request,HttpServletResponse response){
		int newId = 0;
		String strTemp = "";
		
		int infoId = StrEdit.StrToInt(request.getParameter("infoId"));
		int ChannelID = StrEdit.StrToInt(request.getParameter("ChannelID"));
		String fileName=StrEdit.StrChkNull(request.getParameter("fileName"));
		String filePath=StrEdit.StrChkNull(request.getParameter("filePath"));
		String fileType=StrEdit.StrChkNull(request.getParameter("fileType"));
		String userName = "";
		User user = ((User)request.getSession().getAttribute("sessionUser"));
		if(user!=null){
			userName = user.getVarValue1();
		}
		data.setVarValue1(String.valueOf(ChannelID));
		data.setVarValue2(String.valueOf(infoId));
		data.setVarValue3(fileName);
		data.setVarValue4(filePath);
		data.setVarValue5(fileType);
		data.setVarValue6("");
		data.setVarValue7("");
		data.setVarValue8(userName);
		data.setAddTime(new Date());
		
		try{
			service.save(data);
			newId = data.getConId();
		}catch(Exception e){
			newId = 0;
			System.out.println("save上传失败" + e.getLocalizedMessage());
		}
		
		if(newId>0){
			strTemp = "{\"status\":\"1\",\"newId\":\""+newId+"\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"上传失败\"}";
		}
		
		return strTemp;
	}
	
	/*
	 * 添加图片数据
	 */
	@RequestMapping("/savePicture")
	@ResponseBody
	public String savePicture(Attachment data,HttpServletRequest request,HttpServletResponse response){
		int newId = 0;
		String strTemp = "";
		
		int infoId = StrEdit.StrToInt(request.getParameter("infoId"));
		int ChannelID = StrEdit.StrToInt(request.getParameter("ChannelID"));
		String fileName=StrEdit.StrChkNull(request.getParameter("fileName"));
		String filePath=StrEdit.StrChkNull(request.getParameter("filePath"));
		String fileType=StrEdit.StrChkNull(request.getParameter("fileType"));
		String userName = "";
		User user = ((User)request.getSession().getAttribute("sessionUser"));
		if(user!=null){
			userName = user.getVarValue1();
		}
		data.setVarValue1(String.valueOf(ChannelID));
		data.setVarValue2(String.valueOf(infoId));
		data.setVarValue3(fileName);
		data.setVarValue4(filePath);
		data.setVarValue5(fileType);
		data.setVarValue6("");
		data.setVarValue7("");
		data.setVarValue8(userName);
		data.setAddTime(new Date());
		
		try{
			service.save(data);
			newId = data.getConId();
		}catch(Exception e){
			newId = 0;
			System.out.println("save上传失败" + e.getLocalizedMessage());
		}
		
		if(newId>0){
			strTemp = "{\"status\":\"1\",\"newId\":\""+newId+"\",\"newFilePath\":\""+filePath+"\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"上传失败\"}";
		}
		
		return strTemp;
	}
	
	/*
	 * 删除数据，通过id和路径
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public String delete(int media_id,HttpServletRequest request){//这里的参数media_id一定要传递过，之前用id就无效
		String strTemp = "";
		
		int attachId = StrEdit.StrToInt(request.getParameter("media_id"));
		String filePath = StrEdit.StrChkNullbasic(request.getParameter("filePath"));
		
		FileEdit del = new FileEdit();
		String sPath = request.getSession().getServletContext().getRealPath("/");
		
		boolean boolValue = service.delete(attachId);
		if(boolValue){
			del.deleteFile1(sPath + filePath);
			strTemp = "{\"status\":\"1\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"删除数据失败\"}";
		}
		
		return strTemp;
	}
	
	/*
	 * 删除数据，通过路径
	 */
	@RequestMapping("/deleteByFile")
	@ResponseBody
	public String deleteByFile(HttpServletRequest request){
		String strTemp = "";
		
		String filePath = StrEdit.StrChkNullbasic(request.getParameter("filePath"));
		if(filePath.equals("")){
			strTemp = "{\"status\":\"0\",\"errors\":\"文件不存在\"}";
		}
		
		FileEdit del = new FileEdit();
		String sPath = request.getSession().getServletContext().getRealPath("/");
		
		boolean boolValue = service.deleteByFile(filePath);
		if(boolValue){
			del.deleteFile1(sPath + filePath);
			strTemp = "{\"status\":\"1\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"删除数据失败\"}";
		}
		
		return strTemp;
	}
	
}
