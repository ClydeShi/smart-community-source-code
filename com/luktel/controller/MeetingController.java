package com.luktel.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.luktel.model.Attachment;
import com.luktel.model.Meeting;
import com.luktel.model.MeetingRoom;
import com.luktel.model.Storage;
import com.luktel.model.User;
import com.luktel.service.AttachmentService;
import com.luktel.service.BuildingService;
import com.luktel.service.MeetingService;
import com.luktel.service.StorageService;
import com.luktel.service.DictionaryService;
import com.luktel.service.MeetingRoomService;
import com.util.common.Contants;
import com.util.common.PageModel;
import com.util.common.StrEdit;

/*
 * @Controller是标记在类UserController上面的，所以类UserController就是一个SpringMVC Controller对象了
 * 使用@RequestMapping("/getAllUser")标记在Controller方法上，表示当请求/getAllUser的时候访问的是LoginController的getAllUser方法，
 * 该方法返回了一个包括Model和View的ModelAndView对象113323
 */

@Controller//@Controller注解标识一个控制器
@RequestMapping("/meeting")//如果@RequestMapping注解在类级别上，则表示相对路径，在方法级别上，则标记访问的路径
public class MeetingController{
	
	@Autowired
	private MeetingService service;
	
	@Autowired
	private MeetingRoomService meetingroom;
	
	@Autowired
	private StorageService storage;
	
	@Autowired
	private BuildingService building;
	
	@Autowired
	private AttachmentService attach;
	
	@Autowired
	private DictionaryService dictionary;
	
	@RequestMapping("/list")
	public String list(HttpServletRequest request) {
		String modelName = "", redirect = "";
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		if(channelId==1){
			modelName = "会议室";
			redirect = "front/meeting/list";
		}else if(channelId==2){
			modelName = "场地";
			redirect = "front/meeting/list";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelName", modelName);
		return redirect;
	}
	
	@RequestMapping("/auditlist")
	public String auditlist(HttpServletRequest request) {
		String modelName = "", redirect = "";
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		if(channelId==1){
			modelName = "会议室";
			redirect = "front/meeting/auditlist";
		}else if(channelId==2){
			modelName = "场地";
			redirect = "front/meeting/auditlist";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelName", modelName);
		return redirect;
	}
	
	@RequestMapping("/reservelist")
	public String reservelist(HttpServletRequest request) {
		int id = StrEdit.StrToInt(request.getParameter("id"));
		String modelName = "", redirect = "";
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		if(channelId==1){
			modelName = "会议室";
			redirect = "front/meeting/reservelist";
		}else if(channelId==2){
			modelName = "场地";
			redirect = "front/meeting/reservelist";
		}
		request.setAttribute("id", id);
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelName", modelName);
		return redirect;
	}
	
	@RequestMapping("/getJsonById")
	@ResponseBody
	public String getJsonById(int id){
		String strTemp = "";
		Meeting data = service.findById(id);
		strTemp = "{\"status\":\"1\",\"id\":\""+id+"\",\"name\":\""+data.getVarValue1()+"\"}";
		return strTemp;
	}
	
	@RequestMapping("/get")
	@ResponseBody
	public Map<String, Object> getAll(PageModel<Meeting> pageModel, Meeting data, HttpServletRequest request, Model model) {
		
		//System.out.println("------------------get------------------");
		String keywords = StrEdit.StrChkNull(data.getKeywords());
		
		Map<String,Object> newMap = new HashMap<String,Object>();
		
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		int classId = StrEdit.StrToInt(request.getParameter("classId"));		
		String varValue1 = StrEdit.StrChkNull(request.getParameter("varValue1"));
		String varValue2 = StrEdit.StrChkNull(request.getParameter("varValue2"));
		String varValue4 = StrEdit.StrChkNull(request.getParameter("varValue4"));
		String varValue6 = StrEdit.StrChkNull(request.getParameter("varValue6"));
		String varValue8 = StrEdit.StrChkNull(request.getParameter("varValue8"));
		String varValue12 = StrEdit.StrChkNull(request.getParameter("varValue12"));
		String date = StrEdit.StrChkNull(request.getParameter("date"));
		String startDate = "";
		String endDate = "";
		if(!date.equals("")){
			startDate = date + " 00:00:00";
			endDate = date + " 23:59:59";
		}
		
		int pageNumber = StrEdit.StrToInt(request.getParameter("pageNumber"));
		int pageSize = StrEdit.StrToInt(request.getParameter("pageSize"));
		int startRow = 0;
        if(pageNumber!=0) {
        	startRow = (pageNumber - 1) * pageSize;
        }
        newMap.put("startRow", startRow);
        newMap.put("pageSize", pageSize);
		/*int pageStart = (pageNumber -1) * pageSize+1;//oracle分页
        int pageEnd = pageStart + pageSize -1;
        newMap.put("pageStart", pageStart);
        newMap.put("pageEnd", pageEnd);*/
        newMap.put("channelId", channelId);
        newMap.put("classId", classId);
        newMap.put("keywords", keywords);
        newMap.put("varValue1", varValue1);
        newMap.put("varValue2", varValue2);
        newMap.put("varValue4", varValue4);
        newMap.put("varValue6", varValue6);
        newMap.put("varValue8", varValue8);
        newMap.put("varValue12", varValue12);
        newMap.put("startDate", startDate);
        newMap.put("endDate", endDate);
		List<Meeting> list = service.pageList(newMap);
		int totalRecords = service.totalRecord(newMap);
		pageModel.setTotal(totalRecords);
		
		Object jsonObject = JSONObject.toJSON(list);
		//System.out.println("jsonObject:"+jsonObject);
		//System.out.println("totalRecords1:"+totalRecords);
		
		int pn = 1;
		if (pageNumber != 0) {
			pn = new Integer(pageNumber);
		}
		pageModel.setPages(totalRecords, pn);//设置总记录数和当前页面
		Map<String, Object> map = new HashMap<>();
		map.put("page", pageModel);
		map.put("list", list);
		return map;
	}
	
	@RequestMapping("/addfirst")
	public String addfirst(HttpServletRequest request,Model model){
		
		String modelName = "", redirect = "";
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		if(channelId==1){
			modelName = "会议室";
			redirect = "front/meeting/meetingroomlist";//通过资产管理入库椅子数量显示会议室
		}else if(channelId==2){
			modelName = "场地";
			redirect = "front/meeting/meetingroomlistdirect";//直接显示场地
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelName", modelName);
		return redirect;
	}
	
	@RequestMapping("/add")
	public String toAdd(Meeting data,HttpServletRequest request,Model model){
		/*int channelID = StrEdit.StrToInt(request.getParameter("channelID"));
		int parentId = StrEdit.StrToInt(request.getParameter("parentId"));
		
		String parentName = "";
		if(parentId>0){
			Map<String,Object> newMap1 = new HashMap<String,Object>();
			newMap1.put("task", "className");
	        newMap1.put("conId", parentId);
	        parentName = service.getStringValue(newMap1);
		}
        
		request.setAttribute("channelID", channelID);
		request.setAttribute("parentId", parentId);
		request.setAttribute("parentName", parentName);
		
		data.setVarValue7("无");*/
		
		int meetingRoomId = StrEdit.StrToInt(request.getParameter("meetingRoomId"));
		
		MeetingRoom meetingroomdata = meetingroom.findById(meetingRoomId);
		model.addAttribute("meetingroomdata", meetingroomdata);
		String meetingRoomName = meetingroomdata.getVarValue1();
		
		String modelName = "", redirect = "";
		int channelId = meetingroomdata.getChannelId();
		if(channelId==1){
			modelName = "会议室";
			redirect = "front/meeting/add";
		}else if(channelId==2){
			modelName = "场地";
			redirect = "front/meeting/add";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelName", modelName);
		
		/*if(meetingRoomId>0){
			Map<String,Object> newMap1 = new HashMap<String,Object>();
			newMap1.put("task", "name");
	        newMap1.put("conId", meetingRoomId);
	        meetingRoomName = meetingroom.getStringValue(newMap1);
		}*/
		request.setAttribute("meetingRoomId", meetingRoomId);
		request.setAttribute("meetingRoomName", meetingRoomName);
		
		Map<String,Object> newMap2 = new HashMap<String,Object>();
        newMap2.put("task", "getStorageByMeetingRoom");
        newMap2.put("meetingRoomId", meetingRoomId);
        List<Storage> storagelist = storage.getListByMap(newMap2);
        request.setAttribute("storagelist", storagelist);
        request.setAttribute("storagenumber", storagelist.size());
		
		Map<Long, String> typeMap = new HashMap<Long, String>();
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
        return redirect;
	}
	
	@RequestMapping("/save")
	@ResponseBody
	public String save(Meeting data,HttpServletRequest request,HttpServletResponse response){
		int conId = 0;
		String strTemp = "", strValue = "";
		
		int userId = 0;
		String userName = "";
		User sessionUser = ((User)request.getSession().getAttribute(Contants.SESSION_USER));
		if(sessionUser!=null){
			userId = sessionUser.getConId();
			userName = sessionUser.getVarValue1();
		}
		
		String meetingRoomIds = data.getVarValue2();
		if(meetingRoomIds.equals("")){
			meetingRoomIds = "0";
		}
		
		String orderNo = StrEdit.getSN("1");
		data.setVarValue12(orderNo);
		
		MeetingRoom meetingroomdata = meetingroom.findById(StrEdit.StrToInt(meetingRoomIds));
		String hourminutes1 = meetingroomdata.getVarValue11();
		String hourminutes2 = meetingroomdata.getVarValue12();
		
		//String startDate = StrEdit.StrChkNull(request.getParameter("dateValue1"));
		//String endDate = StrEdit.StrChkNull(request.getParameter("dateValue2"));
		String startDate = StrEdit.dateToStrLong(data.getDateValue1());
		String endDate = StrEdit.dateToStrLong(data.getDateValue2());
		int hour1 = StrEdit.getHour(data.getDateValue1());
		int minute1 = StrEdit.getMinute(data.getDateValue1());
		int hour2 = StrEdit.getHour(data.getDateValue2());
		int minute2 = StrEdit.getMinute(data.getDateValue2());
		
		if(!StrEdit.compareHM(hour1+":"+minute1, hourminutes1, hourminutes2)){
			strValue = "该时间段内不可用，可预定时间范围："+hourminutes1+"至"+hourminutes2;
			strTemp = "{\"status\":\"0\",\"errors\":\""+strValue+"\"}";
        	return strTemp;
		}
		
		if(!StrEdit.compareHM(hour2+":"+minute2, hourminutes1, hourminutes2)){
			strValue = "该时间段内不可用，可预定时间范围："+hourminutes1+"至"+hourminutes2;
			strTemp = "{\"status\":\"0\",\"errors\":\""+strValue+"\"}";
        	return strTemp;
		}
		
		if(StrEdit.compareDate(startDate, endDate)!=1){
			strValue = "开始时间必须比结束时间早";
			strTemp = "{\"status\":\"0\",\"errors\":\""+strValue+"\"}";
        	return strTemp;
        }
		
		Map<String,Object> newMap = new HashMap<String,Object>();//判断该时间段内的会议室是否可用，不包含已经选择的会议室
		newMap.put("task", "checkMeeting");
        newMap.put("meetingRoomId", meetingRoomIds);
        newMap.put("meetingId", conId);
        newMap.put("startDate", startDate);
        newMap.put("endDate", endDate);
        int isExist = service.getIntValue(newMap);
        if(isExist>0){
        	strValue = "该时间段内已经被预定";
			strTemp = "{\"status\":\"0\",\"errors\":\""+strValue+"\"}";
        	return strTemp;
        }
        
		if(meetingRoomIds.indexOf(",")==-1){//选择一个会议室才判断是否收费，如果不收费则直接申请成功，其他情况走审核流程
			Map<String,Object> newMap1 = new HashMap<String,Object>();
			newMap1.put("task", "isFree");
			newMap1.put("conId", meetingRoomIds);
	        String isFree = meetingroom.getStringValue(newMap1);
	        if(isFree.equals("Y")){
	        	data.setVarValue6("5");
	        }else{
	        	/*Map<String,Object> newMap2 = new HashMap<String,Object>();
	        	newMap2.put("task", "price");
	        	newMap2.put("conId", meetingRoomIds);
		        String price = meetingroom.getStringValue(newMap2);*/
	        	Double standard = StrEdit.StrToDouble(meetingroomdata.getVarValue4());
	        	Double unitePrice = StrEdit.StrToDouble(meetingroomdata.getDecValue1());
		        Long totalMinute = StrEdit.getDateMinutes(startDate,endDate);
		        Double totalAmount = 0.0;
				if(standard>0 && unitePrice>0){
					double times = totalMinute/standard;//总分钟除以收费标准分钟，得到次数
					double cishu = Math.ceil(times);//有小数+1，没满一次算一次
					totalAmount = unitePrice*cishu;
				}
		        data.setDecValue1(String.valueOf(unitePrice));
		        data.setDecValue2(String.valueOf(totalAmount));
	        }
		}
        
		try{
			service.save(data);
			conId = data.getConId();
		}catch(Exception e){
			conId = 0;
			System.out.println("save插入数据失败" + e.getLocalizedMessage());
		}
		
		if(conId>0){
			
			String attachIds=StrEdit.StrChkNull(request.getParameter("attachIds"));//附件
			if(!attachIds.equals("")){
				String[] arrAttachIds = attachIds.split("\\,");
				for(int m=0;m<arrAttachIds.length;m++){
					boolean boolValue = attach.update(StrEdit.StrToInt(arrAttachIds[m]),conId);
					if(!boolValue){
						System.out.println("附件信息ID更新失败："+arrAttachIds[m]);
					}
				}
			}
			
			strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
		}else{
			strValue = "添加失败";
			strTemp = "{\"status\":\"0\",\"errors\":\""+strValue+"\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/update")
	@ResponseBody
	public String update(Meeting data,HttpServletRequest request){
		int conId = data.getConId();
		String strTemp = "", strValue = "";
		
		int userId = 0;
		String userName = "";
		User sessionUser = ((User)request.getSession().getAttribute(Contants.SESSION_USER));
		if(sessionUser!=null){
			userId = sessionUser.getConId();
			userName = sessionUser.getVarValue1();
		}
		
		String meetingRoomIds = data.getVarValue2();
		if(meetingRoomIds.equals("")){
			meetingRoomIds = "0";
		}
		
		MeetingRoom meetingroomdata = meetingroom.findById(StrEdit.StrToInt(meetingRoomIds));
		String hourminutes1 = meetingroomdata.getVarValue11();
		String hourminutes2 = meetingroomdata.getVarValue12();
		
		String startDate = StrEdit.dateToStrLong(data.getDateValue1());
		String endDate = StrEdit.dateToStrLong(data.getDateValue2());
		int hour1 = StrEdit.getHour(data.getDateValue1());
		int minute1 = StrEdit.getMinute(data.getDateValue1());
		int hour2 = StrEdit.getHour(data.getDateValue2());
		int minute2 = StrEdit.getMinute(data.getDateValue2());
		//System.out.println(hourminutes1);
		//System.out.println(hour1+":"+minute1);
		
		if(!StrEdit.compareHM(hour1+":"+minute1, hourminutes1, hourminutes2)){
			strValue = "该时间段内不可用，可预定时间范围："+hourminutes1+"至"+hourminutes2;
			strTemp = "{\"status\":\"0\",\"errors\":\""+strValue+"\"}";
        	return strTemp;
		}
		
		if(!StrEdit.compareHM(hour2+":"+minute2, hourminutes1, hourminutes2)){
			strValue = "该时间段内不可用，可预定时间范围："+hourminutes1+"至"+hourminutes2;
			strTemp = "{\"status\":\"0\",\"errors\":\""+strValue+"\"}";
        	return strTemp;
		}
		
		Map<String,Object> newMap = new HashMap<String,Object>();//判断该时间段内的会议室是否可用，不包含已经选择的会议室
		newMap.put("task", "checkMeeting");
        newMap.put("meetingRoomId", meetingRoomIds);
        newMap.put("meetingId", conId);
        newMap.put("startDate", startDate);
        newMap.put("endDate", endDate);
        int isExist = service.getIntValue(newMap);
        if(isExist>0){
        	strValue = "该时间段内不可用";
			strTemp = "{\"status\":\"0\",\"errors\":\""+strValue+"\"}";
        	return strTemp;
        }
        
		if(meetingRoomIds.indexOf(",")==-1){//选择一个会议室才判断是否收费，如果不收费则直接申请成功，其他情况走审核流程
			Map<String,Object> newMap1 = new HashMap<String,Object>();
			newMap1.put("task", "isFree");
			newMap1.put("conId", meetingRoomIds);
	        String isFree = meetingroom.getStringValue(newMap1);
	        if(isFree.equals("Y")){
	        	data.setVarValue6("5");
	        }else{
	        	/*Map<String,Object> newMap2 = new HashMap<String,Object>();
	        	newMap2.put("task", "price");
	        	newMap2.put("conId", meetingRoomIds);
		        String price = meetingroom.getStringValue(newMap2);*/
	        	Double standard = StrEdit.StrToDouble(meetingroomdata.getVarValue4());
	        	Double unitePrice = StrEdit.StrToDouble(meetingroomdata.getDecValue1());
		        Long totalMinute = StrEdit.getDateMinutes(startDate,endDate);
		        Double totalAmount = 0.0;
				if(standard>0 && unitePrice>0){
					double times = totalMinute/standard;//总分钟除以收费标准分钟，得到次数
					double cishu = Math.ceil(times);//有小数+1，没满一次算一次
					totalAmount = unitePrice*cishu;
				}
		        data.setDecValue1(String.valueOf(unitePrice));
		        data.setDecValue2(String.valueOf(totalAmount));
	        }
		}
		
		boolean boolValue = service.update(data);
		if(boolValue){
			
			strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
		}else{
			strValue = "更新失败";
			strTemp = "{\"status\":\"0\",\"errors\":\""+strValue+"\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/getById")
	public String getById(int id,HttpServletRequest request,Model model){
		Map<Long, String> typeMap = new HashMap<Long, String>();
		
		Meeting data = service.findById(id);
		String modelName = "", redirect = "";
		int channelId = data.getChannelId();
		if(channelId==1){
			modelName = "会议室";
			redirect = "front/meeting/add";
		}else if(channelId==2){
			modelName = "场地";
			redirect = "front/meeting/add";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelName", modelName);
		
		int meetingRoomId = StrEdit.StrToInt(data.getVarValue2());
		String meetingRoomName = "";
		MeetingRoom meetingroomdata = meetingroom.findById(meetingRoomId);
		model.addAttribute("meetingroomdata", meetingroomdata);
		if(meetingRoomId>0){
			Map<String,Object> newMap1 = new HashMap<String,Object>();
			newMap1.put("task", "name");
	        newMap1.put("conId", meetingRoomId);
	        meetingRoomName = meetingroom.getStringValue(newMap1);
		}
		request.setAttribute("meetingRoomId", meetingRoomId);
		request.setAttribute("meetingRoomName", meetingRoomName);
		
		Map<String,Object> newMap2 = new HashMap<String,Object>();
        newMap2.put("task", "getStorageByMeetingRoom");
        newMap2.put("meetingRoomId", meetingRoomId);
        List<Storage> storagelist = storage.getListByMap(newMap2);
        request.setAttribute("storagelist", storagelist);
        request.setAttribute("storagenumber", storagelist.size());
        
        List<Attachment> attachList1 = attach.list(1021,id);
        List<Attachment> attachList2 = attach.list(1022,id);
        
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
        model.addAttribute("attachList1", attachList1);
        model.addAttribute("attachList2", attachList2);
        
        return redirect;
	}
	
	@RequestMapping("/getByIdAudit")
	public String getByIdAudit(int id,HttpServletRequest request,Model model){
		Map<Long, String> typeMap = new HashMap<Long, String>();
        
		Meeting data = service.findById(id);
		String modelName = "", redirect = "";
		int channelId = data.getChannelId();
		if(channelId==1){
			modelName = "会议室";
			redirect = "front/meeting/auditadd";
		}else if(channelId==2){
			modelName = "场地";
			redirect = "front/meeting/auditadd";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelName", modelName);
		
		int meetingRoomId = StrEdit.StrToInt(data.getVarValue2());
		String meetingRoomName = "";
		MeetingRoom meetingroomdata = meetingroom.findById(meetingRoomId);
		model.addAttribute("meetingroomdata", meetingroomdata);
		if(meetingRoomId>0){
			Map<String,Object> newMap1 = new HashMap<String,Object>();
			newMap1.put("task", "name");
	        newMap1.put("conId", meetingRoomId);
	        meetingRoomName = meetingroom.getStringValue(newMap1);
		}
		request.setAttribute("meetingRoomId", meetingRoomId);
		request.setAttribute("meetingRoomName", meetingRoomName);
		
		Map<String,Object> newMap2 = new HashMap<String,Object>();
        newMap2.put("task", "getStorageByMeetingRoom");
        newMap2.put("meetingRoomId", meetingRoomId);
        List<Storage> storagelist = storage.getListByMap(newMap2);
        request.setAttribute("storagelist", storagelist);
        request.setAttribute("storagenumber", storagelist.size());
        
        List<Attachment> attachList1 = attach.list(1021,id);
        List<Attachment> attachList2 = attach.list(1022,id);
        
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
        model.addAttribute("attachList1", attachList1);
        model.addAttribute("attachList2", attachList2);
        
        return redirect;
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public String delete(int id,HttpServletRequest request){
		String strTemp = "";
		boolean boolValue = service.delete(id);
		if(boolValue){
			strTemp = "{\"status\":\"1\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"删除失败\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/audit")
	@ResponseBody
	public String audit(Meeting data,HttpServletRequest request){
		String strTemp = "";
		boolean boolValue = false;
		String task = StrEdit.StrChkNull(request.getParameter("task"));
		Date currTime = new Date();
		
		if(task.equals("audit")){
			int status = StrEdit.StrToInt(request.getParameter("status"));
			String selectids = StrEdit.StrChkNull(request.getParameter("selectids"));
			String[] arrParameter = selectids.split("\\,");//如果不包含,则直接返回原字符的一维数组
	    	if (arrParameter!=null){
				for(int i=0;i<arrParameter.length;i++){
					Map<String,Object> newMap = new HashMap<String,Object>();
					newMap.put("task", task);
					newMap.put("varValue6", status);
					newMap.put("updateTime", currTime);
			        newMap.put("conId", StrEdit.StrToInt(arrParameter[i]));
			        boolValue = service.updateByMap(newMap);
				}
			}
		}
		if(task.equals("auditpass")){
			int status = StrEdit.StrToInt(request.getParameter("status"));
			String selectids = StrEdit.StrChkNull(request.getParameter("selectids"));
			String[] arrParameter = selectids.split("\\,");//如果不包含,则直接返回原字符的一维数组
	    	if (arrParameter!=null){
				for(int i=0;i<arrParameter.length;i++){
					Map<String,Object> newMap = new HashMap<String,Object>();
					newMap.put("task", task);
					newMap.put("varValue6", status);
					newMap.put("updateTime", currTime);
			        newMap.put("conId", StrEdit.StrToInt(arrParameter[i]));
			        boolValue = service.updateByMap(newMap);
				}
			}
		}
		if(task.equals("auditreturn")){
			int status = StrEdit.StrToInt(request.getParameter("status"));
			String reason = StrEdit.StrChkNull(request.getParameter("reason"));
			String selectids = StrEdit.StrChkNull(request.getParameter("selectids"));
			String[] arrParameter = selectids.split("\\,");//如果不包含,则直接返回原字符的一维数组
	    	if (arrParameter!=null){
				for(int i=0;i<arrParameter.length;i++){
					Map<String,Object> newMap = new HashMap<String,Object>();
					newMap.put("task", task);
					newMap.put("varValue6", status);
					newMap.put("varValue7", reason);
					newMap.put("updateTime", currTime);
			        newMap.put("conId", StrEdit.StrToInt(arrParameter[i]));
			        boolValue = service.updateByMap(newMap);
				}
			}
		}
		if(task.equals("auditpay")){
			int status = StrEdit.StrToInt(request.getParameter("status"));
			String payType = StrEdit.StrChkNull(request.getParameter("payType"));
			String id = StrEdit.StrChkNull(request.getParameter("id"));
			Map<String,Object> newMap = new HashMap<String,Object>();
			newMap.put("task", task);
			newMap.put("varValue6", status);
			newMap.put("varValue20", payType);
			newMap.put("updateTime", currTime);
	        newMap.put("conId", id);
	        boolValue = service.updateByMap(newMap);
		}
		if(boolValue){
			strTemp = "{\"status\":\"1\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"更新失败\"}";
		}
		
		return strTemp;
	}
	
}
