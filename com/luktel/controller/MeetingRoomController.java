package com.luktel.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.luktel.model.Attachment;
import com.luktel.model.Building;
import com.luktel.model.MeetingRoom;
import com.luktel.model.Storage;
import com.luktel.model.User;
import com.luktel.service.AttachmentService;
import com.luktel.service.BuildingService;
import com.luktel.service.MeetingRoomService;
import com.luktel.service.MeetingService;
import com.luktel.service.StorageService;
import com.luktel.service.DictionaryService;
import com.util.common.Contants;
import com.util.common.PageModel;
import com.util.common.StrEdit;

/*
 * @Controller是标记在类UserController上面的，所以类UserController就是一个SpringMVC Controller对象了
 * 使用@RequestMapping("/getAllUser")标记在Controller方法上，表示当请求/getAllUser的时候访问的是LoginController的getAllUser方法，
 * 该方法返回了一个包括Model和View的ModelAndView对象113323
 */

@Controller//@Controller注解标识一个控制器
@RequestMapping("/meetingroom")//如果@RequestMapping注解在类级别上，则表示相对路径，在方法级别上，则标记访问的路径
public class MeetingRoomController{
	
	@Autowired
	private MeetingRoomService service;
	
	@Autowired
	private MeetingService meeting;
	
	@Autowired
	private BuildingService building;
	
	@Autowired
	private StorageService storage;
	
	@Autowired
	private AttachmentService attach;
	
	@Autowired
	private DictionaryService dictionary;
	
	@RequestMapping("/list")
	public String list(HttpServletRequest request) {
		String modelName = "", redirect = "";
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		if(channelId==1){
			modelName = "会议室";
			redirect = "front/meetingroom/list";
		}else if(channelId==2){
			modelName = "场地";
			redirect = "front/meetingroom/list";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelName", modelName);
		return redirect;
	}
	
	@RequestMapping("/treeselect")
	public String treecheck(HttpServletRequest request) {
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		int type = StrEdit.StrToInt(request.getParameter("type"));
		int conId = StrEdit.StrToInt(request.getParameter("conId"));
		String selectIds = StrEdit.StrChkNull(request.getParameter("selectIds"));
		String startDate = StrEdit.StrChkNull(request.getParameter("startDate"));
		String endDate = StrEdit.StrChkNull(request.getParameter("endDate"));
		request.setAttribute("type", type);
		request.setAttribute("channelId", channelId);
		request.setAttribute("conId", conId);
		request.setAttribute("selectIds", selectIds);
		request.setAttribute("startDate", startDate);
		request.setAttribute("endDate", endDate);
		return "front/meetingroom/treeselect";
	}
	
	@RequestMapping("/treeData")
	@ResponseBody
	public String treeData(HttpServletRequest request) {//返回json
		StringBuffer strBuff = new StringBuffer("");
		strBuff.append("[");
		
		int type = StrEdit.StrToInt(request.getParameter("type"));
		
		List<Building> list1 = new ArrayList<Building>();
		Map<String,Object> newMap1 = new HashMap<String,Object>();
		newMap1.put("task", "all");
        list1 = building.getListByMap(newMap1);
        for(int m=0; m<list1.size(); m++) {
        	if(m>0){strBuff.append(",");}
        	Building temp = list1.get(m);
        	strBuff.append("{");
        	if(type==3 || type==4){//人员多选才显示isParent，才会加载人员信息
        		strBuff.append("\"isParent\": \"true\",");
        	}
			strBuff.append("\"id\": \""+temp.getConId()+"\",");
			strBuff.append("\"pId\": \"0\",");
			strBuff.append("\"name\": \""+temp.getVarValue1()+"\"");
			strBuff.append("}");
			
        }
        strBuff.append("]");
        //System.out.println("list:" + strBuff.toString());
		
		return strBuff.toString();
	}
	
	@RequestMapping("/treeDataMeetingRoom")
	@ResponseBody
	public String treeDataMeetingRoom(HttpServletRequest request) {//返回json
		StringBuffer strBuff = new StringBuffer("");
		strBuff.append("[");
		
		int conId = StrEdit.StrToInt(request.getParameter("conId"));
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		int buildingId = StrEdit.StrToInt(request.getParameter("buildingId"));
		String startDate = StrEdit.StrChkNull(request.getParameter("startDate"));
		String endDate = StrEdit.StrChkNull(request.getParameter("endDate"));
		
		List<MeetingRoom> list1 = new ArrayList<MeetingRoom>();
		Map<String,Object> newMap1 = new HashMap<String,Object>();
		newMap1.put("task", "getMeetingRoom");
		newMap1.put("channelId", channelId);
		newMap1.put("buildingId", buildingId);
        list1 = service.getListByMap(newMap1);
        for(int m=0; m<list1.size(); m++) {
        	if(m>0){strBuff.append(",");}
        	MeetingRoom temp = list1.get(m);
        	
        	Map<String,Object> newMap = new HashMap<String,Object>();//判断该时间段内的会议室是否可用，不包含已经选择的会议室
			newMap.put("task", "checkMeeting");
	        newMap.put("meetingRoomId", temp.getConId());
	        newMap.put("meetingId", conId);
	        newMap.put("startDate", startDate);
	        newMap.put("endDate", endDate);
	        int isExist = meeting.getIntValue(newMap);
	        
        	strBuff.append("{");
			strBuff.append("\"id\": \"u_"+temp.getConId()+"\",");
			strBuff.append("\"pId\": \""+temp.getIntValue2()+"\",");
			
			if(isExist>0){
				strBuff.append("\"nocheck\": \"true\",");
				strBuff.append("\"name\": \""+temp.getVarValue1()+"该时间段不可用\"");
			}else{
				strBuff.append("\"name\": \""+temp.getVarValue1()+"\"");
			}
			
			strBuff.append("}");
        }
        strBuff.append("]");
		//System.out.println("list:" + strBuff.toString());
		
		return strBuff.toString();
	}
	
	@RequestMapping("/meetingroomselect")
	public String meetingroomselect(HttpServletRequest request) {
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		int type = StrEdit.StrToInt(request.getParameter("type"));
		request.setAttribute("channelId", channelId);
		request.setAttribute("type", type);
		return "front/meetingroom/meetingroomselect";
	}
	
	@RequestMapping("/treeDataMeetingRoomBasic")
	@ResponseBody
	public String treeDataMeetingRoomBasic(HttpServletRequest request) {//返回json
		StringBuffer strBuff = new StringBuffer("");
		strBuff.append("[");
		
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		int buildingId = StrEdit.StrToInt(request.getParameter("buildingId"));
		
		List<MeetingRoom> list1 = new ArrayList<MeetingRoom>();
		Map<String,Object> newMap1 = new HashMap<String,Object>();
		newMap1.put("task", "getMeetingRoom");
		newMap1.put("channelId", channelId);
        newMap1.put("buildingId", buildingId);
        list1 = service.getListByMap(newMap1);
        for(int m=0; m<list1.size(); m++) {
        	if(m>0){strBuff.append(",");}
        	MeetingRoom temp = list1.get(m);
	        
        	strBuff.append("{");
			strBuff.append("\"id\": \"u_"+temp.getConId()+"\",");
			strBuff.append("\"pId\": \""+temp.getIntValue2()+"\",");
			
			strBuff.append("\"name\": \""+temp.getVarValue1()+"\",");
			
			strBuff.append("}");
        }
        strBuff.append("]");
		//System.out.println("list:" + strBuff.toString());
		
		return strBuff.toString();
	}
	
	@RequestMapping("/getJsonById")
	@ResponseBody
	public String getJsonById(int id){
		String strTemp = "";
		MeetingRoom data = service.findById(id);
		strTemp = "{\"status\":\"1\",\"id\":\""+id+"\",\"name\":\""+data.getVarValue1()+"\"}";
		return strTemp;
	}
	
	@RequestMapping("/get")
	@ResponseBody
	public Map<String, Object> getAll(PageModel<MeetingRoom> pageModel, MeetingRoom data, HttpServletRequest request, Model model) {
		
		//System.out.println("------------------get------------------");
		String keywords = StrEdit.StrChkNull(data.getKeywords());
		
		Map<String,Object> newMap = new HashMap<String,Object>();
		
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		int classId = StrEdit.StrToInt(request.getParameter("classId"));
		String varValue1 = StrEdit.StrChkNull(request.getParameter("varValue1"));
		String varValue2 = StrEdit.StrChkNull(request.getParameter("varValue2"));
		String varValue4 = StrEdit.StrChkNull(request.getParameter("varValue4"));
		String varValue8 = StrEdit.StrChkNull(request.getParameter("varValue8"));
		
		int pageNumber = StrEdit.StrToInt(request.getParameter("pageNumber"));
		int pageSize = StrEdit.StrToInt(request.getParameter("pageSize"));
		int startRow = 0;
        if(pageNumber!=0) {
        	startRow = (pageNumber - 1) * pageSize;
        }
        newMap.put("startRow", startRow);
        newMap.put("pageSize", pageSize);
		/*int pageStart = (pageNumber -1) * pageSize+1;//oracle分页
        int pageEnd = pageStart + pageSize -1;
        newMap.put("pageStart", pageStart);
        newMap.put("pageEnd", pageEnd);*/
        newMap.put("channelId", channelId);
        newMap.put("classId", classId);
        newMap.put("keywords", keywords);
        newMap.put("varValue1", varValue1);
        newMap.put("varValue2", varValue2);
        newMap.put("varValue4", varValue4);
        newMap.put("varValue8", varValue8);
		List<MeetingRoom> list = service.pageList(newMap);
		int totalRecords = service.totalRecord(newMap);
		pageModel.setTotal(totalRecords);
		
		Object jsonObject = JSONObject.toJSON(list);
		//System.out.println("jsonObject:"+jsonObject);
		//System.out.println("totalRecords1:"+totalRecords);
		
		int pn = 1;
		if (pageNumber != 0) {
			pn = new Integer(pageNumber);
		}
		pageModel.setPages(totalRecords, pn);//设置总记录数和当前页面
		Map<String, Object> map = new HashMap<>();
		map.put("page", pageModel);
		map.put("list", list);
		return map;
	}
	
	@RequestMapping("/getforzc")
	@ResponseBody
	public Map<String, Object> getforzc(PageModel<MeetingRoom> pageModel, MeetingRoom data, HttpServletRequest request, Model model) {
		
		//System.out.println("------------------get------------------");
		String keywords = StrEdit.StrChkNull(data.getKeywords());
		
		Map<String,Object> newMap = new HashMap<String,Object>();
		
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		int classId = StrEdit.StrToInt(request.getParameter("classId"));
		String name = StrEdit.StrChkNull(request.getParameter("name"));
		String quantity = StrEdit.StrChkNull(request.getParameter("quantity"));
		
		int pageNumber = StrEdit.StrToInt(request.getParameter("pageNumber"));
		int pageSize = StrEdit.StrToInt(request.getParameter("pageSize"));
		int startRow = 0;
        if(pageNumber!=0) {
        	startRow = (pageNumber - 1) * pageSize;
        }
        newMap.put("task", "getList");
        newMap.put("startRow", startRow);
        newMap.put("pageSize", pageSize);
        newMap.put("channelId", channelId);
        newMap.put("classId", classId);
        newMap.put("keywords", keywords);
        newMap.put("name", name);
        newMap.put("quantity", quantity);
		List<MeetingRoom> list = service.getListByMap(newMap);
		int totalRecords = service.getIntValue(newMap);
		pageModel.setTotal(totalRecords);
		
		//Object jsonObject = JSONObject.toJSON(list);
		//System.out.println("jsonObject:"+jsonObject);
		//System.out.println("totalRecords1:"+totalRecords);
		
		int pn = 1;
		if (pageNumber != 0) {
			pn = new Integer(pageNumber);
		}
		pageModel.setPages(totalRecords, pn);//设置总记录数和当前页面
		Map<String, Object> map = new HashMap<>();
		map.put("page", pageModel);
		map.put("list", list);
		return map;
	}
	
	@RequestMapping("/add")
	public String toAdd(MeetingRoom data,HttpServletRequest request,Model model){
		/*int channelID = StrEdit.StrToInt(request.getParameter("channelID"));
		int parentId = StrEdit.StrToInt(request.getParameter("parentId"));
		
		String parentName = "";
		if(parentId>0){
			Map<String,Object> newMap1 = new HashMap<String,Object>();
			newMap1.put("task", "className");
	        newMap1.put("conId", parentId);
	        parentName = service.getStringValue(newMap1);
		}
        
		request.setAttribute("channelID", channelID);
		request.setAttribute("parentId", parentId);
		request.setAttribute("parentName", parentName);
		
		data.setVarValue7("无");*/
		
		String modelName = "", redirect = "";
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		if(channelId==1){
			modelName = "会议室";
			redirect = "front/meetingroom/add";
		}else if(channelId==2){
			modelName = "场地";
			redirect = "front/meetingroom/add";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelName", modelName);
		
		List<String> dictionery3 = dictionary.selectDictionaryList(3);
		model.addAttribute("dictionery3", dictionery3);
		
		Map<String,Object> newMap1 = new HashMap<String,Object>();
		newMap1.put("task", "all");
		List<Building> list1 = building.getListByMap(newMap1);
        model.addAttribute("list1", list1);
		
		Map<Long, String> typeMap = new HashMap<Long, String>();
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
        return redirect;
	}
	
	@RequestMapping("/save")
	@ResponseBody
	public String save(MeetingRoom data,HttpServletRequest request,HttpServletResponse response){
		int conId = 0;
		String strTemp = "";
		
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("task", "buildingName");
		paramMap.put("conId", data.getIntValue2());
		String buildingName = building.getStringValue(paramMap);
		data.setVarValue2(buildingName);
		
		int userId = 0;
		String userName = "";
		User sessionUser = ((User)request.getSession().getAttribute(Contants.SESSION_USER));
		if(sessionUser!=null){
			userId = sessionUser.getConId();
			userName = sessionUser.getVarValue1();
		}
		
		try{
			service.save(data);
			conId = data.getConId();
		}catch(Exception e){
			conId = 0;
			System.out.println("save插入数据失败" + e.getLocalizedMessage());
		}
		
		if(conId>0){
			
			String attachIds=StrEdit.StrChkNull(request.getParameter("attachIds"));//附件
			if(!attachIds.equals("")){
				String[] arrAttachIds = attachIds.split("\\,");
				for(int m=0;m<arrAttachIds.length;m++){
					boolean boolValue = attach.update(StrEdit.StrToInt(arrAttachIds[m]),conId);
					if(!boolValue){
						System.out.println("附件信息ID更新失败："+arrAttachIds[m]);
					}
				}
			}
			
			strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"添加失败\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/update")
	@ResponseBody
	public String update(MeetingRoom data,HttpServletRequest request){
		int conId = data.getConId();
		String strTemp = "";
		
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("task", "buildingName");
		paramMap.put("conId", data.getIntValue2());
		String buildingName = building.getStringValue(paramMap);
		data.setVarValue2(buildingName);
		
		int userId = 0;
		String userName = "";
		User sessionUser = ((User)request.getSession().getAttribute(Contants.SESSION_USER));
		if(sessionUser!=null){
			userId = sessionUser.getConId();
			userName = sessionUser.getVarValue1();
		}
		
		boolean boolValue = service.update(data);
		if(boolValue){
			
			strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"更新失败\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/getById")
	public String getById(int id,HttpServletRequest request,Model model){
		Map<Long, String> typeMap = new HashMap<Long, String>();
		MeetingRoom data = service.findById(id);
		String modelName = "", redirect = "";
		int channelId = data.getChannelId();
		if(channelId==1){
			modelName = "会议室";
			redirect = "front/meetingroom/add";
		}else if(channelId==2){
			modelName = "场地";
			redirect = "front/meetingroom/add";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelName", modelName);
		
		List<String> dictionery3 = dictionary.selectDictionaryList(3);
		model.addAttribute("dictionery3", dictionery3);
		
		Map<String,Object> newMap1 = new HashMap<String,Object>();
		newMap1.put("task", "all");
		List<Building> list1 = building.getListByMap(newMap1);
        model.addAttribute("list1", list1);
        
        Map<String,Object> newMap2 = new HashMap<String,Object>();
        newMap2.put("task", "getStorageByMeetingRoom");
        newMap2.put("meetingRoomId", id);
        List<Storage> storagelist = storage.getListByMap(newMap2);
        request.setAttribute("storagelist", storagelist);
        request.setAttribute("storagenumber", storagelist.size());
        
        List<Attachment> attachList1 = attach.list(1041,id);
		model.addAttribute("attachList1", attachList1);
		
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
        
        return redirect;
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public String delete(int id,HttpServletRequest request){
		String strTemp = "";
		boolean boolValue = service.delete(id);
		if(boolValue){
			strTemp = "{\"status\":\"1\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"删除失败\"}";
		}
		
		return strTemp;
	}
	
}
