package com.luktel.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.luktel.model.Attachment;
import com.luktel.model.GoodsName;
import com.luktel.model.User;
import com.luktel.service.AttachmentService;
import com.luktel.service.BuildingService;
import com.luktel.service.GoodsNameService;
import com.luktel.service.DictionaryService;
import com.util.common.Contants;
import com.util.common.PageModel;
import com.util.common.StrEdit;

/*
 * @Controller是标记在类UserController上面的，所以类UserController就是一个SpringMVC Controller对象了
 * 使用@RequestMapping("/getAllUser")标记在Controller方法上，表示当请求/getAllUser的时候访问的是LoginController的getAllUser方法，
 * 该方法返回了一个包括Model和View的ModelAndView对象113323
 */

@Controller//@Controller注解标识一个控制器
@RequestMapping("/goodsname")//如果@RequestMapping注解在类级别上，则表示相对路径，在方法级别上，则标记访问的路径
public class GoodsNameController{
	
	@Autowired
	private GoodsNameService service;
	
	@Autowired
	private BuildingService building;
	
	@Autowired
	private AttachmentService attach;
	
	@Autowired
	private DictionaryService dictionary;
	
	@RequestMapping("/list")
	public String list(HttpServletRequest request) {
		String modelName = "";
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		if(channelId==1){
			modelName = "物品分类";
		}else if(channelId==2){
			modelName = "资产分类";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelName", modelName);
		return "front/goodsname/list";
	}
	
	@RequestMapping("/goodsnameselectlayer")
	public String goodsnameselectlayer(HttpServletRequest request) {
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		request.setAttribute("channelId", channelId);
		return "front/goodsname/goodsnameselectlayer";
	}
	
	@RequestMapping("/goodsnameselectlayerzc")
	public String goodsnameselectlayerzc(HttpServletRequest request) {
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		request.setAttribute("channelId", channelId);
		return "front/goodsname/goodsnameselectlayerzc";
	}
	
	@RequestMapping("/auditlist")
	public String auditlist(HttpServletRequest request) {
		String modelName = "";
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		if(channelId==1){
			modelName = "物品分类";
		}else if(channelId==2){
			modelName = "资产分类";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelName", modelName);
		return "front/goodsname/auditlist";
	}
	
	@RequestMapping("/getJsonById")
	@ResponseBody
	public String getJsonById(int id){
		String strTemp = "";
		GoodsName data = service.findById(id);
		strTemp = "{\"status\":\"1\",\"id\":\""+id+"\",\"name\":\""+data.getVarValue1()+"\",\"no\":\""+data.getVarValue2()+"\",\"supplier\":\""+data.getVarValue3()+"\",\"price\":\""+data.getVarValue4()+"\",\"unite\":\""+data.getVarValue5()+"\"}";
		return strTemp;
	}
	
	@RequestMapping("/getJsonByIdzc")
	@ResponseBody
	public String getJsonByIdzc(int id){
		String strTemp = "";
		GoodsName data = service.findById(id);
		strTemp = "{\"status\":\"1\",\"id\":\""+id+"\",\"name\":\""+data.getVarValue1()+"\",\"no\":\""+data.getVarValue12()+"\",\"category\":\""+data.getVarValue11()+"\",\"price\":\""+data.getVarValue4()+"\",\"unite\":\""+data.getVarValue5()+"\",\"quantity\":\"1\"}";
		return strTemp;
	}
	
	@RequestMapping("/get")
	@ResponseBody
	public Map<String, Object> getAll(PageModel<GoodsName> pageModel, GoodsName data, HttpServletRequest request, Model model) {
		
		//System.out.println("------------------get------------------");
		String keywords = StrEdit.StrChkNull(data.getKeywords());
		
		Map<String,Object> newMap = new HashMap<String,Object>();
		
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		int classId = StrEdit.StrToInt(request.getParameter("classId"));
		int intValue2 = StrEdit.StrToInt(request.getParameter("intValue2"));
		String varValue1 = StrEdit.StrChkNull(request.getParameter("varValue1"));
		String varValue2 = StrEdit.StrChkNull(request.getParameter("varValue2"));
		String varValue3 = StrEdit.StrChkNull(request.getParameter("varValue3"));
		String varValue4 = StrEdit.StrChkNull(request.getParameter("varValue4"));
		String varValue6 = StrEdit.StrChkNull(request.getParameter("varValue6"));
		String varValue8 = StrEdit.StrChkNull(request.getParameter("varValue8"));
		String date = StrEdit.StrChkNull(request.getParameter("date"));
		String startDate = "";
		String endDate = "";
		if(!date.equals("")){
			startDate = date + " 00:00:00";
			endDate = date + " 23:59:59";
		}
		
		int pageNumber = StrEdit.StrToInt(request.getParameter("pageNumber"));
		int pageSize = StrEdit.StrToInt(request.getParameter("pageSize"));
		int startRow = 0;
        if(pageNumber!=0) {
        	startRow = (pageNumber - 1) * pageSize;
        }
        newMap.put("startRow", startRow);
        newMap.put("pageSize", pageSize);
		/*int pageStart = (pageNumber -1) * pageSize+1;//oracle分页
        int pageEnd = pageStart + pageSize -1;
        newMap.put("pageStart", pageStart);
        newMap.put("pageEnd", pageEnd);*/
        newMap.put("channelId", channelId);
        newMap.put("classId", classId);
        newMap.put("intValue2", intValue2);
        newMap.put("keywords", keywords);
        newMap.put("varValue1", varValue1);
        newMap.put("varValue2", varValue2);
        newMap.put("varValue3", varValue3);
        newMap.put("varValue4", varValue4);
        newMap.put("varValue6", varValue6);
        newMap.put("varValue8", varValue8);
        newMap.put("startDate", startDate);
        newMap.put("endDate", endDate);
		List<GoodsName> list = service.pageList(newMap);
		int totalRecords = service.totalRecord(newMap);
		pageModel.setTotal(totalRecords);
		
		//Object jsonObject = JSONObject.toJSON(list);
		//System.out.println("jsonObject:"+jsonObject);
		//System.out.println("totalRecords1:"+totalRecords);
		
		int pn = 1;
		if (pageNumber != 0) {
			pn = new Integer(pageNumber);
		}
		pageModel.setPages(totalRecords, pn);//设置总记录数和当前页面
		Map<String, Object> map = new HashMap<>();
		map.put("page", pageModel);
		map.put("list", list);
		return map;
	}
	
	@RequestMapping("/getlayer")
	@ResponseBody
	public Map<String, Object> getlayer(PageModel<GoodsName> pageModel, GoodsName data, HttpServletRequest request, Model model) {
		
		//System.out.println("------------------get------------------");
		
		Map<String,Object> newMap = new HashMap<String,Object>();
		
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		int classId = StrEdit.StrToInt(request.getParameter("classId"));
		int intValue2 = StrEdit.StrToInt(request.getParameter("intValue2"));
		String keywords = StrEdit.StrChkNull(request.getParameter("keywords"));
		
		int pageNumber = StrEdit.StrToInt(request.getParameter("pageNumber"));
		int pageSize = StrEdit.StrToInt(request.getParameter("pageSize"));
		int startRow = 0;
        if(pageNumber!=0) {
        	startRow = (pageNumber - 1) * pageSize;
        }
        newMap.put("task", "getList");
        newMap.put("startRow", startRow);
        newMap.put("pageSize", pageSize);
        newMap.put("channelId", channelId);
        newMap.put("classId", classId);
        newMap.put("intValue2", intValue2);
        newMap.put("keywords", keywords);
		List<GoodsName> list = service.getListByMap(newMap);
		int totalRecords = service.getIntValue(newMap);
		pageModel.setTotal(totalRecords);
		
		//Object jsonObject = JSONObject.toJSON(list);
		
		int pn = 1;
		if (pageNumber != 0) {
			pn = new Integer(pageNumber);
		}
		pageModel.setPages(totalRecords, pn);
		Map<String, Object> map = new HashMap<>();
		map.put("page", pageModel);
		map.put("list", list);
		return map;
	}
	
	@RequestMapping("/getlayerzc")
	@ResponseBody
	public Map<String, Object> getlayerzc(PageModel<GoodsName> pageModel, GoodsName data, HttpServletRequest request, Model model) {
		
		//System.out.println("------------------get------------------");
		
		Map<String,Object> newMap = new HashMap<String,Object>();
		
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		int classId = StrEdit.StrToInt(request.getParameter("classId"));
		int intValue2 = StrEdit.StrToInt(request.getParameter("intValue2"));
		String keywords = StrEdit.StrChkNull(request.getParameter("keywords"));
		
		int pageNumber = StrEdit.StrToInt(request.getParameter("pageNumber"));
		int pageSize = StrEdit.StrToInt(request.getParameter("pageSize"));
		int startRow = 0;
        if(pageNumber!=0) {
        	startRow = (pageNumber - 1) * pageSize;
        }
        newMap.put("task", "getListZC");
        newMap.put("startRow", startRow);
        newMap.put("pageSize", pageSize);
        newMap.put("channelId", channelId);
        newMap.put("classId", classId);
        newMap.put("intValue2", intValue2);
        newMap.put("keywords", keywords);
		List<GoodsName> list = service.getListByMap(newMap);
		int totalRecords = service.getIntValue(newMap);
		pageModel.setTotal(totalRecords);
		
		//Object jsonObject = JSONObject.toJSON(list);
		
		int pn = 1;
		if (pageNumber != 0) {
			pn = new Integer(pageNumber);
		}
		pageModel.setPages(totalRecords, pn);
		Map<String, Object> map = new HashMap<>();
		map.put("page", pageModel);
		map.put("list", list);
		return map;
	}
	
	@RequestMapping("/add")
	public String toAdd(GoodsName data,HttpServletRequest request,Model model){
		String modelName = "";
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		if(channelId==1){
			modelName = "物品分类";
		}else if(channelId==2){
			modelName = "资产分类";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelName", modelName);
		
		List<String> dictionery7 = dictionary.selectDictionaryList(7);
		model.addAttribute("dictionery7", dictionery7);
		
		Map<Long, String> typeMap = new HashMap<Long, String>();
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
		return "/front/goodsname/add";
	}
	
	@RequestMapping("/save")
	@ResponseBody
	public String save(GoodsName data,HttpServletRequest request,HttpServletResponse response){
		int conId = 0;
		String strTemp = "";
		
		int userId = 0;
		String userName = "";
		User sessionUser = ((User)request.getSession().getAttribute(Contants.SESSION_USER));
		if(sessionUser!=null){
			userId = sessionUser.getConId();
			userName = sessionUser.getVarValue1();
		}
		
		String autoNo = "";
		int intValue2 = data.getIntValue2();
		if(intValue2==1){
			autoNo = StrEdit.getSN("BJGJ");
		}else if(intValue2==2){
			autoNo = StrEdit.getSN("BJHC");
		}else if(intValue2==3){
			autoNo = StrEdit.getSN("WXGJ");
		}else if(intValue2==4){
			autoNo = StrEdit.getSN("WXHC");
		}else if(intValue2==5){
			autoNo = StrEdit.getSN(data.getVarValue12());
		}
		data.setVarValue2(autoNo);
		
		try{
			service.save(data);
			conId = data.getConId();
		}catch(Exception e){
			conId = 0;
			System.out.println("save插入数据失败" + e.getLocalizedMessage());
		}
		
		if(conId>0){
			
			String attachIds=StrEdit.StrChkNull(request.getParameter("attachIds"));//附件
			if(!attachIds.equals("")){
				String[] arrAttachIds = attachIds.split("\\,");
				for(int m=0;m<arrAttachIds.length;m++){
					boolean boolValue = attach.update(StrEdit.StrToInt(arrAttachIds[m]),conId);
					if(!boolValue){
						System.out.println("附件信息ID更新失败："+arrAttachIds[m]);
					}
				}
			}
			
			strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"添加失败\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/update")
	@ResponseBody
	public String update(GoodsName data,HttpServletRequest request){
		int conId = data.getConId();
		String strTemp = "";
		
		int userId = 0;
		String userName = "";
		User sessionUser = ((User)request.getSession().getAttribute(Contants.SESSION_USER));
		if(sessionUser!=null){
			userId = sessionUser.getConId();
			userName = sessionUser.getVarValue1();
		}
		
		boolean boolValue = service.update(data);
		if(boolValue){
			
			strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"更新失败\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/getById")
	public String getById(int id,HttpServletRequest request,Model model){
		Map<Long, String> typeMap = new HashMap<Long, String>();
		GoodsName data = service.findById(id);
		String modelName = "";
		int channelId = data.getChannelId();
		if(channelId==1){
			modelName = "物品分类";
		}else if(channelId==2){
			modelName = "资产分类";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelName", modelName);
		
		List<String> dictionery7 = dictionary.selectDictionaryList(7);
		model.addAttribute("dictionery7", dictionery7);
        
        List<Attachment> attachList1011 = attach.list(1011,id);
        
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
        model.addAttribute("attachList1011", attachList1011);
        
		return "/front/goodsname/add";
	}
	
	@RequestMapping("/getByIdAudit")
	public String getByIdAudit(int id,HttpServletRequest request,Model model){
		Map<Long, String> typeMap = new HashMap<Long, String>();
		GoodsName data = service.findById(id);
		String modelName = "";
		int channelId = data.getChannelId();
		if(channelId==1){
			modelName = "物品分类";
		}else if(channelId==2){
			modelName = "资产分类";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelName", modelName);
		
		List<String> dictionery5 = dictionary.selectDictionaryList(5);
		model.addAttribute("dictionery5", dictionery5);
        
        List<Attachment> attachList1001 = attach.list(1001,id);
        List<String> dictionery4 = dictionary.selectDictionaryList(4);
        
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
        model.addAttribute("attachList1001", attachList1001);
        model.addAttribute("dictionery4", dictionery4);
        
		return "/front/goodsname/auditadd";
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public String delete(int id,HttpServletRequest request){
		String strTemp = "", strValue = "";
		boolean boolValue = false;
		String authority = request.getSession().getAttribute(Contants.SESSION_AUTHORITY).toString();
		if(authority.contains("goodsnamedel")){//authority.indexOf("goodsnamedel")!=0
			boolValue = service.delete(id);
			if(boolValue){
				strTemp = "{\"status\":\"1\"}";
			}else{
				strValue = "删除失败";
				strTemp = "{\"status\":\"0\",\"errors\":\""+strValue+"\"}";
			}
		}else{
			strValue = "您没有此操作权限";
			strTemp = "{\"status\":\"2\",\"errors\":\""+strValue+"\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/audit")
	@ResponseBody
	public String audit(HttpServletRequest request){
		String strTemp = "";
		boolean boolValue = false;
		String task = StrEdit.StrChkNull(request.getParameter("task"));
		Date currTime = new Date();
		
		if(task.equals("audit")){
			int status = StrEdit.StrToInt(request.getParameter("status"));
			String selectids = StrEdit.StrChkNull(request.getParameter("selectids"));
			String[] arrParameter = selectids.split("\\,");//如果不包含,则直接返回原字符的一维数组
	    	if (arrParameter!=null){
				for(int i=0;i<arrParameter.length;i++){
					Map<String,Object> newMap = new HashMap<String,Object>();
					newMap.put("task", task);
					newMap.put("varValue6", status);
					newMap.put("updateTime", currTime);
			        newMap.put("conId", StrEdit.StrToInt(arrParameter[i]));
			        boolValue = service.updateByMap(newMap);
				}
			}
		}
		if(task.equals("auditpass")){
			int status = StrEdit.StrToInt(request.getParameter("status"));
			String selectids = StrEdit.StrChkNull(request.getParameter("selectids"));
			String personIds = StrEdit.StrChkNull(request.getParameter("personIds"));
			String personNames = StrEdit.StrChkNull(request.getParameter("personNames"));
			String[] arrParameter = selectids.split("\\,");//如果不包含,则直接返回原字符的一维数组
	    	if (arrParameter!=null){
				for(int i=0;i<arrParameter.length;i++){
					Map<String,Object> newMap = new HashMap<String,Object>();
					newMap.put("task", task);
					newMap.put("varValue6", status);
					newMap.put("varValue10", personIds);
					newMap.put("varValue11", personNames);
					newMap.put("updateTime", currTime);
			        newMap.put("conId", StrEdit.StrToInt(arrParameter[i]));
			        boolValue = service.updateByMap(newMap);
				}
			}
		}
		if(task.equals("auditreturn")){
			int status = StrEdit.StrToInt(request.getParameter("status"));
			String reason = StrEdit.StrChkNull(request.getParameter("reason"));
			String memo = StrEdit.StrChkNull(request.getParameter("memo"));
			String selectids = StrEdit.StrChkNull(request.getParameter("selectids"));
			String[] arrParameter = selectids.split("\\,");//如果不包含,则直接返回原字符的一维数组
	    	if (arrParameter!=null){
				for(int i=0;i<arrParameter.length;i++){
					Map<String,Object> newMap = new HashMap<String,Object>();
					newMap.put("task", task);
					newMap.put("varValue6", status);
					newMap.put("varValue7", reason);
					newMap.put("varValue8", memo);
					newMap.put("updateTime", currTime);
			        newMap.put("conId", StrEdit.StrToInt(arrParameter[i]));
			        boolValue = service.updateByMap(newMap);
				}
			}
		}
		if(task.equals("auditresult")){
			int status = StrEdit.StrToInt(request.getParameter("status"));
			String reason = StrEdit.StrChkNull(request.getParameter("reason"));
			String memo = StrEdit.StrChkNull(request.getParameter("memo"));
			String audituserid = StrEdit.StrChkNull(request.getParameter("audituserid"));
			String auditname = StrEdit.StrChkNull(request.getParameter("auditname"));
			String auditdate = StrEdit.StrChkNull(request.getParameter("auditdate"));
			String selectids = StrEdit.StrChkNull(request.getParameter("selectids"));
			String[] arrParameter = selectids.split("\\,");//如果不包含,则直接返回原字符的一维数组
	    	if (arrParameter!=null){
				for(int i=0;i<arrParameter.length;i++){
					Map<String,Object> newMap = new HashMap<String,Object>();
					newMap.put("task", task);
					newMap.put("varValue6", status);
					newMap.put("varValue7", reason);
					newMap.put("varValue8", memo);
					newMap.put("varValue12", audituserid);
					newMap.put("varValue13", auditname);
					newMap.put("updateTime", currTime);
					newMap.put("dateValue3", auditdate);
			        newMap.put("conId", StrEdit.StrToInt(arrParameter[i]));
			        boolValue = service.updateByMap(newMap);
				}
			}
		}
		if(task.equals("auditpay")){
			int status = StrEdit.StrToInt(request.getParameter("status"));
			String payType = StrEdit.StrChkNull(request.getParameter("payType"));
			String id = StrEdit.StrChkNull(request.getParameter("id"));
			Map<String,Object> newMap = new HashMap<String,Object>();
			newMap.put("task", task);
			newMap.put("varValue5", status);
			newMap.put("varValue20", payType);
			newMap.put("updateTime", currTime);
	        newMap.put("conId", id);
	        boolValue = service.updateByMap(newMap);
		}
		if(boolValue){
			strTemp = "{\"status\":\"1\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"更新失败\"}";
		}
		
		return strTemp;
	}
	
}
