package com.luktel.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.luktel.model.Dictionary;
import com.luktel.model.User;
import com.luktel.service.DeptmentService;
import com.luktel.service.DictionaryService;
import com.luktel.service.UserRoleService;
import com.luktel.service.UserService;
import com.util.common.Contants;
import com.util.common.MD5;
import com.util.common.PageModel;
import com.util.common.PageNew;
import com.util.common.StrEdit;

/*
 * @Controller是标记在类UserController上面的，所以类UserController就是一个SpringMVC Controller对象了
 * 使用@RequestMapping("/getAllUser")标记在Controller方法上，表示当请求/getAllUser的时候访问的是LoginController的getAllUser方法，
 * 该方法返回了一个包括Model和View的ModelAndView对象113323
 */

@Controller//@Controller注解标识一个控制器
@RequestMapping("/user")//如果@RequestMapping注解在类级别上，则表示相对路径，在方法级别上，则标记访问的路径
public class UserController{
	
	@Autowired
	private UserService service;
	
	@Autowired
	private DictionaryService dictionary;
	
	@Autowired
	private UserRoleService userRole;
	
	@Autowired
	private DeptmentService deptment;
	
	@RequestMapping("/")
	public String welcome() {
		return "front/user/userList";
	}
	
	/*@RequestMapping("/userselectindex")
	public String userselectindex() {
		return "front/user/userselectindex";
	}
	
	@RequestMapping("/deptmentmenu")
	public String deptmentmenu(Model model){
		Map<Long, String> typeMap = new HashMap<Long, String>();
		List<Map<String,Object>> typeMaps = deptment.selectDictionaryMapList(0);
        for (Map<String, Object> map : typeMaps) {
        	Long ids = null;
            String name = null;
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                if ("CONID".equals(entry.getKey())) {
                	ids = ((java.math.BigDecimal) entry.getValue()).longValue();
                } else if ("VARVALUE1".equals(entry.getKey())) {
                	name = (String) entry.getValue();
                }
            }
            typeMap.put(ids, name);
        }
        model.addAttribute("typeMap", typeMap);
		return "/front/user/deptmentmenu";
	}*/
	
	@RequestMapping("/userselectbydept")
	public String userselectbydept() {
		return "front/user/userselectbydept";
	}
	
	@RequestMapping("/userselect")
	public String userselect(HttpServletRequest request) {
		int type = StrEdit.StrToInt(request.getParameter("type"));
		
		Map<Long, String> typeMap = new HashMap<Long, String>();
		List<Map<String,Object>> typeMaps = deptment.selectMapList(0);
        for (Map<String, Object> map : typeMaps) {
        	Long ids = null;
            String name = null;
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                if ("CONID".equals(entry.getKey())) {
                	ids = ((java.math.BigDecimal) entry.getValue()).longValue();
                } else if ("VARVALUE1".equals(entry.getKey())) {
                	name = (String) entry.getValue();
                }
            }
            typeMap.put(ids, name);
        }
        request.setAttribute("typeMap", typeMap);
		request.setAttribute("type", type);
		return "front/user/userselect";
	}
	
	@RequestMapping("/userselectajax")
	public String userselectajax() {
		return "front/user/userselectajax";
	}
	
	@RequestMapping("/multiuserselect")
	public String multiuserselect(HttpServletRequest request) {
		int type = StrEdit.StrToInt(request.getParameter("type"));
		
		Map<Long, String> typeMap = new HashMap<Long, String>();
		List<Map<String,Object>> typeMaps = deptment.selectMapList(0);
        for (Map<String, Object> map : typeMaps) {
        	Long ids = null;
            String name = null;
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                if ("CONID".equals(entry.getKey())) {
                	ids = ((java.math.BigDecimal) entry.getValue()).longValue();
                } else if ("VARVALUE1".equals(entry.getKey())) {
                	name = (String) entry.getValue();
                }
            }
            typeMap.put(ids, name);
        }
        request.setAttribute("typeMap", typeMap);
		request.setAttribute("type", type);
		return "front/user/multiuserselect";
	}
	
	@RequestMapping("/multiuserselectajax")
	public String multiuserselectajax() {
		return "front/user/multiuserselectajax";
	}
	
	@RequestMapping("/getUserById")
	@ResponseBody
	public String getDepartmentById(int id){
		String strTemp = "";
		User data = service.findById(id);
		//int station = StrEdit.StrToInt(data.getVarValue14());
		//Dictionary dictionery = dictionary.findById(station);
		//String dictionaryName = dictionery.getVarValue1();
		strTemp = "{\"status\":\"1\",\"id\":\""+id+"\",\"name\":\""+data.getVarValue3()+"\",\"deptment\":\""+data.getVarValue13()+"\",\"station\":\""+data.getVarValue14()+"\",\"mobile\":\""+data.getVarValue8()+"\",\"tel\":\""+data.getVarValue9()+"\"}";
		return strTemp;
	}
	
	@RequestMapping("/userList")
	public String userList() {
		return "front/user/userList";
	}
	
	@RequestMapping("/editpassword")
	public String editpassword(HttpServletRequest request) {
		return "front/user/editpassword";
	}
	
	@RequestMapping("/get")
	@ResponseBody
	public Map<String, Object> getAll(PageModel<User> pageModel, User data, HttpServletRequest request, Model model) {
		
		//System.out.println("------------------get------------------");
		String keywords = StrEdit.StrChkNull(data.getKeywords());
		
		Map<String,Object> newMap = new HashMap<String,Object>();
		
		int deptId = StrEdit.StrToInt(request.getParameter("deptId"));
		
		int pageNumber = StrEdit.StrToInt(request.getParameter("pageNumber"));
		int pageSize = StrEdit.StrToInt(request.getParameter("pageSize"));
		
		int startRow = 0;
        if(pageNumber!=0) {
        	startRow = (pageNumber - 1) * pageSize;
        }
        newMap.put("startRow", startRow);
        newMap.put("pageSize", pageSize);
        
        newMap.put("intValue2", deptId);
        newMap.put("keywords", keywords);
		List<User> list = service.pageList(newMap);
		int totalRecords = service.totalRecord(newMap);
		pageModel.setTotal(totalRecords);
		
		//Object jsonObject = JSONObject.toJSON(list);
		//System.out.println("jsonObject:"+jsonObject);
		
		int pn = 1;
		if (pageNumber != 0) {
			pn = new Integer(pageNumber);
		}
		pageModel.setPages(totalRecords, pn);//设置总记录数和当前页面
		Map<String, Object> map = new HashMap<>();
		map.put("page", pageModel);
		map.put("list", list);
		return map;
	}
	
	/*
	 * 跳转到添加界面
	 */
	@RequestMapping("/toAdd")
	public String toAdd(User data,HttpServletRequest request,Model model){
		List<String> dictionery1 = dictionary.selectDictionaryList(1);
		//List<String> userRoleList = userRole.selectList(1);
        //request.setAttribute("dictionery5", dictionery5);
		Map<Long, String> userRoleMap = new HashMap<Long, String>();
		List<Map<String,Object>> userRoleMaps = userRole.selectMapList(1);
        for (Map<String, Object> map : userRoleMaps) {
        	Long ids = null;
            String name = null;
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                if ("CONID".equals(entry.getKey())) {
                	ids = (Long) entry.getValue();
                } else if ("VARVALUE1".equals(entry.getKey())) {
                	name = (String) entry.getValue();
                }
            }
            userRoleMap.put(ids, name);
        }
        model.addAttribute("userRoleMap", userRoleMap);
		model.addAttribute("dictionery1", dictionery1);
        model.addAttribute("data", data);
		return "/front/user/addUser";
	}
	
	/*
	 * 不使用model
	 */
	@RequestMapping("/toAdd2")
	public String toAdd2(User data,HttpServletRequest request){
		Map<Long, String> stationMap = new HashMap<Long, String>();
		List<Map<String,Object>> stationMaps = dictionary.selectDictionaryMapList(5);
        for (Map<String, Object> map : stationMaps) {
        	Long ids = null;
            String name = null;
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                if ("CONID".equals(entry.getKey())) {
                	ids = (Long) entry.getValue();
                } else if ("VARVALUE1".equals(entry.getKey())) {
                	name = (String) entry.getValue();
                }
            }
            stationMap.put(ids, name);
        }
        request.setAttribute("data", data);
        request.setAttribute("stationMap", stationMap);
		return "/front/user/addUser";
	}
	
	/*
	 * 添加数据
	 * mybatis insert、update 、delete默认返回值解释与如何设置返回表主键 https://www.cnblogs.com/xuanaiwu/articles/6036682.html
	 * https://segmentfault.com/q/1010000002480348/a-1020000002480446
	 * http://blog.csdn.net/qq877507054/article/details/52725892
	 */
	@RequestMapping("/save")
	@ResponseBody
	public String save(User user,HttpServletRequest request,HttpServletResponse response){
		int conId = 0;
		String strTemp = "";
		String varValue2 = StrEdit.StrChkNull(user.getVarValue2());
		
		try{
			service.save(user);
			conId = user.getConId();
		}catch(Exception e){//插入数据的时候，如果有的字段必填，但是没有数据，就会出现异常
			conId = 0;
			System.out.println("save插入数据失败" + e.getLocalizedMessage());
		}
		
		if(conId>0){
			strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"添加失败\"}";
		}
		
		return strTemp;
	}
	
	/*
	 * 编辑数据
	 */
	@RequestMapping("/update")
	@ResponseBody
	public String update(User user,HttpServletRequest request){
		int conId = user.getConId();
		String varValue2 = StrEdit.StrChkNull(user.getVarValue2()).trim();
		if(!varValue2.equals("")){
			varValue2 = MD5.toMD5(varValue2);
			user.setVarValue2(varValue2);
		}
		String strTemp = "";
		boolean boolValue = service.update(user);
		if(boolValue){
			strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"更新失败\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/updatepassword")
	@ResponseBody
	public String updatepassword(User data,HttpServletRequest request){
		String strTemp = "";
		
		int sessionUserId = 0;
		User sessionUser = ((User)request.getSession().getAttribute(Contants.SESSION_USER));
		if(sessionUser!=null){
			sessionUserId = sessionUser.getConId();
		}
		
		if(sessionUserId<=0){
			strTemp = "{\"status\":\"0\",\"errors\":\"请登录后再修改密码\"}";//tomcat重启了需要登录
			return strTemp;
		}
		
		System.out.println("sessionUserId:"+sessionUserId);
		data.setConId(sessionUserId);
		data.setTask("updatepassword");
		String varValue2 = StrEdit.StrChkNull(request.getParameter("varValue2"));
		if(sessionUserId==0 || varValue2.equals("")){
			strTemp = "{\"status\":\"0\",\"errors\":\"参数错误\"}";
			return strTemp;
		}
		
		String oldPassword = StrEdit.StrChkNull(request.getParameter("oldPassword"));
		Map<String,Object> newMap = new HashMap<String,Object>();
		newMap.put("task", "checkpassword");
		newMap.put("conId", sessionUserId);
		newMap.put("varValue2", MD5.toMD5(oldPassword));
		int userId = service.getIntValue(newMap);
		if(userId<=0){
			strTemp = "{\"status\":\"0\",\"errors\":\"原密码错误\"}";
			return strTemp;
		}
		
		if(!varValue2.equals("")){
			varValue2 = MD5.toMD5(varValue2);
			data.setVarValue2(varValue2);
		}
		
		boolean boolValue = service.updateSpecialTask(data);
		if(boolValue){
			strTemp = "{\"status\":\"1\",\"conId\":\""+sessionUserId+"\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"更新失败\"}";
		}
		
		return strTemp;
	}
	
	/*
	 * 根据id查看详情
	 */
	@RequestMapping("/getById")
	public String getById(int id,HttpServletRequest request,Model model){
		List<String> dictionery1 = dictionary.selectDictionaryList(1);
		//List<String> userRoleList = userRole.selectList(1);
        //request.setAttribute("dictionery5", dictionery1);
		Map<Long, String> userRoleMap = new HashMap<Long, String>();
		List<Map<String,Object>> userRoleMaps = userRole.selectMapList(1);
        for (Map<String, Object> map : userRoleMaps) {
        	Long ids = null;
            String name = null;
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                if ("CONID".equals(entry.getKey())) {
                	ids = (Long) entry.getValue();
                } else if ("VARVALUE1".equals(entry.getKey())) {
                	name = (String) entry.getValue();
                }
            }
            userRoleMap.put(ids, name);
        }
        model.addAttribute("userRoleMap", userRoleMap);
        model.addAttribute("dictionery1", dictionery1);
        model.addAttribute("data", service.findById(id));
		return "/front/user/addUser";
	}
	
	/*
	 * 删除数据
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public String delete(int id,HttpServletRequest request){
		String strTemp = "";
		boolean boolValue = service.delete(id);
		if(boolValue){
			strTemp = "{\"status\":\"1\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"删除失败\"}";
		}
		
		return strTemp;
	}
	
	/*
	 * 添加用户并重定向
	 */
	@RequestMapping("/save2")
	public String save2(User user,HttpServletRequest request){
		service.save(user);
		return "redirect:/user/userList";
	}
	
	/*
	 * 编辑用户
	 */
	@RequestMapping("/update2")
	public String update2(User user,HttpServletRequest request){
		boolean boolValue = service.update(user);
		if(boolValue){
			System.out.println("update success");
			user = service.findById(user.getConId());
			request.setAttribute("user", user);
			return "redirect:/user/userList";
		}else{
			System.out.println("update error");
			return "/front/user/error";
		}
	}
	
	/*
	 * 根据id返回对象
	 */
	@RequestMapping("/getById2")
	@ResponseBody
	public User getById2(int id) {
		User user = service.findById(id);
		return user;
	}
	
	/*
	 * 删除用户
	 */
	@RequestMapping("/delById")
	public void delById(int id,HttpServletRequest request,HttpServletResponse response){
		String result = "{\"result\":\"error\"}";
		if(service.delete(id)){
			result = "{\"result\":\"success\"}";
		}
		response.setContentType("application/json");
		try {
			PrintWriter out = response.getWriter();
			out.write(result);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
