package com.luktel.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.luktel.model.Article;
import com.luktel.model.Attachment;
import com.luktel.model.Clean;
import com.luktel.model.User;
import com.luktel.service.ArticleService;
import com.luktel.service.AttachmentService;
import com.luktel.service.BuildingService;
import com.luktel.service.CleanService;
import com.luktel.service.DictionaryService;
import com.util.common.Contants;
import com.util.common.PageModel;
import com.util.common.StrEdit;

/*
 * @Controller是标记在类UserController上面的，所以类UserController就是一个SpringMVC Controller对象了
 * 使用@RequestMapping("/getAllUser")标记在Controller方法上，表示当请求/getAllUser的时候访问的是LoginController的getAllUser方法，
 * 该方法返回了一个包括Model和View的ModelAndView对象113323
 */

@Controller//@Controller注解标识一个控制器
@RequestMapping("/clean")//如果@RequestMapping注解在类级别上，则表示相对路径，在方法级别上，则标记访问的路径
public class CleanController{
	
	@Autowired
	private CleanService service;
	
	@Autowired
	private BuildingService building;
	
	@Autowired
	private AttachmentService attach;
	
	@Autowired
	private ArticleService articleservice;
	
	@Autowired
	private DictionaryService dictionary;
	
	@RequestMapping("/list")
	public String list(HttpServletRequest request) {
		String modelName = "", redirect = "";
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		if(channelId==1){
			modelName = "保洁";
			redirect = "front/clean/list";
		}else if(channelId==2){
			modelName = "报修";
			redirect = "front/clean/list";
		}else if(channelId==3){
			modelName = "巡检";
			redirect = "front/clean/xunjian/list";
		}else if(channelId==4){
			modelName = "投诉建议";
			redirect = "front/clean/complain/list";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelName", modelName);
		return redirect;
	}
	
	@RequestMapping("/auditlist")
	public String auditlist(HttpServletRequest request) {
		String modelName = "";
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		if(channelId==1){
			modelName = "保洁";
		}else if(channelId==2){
			modelName = "报修";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelName", modelName);
		return "front/clean/auditlist";
	}
	
	@RequestMapping("/getJsonById")
	@ResponseBody
	public String getJsonById(int id){
		String strTemp = "";
		Clean data = service.findById(id);
		strTemp = "{\"status\":\"1\",\"id\":\""+id+"\",\"name\":\""+data.getVarValue1()+"\"}";
		return strTemp;
	}
	
	@RequestMapping("/get")
	@ResponseBody
	public Map<String, Object> getAll(PageModel<Clean> pageModel, Clean data, HttpServletRequest request, Model model) {
		
		//System.out.println("------------------get------------------");
		String keywords = StrEdit.StrChkNull(data.getKeywords());
		
		Map<String,Object> newMap = new HashMap<String,Object>();
		
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		int classId = StrEdit.StrToInt(request.getParameter("classId"));
		int intValue2 = StrEdit.StrToInt(request.getParameter("intValue2"));
		String varValue1 = StrEdit.StrChkNull(request.getParameter("varValue1"));
		String varValue2 = StrEdit.StrChkNull(request.getParameter("varValue2"));
		String varValue4 = StrEdit.StrChkNull(request.getParameter("varValue4"));
		String varValue6 = StrEdit.StrChkNull(request.getParameter("varValue6"));
		String varValue8 = StrEdit.StrChkNull(request.getParameter("varValue8"));
		String varValue13 = StrEdit.StrChkNull(request.getParameter("varValue13"));
		String varValue40 = StrEdit.StrChkNull(request.getParameter("varValue40"));
		String date = StrEdit.StrChkNull(request.getParameter("date"));
		String startDate = "";
		String endDate = "";
		if(!date.equals("")){
			startDate = date + " 00:00:00";
			endDate = date + " 23:59:59";
		}
		
		int pageNumber = StrEdit.StrToInt(request.getParameter("pageNumber"));
		int pageSize = StrEdit.StrToInt(request.getParameter("pageSize"));
		int startRow = 0;
        if(pageNumber!=0) {
        	startRow = (pageNumber - 1) * pageSize;
        }
        newMap.put("startRow", startRow);
        newMap.put("pageSize", pageSize);
		/*int pageStart = (pageNumber -1) * pageSize+1;//oracle分页
        int pageEnd = pageStart + pageSize -1;
        newMap.put("pageStart", pageStart);
        newMap.put("pageEnd", pageEnd);*/
        newMap.put("channelId", channelId);
        newMap.put("classId", classId);
        newMap.put("intValue2", intValue2);
        newMap.put("keywords", keywords);
        newMap.put("varValue1", varValue1);
        newMap.put("varValue2", varValue2);
        newMap.put("varValue4", varValue4);
        newMap.put("varValue6", varValue6);
        newMap.put("varValue8", varValue8);
        newMap.put("varValue13", varValue13);
        newMap.put("varValue40", varValue40);
        newMap.put("startDate", startDate);
        newMap.put("endDate", endDate);
		List<Clean> list = service.pageList(newMap);
		int totalRecords = service.totalRecord(newMap);
		pageModel.setTotal(totalRecords);
		
		//Object jsonObject = JSONObject.toJSON(list);
		//System.out.println("jsonObject:"+jsonObject);
		//System.out.println("totalRecords1:"+totalRecords);
		
		int pn = 1;
		if (pageNumber != 0) {
			pn = new Integer(pageNumber);
		}
		pageModel.setPages(totalRecords, pn);//设置总记录数和当前页面
		Map<String, Object> map = new HashMap<>();
		map.put("page", pageModel);
		map.put("list", list);
		return map;
	}
	
	@RequestMapping("/add")
	public String toAdd(Clean data,HttpServletRequest request,Model model){
		String modelName = "", redirect = "";
		String articleContent = "";
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		if(channelId==1){
			modelName = "保洁";
			redirect = "front/clean/add";
			
			Map<String,Object> newMap1 = new HashMap<String,Object>();
			newMap1.put("task", "getDescByIntValue2");
	        newMap1.put("intValue2", "1002");
	        articleContent = articleservice.getStringValue(newMap1);
	        
		}else if(channelId==2){
			modelName = "报修";
			redirect = "front/clean/add";
			
			Map<String,Object> newMap1 = new HashMap<String,Object>();
			newMap1.put("task", "getDescByIntValue2");
	        newMap1.put("intValue2", "1001");
	        articleContent = articleservice.getStringValue(newMap1);
	        
		}else if(channelId==3){
			modelName = "巡检";
			redirect = "front/clean/xunjian/add";
		}else if(channelId==4){
			modelName = "投诉建议";
			redirect = "front/clean/complain/add";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelName", modelName);
		request.setAttribute("articleContent", articleContent);
		
		List<String> dictionery11 = dictionary.selectDictionaryList(11);
		model.addAttribute("dictionery11", dictionery11);
		
		Map<Long, String> typeMap = new HashMap<Long, String>();
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
        return redirect;
	}
	
	@RequestMapping("/save")
	@ResponseBody
	public String save(Clean data,HttpServletRequest request,HttpServletResponse response){
		int conId = 0;
		String strTemp = "";
		
		int userId = 0;
		String userName = "";
		User sessionUser = ((User)request.getSession().getAttribute(Contants.SESSION_USER));
		if(sessionUser!=null){
			userId = sessionUser.getConId();
			userName = sessionUser.getVarValue1();
		}
		
		String orderNo = StrEdit.getSN("2");
		data.setVarValue13(orderNo);
        
		try{
			service.save(data);
			conId = data.getConId();
		}catch(Exception e){
			conId = 0;
			System.out.println("save插入数据失败" + e.getLocalizedMessage());
		}
		
		if(conId>0){
			
			String attachIds=StrEdit.StrChkNull(request.getParameter("attachIds"));//附件
			if(!attachIds.equals("")){
				String[] arrAttachIds = attachIds.split("\\,");
				for(int m=0;m<arrAttachIds.length;m++){
					boolean boolValue = attach.update(StrEdit.StrToInt(arrAttachIds[m]),conId);
					if(!boolValue){
						System.out.println("附件信息ID更新失败："+arrAttachIds[m]);
					}
				}
			}
			
			String attachId1002=StrEdit.StrChkNull(request.getParameter("attachId1002"));
			if(!attachId1002.equals("")){
				String[] arrAttachIds = attachId1002.split("\\,");
				for(int m=0;m<arrAttachIds.length;m++){
					boolean boolValue = attach.update(StrEdit.StrToInt(arrAttachIds[m]),conId);
					if(!boolValue){
						System.out.println("附件信息ID更新失败："+arrAttachIds[m]);
					}
				}
			}
			
			strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"添加失败\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/update")
	@ResponseBody
	public String update(Clean data,HttpServletRequest request){
		int conId = data.getConId();
		String strTemp = "";
		
		int userId = 0;
		String userName = "";
		User sessionUser = ((User)request.getSession().getAttribute(Contants.SESSION_USER));
		if(sessionUser!=null){
			userId = sessionUser.getConId();
			userName = sessionUser.getVarValue1();
		}
		
		boolean boolValue = service.update(data);
		if(boolValue){
			
			strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"更新失败\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/getById")
	public String getById(int id,HttpServletRequest request,Model model){
		Map<Long, String> typeMap = new HashMap<Long, String>();
		Clean data = service.findById(id);
		String modelName = "", redirect = "";
		String articleContent = "";
		int channelId = data.getChannelId();
		if(channelId==1){
			modelName = "保洁";
			redirect = "front/clean/add";
			
			Map<String,Object> newMap1 = new HashMap<String,Object>();
			newMap1.put("task", "getDescByIntValue2");
	        newMap1.put("intValue2", "1002");
	        articleContent = articleservice.getStringValue(newMap1);
	        
		}else if(channelId==2){
			modelName = "报修";
			redirect = "front/clean/add";
			
			Map<String,Object> newMap1 = new HashMap<String,Object>();
			newMap1.put("task", "getDescByIntValue2");
	        newMap1.put("intValue2", "1001");
	        articleContent = articleservice.getStringValue(newMap1);
	        
		}else if(channelId==3){
			modelName = "巡检";
			redirect = "front/clean/xunjian/add";
		}else if(channelId==4){
			modelName = "投诉建议";
			redirect = "front/clean/complain/add";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelName", modelName);
		request.setAttribute("articleContent", articleContent);
		
		List<String> dictionery11 = dictionary.selectDictionaryList(11);
		model.addAttribute("dictionery11", dictionery11);
        
		List<Attachment> attachList1 = attach.list(1311,id);
        List<Attachment> attachList2 = attach.list(1312,id);
        model.addAttribute("attachList1", attachList1);
        model.addAttribute("attachList2", attachList2);
        
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
        
        return redirect;
	}
	
	@RequestMapping("/getByIdAudit")
	public String getByIdAudit(int id,HttpServletRequest request,Model model){
		Map<Long, String> typeMap = new HashMap<Long, String>();
		Clean data = service.findById(id);
		String modelName = "";
		int channelId = data.getChannelId();
		if(channelId==1){
			modelName = "保洁";
		}else if(channelId==2){
			modelName = "报修";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelName", modelName);
        
		List<Attachment> attachList1 = attach.list(1311,id);
        List<Attachment> attachList2 = attach.list(1312,id);
        model.addAttribute("attachList1", attachList1);
        model.addAttribute("attachList2", attachList2);
        
        List<String> dictionery11 = dictionary.selectDictionaryList(11);
		model.addAttribute("dictionery11", dictionery11);
        
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
        
		return "/front/clean/auditadd";
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public String delete(int id,HttpServletRequest request){
		String strTemp = "";
		boolean boolValue = service.delete(id);
		if(boolValue){
			strTemp = "{\"status\":\"1\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"删除失败\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/audit")
	@ResponseBody
	public String audit(Clean data,HttpServletRequest request){
		String strTemp = "";
		boolean boolValue = false;
		String task = StrEdit.StrChkNull(request.getParameter("task"));
		Date currTime = new Date();
		
		if(task.equals("audit")){
			int status = StrEdit.StrToInt(request.getParameter("status"));
			String selectids = StrEdit.StrChkNull(request.getParameter("selectids"));
			String[] arrParameter = selectids.split("\\,");//如果不包含,则直接返回原字符的一维数组
	    	if (arrParameter!=null){
				for(int i=0;i<arrParameter.length;i++){
					Map<String,Object> newMap = new HashMap<String,Object>();
					newMap.put("task", task);
					newMap.put("varValue6", status);
					newMap.put("updateTime", currTime);
			        newMap.put("conId", StrEdit.StrToInt(arrParameter[i]));
			        boolValue = service.updateByMap(newMap);
				}
			}
		}
		if(task.equals("auditpass")){
			int status = StrEdit.StrToInt(request.getParameter("status"));
			String selectids = StrEdit.StrChkNull(request.getParameter("selectids"));
			String personIds = StrEdit.StrChkNull(request.getParameter("personIds"));
			String personNames = StrEdit.StrChkNull(request.getParameter("personNames"));
			String[] arrParameter = selectids.split("\\,");//如果不包含,则直接返回原字符的一维数组
	    	if (arrParameter!=null){
				for(int i=0;i<arrParameter.length;i++){
					Map<String,Object> newMap = new HashMap<String,Object>();
					newMap.put("task", task);
					newMap.put("varValue6", status);
					newMap.put("varValue10", personIds);
					newMap.put("varValue11", personNames);
					newMap.put("updateTime", currTime);
			        newMap.put("conId", StrEdit.StrToInt(arrParameter[i]));
			        boolValue = service.updateByMap(newMap);
				}
			}
		}
		if(task.equals("auditreturn")){
			int status = StrEdit.StrToInt(request.getParameter("status"));
			String reason = StrEdit.StrChkNull(request.getParameter("reason"));
			String memo = StrEdit.StrChkNull(request.getParameter("memo"));
			String selectids = StrEdit.StrChkNull(request.getParameter("selectids"));
			String[] arrParameter = selectids.split("\\,");//如果不包含,则直接返回原字符的一维数组
	    	if (arrParameter!=null){
				for(int i=0;i<arrParameter.length;i++){
					Map<String,Object> newMap = new HashMap<String,Object>();
					newMap.put("task", task);
					newMap.put("varValue6", status);
					newMap.put("varValue7", reason);
					newMap.put("varValue8", memo);
					newMap.put("updateTime", currTime);
			        newMap.put("conId", StrEdit.StrToInt(arrParameter[i]));
			        boolValue = service.updateByMap(newMap);
				}
			}
		}
		if(task.equals("chuli")){
			int id = StrEdit.StrToInt(request.getParameter("id"));
			int status = StrEdit.StrToInt(request.getParameter("status"));
			String memo = StrEdit.StrChkNull(request.getParameter("memo"));
			Map<String,Object> newMap = new HashMap<String,Object>();
			newMap.put("task", task);
			newMap.put("varValue6", status);
			newMap.put("varValue7", "");
			newMap.put("varValue8", memo);
			newMap.put("updateTime", currTime);
	        newMap.put("conId", id);
	        boolValue = service.updateByMap(newMap);
		}
		if(task.equals("fenpei")){
			int id = StrEdit.StrToInt(request.getParameter("id"));
			int status = StrEdit.StrToInt(request.getParameter("status"));
			String memo = StrEdit.StrChkNull(request.getParameter("memo"));
			String personIds = StrEdit.StrChkNull(request.getParameter("personIds"));
			String personNames = StrEdit.StrChkNull(request.getParameter("personNames"));
			Map<String,Object> newMap = new HashMap<String,Object>();
			newMap.put("task", task);
			newMap.put("varValue6", status);
			newMap.put("varValue7", "");
			newMap.put("varValue8", memo);
			newMap.put("varValue10", personIds);
			newMap.put("varValue11", personNames);
			newMap.put("updateTime", currTime);
	        newMap.put("conId", id);
	        boolValue = service.updateByMap(newMap);
		}
		if(task.equals("auditpay")){
			int status = StrEdit.StrToInt(request.getParameter("status"));
			String payType = StrEdit.StrChkNull(request.getParameter("payType"));
			String id = StrEdit.StrChkNull(request.getParameter("id"));
			Map<String,Object> newMap = new HashMap<String,Object>();
			newMap.put("task", task);
			newMap.put("varValue6", status);
			newMap.put("varValue20", payType);
			newMap.put("updateTime", currTime);
	        newMap.put("conId", id);
	        boolValue = service.updateByMap(newMap);
		}
		if(boolValue){
			strTemp = "{\"status\":\"1\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"更新失败\"}";
		}
		
		return strTemp;
	}
	
}
