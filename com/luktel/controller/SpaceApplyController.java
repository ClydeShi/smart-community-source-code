package com.luktel.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.luktel.model.Attachment;
import com.luktel.model.Space;
import com.luktel.model.SpaceApply;
import com.luktel.model.User;
import com.luktel.service.AttachmentService;
import com.luktel.service.BuildingService;
import com.luktel.service.SpaceApplyService;
import com.luktel.service.SpaceService;
import com.luktel.service.DictionaryService;
import com.luktel.service.MeetingRoomService;
import com.util.common.Contants;
import com.util.common.PageModel;
import com.util.common.StrEdit;

/*
 * @Controller是标记在类UserController上面的，所以类UserController就是一个SpringMVC Controller对象了
 * 使用@RequestMapping("/getAllUser")标记在Controller方法上，表示当请求/getAllUser的时候访问的是LoginController的getAllUser方法，
 * 该方法返回了一个包括Model和View的ModelAndView对象113323
 */

@Controller//@Controller注解标识一个控制器
@RequestMapping("/spaceapply")//如果@RequestMapping注解在类级别上，则表示相对路径，在方法级别上，则标记访问的路径
public class SpaceApplyController{
	
	@Autowired
	private SpaceApplyService service;
	
	@Autowired
	private SpaceService spaceservice;
	
	@Autowired
	private BuildingService building;
	
	@Autowired
	private AttachmentService attach;
	
	@Autowired
	private DictionaryService dictionary;
	
	@RequestMapping("/list")
	public String list() {
		return "front/spaceapply/list";
	}
	
	@RequestMapping("/auditlist")
	public String auditlist() {
		return "front/spaceapply/auditlist";
	}
	
	@RequestMapping("/reservelist")
	public String reservelist(HttpServletRequest request) {
		int id = StrEdit.StrToInt(request.getParameter("id"));
		request.setAttribute("id", id);
		return "front/spaceapply/reservelist";
	}
	
	@RequestMapping("/getJsonById")
	@ResponseBody
	public String getJsonById(int id){
		String strTemp = "";
		SpaceApply data = service.findById(id);
		strTemp = "{\"status\":\"1\",\"id\":\""+id+"\",\"name\":\""+data.getVarValue1()+"\"}";
		return strTemp;
	}
	
	@RequestMapping("/get")
	@ResponseBody
	public Map<String, Object> getAll(PageModel<SpaceApply> pageModel, SpaceApply data, HttpServletRequest request, Model model) {
		
		//System.out.println("------------------get------------------");
		String keywords = StrEdit.StrChkNull(data.getKeywords());
		
		Map<String,Object> newMap = new HashMap<String,Object>();
		
		String varValue1 = StrEdit.StrChkNull(request.getParameter("varValue1"));
		String varValue2 = StrEdit.StrChkNull(request.getParameter("varValue2"));
		String varValue4 = StrEdit.StrChkNull(request.getParameter("varValue4"));
		String varValue6 = StrEdit.StrChkNull(request.getParameter("varValue6"));
		String varValue8 = StrEdit.StrChkNull(request.getParameter("varValue8"));
		String date = StrEdit.StrChkNull(request.getParameter("date"));
		String startDate = "";
		String endDate = "";
		if(!date.equals("")){
			startDate = date + " 00:00:00";
			endDate = date + " 23:59:59";
		}
		
		int pageNumber = StrEdit.StrToInt(request.getParameter("pageNumber"));
		int pageSize = StrEdit.StrToInt(request.getParameter("pageSize"));
		int startRow = 0;
        if(pageNumber!=0) {
        	startRow = (pageNumber - 1) * pageSize;
        }
        newMap.put("startRow", startRow);
        newMap.put("pageSize", pageSize);
		/*int pageStart = (pageNumber -1) * pageSize+1;//oracle分页
        int pageEnd = pageStart + pageSize -1;
        newMap.put("pageStart", pageStart);
        newMap.put("pageEnd", pageEnd);*/
        newMap.put("keywords", keywords);
        newMap.put("varValue1", varValue1);
        newMap.put("varValue2", varValue2);
        newMap.put("varValue4", varValue4);
        newMap.put("varValue6", varValue6);
        newMap.put("varValue8", varValue8);
        newMap.put("startDate", startDate);
        newMap.put("endDate", endDate);
		List<SpaceApply> list = service.pageList(newMap);
		int totalRecords = service.totalRecord(newMap);
		pageModel.setTotal(totalRecords);
		
		Object jsonObject = JSONObject.toJSON(list);
		//System.out.println("jsonObject:"+jsonObject);
		//System.out.println("totalRecords1:"+totalRecords);
		
		int pn = 1;
		if (pageNumber != 0) {
			pn = new Integer(pageNumber);
		}
		pageModel.setPages(totalRecords, pn);//设置总记录数和当前页面
		Map<String, Object> map = new HashMap<>();
		map.put("page", pageModel);
		map.put("list", list);
		return map;
	}
	
	@RequestMapping("/add")
	public String toAdd(SpaceApply data,HttpServletRequest request,Model model){
		/*int channelID = StrEdit.StrToInt(request.getParameter("channelID"));
		int parentId = StrEdit.StrToInt(request.getParameter("parentId"));
		
		String parentName = "";
		if(parentId>0){
			Map<String,Object> newMap1 = new HashMap<String,Object>();
			newMap1.put("task", "className");
	        newMap1.put("conId", parentId);
	        parentName = service.getStringValue(newMap1);
		}
        
		request.setAttribute("channelID", channelID);
		request.setAttribute("parentId", parentId);
		request.setAttribute("parentName", parentName);
		
		data.setVarValue7("无");*/
		
		Map<Long, String> typeMap = new HashMap<Long, String>();
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
		return "/front/spaceapply/add";
	}
	
	@RequestMapping("/save")
	@ResponseBody
	public String save(SpaceApply data,HttpServletRequest request,HttpServletResponse response){
		int conId = 0;
		String strTemp = "", strValue = "";
		
		int userId = 0;
		String userName = "";
		User sessionUser = ((User)request.getSession().getAttribute(Contants.SESSION_USER));
		if(sessionUser!=null){
			userId = sessionUser.getConId();
			userName = sessionUser.getVarValue1();
		}
		
		String spaceId = data.getVarValue2();
		if(spaceId.equals("")){
			spaceId = "0";
		}
		Space spacedata = spaceservice.findById(StrEdit.StrToInt(spaceId));
		String hourminutes1 = spacedata.getVarValue11();
		String hourminutes2 = spacedata.getVarValue12();
		
		String startDate = StrEdit.dateToStrLong(data.getDateValue1());
		String endDate = StrEdit.dateToStrLong(data.getDateValue2());
		int hour1 = StrEdit.getHour(data.getDateValue1());
		int minute1 = StrEdit.getMinute(data.getDateValue1());
		int hour2 = StrEdit.getHour(data.getDateValue2());
		int minute2 = StrEdit.getMinute(data.getDateValue2());
		
		if(!StrEdit.compareHM(hour1+":"+minute1, hourminutes1, hourminutes2)){
			strValue = "该时间段内不可用，可预定时间范围："+hourminutes1+"至"+hourminutes2;
			strTemp = "{\"status\":\"0\",\"errors\":\""+strValue+"\"}";
        	return strTemp;
		}
		
		if(!StrEdit.compareHM(hour2+":"+minute2, hourminutes1, hourminutes2)){
			strValue = "该时间段内不可用，可预定时间范围："+hourminutes1+"至"+hourminutes2;
			strTemp = "{\"status\":\"0\",\"errors\":\""+strValue+"\"}";
        	return strTemp;
		}
		
		if(StrEdit.compareDate(startDate, endDate)!=1){
			strValue = "开始时间必须比结束时间早";
			strTemp = "{\"status\":\"0\",\"errors\":\""+strValue+"\"}";
        	return strTemp;
        }
		
		Map<String,Object> newMap = new HashMap<String,Object>();//判断该时间段内的会议室是否可用，不包含已经选择的会议室
		newMap.put("task", "checkMeeting");
        newMap.put("meetingRoomId", spaceId);
        newMap.put("meetingId", conId);
        newMap.put("startDate", startDate);
        newMap.put("endDate", endDate);
        int isExist = service.getIntValue(newMap);
        if(isExist>0){
        	strValue = "该时间段内不可用";
			strTemp = "{\"status\":\"0\",\"errors\":\""+strValue+"\"}";
        	return strTemp;
        }
		
		if(spaceId.indexOf(",")==-1){//选择一个会议室才判断是否收费，如果不收费则直接申请成功，其他情况走审核流程
			Map<String,Object> newMap1 = new HashMap<String,Object>();
			newMap1.put("task", "isFree");
			newMap1.put("conId", spaceId);
	        String isFree = spaceservice.getStringValue(newMap1);
	        if(isFree.equals("Y")){
	        	data.setVarValue6("4");
	        }else{
	        	/*Map<String,Object> newMap2 = new HashMap<String,Object>();
	        	newMap2.put("task", "price");
	        	newMap2.put("conId", spaceId);
		        String price = spaceservice.getStringValue(newMap2);
		        long hours = StrEdit.getDateHours(data.getDateValue1(), data.getDateValue2());
		        double amount = StrEdit.StrToDouble(price) * hours;
		        data.setDecValue1(price);
		        data.setDecValue2(String.valueOf(amount));*/
	        	Double standard = StrEdit.StrToDouble(spacedata.getVarValue4());
	        	Double unitePrice = StrEdit.StrToDouble(spacedata.getDecValue1());
		        Long totalMinute = StrEdit.getDateMinutes(startDate,endDate);
		        Double totalAmount = 0.0;
				if(standard>0 && unitePrice>0){
					double times = totalMinute/standard;//总分钟除以收费标准分钟，得到次数
					double cishu = Math.ceil(times);//有小数+1，没满一次算一次
					totalAmount = unitePrice*cishu;
				}
		        data.setDecValue1(String.valueOf(unitePrice));
		        data.setDecValue2(String.valueOf(totalAmount));
	        }
		}
        
		try{
			service.save(data);
			conId = data.getConId();
		}catch(Exception e){
			conId = 0;
			System.out.println("save插入数据失败" + e.getLocalizedMessage());
		}
		
		if(conId>0){
			
			String attachIds=StrEdit.StrChkNull(request.getParameter("attachIds"));//附件
			if(!attachIds.equals("")){
				String[] arrAttachIds = attachIds.split("\\,");
				for(int m=0;m<arrAttachIds.length;m++){
					boolean boolValue = attach.update(StrEdit.StrToInt(arrAttachIds[m]),conId);
					if(!boolValue){
						System.out.println("附件信息ID更新失败："+arrAttachIds[m]);
					}
				}
			}
			
			strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"添加失败\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/update")
	@ResponseBody
	public String update(SpaceApply data,HttpServletRequest request){
		int conId = data.getConId();
		String strTemp = "", strValue = "";
		
		int userId = 0;
		String userName = "";
		User sessionUser = ((User)request.getSession().getAttribute(Contants.SESSION_USER));
		if(sessionUser!=null){
			userId = sessionUser.getConId();
			userName = sessionUser.getVarValue1();
		}
		
		String spaceId = data.getVarValue2();
		if(spaceId.equals("")){
			spaceId = "0";
		}
		Space spacedata = spaceservice.findById(StrEdit.StrToInt(spaceId));
		String hourminutes1 = spacedata.getVarValue11();
		String hourminutes2 = spacedata.getVarValue12();
		
		String startDate = StrEdit.dateToStrLong(data.getDateValue1());
		String endDate = StrEdit.dateToStrLong(data.getDateValue2());
		int hour1 = StrEdit.getHour(data.getDateValue1());
		int minute1 = StrEdit.getMinute(data.getDateValue1());
		int hour2 = StrEdit.getHour(data.getDateValue2());
		int minute2 = StrEdit.getMinute(data.getDateValue2());
		
		if(!StrEdit.compareHM(hour1+":"+minute1, hourminutes1, hourminutes2)){
			strValue = "该时间段内不可用，可预定时间范围："+hourminutes1+"至"+hourminutes2;
			strTemp = "{\"status\":\"0\",\"errors\":\""+strValue+"\"}";
        	return strTemp;
		}
		
		if(!StrEdit.compareHM(hour2+":"+minute2, hourminutes1, hourminutes2)){
			strValue = "该时间段内不可用，可预定时间范围："+hourminutes1+"至"+hourminutes2;
			strTemp = "{\"status\":\"0\",\"errors\":\""+strValue+"\"}";
        	return strTemp;
		}
		
		if(StrEdit.compareDate(startDate, endDate)!=1){
			strValue = "开始时间必须比结束时间早";
			strTemp = "{\"status\":\"0\",\"errors\":\""+strValue+"\"}";
        	return strTemp;
        }
		
		Map<String,Object> newMap = new HashMap<String,Object>();//判断该时间段内的会议室是否可用，不包含已经选择的会议室
		newMap.put("task", "checkMeeting");
        newMap.put("meetingRoomId", spaceId);
        newMap.put("meetingId", conId);
        newMap.put("startDate", startDate);
        newMap.put("endDate", endDate);
        int isExist = service.getIntValue(newMap);
        if(isExist>0){
        	strValue = "该时间段内不可用";
			strTemp = "{\"status\":\"0\",\"errors\":\""+strValue+"\"}";
        	return strTemp;
        }
		
		if(spaceId.indexOf(",")==-1){//选择一个会议室才判断是否收费，如果不收费则直接申请成功，其他情况走审核流程
			Map<String,Object> newMap1 = new HashMap<String,Object>();
			newMap1.put("task", "isFree");
			newMap1.put("conId", spaceId);
	        String isFree = spaceservice.getStringValue(newMap1);
	        if(isFree.equals("Y")){
	        	data.setVarValue6("4");
	        }else{
	        	/*Map<String,Object> newMap2 = new HashMap<String,Object>();
	        	newMap2.put("task", "price");
	        	newMap2.put("conId", meetingRoomIds);
		        String price = meetingroom.getStringValue(newMap2);
		        long hours = StrEdit.getDateHours(data.getDateValue1(), data.getDateValue2());
		        double amount = StrEdit.StrToDouble(price) * hours;
		        data.setDecValue1(price);
		        data.setDecValue2(String.valueOf(amount));*/
	        	Double standard = StrEdit.StrToDouble(spacedata.getVarValue4());
	        	Double unitePrice = StrEdit.StrToDouble(spacedata.getDecValue1());
		        Long totalMinute = StrEdit.getDateMinutes(startDate,endDate);
		        Double totalAmount = 0.0;
				if(standard>0 && unitePrice>0){
					double times = totalMinute/standard;//总分钟除以收费标准分钟，得到次数
					double cishu = Math.ceil(times);//有小数+1，没满一次算一次
					totalAmount = unitePrice*cishu;
				}
		        data.setDecValue1(String.valueOf(unitePrice));
		        data.setDecValue2(String.valueOf(totalAmount));
	        }
		}
		
		boolean boolValue = service.update(data);
		if(boolValue){
			
			strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"更新失败\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/getById")
	public String getById(int id,HttpServletRequest request,Model model){
		Map<Long, String> typeMap = new HashMap<Long, String>();
        
        List<Attachment> attachList1 = attach.list(1021,id);
        List<Attachment> attachList2 = attach.list(1022,id);
        
        model.addAttribute("data", service.findById(id));
        model.addAttribute("typeMap", typeMap);
        model.addAttribute("attachList1", attachList1);
        model.addAttribute("attachList2", attachList2);
        
		return "/front/spaceapply/add";
	}
	
	@RequestMapping("/getByIdAudit")
	public String getByIdAudit(int id,HttpServletRequest request,Model model){
		Map<Long, String> typeMap = new HashMap<Long, String>();
        
        List<Attachment> attachList1001 = attach.list(1001,id);
        
        model.addAttribute("data", service.findById(id));
        model.addAttribute("typeMap", typeMap);
        model.addAttribute("attachList1001", attachList1001);
        
		return "/front/spaceapply/auditadd";
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public String delete(int id,HttpServletRequest request){
		String strTemp = "";
		boolean boolValue = service.delete(id);
		if(boolValue){
			strTemp = "{\"status\":\"1\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"删除失败\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/audit")
	@ResponseBody
	public String audit(SpaceApply data,HttpServletRequest request){
		String strTemp = "";
		boolean boolValue = false;
		String task = StrEdit.StrChkNull(request.getParameter("task"));
		Date currTime = new Date();
		
		if(task.equals("audit")){
			int status = StrEdit.StrToInt(request.getParameter("status"));
			String selectids = StrEdit.StrChkNull(request.getParameter("selectids"));
			String[] arrParameter = selectids.split("\\,");//如果不包含,则直接返回原字符的一维数组
	    	if (arrParameter!=null){
				for(int i=0;i<arrParameter.length;i++){
					Map<String,Object> newMap = new HashMap<String,Object>();
					newMap.put("task", task);
					newMap.put("varValue6", status);
					newMap.put("updateTime", currTime);
			        newMap.put("conId", StrEdit.StrToInt(arrParameter[i]));
			        boolValue = service.updateByMap(newMap);
				}
			}
		}
		if(task.equals("auditpass")){
			int status = StrEdit.StrToInt(request.getParameter("status"));
			String selectids = StrEdit.StrChkNull(request.getParameter("selectids"));
			String[] arrParameter = selectids.split("\\,");//如果不包含,则直接返回原字符的一维数组
	    	if (arrParameter!=null){
				for(int i=0;i<arrParameter.length;i++){
					Map<String,Object> newMap = new HashMap<String,Object>();
					newMap.put("task", task);
					newMap.put("varValue6", status);
					newMap.put("updateTime", currTime);
			        newMap.put("conId", StrEdit.StrToInt(arrParameter[i]));
			        boolValue = service.updateByMap(newMap);
				}
			}
		}
		if(task.equals("auditreturn")){
			int status = StrEdit.StrToInt(request.getParameter("status"));
			String reason = StrEdit.StrChkNull(request.getParameter("reason"));
			String selectids = StrEdit.StrChkNull(request.getParameter("selectids"));
			String[] arrParameter = selectids.split("\\,");//如果不包含,则直接返回原字符的一维数组
	    	if (arrParameter!=null){
				for(int i=0;i<arrParameter.length;i++){
					Map<String,Object> newMap = new HashMap<String,Object>();
					newMap.put("task", task);
					newMap.put("varValue6", status);
					newMap.put("varValue7", reason);
					newMap.put("updateTime", currTime);
			        newMap.put("conId", StrEdit.StrToInt(arrParameter[i]));
			        boolValue = service.updateByMap(newMap);
				}
			}
		}
		if(task.equals("auditpay")){
			int status = StrEdit.StrToInt(request.getParameter("status"));
			String payType = StrEdit.StrChkNull(request.getParameter("payType"));
			String id = StrEdit.StrChkNull(request.getParameter("id"));
			Map<String,Object> newMap = new HashMap<String,Object>();
			newMap.put("task", task);
			newMap.put("varValue5", status);
			newMap.put("varValue20", payType);
			newMap.put("updateTime", currTime);
	        newMap.put("conId", id);
	        boolValue = service.updateByMap(newMap);
		}
		if(boolValue){
			strTemp = "{\"status\":\"1\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"更新失败\"}";
		}
		
		return strTemp;
	}
	
}
