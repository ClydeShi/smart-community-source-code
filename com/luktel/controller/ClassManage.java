package com.luktel.controller;

import java.sql.*;

import com.util.common.StrEdit;
import com.util.common.DBConn2;

public class ClassManage {
    private DBConn2 mdb = null;
    private ResultSet rs=null;
    
    private int userId = 0;
    private int userCat = 0;
    private String userName = "";
    
    private String strValue1 = "";
    
    public ClassManage() {
    	mdb = new DBConn2();
    }
    
    public int getUserCat() {
		return userCat;
	}

	public void setUserCat(int userCat) {
		this.userCat = userCat;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getStrValue1() {
		return strValue1;
	}

	public void setStrValue1(String strValue1) {
		this.strValue1 = strValue1;
	}
	
    public int createNew(int ChannelID,int ParentID,String className,String varValue8,String text1,String addTime) throws Exception {
    	
    	int flag = 0;
    	String strTemp = "未知";
    	String varValue1 = "", varValue2 = "", varValue3 = "", varValue4 = "", varValue5 = "", varValue6 = "", varValue7 = "";
    	
    	System.out.println("------------------------------开始添加------------------------------");
    	
    	if(ParentID<0 || className.equals("")){
    		flag = 1001;
    		strTemp = "参数错误";
    		System.out.println(strTemp);
    		this.setStrValue1(strTemp);
    		return flag;
    	}
    	
    	Connection conn=null;
		Statement state=null;
		conn = mdb.getNewConnection();
		conn.setAutoCommit(false);
		
		Connection con = null;
		Statement stmt = null;
		
		try{
			
			state = conn.createStatement();
			
			con = mdb.getNewConnection();
	    	stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
	    	//Statement stmt2 = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
	    	
	    	String sql = "", ParentName = "", ParentPath = "", arrChildID = "", ParentDir = "", ClassDir = "";
	    	int i = 0, MaxClassID = 0, MaxRootID = 0, Child = 0, PrevOrderID = 0, PrevID = 0, Depth = 0;
	    	int RootID = 0 , ParentDepth = 0;
	    	
	    	//判断同一频道,同一上级目录,是否重名
	    	sql = "select * from deptmentconn where ChannelID="+ChannelID+" and ParentID="+ParentID+" AND ClassName='"+className+"'";
	    	rs = mdb.executeQuery(sql);
	    	if(rs.next()){
	    	  if(ParentID == 0){
	    		  flag = 1001;
		    	  strTemp = "已经存在一级栏目："+className+"";
		          System.out.println(strTemp);
		    	  this.setStrValue1(strTemp);
		    	  return flag;
	    	  }else{
	    		  flag = 1001;
		    	  strTemp = "已经存在子栏目："+className+"";
		          System.out.println(strTemp);
		    	  this.setStrValue1(strTemp);
		    	  return flag;
	    	  }
	    	}
	    	rs.close();
	    	
	    	//取得最大id
	    	int ClassID = 0;
	    	sql = "select max(ClassID) as maxClassID from deptmentconn";
	    	//sql = "select max(ClassID) as maxClassID from deptmentconn where ChannelID="+ChannelID+"";
	    	rs = mdb.executeQuery(sql);
	    	if(rs.next()){
	    	  MaxClassID = rs.getInt("maxClassID");
	    	  //if(MaxClassID == null)
	    	  //MaxClassID = 0;
	    	}
	    	ClassID = MaxClassID + 1;
	    	rs.close();
	    	
	    	//判断ID是否使用
	    	sql = "select * from deptmentconn where ClassID="+ClassID+" and ChannelID="+ChannelID+"";
	    	rs = mdb.executeQuery(sql);
	    	if(rs.next()){
	    		flag = 1001;
	    		strTemp = "ID已经被使用："+ClassID+"";
	    		System.out.println(strTemp);
	    		this.setStrValue1(strTemp);
	    		return flag;
	    	}
	    	rs.close();
	    	
	    	//取得最大根目录id,用作排序,所有同一个栏目下的RootID相同
	    	sql = "select max(RootID) as maxRootID from deptmentconn where ChannelID="+ChannelID+"";
	    	rs = mdb.executeQuery(sql);
	    	if(rs.next()){
	    	  MaxRootID = rs.getInt("maxRootID");
	    	  //if(MaxRootID == null)
	    	  //MaxRootID = 0;
	    	}

	    	RootID = MaxRootID + 1;
	    	rs.close();
	    	//System.out.println("最大根目录id:" + ClassID);
	    	
	    	//添加子栏目
	    	if(ParentID>0){
	    	    sql = "Select * from deptmentconn Where ClassID="+ParentID+"";//判断上级目录是否存在
	    	    rs = mdb.executeQuery(sql);
	    		if(rs.next()){
	    		    RootID = rs.getInt("RootID");
	    	        ParentName = rs.getString("ClassName");
	    	        ParentDepth = rs.getInt("Depth");
	    	        ParentPath = rs.getString("ParentPath") + "," + rs.getInt("ClassID");    //得到此栏目的父级栏目路径
	    	        Child = rs.getInt("Child");
	    	        arrChildID = rs.getString("arrChildID") + "," + ClassID;
	    	        ClassDir = rs.getString("ClassDir");
	    	        ParentDir = rs.getString("ParentDir") + rs.getString("ClassDir") + "/";  //取出上级的父目录和上级目录作为当前栏目的父目录
	    			
	    			//更新本栏目的所有上级栏目的子栏目ID数组
	    			sql = "select ClassID,arrChildID from deptmentconn where ClassID in (" + ParentPath + ")";
	    	        ResultSet rs2 = stmt.executeQuery(sql);
	    	        while(rs2.next()){
	    	        	sql = "update deptmentconn set arrChildID='" + rs2.getString("arrChildID") + "," + ClassID + "' where ClassID=" + rs2.getInt("ClassID");
	    	        	state.execute(sql);
	    	        	//System.out.println(sql);
	    	        	//stmt2.executeUpdate("update deptmentconn set arrChildID='" + rs2.getString("arrChildID") + "," + ClassID + "' where ClassID=" + rs2.getInt("ClassID"));
	    			}
	    			rs2.close();
	    			//stmt2.close();
	    			
	    			if(Child > 0){//存在子栏目,获取同栏目最大的序号
	    			
	    	            //得到父栏目的所有子栏目中最后一个栏目的OrderID,增加1作为序号id
	    				ResultSet rs3 = stmt.executeQuery("select max(OrderID) as maxOrderID from deptmentconn where ClassID in ( " + arrChildID + ")");
	    				if(rs3.next()){
	    				    PrevOrderID = rs3.getInt("maxOrderID");
	    				}
	    	            rs3.close();
	    	            
	    	            //得到本栏目的上一个栏目ID
	    				rs3 = stmt.executeQuery("select ClassID from deptmentconn where ChannelID=" + ChannelID + " and ParentID=" + ParentID + " order by OrderID desc limit 1");
	    				if(rs3.next()){
	    				    PrevID = rs3.getInt("ClassID");
	    				}
	    	            rs3.close();
	    				
	    			}else{//不存在子栏目
	    				
	    				PrevOrderID = rs.getInt("OrderID");
	    	            PrevID = 0;
	    	            
	    			}
	    			
	    		}else{
	    			flag = 1001;
	    			strTemp = "所属栏目已经被删除！";
	    			System.out.println(strTemp);
	        		this.setStrValue1(strTemp);
	    			return flag;
	    		}
	    		
	    	}else{//一级栏目
	    		    
	    			if(MaxRootID > 0){//如果存在最大根目录,则取其ClassID作为PrevID
	    				ResultSet rs4 = stmt.executeQuery("select ClassID from deptmentconn where ChannelID=" + ChannelID + " and RootID=" + MaxRootID + " and Depth=0");
	    				if(rs4.next()){
	    				  PrevID = rs4.getInt("ClassID");
	    				}
	    				rs4.close();
	    			}else{
	    			    PrevID = 0;
	    			}
	    			
	    	        PrevOrderID = 0;
	    	        ParentPath = "0";
	    	        ParentDir = "";
	    	}

	    	if(ParentID > 0){
	    	    Depth = ParentDepth + 1;
	    	}else{
	    	    Depth = 0;
	    	}
	    	
	    	sql = "insert into deptmentconn(ChannelID,ClassID,RootID,ParentID,Depth,ParentPath,OrderID,Child,PrevID,NextID,arrChildID,ClassName,ClassDir,ParentDir,VARVALUE1,VARVALUE2,VARVALUE3,VARVALUE4,VARVALUE5,VARVALUE6,VARVALUE7,VARVALUE8,TEXT1,ADDTIME,STATUS)";
	    	sql = sql + "values("+ChannelID+","+ClassID+","+RootID+","+ParentID+","+Depth+",'"+ParentPath+"',"+PrevOrderID+",0,"+PrevID+",0,'"+ClassID+"','"+className+"','"+ClassDir+"','"+ParentDir+"','"+varValue1+"','"+varValue2+"','"+varValue3+"','"+varValue4+"','"+varValue5+"','"+varValue6+"','"+varValue7+"','"+varValue8+"','"+text1+"','"+addTime+"',10)";
	    	state.execute(sql);
	    	//i = stmt.executeUpdate(sql);
	    	
	    	//更新与本栏目同一父栏目的上一个栏目的"NextID"字段值
	    	if(PrevID > 0){
	    		sql = "update deptmentconn set NextID=" + ClassID + " where ClassID=" + PrevID;
	    	    stmt.executeUpdate(sql);
	    	}

	    	if(ParentID > 0){
	    	    //更新其父类的子栏目数
	    		sql = "update deptmentconn set Child=Child+1 where ClassID=" + ParentID;
	    		state.execute(sql);
	    	    //stmt.executeUpdate(sql);
	    		//更新该栏目排序,以及大于本分类下的栏目排序序号,即上级栏目的同类栏目,将他们序号加大１,排到该子栏目后面
	    	    sql = "update deptmentconn set OrderID=OrderID+1 where ChannelID=" + ChannelID + " and RootID=" + RootID + " and OrderID>" + PrevOrderID;
	    	    state.execute(sql);
	    		//stmt.executeUpdate(sql);
	    		sql = "update deptmentconn set OrderID=" + PrevOrderID + "+1 where ClassID=" + ClassID;
	    		state.execute(sql);
	    		//stmt.executeUpdate(sql);
	    	}
	    	
	    	conn.commit();  //数据库操作最终提交给数据库
			conn.setAutoCommit(true);
	    	
	    	flag = 1000;
    		strTemp = "ok";
    		System.out.println("添加成功");
    		this.setStrValue1(String.valueOf(ClassID));
			
		}catch(Exception ex){
			flag = 1001;
    		strTemp = "添加失败，出现异常";
    		this.setStrValue1(strTemp);
			conn.rollback();
			System.out.println(strTemp);
			System.out.println(ex.getMessage());
		}finally{
			if(stmt!=null){stmt.close();}
			if(con!=null){con.close();}
			
			if(state!=null){state.close();}
			if(conn!=null){conn.close();}
			mdb.close();
		}
		
        return flag;
    }
    
    public String modifyInfo(int ClassID,String ClassName,int ClassType,String ClassDir,String LinkUrl,String ClassMemo) throws Exception {
    	
    	String reValue = "", sql = "";
    	int i = 0;
    	
    	if(ClassName.trim().equals("")){
    		reValue = "<li>栏目名称不能为空！</li>";
    		return reValue;
    	}
    	
    	sql = "Select * from memberconn Where ClassID="+ClassID+"";
    	rs = mdb.executeQuery(sql);
    	if(!rs.next()){
    		reValue = "<li>找不到指定的栏目！</li>";
    		return reValue;
    	}
    	rs.close();
    	
    	sql = "update memberconn set ClassName = '"+ClassName+"',ClassType = "+ClassType+",ClassDir = '"+ClassDir+"',LinkUrl = '"+LinkUrl+"',ClassMemo = '"+ClassMemo+"' where ClassID = "+ClassID;
    	i = mdb.executeUpdate(sql);
    	if(i > 0){
    		reValue = "ok";
    	}else{
    		reValue = "修改失败";
    	}
    	
    	mdb.close();
        
        return reValue;
    }
    
    public int delInfo(int ClassID) throws Exception {
    	
    	int flag = 0;
    	String strTemp = "未知";
    	
    	Connection conn=null;
		Statement state=null;
		conn = mdb.getNewConnection();
		conn.setAutoCommit(false);
		
		Connection con = null;
		Statement stmt = null;
		
		try{
			
			state = conn.createStatement();
			
			con = mdb.getNewConnection();
	    	stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
			
			String reValue = "", sql = "", ParentPath = "", arrChildID = "";
	    	int NextID = 0, i = 0, ParentID = 0, PrevID = 0, Depth = 0, OrderID = 0, RootID = 0;
	    	
	    	if(ClassID == 0){
	    		flag = 1001;
	    		strTemp = "参数错误";
	    		System.out.println(strTemp);
	    		this.setStrValue1(strTemp);
	    		return flag;
	    	}
	    	
	    	sql = "select * from deptmentconn where ClassID="+ClassID+"";System.out.println(sql);
	    	rs = mdb.executeQuery(sql);
	    	if(rs.next()){
	    		
	    		PrevID = rs.getInt("PrevID");
	    	    NextID = rs.getInt("NextID");
	    	    arrChildID = rs.getString("arrChildID");
	    	    RootID = rs.getInt("RootID");
	    	    OrderID = rs.getInt("OrderID");
	    	    Depth = rs.getInt("Depth");
	    	    ParentID = rs.getInt("ParentID");
	    	    ParentPath = rs.getString("ParentPath");
	    	    
	    	    if(Depth > 0){//如果删除的栏目深度大于0,即存在上级目录,则把上级目录的子目录数减1
	    	    	sql = "update deptmentconn set Child=Child-1 where ClassID=" + ParentID;
	    	    	state.execute(sql);
		        	
	    	    	//stmt.executeUpdate("update memberconn set Child=Child-1 where ClassID=" + ParentID);
	    	    	
	    	    	//更新此栏目的原来所有上级栏目的子栏目ID数组
	    			sql = "select ClassID,arrChildID from deptmentconn where ClassID in (" + ParentPath + ")";
	    	        ResultSet rs2 = stmt.executeQuery(sql);
	    	        while(rs2.next()){
	    	        	sql = "update deptmentconn set arrChildID='" + removeID(rs2.getString("arrChildID"), arrChildID) + "' where ClassID=" + rs2.getInt("ClassID");
	    	        	state.execute(sql);
	    	        	
	    	        	//stmt2.executeUpdate("update deptmentconn set arrChildID='" + removeID(rs2.getString("arrChildID"), arrChildID) + "' where ClassID=" + rs2.getInt("ClassID"));
	    			}
	    			rs2.close();
	    			//stmt2.close();
	    			
	    			//更新与此栏目同根且排序在其之下的栏目 UBound(Split(arrChildID, ","))
	    			sql = "update deptmentconn set OrderID=OrderID-" + (arrChildID.split("\\,").length - 1) + " + 1 where RootID=" + RootID + " and OrderID>" + OrderID;
		        	state.execute(sql);
		        	
	    			//stmt.executeUpdate("update deptmentconn set OrderID=OrderID-" + (arrChildID.split("\\,").length - 1) + 1 + " where RootID=" + RootID + " and OrderID>" + OrderID);
	    	    }
	    	    
	    	    //修改上一栏目的NextID和下一栏目的PrevID
	    	    if(PrevID > 0){
	    	    	sql = "update deptmentconn set NextID=" + NextID + " where ClassID=" + PrevID;
		    		state.execute(sql);
		    		
	    	    	//stmt.executeUpdate("update deptmentconn set NextID=" + NextID + " where ClassID=" + PrevID);
	    	    }
	    	    if(NextID > 0){
	    	    	sql = "update deptmentconn set PrevID=" + PrevID + " where ClassID=" + NextID;
		    		state.execute(sql);
		    		
	    	    	//stmt.executeUpdate("update deptmentconn set PrevID=" + PrevID + " where ClassID=" + NextID);
	    	    }
	    	    
	    	    //删除本栏目（包括子栏目）
	    	    sql = "delete from deptmentconn where ClassID in (" + arrChildID + ")";
	    		state.execute(sql);
	    		
	    		conn.commit();  //数据库操作最终提交给数据库
				conn.setAutoCommit(true);
		    	
		    	flag = 1000;
	    		strTemp = "删除成功";
	    		System.out.println(strTemp);
	    		this.setStrValue1(strTemp);
	    		
	    	    //i = stmt.executeUpdate("delete from deptmentconn where ClassID in (" + arrChildID + ")");
	    		
	    	}else{
	    		flag = 1001;
	    		strTemp = "classId不存在";
	    		this.setStrValue1(strTemp);
	    		return flag;
	    	}
			
		}catch(Exception ex){
			flag = 1001;
    		strTemp = "删除失败，出现异常";
    		this.setStrValue1(strTemp);
			conn.rollback();
			System.out.println(strTemp);
			System.out.println(ex.getMessage());
		}finally{
			if(stmt!=null){stmt.close();}
			if(con!=null){con.close();}
			
			if(state!=null){state.close();}
			if(conn!=null){conn.close();}
			mdb.close();
		}
		
		return flag;
    }
    
    public void doPatch() throws Exception {
    	
    	Connection con = mdb.getNewConnection();
    	Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
    	
    	//更新一级栏目
    	int PrevID = 0, ClassID = 0 , ClassType = 0;
    	String ParentDir = "", ClassDir = "", sql = "";
    	sql = "select ClassID,RootID,OrderID,Depth,ParentID,ParentPath,Child,arrChildID,PrevID,NextID,ClassType,ParentDir,ClassDir ";
    	sql = sql + "from memberconn where ParentID=0 order by RootID";
    	ResultSet rs = mdb.executeQuery(sql);
    	while(rs.next()){
    		ClassID = rs.getInt("ClassID");
    		ClassType = rs.getInt("ClassType");
    		ClassDir = rs.getString("ClassDir");

    		if(PrevID != ClassID && PrevID > 0){
    			stmt.executeUpdate("update memberconn set NextID=" + ClassID + " where ClassID=" + PrevID + "");//更新上一条记录的NextID为当前ClassID
    		}

    		if(ClassType == 1){
        		ParentDir = "/";
        	}

    		String sql2 = "update memberconn set OrderID=0,Depth=0,ParentPath=0,PrevID="+PrevID+",NextID=0,arrChildID='"+ClassID+"',ParentDir='"+ParentDir+"' where ClassID=" + ClassID + "";
    		//System.out.println("更新一级栏目数据:"+sql2);
    		stmt.executeUpdate(sql2);

    		PrevID = ClassID;//当前ClassID作为下一次循环的PrevID使用

    		int iOrderID = 1;

    		UpdateClass(ClassID, 1, "0", "/" + ClassDir + "/", iOrderID);//当前栏目id作为ParentID,取出其子栏目

    	}
    	
    	stmt.close();
    	con.close();
    	mdb.close();

    	//System.out.println("修复栏目结构成功！");
    	
    }
    
    public int iOrderID = 1;
    public void UpdateClass(int iParentID, int iDepth, String sParentPath, String sParentDir, int mOrderID) throws Exception {

    	iOrderID = mOrderID;

    	//System.out.println("____________________________________");
    	//System.out.println("更新频道:"+ChannelID+",栏目:"+iParentID+"的下级栏目数据");
    	
    	Connection con = mdb.getNewConnection();
        Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
        Statement stmt2 = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
    	Statement stmtnew = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
    	
    	int PrevID = 0, ClassID = 0, ClassType = 0;
    	String ParentDir = "", ClassDir = "", ParentPath = "", arrChildID = "",sql = "";
    	ParentPath = sParentPath + "," + iParentID;
    	//System.out.println("TOP ParentPath:"+ParentPath);
    	//System.out.println("iOrderID:"+iOrderID);

    	int total = 0;
    	sql = "select count(*) as total from memberconn where ParentID=" + iParentID + "";
    	ResultSet rs = stmt.executeQuery(sql);
    	if(rs.next()){
    		total = rs.getInt("total");
    	}
    	rs.close();
    	//System.out.println(iParentID+"的下级栏目共有记录:"+total);
    	
    	if(total > 0){//查询是否有子栏目,更新上级栏目Child
    		stmt.executeUpdate("update memberconn set Child=" + total + " where ClassID=" + iParentID + "");
    	}else{
    		stmt.executeUpdate("update memberconn set Child=0 where ClassID=" + iParentID + "");

    	}
    	
    	PrevID = 0;
    	
    	sql = "select ClassID,RootID,OrderID,Depth,ParentID,ParentPath,Child,arrChildID,PrevID,NextID,ClassType,ParentDir,ClassDir ";
    	sql = sql + "from memberconn where ParentID=" + iParentID + " order by OrderID";
    	//System.out.println(sql);  1
    	ResultSet rs2 = stmtnew.executeQuery(sql);
    	while(rs2.next()){
    		//System.out.println(sql);  2  递归调用时,1的sql随着参数变化,2的的sql保持开始的

    		ClassID = rs2.getInt("ClassID");
    		ClassType = rs2.getInt("ClassType");
    		ClassDir = rs2.getString("ClassDir");

    		//System.out.println("ParentPath:"+ParentPath);
    		//在while里递归调用UpdateClass时,ParentPath,sql等还是保留最开始的值,相当于调用是另外一个线程
    		//在里面执行完递归调用后,返回到原先的继续执行,这里的ParentPath只保留开始传来的值
    		//System.out.println("栏目 ClassID:"+ClassID);
    		//System.out.println("iOrderID:"+iOrderID);
    		
    		//更新当前子栏目的所有上级栏目的arrChildID,stmt循环使用,在这里不能关闭
    		ResultSet rs3 = stmt.executeQuery("select ClassID,arrChildID from memberconn where ClassID in (" + ParentPath + ")");		
    		while(rs3.next()){
    			int ClassID3 = rs3.getInt("ClassID");
    			arrChildID = rs3.getString("arrChildID");
    			String newChildID = arrChildID+","+ClassID;
    			//System.out.println("更新上级栏目"+ClassID3+"的arrChildID:"+newChildID);
    			stmt2.executeUpdate("update memberconn set arrChildID='"+newChildID+"' where ClassID=" + ClassID3 + "");
    		}

    		if(PrevID != ClassID && PrevID > 0){
    			stmt2.executeUpdate("update memberconn set NextID=" + ClassID + " where ClassID=" + PrevID + "");//更新上一条记录的NextID为当前ClassID
    		}

    		if(ClassType == 1){
        		ParentDir = sParentDir;
        		//System.out.println("ParentDir:"+ParentDir);
        	}

    		String sql2 = "update memberconn set OrderID="+iOrderID+",Depth="+iDepth+",ParentPath='"+ParentPath+"',PrevID="+PrevID+",NextID=0,";
    		sql2 = sql2 + "arrChildID='"+ClassID+"',ParentDir='"+ParentDir+"' where ClassID=" + ClassID + "";		
    		stmt2.executeUpdate(sql2);
    		//System.out.println("更新栏目:"+ClassID);

    		PrevID = ClassID;//当前ClassID作为下一次循环的PrevID使用

    		iOrderID = iOrderID + 1;

    		UpdateClass(ClassID, iDepth + 1, ParentPath, sParentDir + ClassDir + "/", iOrderID);
    		//递归调用更新下一级目录,在继续更新同级目录的下一个记录
    		
    	}
    	
    	stmt.close();
		stmt2.close();
    	stmtnew.close();
    	con.close();

    }
    
    /*
     * 从字符串中去除另外一个字符串里的数据,以","为分割符
     */
    public String removeID(String classID, String removeId){
    	String arrClassID3 = "", reValue = "";
    	int i = 0, j = 0;
    	boolean bFound = false;
    	
    	if(classID == null){
    		reValue = "";
    	}else if(classID.trim().equals(removeId.trim())){
    		reValue = "";
    	}else{
    		String[] arrClassID = classID.split("\\,");//源数组
    		
    		if(removeId.indexOf(",") != -1){//包含,的
    			String[] arrClassID2 = removeId.split("\\,");//被去除数组
    			
    			for(i = 0; i < arrClassID.length; i++){
    				bFound = false;
    				
    				for(j = 0; j < arrClassID2.length; j++){
    					if(arrClassID[i].equals(arrClassID2[j])){
    						bFound = true;
    						break;
    					}
    				}
    				if(!bFound){
    					if(arrClassID3.equals(""))
    						arrClassID3 = arrClassID[i];
    					else
    						arrClassID3 = arrClassID3 + "," + arrClassID[i];
    				}
    				
    			}
    			
    		}else{
    			
    			for(i = 0; i < arrClassID.length; i++){
    				if(!arrClassID[i].equals(removeId)){
    					if(arrClassID3.equals(""))
    						arrClassID3 = arrClassID[i];
    					else
    						arrClassID3 = arrClassID3 + "," + arrClassID[i];
    				}
    			}
    			
    			
    		}
    		
    		reValue = arrClassID3;
    		
    		
    	}
    	
    	return reValue;
    }
    
    public String getNext(String current) throws Exception{
    	int postion = current.length() - 1;
    	byte []barr = current.getBytes();
    	barr[postion] += 1;
    	return new String(barr);
    }
    
    public int getDepthByID(int id) throws Exception{
        int i=0;
        String sql = "select Depth from memberconn where ClassID="+id+"";
        try{
            rs = mdb.executeQuery(sql);
            if(rs.next()){
            	i = rs.getInt("Depth");
            }
        }
        catch(Exception e){
            System.out.println("UserManage getDepthByID() " +e.getLocalizedMessage());
        }
        finally{
            rs.close();
            mdb.close();
        }
        return i;
    }
    
    
    
}
