package com.luktel.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.luktel.model.Deptments;
import com.luktel.model.Attachment;
import com.luktel.service.DeptmentsService;
import com.luktel.service.AttachmentService;
import com.util.common.PageModel;
import com.util.common.StrEdit;

/*
 * @Controller是标记在类UserController上面的，所以类UserController就是一个SpringMVC Controller对象了
 * 使用@RequestMapping("/getAllUser")标记在Controller方法上，表示当请求/getAllUser的时候访问的是LoginController的getAllUser方法，
 * 该方法返回了一个包括Model和View的ModelAndView对象113323
 */

@Controller//@Controller注解标识一个控制器
@RequestMapping("/deptments")//如果@RequestMapping注解在类级别上，则表示相对路径，在方法级别上，则标记访问的路径
public class DeptmentsController{
	
	@Autowired
	private DeptmentsService service;
	
	@Autowired
	private AttachmentService attach;
	
	@RequestMapping("/list")
	public String list() {
		return "front/deptments/list";
	}
	
	@RequestMapping("/all")
	public String all(HttpServletRequest request) {
		int channelID = StrEdit.StrToInt(request.getParameter("channelID"));
		request.setAttribute("channelID", channelID);
		
		List<Deptments> list1 = new ArrayList<Deptments>();
		Map<String,Object> newMap1 = new HashMap<String,Object>();
		newMap1.put("task", "all");
        newMap1.put("channelID", channelID);
        list1 = service.getListByMap(newMap1);
		request.setAttribute("list1", list1);
		
		return "front/deptments/list";
	}
	
	@RequestMapping("/treeDeptment")
	@ResponseBody
	public String treeDeptment(HttpServletRequest request) {//带结果的json,用于deptmentselect
		StringBuffer strBuff = new StringBuffer("");
		strBuff.append("{");
		strBuff.append("\"categoryList\": [");
		
		int channelID = StrEdit.StrToInt(request.getParameter("channelID"));
		request.setAttribute("channelID", channelID);
		
		List<Deptments> list1 = new ArrayList<Deptments>();
		Map<String,Object> newMap1 = new HashMap<String,Object>();
		newMap1.put("task", "all");
        newMap1.put("channelID", channelID);
        list1 = service.getListByMap(newMap1);
        for(int m=0; m<list1.size(); m++) {
        	if(m>0){strBuff.append(",");}
        	Deptments temp = list1.get(m);
        	strBuff.append("{");
			strBuff.append("\"id\": \""+temp.getConId()+"\",");
			strBuff.append("\"pId\": \""+temp.getParentId()+"\",");
			strBuff.append("\"name\": \""+temp.getClassName()+"\"");
			//strBuff.append("\"open\": \"true\"");
			strBuff.append("}");
			
        	//strBuff.append("{ id:"+temp.getConId()+", pId:"+temp.getParentId()+", name:'"+temp.getClassName()+"', open:true}");//这种拼接的返回不是json
        }
        strBuff.append("],");
        strBuff.append("\"resultcode\": \"10000\",");
		strBuff.append("\"message\": \"接口通信成功\"");
		strBuff.append("}");
		//System.out.println("list:" + strBuff.toString());
        
        //String strTemp = "{\"status\":\"2\",\"id\":\"2\",\"name\":\"2\"}";
		
		return strBuff.toString();
	}
	
	@RequestMapping("/treeData")
	@ResponseBody
	public String treeData(HttpServletRequest request) {//返回部门json
		StringBuffer strBuff = new StringBuffer("");
		//strBuff.append("{");
		//strBuff.append("\"categoryList\": [");
		strBuff.append("[");
		
		int type = StrEdit.StrToInt(request.getParameter("type"));
		int channelID = StrEdit.StrToInt(request.getParameter("channelID"));
		
		List<Deptments> list1 = new ArrayList<Deptments>();
		Map<String,Object> newMap1 = new HashMap<String,Object>();
		newMap1.put("task", "all");
        newMap1.put("channelID", channelID);
        list1 = service.getListByMap(newMap1);
        for(int m=0; m<list1.size(); m++) {
        	if(m>0){strBuff.append(",");}
        	Deptments temp = list1.get(m);
        	strBuff.append("{");
        	if(type==3 || type==4){//人员多选才显示isParent，才会加载人员信息
        		strBuff.append("\"isParent\": \"true\",");
        	}
			strBuff.append("\"id\": \""+temp.getConId()+"\",");
			strBuff.append("\"pId\": \""+temp.getParentId()+"\",");
			strBuff.append("\"name\": \""+temp.getClassName()+"\"");
			//strBuff.append("\"open\": \"true\"");
			strBuff.append("}");
			
        	//strBuff.append("{ id:"+temp.getConId()+", pId:"+temp.getParentId()+", name:'"+temp.getClassName()+"', open:true}");//这种拼接的返回不是json
        }
        strBuff.append("]");
        //strBuff.append("],");
        //strBuff.append("\"resultcode\": \"10000\",");
		//strBuff.append("\"message\": \"接口通信成功\"");
		//strBuff.append("}");
		//System.out.println("list:" + strBuff.toString());
        
        //String strTemp = "{\"status\":\"2\",\"id\":\"2\",\"name\":\"2\"}";
		
		return strBuff.toString();
	}
	
	@RequestMapping("/treeDataPerson")
	@ResponseBody
	public String treeDataPerson(HttpServletRequest request) {//返回人员json
		StringBuffer strBuff = new StringBuffer("");
		strBuff.append("[");
		
		int deptId = StrEdit.StrToInt(request.getParameter("deptId"));
		int userType = StrEdit.StrToInt(request.getParameter("userType"));
		
		Map<String,Object> newMap1 = new HashMap<String,Object>();
		newMap1.put("task", "className");
        newMap1.put("conId", deptId);
        String deptName = service.getStringValue(newMap1);
		
		List<Deptments> list1 = new ArrayList<Deptments>();
		Map<String,Object> newMap2 = new HashMap<String,Object>();
		newMap2.put("task", "getUserByDeptId");
		newMap2.put("deptId", deptId);
		newMap2.put("userType", userType);//如果为0则不限制
        list1 = service.getListByMap(newMap2);
        for(int m=0; m<list1.size(); m++) {
        	if(m>0){strBuff.append(",");}
        	Deptments temp = list1.get(m);
        	strBuff.append("{");
			strBuff.append("\"id\": \"u_"+temp.getConId()+"\",");
			strBuff.append("\"pId\": \""+temp.getIntValue2()+"\",");//部门id
			strBuff.append("\"deptName\": \""+deptName+"\",");
			strBuff.append("\"name\": \""+temp.getVarValue3()+"\",");
			strBuff.append("\"mobile\": \""+temp.getVarValue8()+"\"");
			strBuff.append("}");
        }
        strBuff.append("]");
		//System.out.println("list:" + strBuff.toString());
		
		return strBuff.toString();
	}
	
	@RequestMapping("/deptmentselect")
	public String deptmentselect(HttpServletRequest request) {
		int channelID = StrEdit.StrToInt(request.getParameter("channelID"));
		String selectIds = StrEdit.StrChkNull(request.getParameter("selectIds"));
		request.setAttribute("channelID", channelID);
		request.setAttribute("selectIds", selectIds);
		return "front/deptments/deptmentselect";
	}
	
	@RequestMapping("/treeselect")
	public String treecheck(HttpServletRequest request) {
		int type = StrEdit.StrToInt(request.getParameter("type"));
		int channelID = StrEdit.StrToInt(request.getParameter("channelID"));
		int userType = StrEdit.StrToInt(request.getParameter("userType"));
		String selectIds = StrEdit.StrChkNull(request.getParameter("selectIds"));
		request.setAttribute("type", type);
		request.setAttribute("channelID", channelID);
		request.setAttribute("userType", userType);
		request.setAttribute("selectIds", selectIds);
		return "front/deptments/treeselect";
	}
	
	@RequestMapping("/getDepartmentById")
	@ResponseBody
	public String getDepartmentById(int id){
		String strTemp = "";
		Deptments data = service.findById(id);
		strTemp = "{\"status\":\"1\",\"id\":\""+id+"\",\"name\":\""+data.getVarValue1()+"\"}";
		return strTemp;
	}
	
	@RequestMapping("/get")
	@ResponseBody
	public Map<String, Object> getAll(PageModel<Deptments> pageModel, Deptments data, HttpServletRequest request, Model model) {
		
		//System.out.println("------------------get------------------");
		String keywords = StrEdit.StrChkNull(data.getKeywords());
		
		Map<String,Object> newMap = new HashMap<String,Object>();
		
		int pageNumber = StrEdit.StrToInt(request.getParameter("pageNumber"));
		int pageSize = StrEdit.StrToInt(request.getParameter("pageSize"));
		int startRow = 0;
        if(pageNumber!=0) {
        	startRow = (pageNumber - 1) * pageSize;
        }
        newMap.put("startRow", startRow);
        newMap.put("pageSize", pageSize);
		/*int pageStart = (pageNumber -1) * pageSize+1;//oracle分页
        int pageEnd = pageStart + pageSize -1;
        newMap.put("pageStart", pageStart);
        newMap.put("pageEnd", pageEnd);*/
        newMap.put("keywords", keywords);
		List<Deptments> list = service.pageList(newMap);
		int totalRecords = service.totalRecord(newMap);
		pageModel.setTotal(totalRecords);
		
		//Object jsonObject = JSONObject.toJSON(list);
		//System.out.println("jsonObject:"+jsonObject);
		//System.out.println("totalRecords1:"+totalRecords);
		
		int pn = 1;
		if (pageNumber != 0) {
			pn = new Integer(pageNumber);
		}
		pageModel.setPages(totalRecords, pn);//设置总记录数和当前页面
		Map<String, Object> map = new HashMap<>();
		map.put("page", pageModel);
		map.put("list", list);
		return map;
	}
	
	@RequestMapping("/add")
	public String toAdd(Deptments data,HttpServletRequest request,Model model){
		int channelID = StrEdit.StrToInt(request.getParameter("channelID"));
		int parentId = StrEdit.StrToInt(request.getParameter("parentId"));
		
		String parentName = "";
		if(parentId>0){
			Map<String,Object> newMap1 = new HashMap<String,Object>();
			newMap1.put("task", "className");
	        newMap1.put("conId", parentId);
	        parentName = service.getStringValue(newMap1);
		}
        
		request.setAttribute("channelID", channelID);
		request.setAttribute("parentId", parentId);
		request.setAttribute("parentName", parentName);
		
		Map<Long, String> typeMap = new HashMap<Long, String>();
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
		return "/front/deptments/add";
	}
	
	@RequestMapping("/save")
	@ResponseBody
	public String save(Deptments data,HttpServletRequest request,HttpServletResponse response){
		int conId = 0;
		String strTemp = "";
		System.out.println("--------------------------------");
		
		int channelID = StrEdit.StrToInt(request.getParameter("channelID"));
		int parentId = StrEdit.StrToInt(request.getParameter("parentId"));
		String className = StrEdit.StrChkNull(request.getParameter("className"));
		String text1 = StrEdit.StrChkNull(request.getParameter("text1"));
		String varValue8 = StrEdit.StrChkNull(request.getParameter("varValue8"));
		String addTime = StrEdit.StrChkNull(request.getParameter("addTime"));
		System.out.println("channelID"+channelID);
		System.out.println("parentId"+parentId);
		System.out.println("className"+className);
		System.out.println("text1"+text1);
		ClassManage deptments = new ClassManage();
		int flag = 0;
		try {
			flag = deptments.createNew(channelID,parentId,className,varValue8,text1,addTime);
		} catch (Exception e) {
			System.out.println("添加部门异常" + e.getMessage());
			e.printStackTrace();
		}
		String strValue = deptments.getStrValue1();
		if(flag==1000){
			
			strTemp = "{\"status\":\"1\",\"conId\":\""+Integer.parseInt(strValue)+"\"}";
		}else{
			strValue = "添加失败";
			strTemp = "{\"status\":\"0\",\"errors\":\""+strValue+"\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/update")
	@ResponseBody
	public String update(Deptments data,HttpServletRequest request){
		int conId = data.getConId();
		String strTemp = "";
		//Date currTime = new Date();
		//data.setUpdateTime(currTime);
		boolean boolValue = service.update(data);
		if(boolValue){
			strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"更新失败\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/getById")
	public String getById(int id,HttpServletRequest request,Model model){
		Map<Long, String> typeMap = new HashMap<Long, String>();
		Deptments data = service.findById(id);
		int channelID = data.getChannelID();
		request.setAttribute("channelID", channelID);
        
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
        
		return "/front/deptments/add";
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public String delete(int id,HttpServletRequest request){
		String strTemp = "";
		
		ClassManage deptments = new ClassManage();
		int flag = 0;
		try {
			flag = deptments.delInfo(id);
		} catch (Exception e) {
			System.out.println("删除部门异常" + e.getMessage());
			e.printStackTrace();
		}
		String strValue = deptments.getStrValue1();
		if(flag==1000){
			strTemp = "{\"status\":\"1\"}";
		}else{
			strValue = deptments.getStrValue1();
			strTemp = "{\"status\":\"0\",\"errors\":\""+strValue+"\"}";
		}
		
		return strTemp;
	}
	
}
