package com.luktel.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.luktel.model.Attachment;
import com.luktel.model.StorageOut;
import com.luktel.model.StorageOutDetail;
import com.luktel.model.User;
import com.luktel.service.AttachmentService;
import com.luktel.service.StorageOutDetailService;
import com.luktel.service.StorageOutService;
import com.luktel.service.StorageService;
import com.luktel.service.DictionaryService;
import com.util.common.Contants;
import com.util.common.DBConn2;
import com.util.common.PageModel;
import com.util.common.StrEdit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * @Controller是标记在类UserController上面的，所以类UserController就是一个SpringMVC Controller对象了
 * 使用@RequestMapping("/getAllUser")标记在Controller方法上，表示当请求/getAllUser的时候访问的是LoginController的getAllUser方法，
 * 该方法返回了一个包括Model和View的ModelAndView对象113323
 */

@Controller//@Controller注解标识一个控制器
@RequestMapping("/storageout")//如果@RequestMapping注解在类级别上，则表示相对路径，在方法级别上，则标记访问的路径
public class StorageOutController{
	
	private String sql = "";
    private DBConn2 mdb = new DBConn2();
    private ResultSet rs = null;
	private static Logger log = LoggerFactory.getLogger(StorageOutController.class);
	
	@Autowired
	private StorageOutService service;
	
	@Autowired
	private StorageOutDetailService detail;
	
	@Autowired
	private StorageService storage;
	
	@Autowired
	private AttachmentService attach;
	
	@Autowired
	private DictionaryService dictionary;
	
	@RequestMapping("/list")
	public String list(HttpServletRequest request) {
		String modelTitle = "", modelName = "", redirect = "";
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		if(channelId==1){
			modelName = "工具耗材领用列表";
			redirect = "front/storageout/list";
		}else if(channelId==5){
			modelTitle = "资产";
			modelName = "固定资产移交列表";
			redirect = "front/storageout/listzc";
		}else if(channelId==6){
			modelTitle = "资产";
			modelName = "固定资产维修申请列表";
			redirect = "front/storagerepair/list";
		}else if(channelId==7){
			modelTitle = "资产";
			modelName = "固定资产退出列表";
			redirect = "front/storagequit/list";
		}else if(channelId==8){
			modelTitle = "资产";
			modelName = "固定资产归还列表";
			redirect = "front/storageback/list";
		}else if(channelId==15){
			modelTitle = "设备";
			modelName = "设备移交列表";
			redirect = "front/storageout/listzc";
		}else if(channelId==16){
			modelTitle = "设备";
			modelName = "设备维修申请列表";
			redirect = "front/storagerepair/list";
		}else if(channelId==17){
			modelTitle = "设备";
			modelName = "设备退出列表";
			redirect = "front/storagequit/list";
		}else if(channelId==18){
			modelTitle = "设备";
			modelName = "设备归还列表";
			redirect = "front/storageback/list";
		}else if(channelId==19){
			modelTitle = "设备";
			modelName = "运维变更列表";
			redirect = "front/storagechange/list";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelTitle", modelTitle);
		request.setAttribute("modelName", modelName);
		return redirect;
	}
	
	@RequestMapping("/auditlist")
	public String auditlist(HttpServletRequest request) {
		String modelTitle = "", modelName = "", redirect = "";
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		if(channelId==6){
			modelTitle = "资产";
			modelName = "固定资产维修审核";
			redirect = "front/storagerepair/auditlist";
		}else if(channelId==16){
			modelTitle = "设备";
			modelName = "设备维修审核";
			redirect = "front/storagerepair/auditlist";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelTitle", modelTitle);
		request.setAttribute("modelName", modelName);
		return redirect;
	}
	
	@RequestMapping("/getJsonById")
	@ResponseBody
	public String getJsonById(int id){
		String strTemp = "";
		StorageOut data = service.findById(id);
		strTemp = "{\"status\":\"1\",\"id\":\""+id+"\",\"name\":\""+data.getVarValue1()+"\"}";
		return strTemp;
	}
	
	@RequestMapping("/get")
	@ResponseBody
	public Map<String, Object> getAll(PageModel<StorageOut> pageModel, StorageOut data, HttpServletRequest request, Model model) {
		
		//System.out.println("------------------get------------------");
		String keywords = StrEdit.StrChkNull(data.getKeywords());
		
		Map<String,Object> newMap = new HashMap<String,Object>();
		
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		int classId = StrEdit.StrToInt(request.getParameter("classId"));
		int intValue1 = StrEdit.StrToInt(request.getParameter("intValue1"));
		int intValue2 = StrEdit.StrToInt(request.getParameter("intValue2"));
		int intValue3 = StrEdit.StrToInt(request.getParameter("intValue3"));
		String varValue1 = StrEdit.StrChkNull(request.getParameter("varValue1"));
		String varValue2 = StrEdit.StrChkNull(request.getParameter("varValue2"));
		String varValue3 = StrEdit.StrChkNull(request.getParameter("varValue3"));
		String varValue4 = StrEdit.StrChkNull(request.getParameter("varValue4"));
		String varValue5 = StrEdit.StrChkNull(request.getParameter("varValue5"));
		String varValue6 = StrEdit.StrChkNull(request.getParameter("varValue6"));
		String varValue7 = StrEdit.StrChkNull(request.getParameter("varValue7"));
		String varValue8 = StrEdit.StrChkNull(request.getParameter("varValue8"));
		String varValue9 = StrEdit.StrChkNull(request.getParameter("varValue9"));
		String varValue10 = StrEdit.StrChkNull(request.getParameter("varValue10"));
		String varValue39 = StrEdit.StrChkNull(request.getParameter("varValue39"));
		String varValue40 = StrEdit.StrChkNull(request.getParameter("varValue40"));
		String startDate = StrEdit.StrChkNull(request.getParameter("startDate"));
		String endDate = StrEdit.StrChkNull(request.getParameter("endDate"));
		if(!startDate.equals("")){
			startDate = startDate + " 00:00:00";
		}
		if(!endDate.equals("")){
			endDate = endDate + " 23:59:59";
		}
		
		int pageNumber = StrEdit.StrToInt(request.getParameter("pageNumber"));
		int pageSize = StrEdit.StrToInt(request.getParameter("pageSize"));
		int startRow = 0;
        if(pageNumber!=0) {
        	startRow = (pageNumber - 1) * pageSize;
        }
        newMap.put("startRow", startRow);
        newMap.put("pageSize", pageSize);
        newMap.put("channelId", channelId);
        newMap.put("classId", classId);
        newMap.put("intValue1", intValue1);
        newMap.put("intValue2", intValue2);
        newMap.put("intValue3", intValue3);
        newMap.put("keywords", keywords);
        newMap.put("varValue1", varValue1);
        newMap.put("varValue2", varValue2);
        newMap.put("varValue3", varValue3);
        newMap.put("varValue4", varValue4);
        newMap.put("varValue5", varValue5);
        newMap.put("varValue6", varValue6);
        newMap.put("varValue7", varValue7);
        newMap.put("varValue8", varValue8);
        newMap.put("varValue9", varValue9);
        newMap.put("varValue10", varValue10);
        newMap.put("varValue39", varValue39);
        newMap.put("varValue40", varValue40);
        newMap.put("startDate", startDate);
        newMap.put("endDate", endDate);
		List<StorageOut> list = service.pageList(newMap);
		int totalRecords = service.totalRecord(newMap);
		pageModel.setTotal(totalRecords);
		
		//Object jsonObject = JSONObject.toJSON(list);
		//System.out.println("jsonObject:"+jsonObject);
		//System.out.println("totalRecords1:"+totalRecords);
		
		int pn = 1;
		if (pageNumber != 0) {
			pn = new Integer(pageNumber);
		}
		pageModel.setPages(totalRecords, pn);//设置总记录数和当前页面
		Map<String, Object> map = new HashMap<>();
		map.put("page", pageModel);
		map.put("list", list);
		return map;
	}
	
	@RequestMapping("/add")
	public String toAdd(StorageOut data,HttpServletRequest request,Model model){
		String modelTitle = "", modelName = "", redirect = "";
		Date currTime = new Date();
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		if(channelId==1){
			modelName = "工具耗材领用";
			redirect = "front/storageout/add";
		}else if(channelId==5){
			int total = 0;
			Map<String,Object> newMap1 = new HashMap<String,Object>();
			newMap1.put("task", "getTodayNum");
	        newMap1.put("channelId", channelId);
	        Integer sum = service.getIntValue(newMap1);
	        if(sum!=null){
	        	total = sum.intValue();
	        }
	        
	        modelTitle = "资产";
			modelName = "固定资产移交登记";
			String no = "GDZCYJ" + StrEdit.getDate("yyyyMMdd") + StrEdit.autoGenericCode(total+"", 3);
			data.setVarValue1(no);
			data.setDateValue1(currTime);
			redirect = "front/storageout/addzc";
		}else if(channelId==6){
			modelTitle = "资产";
			modelName = "固定资产维修申请";
			int total = 0;
			Map<String,Object> newMap1 = new HashMap<String,Object>();
			newMap1.put("task", "getTodayNum");
	        newMap1.put("channelId", channelId);
	        Integer sum = service.getIntValue(newMap1);
	        if(sum!=null){
	        	total = sum.intValue();
	        }
			String no = "GDZCWX" + StrEdit.getDate("yyyyMMdd") + StrEdit.autoGenericCode(total+"", 3);
			data.setVarValue1(no);
			data.setDateValue1(currTime);
			redirect = "front/storagerepair/add";
		}else if(channelId==7){
			modelTitle = "资产";
			modelName = "固定资产退出登记";
			int total = 0;
			Map<String,Object> newMap1 = new HashMap<String,Object>();
			newMap1.put("task", "getTodayNum");
	        newMap1.put("channelId", channelId);
	        Integer sum = service.getIntValue(newMap1);
	        if(sum!=null){
	        	total = sum.intValue();
	        }
			String no = "GDZCTC" + StrEdit.getDate("yyyyMMdd") + StrEdit.autoGenericCode(total+"", 3);
			data.setVarValue1(no);
			data.setDateValue1(currTime);
			redirect = "front/storagequit/add";
		}else if(channelId==8){
			modelTitle = "资产";
			modelName = "固定资产归还登记";
			int total = 0;
			Map<String,Object> newMap1 = new HashMap<String,Object>();
			newMap1.put("task", "getTodayNum");
	        newMap1.put("channelId", channelId);
	        Integer sum = service.getIntValue(newMap1);
	        if(sum!=null){
	        	total = sum.intValue();
	        }
			String no = "GDZCGH" + StrEdit.getDate("yyyyMMdd") + StrEdit.autoGenericCode(total+"", 3);
			data.setVarValue1(no);
			data.setDateValue1(currTime);
			redirect = "front/storageback/add";
		}else if(channelId==15){
			int total = 0;
			Map<String,Object> newMap1 = new HashMap<String,Object>();
			newMap1.put("task", "getTodayNum");
	        newMap1.put("channelId", channelId);
	        Integer sum = service.getIntValue(newMap1);
	        if(sum!=null){
	        	total = sum.intValue();
	        }
	        
	        modelTitle = "设备";
			modelName = "设备移交登记";
			String no = "SBYJ" + StrEdit.getDate("yyyyMMdd") + StrEdit.autoGenericCode(total+"", 3);
			data.setVarValue1(no);
			data.setDateValue1(currTime);
			redirect = "front/storageout/addzc";
		}else if(channelId==16){
			modelTitle = "设备";
			modelName = "设备维修申请";
			int total = 0;
			Map<String,Object> newMap1 = new HashMap<String,Object>();
			newMap1.put("task", "getTodayNum");
	        newMap1.put("channelId", channelId);
	        Integer sum = service.getIntValue(newMap1);
	        if(sum!=null){
	        	total = sum.intValue();
	        }
			String no = "SBWX" + StrEdit.getDate("yyyyMMdd") + StrEdit.autoGenericCode(total+"", 3);
			data.setVarValue1(no);
			data.setDateValue1(currTime);
			redirect = "front/storagerepair/add";
		}else if(channelId==17){
			modelTitle = "设备";
			modelName = "设备退出登记";
			int total = 0;
			Map<String,Object> newMap1 = new HashMap<String,Object>();
			newMap1.put("task", "getTodayNum");
	        newMap1.put("channelId", channelId);
	        Integer sum = service.getIntValue(newMap1);
	        if(sum!=null){
	        	total = sum.intValue();
	        }
			String no = "SBTC" + StrEdit.getDate("yyyyMMdd") + StrEdit.autoGenericCode(total+"", 3);
			data.setVarValue1(no);
			data.setDateValue1(currTime);
			redirect = "front/storagequit/add";
		}else if(channelId==18){
			modelTitle = "设备";
			modelName = "设备归还登记";
			int total = 0;
			Map<String,Object> newMap1 = new HashMap<String,Object>();
			newMap1.put("task", "getTodayNum");
	        newMap1.put("channelId", channelId);
	        Integer sum = service.getIntValue(newMap1);
	        if(sum!=null){
	        	total = sum.intValue();
	        }
			String no = "SBGH" + StrEdit.getDate("yyyyMMdd") + StrEdit.autoGenericCode(total+"", 3);
			data.setVarValue1(no);
			data.setDateValue1(currTime);
			redirect = "front/storageback/add";
		}else if(channelId==19){
			modelTitle = "设备";
			modelName = "运维变更登记";
			int total = 0;
			Map<String,Object> newMap1 = new HashMap<String,Object>();
			newMap1.put("task", "getTodayNum");
	        newMap1.put("channelId", channelId);
	        Integer sum = service.getIntValue(newMap1);
	        if(sum!=null){
	        	total = sum.intValue();
	        }
			String no = "SBBG" + StrEdit.getDate("yyyyMMdd") + StrEdit.autoGenericCode(total+"", 3);
			data.setVarValue1(no);
			data.setDateValue1(currTime);
			redirect = "front/storagechange/add";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelTitle", modelTitle);
		request.setAttribute("modelName", modelName);
		
		List<String> dictionery5 = dictionary.selectDictionaryList(5);
		List<String> dictionery6 = dictionary.selectDictionaryList(6);
		model.addAttribute("dictionery5", dictionery5);
		model.addAttribute("dictionery6", dictionery6);
		
		Map<Long, String> typeMap = new HashMap<Long, String>();
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
        return redirect;
	}
	
	@RequestMapping("/save")
	@ResponseBody
	public String save(StorageOut data,HttpServletRequest request,HttpServletResponse response){//工具耗材领用
		int conId = 0;
		String strTemp = "";
		
		int userId = 0;
		String userName = "";
		User sessionUser = ((User)request.getSession().getAttribute(Contants.SESSION_USER));
		if(sessionUser!=null){
			userId = sessionUser.getConId();
			userName = sessionUser.getVarValue1();
		}
		
		int channelId = data.getChannelId();
		String vcDate = StrEdit.getSimDateForDB();
		
		sql = "insert into STORAGEOUT(ChannelID,VARVALUE1,VARVALUE2,VARVALUE3,VARVALUE4,VARVALUE5,VARVALUE6,VARVALUE40,INTVALUE3,TEXT1,ADDTIME,UPDATETIME,DATEVALUE1)";
		sql = sql + "values("+channelId+",'"+data.getVarValue1()+"','"+data.getVarValue2()+"','"+data.getVarValue3()+"','"+data.getVarValue4()+"','"+data.getVarValue5()+"','"+data.getVarValue6()+"',";
		sql = sql + "'"+data.getVarValue40()+"',1,'"+data.getText1()+"','"+vcDate+"','"+vcDate+"','"+StrEdit.dateToStrLong(data.getDateValue1())+"')";//dateValue1必须输入
		int i = mdb.executeUpdate(sql);//此连接池事务未提交前获取不到数据id，所以使用直接增加
		if(i<=0){
			strTemp = "{\"status\":\"0\",\"errors\":\"添加失败\"}";
			return strTemp;
		}
		sql = "select CONID from STORAGEOUT where ChannelID='"+channelId+"' and VARVALUE1='"+data.getVarValue1()+"' and ADDTIME='"+vcDate+"' order by CONID desc limit 1";
		ResultSet rs2 = mdb.executeQuery(sql);
		try {
			if(rs2.next()){
				conId = rs2.getInt("CONID");
			}
			rs2.close();
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
		}
		
		if(conId<=0){
			strTemp = "{\"status\":\"0\",\"errors\":\"添加失败\"}";
			return strTemp;
		}
		
		Connection conn=null;
		Statement state=null;
		conn = mdb.getNewConnection();
		boolean boolValue = false;
		try {
    		
    		conn.setAutoCommit(false);
    		
    		sql = "update STORAGEOUT set INTVALUE3='"+data.getIntValue3()+"' where CONID="+conId+"";
			state = conn.createStatement();
			state.execute(sql);
    		
    		if(conId>0){
    			
    			String field_scope=StrEdit.StrChkNull(request.getParameter("field_scope"));
    			String field_scope1=StrEdit.StrChkNull(request.getParameter("field_scope1"));
    			String field_scope2=StrEdit.StrChkNull(request.getParameter("field_scope2"));
    			String field_scope3=StrEdit.StrChkNull(request.getParameter("field_scope3"));
    			String field_scope4=StrEdit.StrChkNull(request.getParameter("field_scope4"));
    			String field_scope5=StrEdit.StrChkNull(request.getParameter("field_scope5"));
    			String field_scope6=StrEdit.StrChkNull(request.getParameter("field_scope6"));
    			String field_scope7=StrEdit.StrChkNull(request.getParameter("field_scope7"));
    			String date_scope1=StrEdit.StrChkNull(request.getParameter("date_scope1"));
    			String date_scope2=StrEdit.StrChkNull(request.getParameter("date_scope2"));
    			
    			if(!field_scope1.equals("")){
    				String[] arrField = field_scope.split("\\,");
    				String[] arrField1 = field_scope1.split("\\,");
    				String[] arrField2 = field_scope2.split("\\,");
    				String[] arrField3 = field_scope3.split("\\,");
    				String[] arrField4 = field_scope4.split("\\,");
    				String[] arrField5 = field_scope5.split("\\,");
    				String[] arrField6 = field_scope6.split("\\,");
    				String[] arrField7 = field_scope7.split("\\,");
    				String[] arrDate1 = date_scope1.split("\\,");
    				String[] arrDate2 = date_scope2.split("\\,");
    				
    				for(int m=0;m<arrField1.length;m++){
    					int detailId = StrEdit.StrToInt(arrField[m]);
    					String strValue1 = arrField1[m];
    					String strValue2 = arrField2[m];
    					String strValue3 = arrField3[m];
    					String strValue4 = arrField4[m];
    					String strValue5 = arrField5[m];
    					String strValue6 = arrField6[m];
    					String strValue7 = arrField7[m];
    					String dateValue1 = arrDate1[m];
    					String dateValue2 = arrDate2[m];
    					
    					if(strValue1.equals("NOINPUT")){
    						strValue1 = "";
    					}
    					if(dateValue1.equals("NOVALUE")){
    						dateValue1 = "";
    					}
    					if(dateValue2.equals("NOVALUE")){
    						dateValue2 = "";
    					}
    					if(strValue2.equals("NOVALUE")){
    						strValue2 = "";
    					}
    					if(strValue3.equals("NOVALUE")){
    						strValue3 = "";
    					}
    					if(strValue4.equals("NOVALUE")){//数量
    						strValue4 = "0";
    					}
    					if(strValue5.equals("NOVALUE")){//价格
    						strValue5 = "0";
    					}
    					if(strValue6.equals("NOVALUE")){
    						strValue6 = "";
    					}
    					if(strValue7.equals("NOVALUE")){
    						strValue7 = "0";
    					}
    					
    					int quantity = StrEdit.StrToInt(strValue4);
    					int goodsId = StrEdit.StrToInt(strValue7);
    					
    					if(!strValue1.equals("")){
    						sql = "insert into STORAGEOUTDETAIL(ChannelID,VARVALUE1,VARVALUE2,VARVALUE3,DECVALUE1,INTVALUE1,INTVALUE2,INTVALUE3,INTVALUE4,TEXT1,ADDTIME,UPDATETIME)";
        		    		sql = sql + "values("+data.getChannelId()+",'"+strValue1+"','"+strValue2+"','"+strValue3+"','"+strValue5+"',"+(m+1)+",'"+conId+"','"+strValue7+"','"+strValue4+"','"+strValue6+"','"+vcDate+"','"+vcDate+"')";
        		    		state.execute(sql);
    					}
    					
    					if(data.getIntValue3()==2 && goodsId>0){
    						int sum = 0;
    			    		sql = "select count(*) as total from STORAGE where ChannelID="+channelId+" and INTVALUE3="+goodsId+"";
    						ResultSet rs4 = mdb.executeQuery(sql);
    						if(rs4.next()){
    							sum = rs4.getInt("total");
    						}
    						rs4.close();
    						if(sum>0){
    							sql = "update STORAGE set INTVALUE4=INTVALUE4-"+quantity+",UPDATETIME='"+vcDate+"' where INTVALUE3="+goodsId+"";
    							state.execute(sql);
    						}else{
    							strTemp = "{\"status\":\"0\",\"errors\":\"操作失败，库存数量不够！\"}";
    							return strTemp;
    						}
    					}
    					
    				}
    			}
    			
    		}

			conn.commit();  //数据库操作最终提交给数据库
			conn.setAutoCommit(true);
			boolValue = true;
			
		} catch (java.sql.SQLException e) {
			boolValue = false;
			strTemp = "出库增加失败";
			try {
				conn.rollback();
			} catch (java.sql.SQLException e1) {
				System.out.println(strTemp);
				e1.printStackTrace();
			}
			log.info("出库增加失败 msg:{}", strTemp);
			System.out.println(strTemp);
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			if(state!=null){
	    		try {
					state.close();
				} catch (java.sql.SQLException e) {
					e.printStackTrace();
				}
	    	}
	    	if(conn!=null){
	    		try {
					conn.close();
				} catch (java.sql.SQLException e) {
					e.printStackTrace();
				}
	    	}
		}
		
    	if(boolValue){
    		strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
    	}else{
    		strTemp = "{\"status\":\"0\",\"errors\":\"添加失败\"}";
    	}
		
		/*int channelId = data.getChannelId();
		try{
			service.save(data);
			conId = data.getConId();
		}catch(Exception e){
			conId = 0;
			System.out.println("save插入数据失败" + e.getLocalizedMessage());
		}
		
		if(conId>0){
			
			String field_scope=StrEdit.StrChkNull(request.getParameter("field_scope"));
			String field_scope1=StrEdit.StrChkNull(request.getParameter("field_scope1"));
			String field_scope2=StrEdit.StrChkNull(request.getParameter("field_scope2"));
			String field_scope3=StrEdit.StrChkNull(request.getParameter("field_scope3"));
			String field_scope4=StrEdit.StrChkNull(request.getParameter("field_scope4"));
			String field_scope5=StrEdit.StrChkNull(request.getParameter("field_scope5"));
			String field_scope6=StrEdit.StrChkNull(request.getParameter("field_scope6"));
			String field_scope7=StrEdit.StrChkNull(request.getParameter("field_scope7"));
			String field_scope8=StrEdit.StrChkNull(request.getParameter("field_scope8"));
			this.updateDetail(conId,channelId,userName,field_scope,field_scope1,field_scope2,field_scope3,field_scope4,field_scope5,field_scope6,field_scope7);
			
			String attachIds=StrEdit.StrChkNull(request.getParameter("attachIds"));//附件
			if(!attachIds.equals("")){
				String[] arrAttachIds = attachIds.split("\\,");
				for(int m=0;m<arrAttachIds.length;m++){
					boolean boolValue = attach.update(StrEdit.StrToInt(arrAttachIds[m]),conId);
					if(!boolValue){
						System.out.println("附件信息ID更新失败："+arrAttachIds[m]);
					}
				}
			}
			
			strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"添加失败\"}";
		}*/
		
		return strTemp;
	}
	
	@RequestMapping("/update")
	@ResponseBody
	public String update(StorageOut data,HttpServletRequest request){//工具耗材领用
		int conId = data.getConId();
		String strTemp = "";
		
		int userId = 0;
		String userName = "";
		User sessionUser = ((User)request.getSession().getAttribute(Contants.SESSION_USER));
		if(sessionUser!=null){
			userId = sessionUser.getConId();
			userName = sessionUser.getVarValue1();
		}
		
		int channelId = data.getChannelId();
		String vcDate = StrEdit.getSimDateForDB();
		Connection conn=null;
		Statement state=null;
		conn = mdb.getNewConnection();
		boolean boolValue = false;
		try {
    		
    		conn.setAutoCommit(false);
    		
    		sql = "update STORAGEOUT set VARVALUE1='"+data.getVarValue1()+"',VARVALUE2='"+data.getVarValue2()+"',VARVALUE3='"+data.getVarValue3()+"',VARVALUE4='"+data.getVarValue4()+"',";
    		sql = sql + "VARVALUE5='"+data.getVarValue5()+"',VARVALUE6='"+data.getVarValue6()+"',VARVALUE40='"+data.getVarValue40()+"',INTVALUE3="+data.getIntValue3()+",TEXT1='"+data.getText1()+"',UPDATETIME='"+vcDate+"',DATEVALUE1='"+StrEdit.dateToStrLong(data.getDateValue1())+"' where CONID="+conId+"";
			state = conn.createStatement();
			state.execute(sql);
    		
    		if(conId>0){
    			
    			String field_scope=StrEdit.StrChkNull(request.getParameter("field_scope"));
    			String field_scope1=StrEdit.StrChkNull(request.getParameter("field_scope1"));
    			String field_scope2=StrEdit.StrChkNull(request.getParameter("field_scope2"));
    			String field_scope3=StrEdit.StrChkNull(request.getParameter("field_scope3"));
    			String field_scope4=StrEdit.StrChkNull(request.getParameter("field_scope4"));
    			String field_scope5=StrEdit.StrChkNull(request.getParameter("field_scope5"));
    			String field_scope6=StrEdit.StrChkNull(request.getParameter("field_scope6"));
    			String field_scope7=StrEdit.StrChkNull(request.getParameter("field_scope7"));
    			String date_scope1=StrEdit.StrChkNull(request.getParameter("date_scope1"));
    			String date_scope2=StrEdit.StrChkNull(request.getParameter("date_scope2"));
    			
    			if(field_scope.equals("")){
    				field_scope = "0";
    			}
				sql = "delete from STORAGEOUTDETAIL where INTVALUE2="+conId+" and CONID not in ("+field_scope+")";//删除已经去掉的id
				state.execute(sql);
    			
    			if(!field_scope1.equals("")){
    				String[] arrField = field_scope.split("\\,");
    				String[] arrField1 = field_scope1.split("\\,");
    				String[] arrField2 = field_scope2.split("\\,");
    				String[] arrField3 = field_scope3.split("\\,");
    				String[] arrField4 = field_scope4.split("\\,");
    				String[] arrField5 = field_scope5.split("\\,");
    				String[] arrField6 = field_scope6.split("\\,");
    				String[] arrField7 = field_scope7.split("\\,");
    				String[] arrDate1 = date_scope1.split("\\,");
    				String[] arrDate2 = date_scope2.split("\\,");
    				
    				for(int m=0;m<arrField1.length;m++){
    					int detailId = StrEdit.StrToInt(arrField[m]);
    					String strValue1 = arrField1[m];
    					String strValue2 = arrField2[m];
    					String strValue3 = arrField3[m];
    					String strValue4 = arrField4[m];
    					String strValue5 = arrField5[m];
    					String strValue6 = arrField6[m];
    					String strValue7 = arrField7[m];
    					String dateValue1 = arrDate1[m];
    					String dateValue2 = arrDate2[m];
    					
    					if(strValue1.equals("NOINPUT")){
    						strValue1 = "";
    					}
    					if(dateValue1.equals("NOVALUE")){
    						dateValue1 = "";
    					}
    					if(dateValue2.equals("NOVALUE")){
    						dateValue2 = "";
    					}
    					if(strValue2.equals("NOVALUE")){
    						strValue2 = "";
    					}
    					if(strValue3.equals("NOVALUE")){
    						strValue3 = "";
    					}
    					if(strValue4.equals("NOVALUE")){//数量
    						strValue4 = "0";
    					}
    					if(strValue5.equals("NOVALUE")){//价格
    						strValue5 = "0";
    					}
    					if(strValue6.equals("NOVALUE")){
    						strValue6 = "";
    					}
    					if(strValue7.equals("NOVALUE")){
    						strValue7 = "0";
    					}
    					
    					int quantity = StrEdit.StrToInt(strValue4);
    					int goodsId = StrEdit.StrToInt(strValue7);
    					
    					if(detailId>0){//更新
    						sql = "update STORAGEOUTDETAIL set VARVALUE1='"+strValue1+"',VARVALUE2='"+strValue2+"',VARVALUE3='"+strValue3+"',DECVALUE1='"+strValue5+"',";
    			    		sql = sql + "INTVALUE1="+(m+1)+",INTVALUE3='"+goodsId+"',INTVALUE4='"+quantity+"',UPDATETIME='"+vcDate+"' where INTVALUE2="+conId+" and CONID="+detailId+"";
    			    		state.execute(sql);
    			    		
    					}else{
    						if(!strValue1.equals("")){
    							sql = "insert into STORAGEOUTDETAIL(ChannelID,VARVALUE1,VARVALUE2,VARVALUE3,DECVALUE1,INTVALUE1,INTVALUE2,INTVALUE3,INTVALUE4,TEXT1,ADDTIME,UPDATETIME)";
            		    		sql = sql + "values("+channelId+",'"+strValue1+"','"+strValue2+"','"+strValue3+"','"+strValue5+"',"+(m+1)+",'"+conId+"','"+goodsId+"','"+quantity+"','"+strValue6+"','"+vcDate+"','"+vcDate+"')";
            		    		state.execute(sql);
    						}
    					}
    					
    					if(data.getIntValue3()==2 && goodsId>0){
    						int sum = 0;
    			    		sql = "select count(*) as total from STORAGE where ChannelID="+channelId+" and INTVALUE3="+goodsId+"";
    						ResultSet rs4 = mdb.executeQuery(sql);
    						if(rs4.next()){
    							sum = rs4.getInt("total");
    						}
    						rs4.close();
    						if(sum>0){
    							sql = "update STORAGE set INTVALUE4=INTVALUE4-"+quantity+",UPDATETIME='"+vcDate+"' where INTVALUE3="+goodsId+"";
    							state.execute(sql);
    						}else{
    							//state.close();
    							//conn.close();
    							strTemp = "{\"status\":\"0\",\"errors\":\"操作失败，库存数量不够！\"}";
    							return strTemp;
    						}
    					}
    		    		
    				}
    			}
    			
    			
    			
    		}

			conn.commit();  //数据库操作最终提交给数据库
			conn.setAutoCommit(true);
			boolValue = true;
			
		} catch (java.sql.SQLException e) {
			boolValue = false;
			strTemp = "出库更新失败";
			try {
				conn.rollback();
			} catch (java.sql.SQLException e1) {
				System.out.println(strTemp);
				e1.printStackTrace();
			}
			log.info("出库更新失败 msg:{}", strTemp);
			System.out.println(strTemp);
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {//中间有return，所以必须在finally里关闭链接
			if(state!=null){
	    		try {
					state.close();
				} catch (java.sql.SQLException e) {
					e.printStackTrace();
				}
	    	}
	    	if(conn!=null){
	    		try {
					conn.close();
				} catch (java.sql.SQLException e) {
					e.printStackTrace();
				}
	    	}
		}
    	
    	if(boolValue){
    		strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
    	}else{
    		strTemp = "{\"status\":\"0\",\"errors\":\"更新失败\"}";
    	}
    	
		/*boolean boolValue = service.update(data);
		if(boolValue){
			
			String field_scope=StrEdit.StrChkNull(request.getParameter("field_scope"));
			String field_scope1=StrEdit.StrChkNull(request.getParameter("field_scope1"));
			String field_scope2=StrEdit.StrChkNull(request.getParameter("field_scope2"));
			String field_scope3=StrEdit.StrChkNull(request.getParameter("field_scope3"));
			String field_scope4=StrEdit.StrChkNull(request.getParameter("field_scope4"));
			String field_scope5=StrEdit.StrChkNull(request.getParameter("field_scope5"));
			String field_scope6=StrEdit.StrChkNull(request.getParameter("field_scope6"));
			String field_scope7=StrEdit.StrChkNull(request.getParameter("field_scope7"));
			this.updateDetail(conId,channelId,userName,field_scope,field_scope1,field_scope2,field_scope3,field_scope4,field_scope5,field_scope6,field_scope7);
			
			strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"更新失败\"}";
		}*/
		
		return strTemp;
	}
	
	@RequestMapping("/savezc")
	@ResponseBody
	public String savezc(StorageOut data,HttpServletRequest request,HttpServletResponse response){//固定资产转移，维修，退出，归还，变更
		int conId = 0;
		String strTemp = "";
		
		int userId = 0;
		String userName = "";
		User sessionUser = ((User)request.getSession().getAttribute(Contants.SESSION_USER));
		if(sessionUser!=null){
			userId = sessionUser.getConId();
			userName = sessionUser.getVarValue1();
		}
		
		int channelId = data.getChannelId();
		String vcDate = StrEdit.getSimDateForDB();
		String rzDate = StrEdit.dateToStr(data.getDateValue1());
		
		sql = "insert into STORAGEOUT(ChannelID,VARVALUE1,VARVALUE2,VARVALUE3,VARVALUE4,VARVALUE5,VARVALUE6,VARVALUE7,VARVALUE8,VARVALUE9,VARVALUE21,VARVALUE22,VARVALUE23,VARVALUE24,VARVALUE25,VARVALUE26,VARVALUE27,VARVALUE39,VARVALUE40,DECVALUE3,INTVALUE3,INTVALUE5,TEXT1,ADDTIME,UPDATETIME,DATEVALUE1)";
		sql = sql + "values("+channelId+",'"+data.getVarValue1()+"','"+data.getVarValue2()+"','"+data.getVarValue3()+"','"+data.getVarValue4()+"','"+data.getVarValue5()+"','"+data.getVarValue6()+"','"+data.getVarValue7()+"',";
		sql = sql + "'"+data.getVarValue8()+"','"+data.getVarValue9()+"','"+data.getVarValue21()+"','"+data.getVarValue22()+"','"+data.getVarValue23()+"','"+data.getVarValue24()+"','"+data.getVarValue25()+"','"+data.getVarValue26()+"','"+data.getVarValue27()+"',";
		sql = sql + "'"+data.getVarValue39()+"','"+data.getVarValue40()+"','"+data.getDecValue3()+"',1,'"+data.getIntValue5()+"','"+data.getText1()+"','"+vcDate+"','"+vcDate+"','"+rzDate+"')";//dateValue1必须输入
		int i = mdb.executeUpdate(sql);//此连接池事务未提交前获取不到数据id，所以使用直接增加
		if(i<=0){
			strTemp = "{\"status\":\"0\",\"errors\":\"添加失败\"}";
			return strTemp;
		}
		sql = "select CONID from STORAGEOUT where ChannelID='"+channelId+"' and VARVALUE1='"+data.getVarValue1()+"' and ADDTIME='"+vcDate+"' order by CONID desc limit 1";
		ResultSet rs2 = mdb.executeQuery(sql);
		try {
			if(rs2.next()){
				conId = rs2.getInt("CONID");
			}
			rs2.close();
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
		}
		
		if(conId<=0){
			strTemp = "{\"status\":\"0\",\"errors\":\"添加失败\"}";
			return strTemp;
		}
		
		Connection conn=null;
		Statement state=null;
		conn = mdb.getNewConnection();
		boolean boolValue = false;
		try {
    		
    		conn.setAutoCommit(false);
    		
    		sql = "update STORAGEOUT set INTVALUE3='"+data.getIntValue3()+"' where CONID="+conId+"";
			state = conn.createStatement();
			state.execute(sql);
    		
    		if(conId>0){
    			
    			String field_scope=StrEdit.StrChkNull(request.getParameter("field_scope"));//详情detailId
    			String id_scope=StrEdit.StrChkNull(request.getParameter("id_scope"));//固定资产id
    			String field_scope1=StrEdit.StrChkNull(request.getParameter("field_scope1"));//资产编码
    			String field_scope2=StrEdit.StrChkNull(request.getParameter("field_scope2"));//资产分类
    			String field_scope3=StrEdit.StrChkNull(request.getParameter("field_scope3"));//名称
    			String field_scope4=StrEdit.StrChkNull(request.getParameter("field_scope4"));//计量单位
    			String field_scope5=StrEdit.StrChkNull(request.getParameter("field_scope5"));//资产数量
    			String field_scope6=StrEdit.StrChkNull(request.getParameter("field_scope6"));//资产原值
    			String field_scope7=StrEdit.StrChkNull(request.getParameter("field_scope7"));//购置方式
    			String field_scope8=StrEdit.StrChkNull(request.getParameter("field_scope8"));//购置日期
    			String field_scope9=StrEdit.StrChkNull(request.getParameter("field_scope9"));//供应商
    			String field_scope10=StrEdit.StrChkNull(request.getParameter("field_scope10"));//保修期限
    			String field_scope11=StrEdit.StrChkNull(request.getParameter("field_scope11"));//使用期限
    			String field_scope12=StrEdit.StrChkNull(request.getParameter("field_scope12"));//原资产号
    			String field_scope13=StrEdit.StrChkNull(request.getParameter("field_scope13"));//原卡片号
    			String field_scope14=StrEdit.StrChkNull(request.getParameter("field_scope14"));//备注
    			String field_scope15=StrEdit.StrChkNull(request.getParameter("field_scope15"));//原使用部门ID
    			String field_scope16=StrEdit.StrChkNull(request.getParameter("field_scope16"));//原使用部门
    			String field_scope17=StrEdit.StrChkNull(request.getParameter("field_scope17"));//原使用人ID
    			String field_scope18=StrEdit.StrChkNull(request.getParameter("field_scope18"));//原使用人
    			String field_scope19=StrEdit.StrChkNull(request.getParameter("field_scope19"));//原存放地点
    			String field_scope20=StrEdit.StrChkNull(request.getParameter("field_scope20"));//维修预估
    			String date_scope1=StrEdit.StrChkNull(request.getParameter("date_scope1"));
    			String date_scope2=StrEdit.StrChkNull(request.getParameter("date_scope2"));
    			
    			if(!field_scope1.equals("")){
    				String[] arrField = field_scope.split("\\,");
    				String[] arrCategoryId = id_scope.split("\\,");
    				String[] arrField1 = field_scope1.split("\\,");
    				String[] arrField2 = field_scope2.split("\\,");
    				String[] arrField3 = field_scope3.split("\\,");
    				String[] arrField4 = field_scope4.split("\\,");
    				String[] arrField5 = field_scope5.split("\\,");
    				String[] arrField6 = field_scope6.split("\\,");
    				String[] arrField7 = field_scope7.split("\\,");
    				String[] arrField8 = field_scope8.split("\\,");
    				String[] arrField9 = field_scope9.split("\\,");
    				String[] arrField10 = field_scope10.split("\\,");
    				String[] arrField11 = field_scope11.split("\\,");
    				String[] arrField12 = field_scope12.split("\\,");
    				String[] arrField13 = field_scope13.split("\\,");
    				String[] arrField14 = field_scope14.split("\\,");
    				String[] arrField15 = field_scope15.split("\\,");
    				String[] arrField16 = field_scope16.split("\\,");
    				String[] arrField17 = field_scope17.split("\\,");
    				String[] arrField18 = field_scope18.split("\\,");
    				String[] arrField19 = field_scope19.split("\\,");
    				String[] arrField20 = field_scope20.split("\\,");
    				String[] arrDate1 = date_scope1.split("\\,");
    				String[] arrDate2 = date_scope2.split("\\,");
    				
    				for(int m=0;m<arrField1.length;m++){
    					int detailId = StrEdit.StrToInt(arrField[m]);
    					int categoryId = StrEdit.StrToInt(arrCategoryId[m]);
    					String strValue1 = arrField1[m];
    					String strValue2 = arrField2[m];
    					String strValue3 = arrField3[m];
    					String strValue4 = arrField4[m];
    					String strValue5 = arrField5[m];
    					String strValue6 = arrField6[m];
    					String strValue7 = arrField7[m];
    					String strValue8 = arrField8[m];
    					String strValue9 = arrField9[m];
    					String strValue10 = arrField10[m];
    					String strValue11 = arrField11[m];
    					String strValue12 = arrField12[m];
    					String strValue13 = arrField13[m];
    					String strValue14 = arrField14[m];
    					String strValue15 = arrField15[m];
    					String strValue16 = arrField16[m];
    					String strValue17 = arrField17[m];
    					String strValue18 = arrField18[m];
    					String strValue19 = arrField19[m];
    					String strValue20 = arrField20[m];
    					String dateValue1 = arrDate1[m];
    					String dateValue2 = arrDate2[m];
    					
    					if(strValue1.equals("NOINPUT")){
    						strValue1 = "";
    					}
    					if(dateValue1.equals("NOVALUE")){
    						dateValue1 = "";
    					}
    					if(dateValue2.equals("NOVALUE")){
    						dateValue2 = "";
    					}
    					if(strValue2.equals("NOVALUE")){
    						strValue2 = "";
    					}
    					if(strValue3.equals("NOVALUE")){
    						strValue3 = "";
    					}
    					if(strValue4.equals("NOVALUE")){
    						strValue4 = "";
    					}
    					if(strValue5.equals("NOVALUE")){//数量
    						strValue5 = "0";
    					}
    					if(strValue6.equals("NOVALUE")){//价格
    						strValue6 = "0";
    					}
    					if(strValue7.equals("NOVALUE")){
    						strValue7 = "";
    					}
    					if(strValue8.equals("NOVALUE")){
    						strValue8 = "";
    					}
    					if(strValue9.equals("NOVALUE")){
    						strValue9 = "";
    					}
    					if(strValue10.equals("NOVALUE")){
    						strValue10 = "";
    					}
    					if(strValue11.equals("NOVALUE")){
    						strValue11 = "";
    					}
    					if(strValue12.equals("NOVALUE")){
    						strValue12 = "";
    					}
    					if(strValue13.equals("NOVALUE")){
    						strValue13 = "";
    					}
    					if(strValue14.equals("NOVALUE")){
    						strValue14 = "";
    					}
    					if(strValue15.equals("NOVALUE")){
    						strValue15 = "";
    					}
    					if(strValue16.equals("NOVALUE")){
    						strValue16 = "";
    					}
    					if(strValue17.equals("NOVALUE")){
    						strValue17 = "";
    					}
    					if(strValue18.equals("NOVALUE")){
    						strValue18 = "";
    					}
    					if(strValue19.equals("NOVALUE")){
    						strValue19 = "";
    					}
    					if(strValue20.equals("NOVALUE")){
    						strValue20 = "0";
    					}
    					
    					String oldDeptId = strValue15;
    					String oldDeptName = strValue16;
    					String oldUserId = strValue17;
    					String oldUser = strValue18;
    					String oldPlace = strValue19;
    					
    					int quantity = StrEdit.StrToInt(strValue5);
    					
    					if(!strValue1.equals("") && categoryId>0){
    						sql = "insert into STORAGEOUTDETAIL(ChannelID,VARVALUE1,VARVALUE2,VARVALUE3,VARVALUE4,VARVALUE5,VARVALUE6,VARVALUE7,VARVALUE8,VARVALUE9,VARVALUE10,VARVALUE11,VARVALUE21,VARVALUE22,VARVALUE23,VARVALUE24,VARVALUE25,DECVALUE1,DECVALUE3,INTVALUE1,INTVALUE2,INTVALUE3,INTVALUE4,TEXT1,ADDTIME,UPDATETIME)";
        		    		sql = sql + "values("+channelId+",'"+strValue1+"','"+strValue3+"','"+strValue4+"','"+strValue2+"','"+strValue7+"','"+strValue9+"','"+strValue10+"','"+strValue11+"','"+strValue12+"','"+strValue13+"','"+strValue8+"','"+oldDeptId+"','"+oldDeptName+"','"+oldUserId+"','"+oldUser+"','"+oldPlace+"','"+strValue6+"','"+strValue20+"',"+(m+1)+",'"+conId+"','"+categoryId+"','"+quantity+"','"+strValue14+"','"+vcDate+"','"+vcDate+"')";
        		    		state.execute(sql);
    					}
    					
    					if((channelId==5 || channelId==15) && data.getIntValue3()==2 && categoryId>0){//移交
    						sql = "update STORAGE set VARVALUE12='"+data.getVarValue7()+"',VARVALUE13='"+data.getVarValue3()+"',VARVALUE14='"+data.getVarValue4()+"',";
    						sql = sql + "VARVALUE15='"+data.getVarValue5()+"',VARVALUE16='"+data.getVarValue6()+"',VARVALUE22='"+data.getVarValue8()+"',VARVALUE23='"+data.getVarValue9()+"',UPDATETIME='"+vcDate+"' where CONID="+categoryId+"";
							state.execute(sql);
    					}
    					
    					if((channelId==6 || channelId==16) && data.getVarValue21().equals("否") && categoryId>0){//故障
    						sql = "update STORAGE set VARVALUE39='2',UPDATETIME='"+vcDate+"' where CONID="+categoryId+"";
							state.execute(sql);
    					}
    					
    					if((channelId==6 || channelId==16) && data.getVarValue21().equals("是") && categoryId>0){//维修
    						sql = "update STORAGE set VARVALUE39='3',UPDATETIME='"+vcDate+"' where CONID="+categoryId+"";
							state.execute(sql);
    					}
    					
    					if((channelId==7 || channelId==17) && data.getVarValue39().equals("4") && categoryId>0){//退出
    						sql = "update STORAGE set VARVALUE39='4',UPDATETIME='"+vcDate+"' where CONID="+categoryId+"";
							state.execute(sql);
    					}
    					
    					if((channelId==8 || channelId==18) && data.getIntValue3()==2 && categoryId>0){//归还
    						sql = "update STORAGE set VARVALUE12='"+data.getVarValue7()+"',VARVALUE13='"+data.getVarValue3()+"',VARVALUE14='"+data.getVarValue4()+"',";
    						sql = sql + "VARVALUE15='"+data.getVarValue5()+"',VARVALUE16='"+data.getVarValue6()+"',VARVALUE22='"+data.getVarValue8()+"',VARVALUE23='"+data.getVarValue9()+"',VARVALUE39='1',UPDATETIME='"+vcDate+"' where CONID="+categoryId+"";
							state.execute(sql);
    					}
    					
    					if(channelId==19 && data.getVarValue39().equals("4") && categoryId>0){//变更
    						sql = "update STORAGE set INTVALUE5='"+data.getIntValue5()+"',UPDATETIME='"+vcDate+"' where CONID="+categoryId+"";
							state.execute(sql);
    					}
    					
    				}
    			}
    			
    		}

			conn.commit();  //数据库操作最终提交给数据库
			conn.setAutoCommit(true);
			boolValue = true;
			
		} catch (java.sql.SQLException e) {
			boolValue = false;
			strTemp = "移交增加失败";
			try {
				conn.rollback();
			} catch (java.sql.SQLException e1) {
				System.out.println(strTemp);
				e1.printStackTrace();
			}
			log.info("移交增加失败 msg:{}", strTemp);
			System.out.println(strTemp);
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			if(state!=null){
	    		try {
					state.close();
				} catch (java.sql.SQLException e) {
					e.printStackTrace();
				}
	    	}
	    	if(conn!=null){
	    		try {
					conn.close();
				} catch (java.sql.SQLException e) {
					e.printStackTrace();
				}
	    	}
		}
		
    	if(boolValue){
    		strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
    	}else{
    		strTemp = "{\"status\":\"0\",\"errors\":\"添加失败\"}";
    	}
		
		return strTemp;
	}
	
	@RequestMapping("/updatezc")
	@ResponseBody
	public String updatezc(StorageOut data,HttpServletRequest request){//固定资产转移，维修，退出，归还，变更
		int conId = data.getConId();
		String strTemp = "";
		
		int userId = 0;
		String userName = "";
		User sessionUser = ((User)request.getSession().getAttribute(Contants.SESSION_USER));
		if(sessionUser!=null){
			userId = sessionUser.getConId();
			userName = sessionUser.getVarValue1();
		}
		
		int channelId = data.getChannelId();
		String vcDate = StrEdit.getSimDateForDB();
		String rzDate = StrEdit.dateToStr(data.getDateValue1());
		Connection conn=null;
		Statement state=null;
		conn = mdb.getNewConnection();
		boolean boolValue = false;
		try {
    		
    		conn.setAutoCommit(false);
    		
    		sql = "update STORAGEOUT set VARVALUE1='"+data.getVarValue1()+"',VARVALUE2='"+data.getVarValue2()+"',VARVALUE3='"+data.getVarValue3()+"',VARVALUE4='"+data.getVarValue4()+"',";
    		sql = sql + "VARVALUE5='"+data.getVarValue5()+"',VARVALUE6='"+data.getVarValue6()+"',VARVALUE8='"+data.getVarValue8()+"',VARVALUE9='"+data.getVarValue9()+"',VARVALUE21='"+data.getVarValue21()+"',VARVALUE22='"+data.getVarValue22()+"',VARVALUE23='"+data.getVarValue23()+"',";
    		sql = sql + "VARVALUE24='"+data.getVarValue24()+"',VARVALUE25='"+data.getVarValue25()+"',VARVALUE26='"+data.getVarValue26()+"',VARVALUE27='"+data.getVarValue27()+"',VARVALUE39='"+data.getVarValue39()+"',VARVALUE40='"+data.getVarValue40()+"',DECVALUE3='"+data.getDecValue3()+"',";
    		sql = sql + "INTVALUE5="+data.getIntValue5()+",INTVALUE3="+data.getIntValue3()+",TEXT1='"+data.getText1()+"',UPDATETIME='"+vcDate+"',DATEVALUE1='"+rzDate+"' where CONID="+conId+"";
			state = conn.createStatement();
			state.execute(sql);
    		
    		if(conId>0){
    			
    			String field_scope=StrEdit.StrChkNull(request.getParameter("field_scope"));//详情detailId
    			String id_scope=StrEdit.StrChkNull(request.getParameter("id_scope"));//分类id
    			String field_scope1=StrEdit.StrChkNull(request.getParameter("field_scope1"));//资产编码
    			String field_scope2=StrEdit.StrChkNull(request.getParameter("field_scope2"));//资产分类
    			String field_scope3=StrEdit.StrChkNull(request.getParameter("field_scope3"));//名称
    			String field_scope4=StrEdit.StrChkNull(request.getParameter("field_scope4"));//计量单位
    			String field_scope5=StrEdit.StrChkNull(request.getParameter("field_scope5"));//资产数量
    			String field_scope6=StrEdit.StrChkNull(request.getParameter("field_scope6"));//资产原值
    			String field_scope7=StrEdit.StrChkNull(request.getParameter("field_scope7"));//购置方式
    			String field_scope8=StrEdit.StrChkNull(request.getParameter("field_scope8"));//购置日期
    			String field_scope9=StrEdit.StrChkNull(request.getParameter("field_scope9"));//供应商
    			String field_scope10=StrEdit.StrChkNull(request.getParameter("field_scope10"));//保修期限
    			String field_scope11=StrEdit.StrChkNull(request.getParameter("field_scope11"));//使用期限
    			String field_scope12=StrEdit.StrChkNull(request.getParameter("field_scope12"));//原资产号
    			String field_scope13=StrEdit.StrChkNull(request.getParameter("field_scope13"));//原卡片号
    			String field_scope14=StrEdit.StrChkNull(request.getParameter("field_scope14"));//备注
    			String field_scope15=StrEdit.StrChkNull(request.getParameter("field_scope15"));//原使用部门ID
    			String field_scope16=StrEdit.StrChkNull(request.getParameter("field_scope16"));//原使用部门
    			String field_scope17=StrEdit.StrChkNull(request.getParameter("field_scope17"));//原使用人ID
    			String field_scope18=StrEdit.StrChkNull(request.getParameter("field_scope18"));//原使用人
    			String field_scope19=StrEdit.StrChkNull(request.getParameter("field_scope19"));//原存放地点
    			String field_scope20=StrEdit.StrChkNull(request.getParameter("field_scope20"));//维修预估
    			String date_scope1=StrEdit.StrChkNull(request.getParameter("date_scope1"));
    			String date_scope2=StrEdit.StrChkNull(request.getParameter("date_scope2"));
    			
    			if(field_scope.equals("")){
    				field_scope = "0";
    			}
				sql = "delete from STORAGEOUTDETAIL where INTVALUE2="+conId+" and CONID not in ("+field_scope+")";//删除已经去掉的id
				state.execute(sql);
    			
    			if(!field_scope1.equals("")){
    				String[] arrField = field_scope.split("\\,");
    				String[] arrCategoryId = id_scope.split("\\,");
    				String[] arrField1 = field_scope1.split("\\,");
    				String[] arrField2 = field_scope2.split("\\,");
    				String[] arrField3 = field_scope3.split("\\,");
    				String[] arrField4 = field_scope4.split("\\,");
    				String[] arrField5 = field_scope5.split("\\,");
    				String[] arrField6 = field_scope6.split("\\,");
    				String[] arrField7 = field_scope7.split("\\,");
    				String[] arrField8 = field_scope8.split("\\,");
    				String[] arrField9 = field_scope9.split("\\,");
    				String[] arrField10 = field_scope10.split("\\,");
    				String[] arrField11 = field_scope11.split("\\,");
    				String[] arrField12 = field_scope12.split("\\,");
    				String[] arrField13 = field_scope13.split("\\,");
    				String[] arrField14 = field_scope14.split("\\,");
    				String[] arrField15 = field_scope15.split("\\,");
    				String[] arrField16 = field_scope16.split("\\,");
    				String[] arrField17 = field_scope17.split("\\,");
    				String[] arrField18 = field_scope18.split("\\,");
    				String[] arrField19 = field_scope19.split("\\,");
    				String[] arrField20 = field_scope20.split("\\,");
    				String[] arrDate1 = date_scope1.split("\\,");
    				String[] arrDate2 = date_scope2.split("\\,");
    				
    				for(int m=0;m<arrField1.length;m++){
    					int detailId = StrEdit.StrToInt(arrField[m]);
    					int categoryId = StrEdit.StrToInt(arrCategoryId[m]);
    					String strValue1 = arrField1[m];
    					String strValue2 = arrField2[m];
    					String strValue3 = arrField3[m];
    					String strValue4 = arrField4[m];
    					String strValue5 = arrField5[m];
    					String strValue6 = arrField6[m];
    					String strValue7 = arrField7[m];
    					String strValue8 = arrField8[m];
    					String strValue9 = arrField9[m];
    					String strValue10 = arrField10[m];
    					String strValue11 = arrField11[m];
    					String strValue12 = arrField12[m];
    					String strValue13 = arrField13[m];
    					String strValue14 = arrField14[m];
    					String strValue15 = arrField15[m];
    					String strValue16 = arrField16[m];
    					String strValue17 = arrField17[m];
    					String strValue18 = arrField18[m];
    					String strValue19 = arrField19[m];
    					String strValue20 = arrField20[m];
    					String dateValue1 = arrDate1[m];
    					String dateValue2 = arrDate2[m];
    					
    					String oldDeptId = strValue15;
    					String oldDeptName = strValue16;
    					String oldUserId = strValue17;
    					String oldUser = strValue18;
    					String oldPlace = strValue19;
    					
    					if(strValue1.equals("NOINPUT")){
    						strValue1 = "";
    					}
    					if(dateValue1.equals("NOVALUE")){
    						dateValue1 = "";
    					}
    					if(dateValue2.equals("NOVALUE")){
    						dateValue2 = "";
    					}
    					if(strValue2.equals("NOVALUE")){
    						strValue2 = "";
    					}
    					if(strValue3.equals("NOVALUE")){
    						strValue3 = "";
    					}
    					if(strValue4.equals("NOVALUE")){
    						strValue4 = "";
    					}
    					if(strValue5.equals("NOVALUE")){//数量
    						strValue5 = "0";
    					}
    					if(strValue6.equals("NOVALUE")){//价格
    						strValue6 = "0";
    					}
    					if(strValue7.equals("NOVALUE")){
    						strValue7 = "";
    					}
    					if(strValue8.equals("NOVALUE")){
    						strValue8 = "";
    					}
    					if(strValue9.equals("NOVALUE")){
    						strValue9 = "";
    					}
    					if(strValue10.equals("NOVALUE")){
    						strValue10 = "";
    					}
    					if(strValue11.equals("NOVALUE")){
    						strValue11 = "";
    					}
    					if(strValue12.equals("NOVALUE")){
    						strValue12 = "";
    					}
    					if(strValue13.equals("NOVALUE")){
    						strValue13 = "";
    					}
    					if(strValue14.equals("NOVALUE")){
    						strValue14 = "";
    					}
    					if(strValue15.equals("NOVALUE")){
    						strValue15 = "";
    					}
    					if(strValue16.equals("NOVALUE")){
    						strValue16 = "";
    					}
    					if(strValue17.equals("NOVALUE")){
    						strValue17 = "";
    					}
    					if(strValue18.equals("NOVALUE")){
    						strValue18 = "";
    					}
    					if(strValue19.equals("NOVALUE")){
    						strValue19 = "";
    					}
    					if(strValue20.equals("NOVALUE")){
    						strValue20 = "0";
    					}
    					
    					int quantity = StrEdit.StrToInt(strValue5);
    					
    					if(detailId>0 && categoryId>0){//更新
    						sql = "update STORAGEOUTDETAIL set VARVALUE1='"+strValue1+"',VARVALUE2='"+strValue3+"',VARVALUE3='"+strValue4+"',VARVALUE4='"+strValue2+"',VARVALUE5='"+strValue7+"',VARVALUE6='"+strValue9+"',";
    						sql = sql + "VARVALUE7='"+strValue10+"',VARVALUE8='"+strValue11+"',VARVALUE9='"+strValue12+"',VARVALUE10='"+strValue13+"',VARVALUE11='"+strValue8+"',VARVALUE21='"+oldDeptId+"',VARVALUE22='"+oldDeptName+"',";
    						sql = sql + "VARVALUE23='"+oldUserId+"',VARVALUE24='"+oldUser+"',VARVALUE25='"+oldPlace+"',DECVALUE1='"+strValue6+"',DECVALUE3='"+strValue20+"',";
    			    		sql = sql + "INTVALUE1="+(m+1)+",INTVALUE3='"+categoryId+"',INTVALUE4='"+quantity+"',TEXT1='"+strValue14+"',UPDATETIME='"+vcDate+"' where INTVALUE2="+conId+" and CONID="+detailId+"";
    			    		state.execute(sql);
    			    		
    					}else{
    						if(!strValue1.equals("") && categoryId>0){
    							sql = "insert into STORAGEOUTDETAIL(ChannelID,VARVALUE1,VARVALUE2,VARVALUE3,VARVALUE4,VARVALUE5,VARVALUE6,VARVALUE7,VARVALUE8,VARVALUE9,VARVALUE10,VARVALUE11,VARVALUE21,VARVALUE22,VARVALUE23,VARVALUE24,VARVALUE25,DECVALUE1,DECVALUE3,INTVALUE1,INTVALUE2,INTVALUE3,INTVALUE4,TEXT1,ADDTIME,UPDATETIME)";
            		    		sql = sql + "values("+channelId+",'"+strValue1+"','"+strValue3+"','"+strValue4+"','"+strValue2+"','"+strValue7+"','"+strValue9+"','"+strValue10+"','"+strValue11+"','"+strValue12+"','"+strValue13+"','"+strValue8+"','"+oldDeptId+"','"+oldDeptName+"','"+oldUserId+"','"+oldUser+"','"+oldPlace+"','"+strValue6+"','"+strValue20+"',"+(m+1)+",'"+conId+"','"+categoryId+"','"+quantity+"','"+strValue14+"','"+vcDate+"','"+vcDate+"')";
            		    		state.execute(sql);
    						}
    					}
    					
    					if((channelId==5 || channelId==15) && data.getIntValue3()==2 && categoryId>0){//移交
    						sql = "update STORAGE set VARVALUE12='"+data.getVarValue7()+"',VARVALUE13='"+data.getVarValue3()+"',VARVALUE14='"+data.getVarValue4()+"',";
    						sql = sql + "VARVALUE15='"+data.getVarValue5()+"',VARVALUE16='"+data.getVarValue6()+"',VARVALUE22='"+data.getVarValue8()+"',VARVALUE23='"+data.getVarValue9()+"',UPDATETIME='"+vcDate+"' where CONID="+categoryId+"";
							state.execute(sql);
    					}
    					
    					if((channelId==6 || channelId==16) && data.getVarValue21().equals("否") && categoryId>0){//故障
    						sql = "update STORAGE set VARVALUE39='2',UPDATETIME='"+vcDate+"' where CONID="+categoryId+"";
							state.execute(sql);
    					}
    					
    					if((channelId==6 || channelId==16) && data.getVarValue21().equals("是") && categoryId>0){//维修
    						sql = "update STORAGE set VARVALUE39='3',UPDATETIME='"+vcDate+"' where CONID="+categoryId+"";
							state.execute(sql);
    					}
    					
    					if((channelId==7 || channelId==17) && data.getVarValue39().equals("4") && categoryId>0){//退出
    						sql = "update STORAGE set VARVALUE39='4',UPDATETIME='"+vcDate+"' where CONID="+categoryId+"";
							state.execute(sql);
    					}
    					
    					if((channelId==8 || channelId==18) && data.getIntValue3()==2 && categoryId>0){//归还
    						sql = "update STORAGE set VARVALUE12='"+data.getVarValue7()+"',VARVALUE13='"+data.getVarValue3()+"',VARVALUE14='"+data.getVarValue4()+"',";
    						sql = sql + "VARVALUE15='"+data.getVarValue5()+"',VARVALUE16='"+data.getVarValue6()+"',VARVALUE22='"+data.getVarValue8()+"',VARVALUE23='"+data.getVarValue9()+"',VARVALUE39='1',UPDATETIME='"+vcDate+"' where CONID="+categoryId+"";
							state.execute(sql);
    					}
    					
    					if(channelId==19 && data.getVarValue39().equals("4") && categoryId>0){//变更
    						sql = "update STORAGE set INTVALUE5='"+data.getIntValue5()+"',UPDATETIME='"+vcDate+"' where CONID="+categoryId+"";
							state.execute(sql);
    					}
    		    		
    				}
    			}
    			
    			
    			
    		}

			conn.commit();  //数据库操作最终提交给数据库
			conn.setAutoCommit(true);
			boolValue = true;
			
		} catch (java.sql.SQLException e) {
			boolValue = false;
			strTemp = "入库更新失败";
			try {
				conn.rollback();
			} catch (java.sql.SQLException e1) {
				System.out.println(strTemp);
				e1.printStackTrace();
			}
			log.info("入库更新失败 msg:{}", strTemp);
			System.out.println(strTemp);
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			if(state!=null){
	    		try {
					state.close();
				} catch (java.sql.SQLException e) {
					e.printStackTrace();
				}
	    	}
	    	if(conn!=null){
	    		try {
					conn.close();
				} catch (java.sql.SQLException e) {
					e.printStackTrace();
				}
	    	}
		}
		
    	if(boolValue){
    		strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
    	}else{
    		strTemp = "{\"status\":\"0\",\"errors\":\"更新失败\"}";
    	}
		
		return strTemp;
	}
	
	@RequestMapping("/getById")
	public String getById(int id,HttpServletRequest request,Model model){
		Map<Long, String> typeMap = new HashMap<Long, String>();
		StorageOut data = service.findById(id);
		String modelTitle = "", modelName = "", redirect = "";
		int channelId = data.getChannelId();
		if(channelId==1){
			modelName = "工具耗材领用";
			redirect = "front/storageout/add";
		}else if(channelId==5){
			modelTitle = "资产";
			modelName = "固定资产移交登记";
			redirect = "front/storageout/addzc";
		}else if(channelId==6){
			modelTitle = "资产";
			modelName = "固定资产维修申请";
			redirect = "front/storagerepair/add";
		}else if(channelId==7){
			modelTitle = "资产";
			modelName = "固定资产退出登记";
			redirect = "front/storagequit/add";
		}else if(channelId==8){
			modelTitle = "资产";
			modelName = "固定资产归还登记";
			redirect = "front/storageback/add";
		}else if(channelId==15){
			modelTitle = "设备";
			modelName = "设备移交登记";
			redirect = "front/storageout/addzc";
		}else if(channelId==16){
			modelTitle = "设备";
			modelName = "设备维修申请";
			redirect = "front/storagerepair/add";
		}else if(channelId==17){
			modelTitle = "设备";
			modelName = "设备退出登记";
			redirect = "front/storagequit/add";
		}else if(channelId==18){
			modelTitle = "设备";
			modelName = "设备归还登记";
			redirect = "front/storageback/add";
		}else if(channelId==19){
			modelTitle = "设备";
			modelName = "运维变更登记";
			redirect = "front/storagechange/add";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelTitle", modelTitle);
		request.setAttribute("modelName", modelName);
		
		List<String> dictionery5 = dictionary.selectDictionaryList(5);
		List<String> dictionery6 = dictionary.selectDictionaryList(6);
		model.addAttribute("dictionery5", dictionery5);
		model.addAttribute("dictionery6", dictionery6);
		
		Map<String,Object> newMap1 = new HashMap<String,Object>();
		newMap1.put("task", "getDetailByIntValue2");
        newMap1.put("intValue2", id);
        List<StorageOutDetail> list1 = detail.getListByMap(newMap1);
        request.setAttribute("list1", list1);
        request.setAttribute("roomnumber", list1.size());
        
        List<Attachment> attachList1001 = attach.list(1001,id);
        
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
        model.addAttribute("attachList1001", attachList1001);
        
		return redirect;
	}
	
	@RequestMapping("/getByIdAudit")
	public String getByIdAudit(int id,HttpServletRequest request,Model model){
		Map<Long, String> typeMap = new HashMap<Long, String>();
		StorageOut data = service.findById(id);
		String modelTitle = "", modelName = "", redirect = "";
		int channelId = data.getChannelId();
		if(channelId==6){
			modelTitle = "资产";
			modelName = "固定资产维修审核";
			redirect = "front/storagerepair/auditadd";
		}else if(channelId==16){
			modelTitle = "设备";
			modelName = "设备维修审核";
			redirect = "front/storagerepair/auditadd";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelTitle", modelTitle);
		request.setAttribute("modelName", modelName);
		
		List<String> dictionery5 = dictionary.selectDictionaryList(5);
		List<String> dictionery6 = dictionary.selectDictionaryList(6);
		model.addAttribute("dictionery5", dictionery5);
		model.addAttribute("dictionery6", dictionery6);
		
		Map<String,Object> newMap1 = new HashMap<String,Object>();
		newMap1.put("task", "getDetailByIntValue2");
        newMap1.put("intValue2", id);
        List<StorageOutDetail> list1 = detail.getListByMap(newMap1);
        request.setAttribute("list1", list1);
        request.setAttribute("roomnumber", list1.size());
        
        List<Attachment> attachList1001 = attach.list(1001,id);
        
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
        model.addAttribute("attachList1001", attachList1001);
        
		return redirect;
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public String delete(int id,HttpServletRequest request){
		String strTemp = "";
		boolean boolValue = service.delete(id);
		if(boolValue){
			
			Map<String,Object> newMap1 = new HashMap<String,Object>();
			newMap1.put("task", "deleteById");
	        newMap1.put("intValue2", id);
	        detail.deleteByMap(newMap1);
	        
			strTemp = "{\"status\":\"1\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"删除失败\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/audit")
	@ResponseBody
	public String audit(StorageOut data,HttpServletRequest request){
		String strTemp = "";
		boolean boolValue = false;
		String task = StrEdit.StrChkNull(request.getParameter("task"));
		Date currTime = new Date();
		
		if(task.equals("auditresult")){
			String isagree = StrEdit.StrChkNull(request.getParameter("isagree"));
			String result = StrEdit.StrChkNull(request.getParameter("result"));
			String returnmemo = StrEdit.StrChkNull(request.getParameter("returnmemo"));
			String audituserid = StrEdit.StrChkNull(request.getParameter("audituserid"));
			String auditname = StrEdit.StrChkNull(request.getParameter("auditname"));
			String auditdate = StrEdit.StrChkNull(request.getParameter("auditdate"));
			String selectids = StrEdit.StrChkNull(request.getParameter("selectids"));
			String[] arrParameter = selectids.split("\\,");//如果不包含,则直接返回原字符的一维数组
	    	if (arrParameter!=null){
				for(int i=0;i<arrParameter.length;i++){
					Map<String,Object> newMap = new HashMap<String,Object>();
					newMap.put("task", task);
					newMap.put("isagree", isagree);
					newMap.put("result", result);
					newMap.put("returnmemo", returnmemo);
					newMap.put("audituserid", audituserid);
					newMap.put("auditname", auditname);
					newMap.put("updateTime", currTime);
					newMap.put("auditdate", auditdate);
			        newMap.put("conId", StrEdit.StrToInt(arrParameter[i]));
			        boolValue = service.updateByMap(newMap);
				}
			}
		}else if(task.equals("repair")){
			int conId = StrEdit.StrToInt(request.getParameter("conId"));
			String selectids = StrEdit.StrChkNull(request.getParameter("selectids"));
			String[] arrParameter = selectids.split("\\,");//如果不包含,则直接返回原字符的一维数组
	    	if (arrParameter!=null){
				for(int i=0;i<arrParameter.length;i++){
					Map<String,Object> newMap = new HashMap<String,Object>();
					newMap.put("task", task);
					newMap.put("varValue21", "是");
					newMap.put("updateTime", currTime);
					newMap.put("conId", conId);
			        newMap.put("storageId", StrEdit.StrToInt(arrParameter[i]));
			        boolValue = service.updateByMap(newMap);
			        boolValue = storage.updateByMap(newMap);
				}
			}
		}
		if(boolValue){
			strTemp = "{\"status\":\"1\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"更新失败\"}";
		}
		
		return strTemp;
	}
	
}
