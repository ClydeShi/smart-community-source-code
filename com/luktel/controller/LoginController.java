package com.luktel.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;



import com.alibaba.fastjson.JSONObject;
import com.luktel.model.User;
import com.luktel.service.UserService;
import com.util.common.Contants;
import com.util.common.MD5;
import com.util.common.Tools;

import jdk.nashorn.internal.ir.RuntimeNode.Request;

import com.util.common.StrEdit;

/*
 * @Controller是标记在类UserController上面的，所以类UserController就是一个SpringMVC Controller对象了
 * 使用@RequestMapping("/getAllUser")标记在Controller方法上，表示当请求/getAllUser的时候访问的是LoginController的getAllUser方法，
 * 该方法返回了一个包括Model和View的ModelAndView对象113323
 */

@Controller//@Controller注解标识一个控制器
public class LoginController{
	
	@Autowired
	private UserService service;
	
	/**
	 * 访问登录页
	 * @return
	 */
	@RequestMapping(value="/login",method=RequestMethod.GET)
	public String loginGet(){
		System.out.println("------------------login------------------");
		return "front/index/login";
	}
	
	/**
	 * 请求登录，验证用户
	 * @param session
	 * @param loginname
	 * @param password
	 * @param code
	 * @return
	 */
	@RequestMapping(value="/login",method=RequestMethod.POST)
	public ModelAndView loginPost(HttpServletRequest request,HttpSession session){//User user如果带user类，则提交的参数要和类里面的参数名称一样
		String sessionCode = (String)session.getAttribute(Contants.SESSION_SECURITY_CODE);
		ModelAndView mv = new ModelAndView();
		String code = request.getParameter("code");
		String userName = StrEdit.StrChkNull(request.getParameter("loginname"));
		String userPassword = StrEdit.StrChkNull(request.getParameter("password"));//登陆页面已经加密
		//userPassword = MD5.toMD5(userPassword);
		String errInfo = "";
		if(Tools.notEmpty(sessionCode) && sessionCode.equalsIgnoreCase(code)){
			
			Map<String,Object> newMap = new HashMap<String,Object>();
			newMap.put("task", "loginerrornumber");
			newMap.put("varValue1", userName);
			int loginErrorNumber = service.getIntValue(newMap);
			if(loginErrorNumber>=10){
				errInfo = "登陆失败超过10次，账户已经被锁定！";
			}else{
				User user = service.getUserByNameAndPwd(userName, userPassword);
				if(user!=null){
					session.setAttribute(Contants.SESSION_USER, user);
					session.removeAttribute(Contants.SESSION_SECURITY_CODE);
					User data = new User();
					data.setTask("removeloginerrornumber");
					data.setVarValue1(userName);
					boolean c = service.updateSpecialTask(data);
					if(c){
						System.out.println("错误次数清零"+userName);
					}
				}else{
					int availNum = 9 - loginErrorNumber;
					errInfo = "用户名或密码错误，登陆失败超过10次，账户会被锁定，剩余"+availNum+"次！";
					User data = new User();
					data.setTask("addloginerrornumber");
					data.setVarValue1(userName);
					boolean c = service.updateSpecialTask(data);
					if(c){
						System.out.println("增加登陆错误次数"+userName);
					}
				}
			}
		}else{
			errInfo = "验证码错误！";
		}
		if(Tools.isEmpty(errInfo)){
			//mv.setViewName("redirect:user/userList");
			mv.setViewName("redirect:index");
		}else{
			mv.addObject("errInfo", errInfo);
			mv.addObject("loginname",userName);
			mv.addObject("password",userPassword);
			mv.setViewName("front/index/login");//跳转到登陆页面
		}
		return mv;
	}
	
	@RequestMapping(value="/login2",method=RequestMethod.POST)
	public ModelAndView loginPost2(HttpSession session,@RequestParam String loginname,@RequestParam String password,@RequestParam String code){
		String sessionCode = (String)session.getAttribute(Contants.SESSION_SECURITY_CODE);
		ModelAndView mv = new ModelAndView();
		String errInfo = "";
		if(Tools.notEmpty(sessionCode) && sessionCode.equalsIgnoreCase(code)){
			User user = service.getUserByNameAndPwd(loginname, password);
			if(user!=null){
				session.setAttribute(Contants.SESSION_USER, user);
				session.removeAttribute(Contants.SESSION_SECURITY_CODE);
			}else{
				errInfo = "用户名或密码错误！";
			}
		}else{
			errInfo = "验证码输入错误！";
		}
		if(Tools.isEmpty(errInfo)){
			//mv.setViewName("redirect:user/userList");
			mv.setViewName("redirect:index");
		}else{
			mv.addObject("errInfo", errInfo);
			mv.addObject("loginname",loginname);
			mv.addObject("password",password);
			mv.setViewName("front/index/login");//跳转到登陆页面
		}
		return mv;
	}
	
	/**
	 * 用户注销
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "logout")//@RequestMapping(value = {"logout", ""}) @RequestMapping(value="/logout")
	public String logout(HttpSession session){
		session.removeAttribute(Contants.SESSION_USER);
		session.invalidate();
		return "redirect:/login";//跳转到登录映射
	}
	
}
