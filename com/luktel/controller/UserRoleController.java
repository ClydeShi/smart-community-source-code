package com.luktel.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.luktel.model.UserRole;
import com.luktel.model.Attachment;
import com.luktel.model.Dictionary;
import com.luktel.service.UserRoleService;
import com.luktel.service.AttachmentService;
import com.util.common.PageModel;
import com.util.common.StrEdit;

/*
 * @Controller是标记在类UserController上面的，所以类UserController就是一个SpringMVC Controller对象了
 * 使用@RequestMapping("/getAllUser")标记在Controller方法上，表示当请求/getAllUser的时候访问的是LoginController的getAllUser方法，
 * 该方法返回了一个包括Model和View的ModelAndView对象113323
 */

@Controller//@Controller注解标识一个控制器
@RequestMapping("/userrole")//如果@RequestMapping注解在类级别上，则表示相对路径，在方法级别上，则标记访问的路径
public class UserRoleController{
	
	@Autowired
	private UserRoleService service;
	
	@Autowired
	private AttachmentService attach;
	
	@RequestMapping("/list")
	public String list() {
		return "front/userrole/list";
	}
	
	@RequestMapping("/select")
	public String select() {
		return "front/userrole/select";
	}
	
	@RequestMapping("/getInfoById")
	@ResponseBody
	public String getInfoById(int id){
		String strTemp = "";
		UserRole data = service.findById(id);
		strTemp = "{\"status\":\"1\",\"id\":\""+id+"\",\"name\":\""+data.getVarValue1()+"\",\"no\":\""+data.getVarValue2()+"\"}";
		return strTemp;
	}
	
	@RequestMapping("/get")
	@ResponseBody
	public Map<String, Object> getAll(PageModel<UserRole> pageModel, UserRole data, HttpServletRequest request, Model model) {
		
		//System.out.println("------------------get------------------");
		String keywords = StrEdit.StrChkNull(data.getKeywords());
		
		Map<String,Object> newMap = new HashMap<String,Object>();
		
		int pageNumber = StrEdit.StrToInt(request.getParameter("pageNumber"));
		int pageSize = StrEdit.StrToInt(request.getParameter("pageSize"));//pageSize传递过来的时候就已经赋值给PageModel了
		String audit = StrEdit.StrChkNull(request.getParameter("audit"));
		String varValue1 = StrEdit.StrChkNull(request.getParameter("varValue1"));
		String varValue2 = StrEdit.StrChkNull(request.getParameter("varValue2"));
		int startRow = 0;
        if(pageNumber!=0) {
        	startRow = (pageNumber - 1) * pageSize;
        }
        newMap.put("startRow", startRow);
        newMap.put("pageSize", pageSize);
		/*int pageStart = (pageNumber -1) * pageSize+1;
        int pageEnd = pageStart + pageSize -1;
        newMap.put("pageStart", pageStart);
        newMap.put("pageEnd", pageEnd);*/
        newMap.put("keywords", keywords);
        newMap.put("audit", audit);
        newMap.put("varValue1", varValue1);
        newMap.put("varValue2", varValue2);
		List<UserRole> list = service.pageList(newMap);
		int totalRecords = service.totalRecord(newMap);
		pageModel.setTotal(totalRecords);
		
		//Object jsonObject = JSONObject.toJSON(list);
		//System.out.println("jsonObject:"+jsonObject);
		//System.out.println("totalRecords1:"+totalRecords);
		
		int pn = 1;
		if (pageNumber != 0) {
			pn = new Integer(pageNumber);
		}
		pageModel.setPages(totalRecords, pn);//设置总记录数和当前页面
		Map<String, Object> map = new HashMap<>();
		map.put("page", pageModel);
		map.put("list", list);
		return map;
	}
	
	@RequestMapping("/add")
	public String toAdd(UserRole data,Model model){
		Map<Long, String> typeMap = new HashMap<Long, String>();
		/*
		List<Map<String,Object>> typeMaps = archive.selectArchiveTypeMapList();
        for (Map<String, Object> map : typeMaps) {
        	Long ids = null;
            String name = null;
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                if ("CONID".equals(entry.getKey())) {
                	ids = ((java.math.BigDecimal) entry.getValue()).longValue();
                } else if ("VARVALUE2".equals(entry.getKey())) {
                	name = (String) entry.getValue();
                }
            }
            typeMap.put(ids, name);
        }
        */
		data.setVarValue15("1");//设置为待提交
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
		return "/front/userrole/add";
	}
	
	@RequestMapping("/save")
	@ResponseBody
	public String save(UserRole data,HttpServletRequest request,HttpServletResponse response){
		int conId = 0;
		String strTemp = "";
		try{
			service.save(data);
			conId = data.getConId();
		}catch(Exception e){//插入数据的时候，如果有的字段必填，但是没有数据，就会出现异常
			conId = 0;
			System.out.println("save插入数据失败" + e.getLocalizedMessage());
		}
		
		//response.setContentType("application/json;charset=UTF-8");
		//response.setContentType("application/json");
		
		if(conId>0){
			
			String attachIds=StrEdit.StrChkNull(request.getParameter("attachIds"));//附件
			if(!attachIds.equals("")){
				String[] arrAttachIds = attachIds.split("\\,");
				for(int m=0;m<arrAttachIds.length;m++){
					boolean boolValue = attach.update(StrEdit.StrToInt(arrAttachIds[m]),conId);
					if(!boolValue){
						System.out.println("附件信息ID更新失败："+arrAttachIds[m]);
					}
				}
			}
			
			strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"添加失败\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/update")
	@ResponseBody
	public String update(UserRole data,HttpServletRequest request){
		int conId = data.getConId();
		String strTemp = "";
		//Date currTime = new Date();
		//data.setUpdateTime(currTime);
		boolean boolValue = service.update(data);
		if(boolValue){
			strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"更新失败\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/getById")
	public String getById(int id,Model model){
		Map<Long, String> typeMap = new HashMap<Long, String>();
        
        List<Attachment> attachList = attach.list(135,id);
        
        model.addAttribute("data", service.findById(id));
        model.addAttribute("typeMap", typeMap);
        model.addAttribute("attachList", attachList);
        
		return "/front/userrole/add";
	}
	
	@RequestMapping("/approvalGetById")
	public String approvalGetById(int id,Model model){
        
        List<Attachment> attachList211 = attach.list(211,id);
        List<Attachment> attachList = attach.list(212,id);
        
        model.addAttribute("data", service.findById(id));
        model.addAttribute("attachList211", attachList211);
        model.addAttribute("attachList", attachList);
        
		return "/front/userrole/approvaladd";
	}
	
	@RequestMapping("/auditupdate")
	@ResponseBody
	public String auditUpdate(UserRole data,HttpServletRequest request){
		int conId = data.getConId();
		data.setTask("audit");
		String strTemp = "";
		String varValue27 = StrEdit.StrChkNull(request.getParameter("varValue27"));
		System.out.println("varValue27:"+varValue27);
		
		boolean boolValue = service.updateByTask(data);
		if(boolValue){
			strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"更新失败\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public String delete(int id,HttpServletRequest request){
		String strTemp = "";
		boolean boolValue = service.delete(id);
		if(boolValue){
			strTemp = "{\"status\":\"1\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"删除失败\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/audit")
	@ResponseBody
	public String audit(int id,HttpServletRequest request){
		String strTemp = "";
		boolean boolValue = false;
		String task = StrEdit.StrChkNull(request.getParameter("task"));
		String selectids = StrEdit.StrChkNull(request.getParameter("selectids"));
		System.out.println("task:"+task);
		if(task.equals("auditstatus")){
			
			String status = StrEdit.StrChkNull(request.getParameter("status"));
			String[] arrParameter = selectids.split("\\,");//如果不包含,则直接返回原字符的一维数组
	    	if (arrParameter!=null){
				for(int i=0;i<arrParameter.length;i++){
					
					Map<String,Object> newMap = new HashMap<String,Object>();
					newMap.put("varValue15", status);
			        newMap.put("conId", StrEdit.StrToInt(arrParameter[i]));
			        boolValue = service.updateByMap(newMap);
			        
				}
			}
			
		}else if(task.equals("auditback")){
			
			String status = StrEdit.StrChkNull(request.getParameter("status"));System.out.println("status:"+status);
			String reason = StrEdit.StrChkNull(request.getParameter("reason"));
			String[] arrParameter = selectids.split("\\,");//如果不包含,则直接返回原字符的一维数组
	    	if (arrParameter!=null){
				for(int i=0;i<arrParameter.length;i++){
					
					Map<String,Object> newMap = new HashMap<String,Object>();
					newMap.put("varValue15", status);
					newMap.put("varValue16", reason);System.out.println("reason:"+reason);
			        newMap.put("conId", StrEdit.StrToInt(arrParameter[i]));
			        boolValue = service.updateByMap(newMap);
			        
				}
			}
			
		}else if(task.equals("professionalaudit")){
			
			String status = StrEdit.StrChkNull(request.getParameter("status"));
			String DATEVALUE1 = StrEdit.StrChkNull(request.getParameter("DATEVALUE1"));
			String VARVALUE21 = StrEdit.StrChkNull(request.getParameter("VARVALUE21"));
			String VARVALUE22 = StrEdit.StrChkNull(request.getParameter("VARVALUE22"));
			String TEXT2 = StrEdit.StrChkNull(request.getParameter("TEXT2"));
			String[] arrParameter = selectids.split("\\,");//如果不包含,则直接返回原字符的一维数组
	    	if (arrParameter!=null){
				for(int i=0;i<arrParameter.length;i++){
					
					UserRole data = new UserRole();
					data.setConId(StrEdit.StrToInt(arrParameter[i]));
					data.setVarValue15(status);
					data.setVarValue21(VARVALUE21);
					data.setVarValue22(VARVALUE22);
					data.setText2(TEXT2);
					Date dateValue1 = StrEdit.getStrToDate(DATEVALUE1, "yyyy-MM-dd");//yyyy-MM-dd HH:mm:ss
					data.setDateValue1(dateValue1);
					boolValue = service.updateByTask(data);
					
					/*Map<String,Object> newMap = new HashMap<String,Object>();
					newMap.put("varValue15", status);
					newMap.put("varValue21", VARVALUE21);
					newMap.put("varValue22", VARVALUE22);
					newMap.put("text2", TEXT2);
					newMap.put("dateValue1", DATEVALUE1);//日期的使用map更新不了，不知道为什么
			        newMap.put("conId", StrEdit.StrToInt(arrParameter[i]));
			        boolValue = service.updateStatus(newMap);*/
			        
				}
			}
			
		}
		if(boolValue){
			strTemp = "{\"status\":\"1\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"操作失败\"}";
		}
		
		return strTemp;
	}
	
}
