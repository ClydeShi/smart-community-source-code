package com.luktel.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.luktel.model.Menu;
import com.luktel.service.MenuService;
import com.util.common.PageModel;
import com.util.common.StrEdit;

/*
 * @Controller是标记在类UserController上面的，所以类UserController就是一个SpringMVC Controller对象了
 * 使用@RequestMapping("/getAllUser")标记在Controller方法上，表示当请求/getAllUser的时候访问的是LoginController的getAllUser方法，
 * 该方法返回了一个包括Model和View的ModelAndView对象113323
 */

@Controller//@Controller注解标识一个控制器
@RequestMapping("/menu")//如果@RequestMapping注解在类级别上，则表示相对路径，在方法级别上，则标记访问的路径
public class MenuController{
	
	@Autowired
	private MenuService service;
	
	@RequestMapping("/list")
	public String list() {
		return "front/menu/list";
	}
	
	@RequestMapping("/iconselect")
	public String iconselect() {
		return "front/menu/iconselect";
	}
	
	@RequestMapping("/order")
	public String order(HttpServletRequest request) {//排序
		int channelID = StrEdit.StrToInt(request.getParameter("channelID"));
		String task = StrEdit.StrChkNull(request.getParameter("task"));
		request.setAttribute("channelID", channelID);
		return "front/menu/order";
	}
	
	@RequestMapping("/orderupdate")
	public String orderupdate(HttpServletRequest request) {//排序更新
		String strTemp = "";
		String result = "";
		int ChannelID=StrEdit.StrToInt(request.getParameter("ChannelID"));
		int ClassID=StrEdit.StrToInt(request.getParameter("ClassID"));
		int MoveNum=StrEdit.StrToInt(request.getParameter("MoveNum"));
		int cRootID=StrEdit.StrToInt(request.getParameter("cRootID"));
		String task = StrEdit.StrChkNull(request.getParameter("task"));
		
		MenuManage menus = new MenuManage();
		
		if(task.equals("upOrder")){
			try {
				result = menus.upOrder(ChannelID,ClassID,cRootID,MoveNum);
			} catch (Exception e) {
				System.out.println("菜单排序异常" + e.getMessage());
				e.printStackTrace();
			}
			if(result.equals("ok")){
				return "redirect:/menu/order?task=order&channelID="+ChannelID;
			}else{
				strTemp = result;
				request.setAttribute("warntext", strTemp);
				return "front/index/warn";
			}
		}
		
		if(task.equals("downOrder")){
			try {
				result = menus.downOrder(ChannelID,ClassID,cRootID,MoveNum);
			} catch (Exception e) {
				System.out.println("菜单排序异常" + e.getMessage());
				e.printStackTrace();
			}
			if(result.equals("ok")){
				return "redirect:/menu/order?task=order&channelID="+ChannelID;
			}else{
				strTemp = result;
				request.setAttribute("warntext", strTemp);
				return "front/index/warn";
			}
		}
		
		if(task.equals("upOrderN")){
			try {
				result = menus.upOrderN(ClassID,MoveNum);
			} catch (Exception e) {
				System.out.println("菜单排序异常" + e.getMessage());
				e.printStackTrace();
			}
			if(result.equals("ok")){
				return "redirect:/menu/order?task=orderN&channelID="+ChannelID;
			}else{
				strTemp = result;
				request.setAttribute("warntext", strTemp);
				return "front/index/warn";
			}
		}
		
		if(task.equals("downOrderN")){
			try {
				result = menus.downOrderN(ClassID,MoveNum);
			} catch (Exception e) {
				System.out.println("菜单排序异常" + e.getMessage());
				e.printStackTrace();
			}
			if(result.equals("ok")){
				return "redirect:/menu/order?task=orderN&channelID="+ChannelID;
			}else{
				strTemp = result;
				request.setAttribute("warntext", strTemp);
				return "front/index/warn";
			}
		}
		
		strTemp = "操作错误";
		request.setAttribute("warntext", strTemp);
		return "front/index/warn";
	}
	
	@RequestMapping("/all")
	public String all(HttpServletRequest request) {
		int channelID = StrEdit.StrToInt(request.getParameter("channelID"));
		request.setAttribute("channelID", channelID);
		
		List<Menu> list1 = new ArrayList<Menu>();
		Map<String,Object> newMap1 = new HashMap<String,Object>();
		newMap1.put("task", "all");
        newMap1.put("channelID", channelID);
        list1 = service.getListByMap(newMap1);
		request.setAttribute("list1", list1);
		
		return "front/menu/list";
	}
	
	@RequestMapping("/treeData")
	@ResponseBody
	public String treeData(HttpServletRequest request) {//返回菜单json 用于选择上级菜单
		StringBuffer strBuff = new StringBuffer("");
		//strBuff.append("{");
		//strBuff.append("\"categoryList\": [");
		strBuff.append("[");
		
		int type = StrEdit.StrToInt(request.getParameter("type"));
		int channelID = StrEdit.StrToInt(request.getParameter("channelID"));
		
		List<Menu> list1 = new ArrayList<Menu>();
		Map<String,Object> newMap1 = new HashMap<String,Object>();
		newMap1.put("task", "all");
        newMap1.put("channelID", channelID);
        list1 = service.getListByMap(newMap1);
        for(int m=0; m<list1.size(); m++) {
        	if(m>0){strBuff.append(",");}
        	Menu temp = list1.get(m);
        	strBuff.append("{");
        	if(type==3 || type==4){//人员多选才显示isParent，才会加载人员信息
        		strBuff.append("\"isParent\": \"true\",");
        	}
			strBuff.append("\"id\": \""+temp.getConId()+"\",");
			strBuff.append("\"pId\": \""+temp.getParentId()+"\",");
			strBuff.append("\"name\": \""+temp.getClassName()+"\"");
			//strBuff.append("\"open\": \"true\"");
			strBuff.append("}");
			
        	//strBuff.append("{ id:"+temp.getConId()+", pId:"+temp.getParentId()+", name:'"+temp.getClassName()+"', open:true}");//这种拼接的返回不是json
        }
        strBuff.append("]");
        //strBuff.append("],");
        //strBuff.append("\"resultcode\": \"10000\",");
		//strBuff.append("\"message\": \"接口通信成功\"");
		//strBuff.append("}");
		//System.out.println("list:" + strBuff.toString());
        
        //String strTemp = "{\"status\":\"2\",\"id\":\"2\",\"name\":\"2\"}";
		
		return strBuff.toString();
	}
	
	@RequestMapping("/treeDataSelect")
	@ResponseBody
	public String treeDataSelect(HttpServletRequest request) {//返回菜单json，选住已有数据 用于角色权限
		StringBuffer strBuff = new StringBuffer("");
		//strBuff.append("{");
		//strBuff.append("\"categoryList\": [");
		strBuff.append("[");
		
		int type = StrEdit.StrToInt(request.getParameter("type"));
		int channelID = StrEdit.StrToInt(request.getParameter("channelID"));
		String selectIds = StrEdit.StrChkNull(request.getParameter("selectIds"));
		
		List<Menu> list1 = new ArrayList<Menu>();
		Map<String,Object> newMap1 = new HashMap<String,Object>();
		newMap1.put("task", "allshow");
        newMap1.put("channelID", channelID);
        list1 = service.getListByMap(newMap1);
        for(int m=0; m<list1.size(); m++) {
        	if(m>0){strBuff.append(",");}
        	Menu temp = list1.get(m);
        	int id = temp.getConId();
        	strBuff.append("{");
			strBuff.append("\"id\": \""+id+"\",");
			strBuff.append("\"pId\": \""+temp.getParentId()+"\",");
			strBuff.append("\"name\": \""+temp.getClassName()+"\",");
			if(StrEdit.isInclude(selectIds, id)){
				strBuff.append("\"checked\": \"true\",");
			}else{
				strBuff.append("\"checked\": \"false\",");
			}
			strBuff.append("\"open\": \"true\"");
			strBuff.append("}");
			
        	//strBuff.append("{ id:"+temp.getConId()+", pId:"+temp.getParentId()+", name:'"+temp.getClassName()+"', open:true}");//这种拼接的返回不是json
        }
        strBuff.append("]");
        //strBuff.append("],");
        //strBuff.append("\"resultcode\": \"10000\",");
		//strBuff.append("\"message\": \"接口通信成功\"");
		//strBuff.append("}");
		//System.out.println("list:" + strBuff.toString());
        
        //String strTemp = "{\"status\":\"2\",\"id\":\"2\",\"name\":\"2\"}";
		
		return strBuff.toString();
	}
	
	@RequestMapping("/treeselect")
	public String treecheck(HttpServletRequest request) {
		int type = StrEdit.StrToInt(request.getParameter("type"));
		int channelID = StrEdit.StrToInt(request.getParameter("channelID"));
		String selectIds = StrEdit.StrChkNull(request.getParameter("selectIds"));
		request.setAttribute("type", type);
		request.setAttribute("channelID", channelID);
		request.setAttribute("selectIds", selectIds);
		return "front/menu/treeselect";
	}
	
	@RequestMapping("/getDepartmentById")
	@ResponseBody
	public String getDepartmentById(int id){
		String strTemp = "";
		Menu data = service.findById(id);
		strTemp = "{\"status\":\"1\",\"id\":\""+id+"\",\"name\":\""+data.getVarValue1()+"\"}";
		return strTemp;
	}
	
	@RequestMapping("/get")
	@ResponseBody
	public Map<String, Object> getAll(PageModel<Menu> pageModel, Menu data, HttpServletRequest request, Model model) {
		
		//System.out.println("------------------get------------------");
		String keywords = StrEdit.StrChkNull(data.getKeywords());
		
		Map<String,Object> newMap = new HashMap<String,Object>();
		
		int pageNumber = StrEdit.StrToInt(request.getParameter("pageNumber"));
		int pageSize = StrEdit.StrToInt(request.getParameter("pageSize"));
		int startRow = 0;
        if(pageNumber!=0) {
        	startRow = (pageNumber - 1) * pageSize;
        }
        newMap.put("startRow", startRow);
        newMap.put("pageSize", pageSize);
		/*int pageStart = (pageNumber -1) * pageSize+1;//oracle分页
        int pageEnd = pageStart + pageSize -1;
        newMap.put("pageStart", pageStart);
        newMap.put("pageEnd", pageEnd);*/
        newMap.put("keywords", keywords);
		List<Menu> list = service.pageList(newMap);
		int totalRecords = service.totalRecord(newMap);
		pageModel.setTotal(totalRecords);
		
		//Object jsonObject = JSONObject.toJSON(list);
		//System.out.println("jsonObject:"+jsonObject);
		//System.out.println("totalRecords1:"+totalRecords);
		
		int pn = 1;
		if (pageNumber != 0) {
			pn = new Integer(pageNumber);
		}
		pageModel.setPages(totalRecords, pn);//设置总记录数和当前页面
		Map<String, Object> map = new HashMap<>();
		map.put("page", pageModel);
		map.put("list", list);
		return map;
	}
	
	@RequestMapping("/add")
	public String toAdd(Menu data,HttpServletRequest request,Model model){
		int channelID = StrEdit.StrToInt(request.getParameter("channelID"));
		int parentId = StrEdit.StrToInt(request.getParameter("parentId"));
		
		String parentName = "";
		if(parentId>0){
			Map<String,Object> newMap1 = new HashMap<String,Object>();
			newMap1.put("task", "className");
	        newMap1.put("conId", parentId);
	        parentName = service.getStringValue(newMap1);
		}
        
		request.setAttribute("channelID", channelID);
		request.setAttribute("parentId", parentId);
		request.setAttribute("parentName", parentName);
		
		data.setVarValue7("无");
		
		Map<Long, String> typeMap = new HashMap<Long, String>();
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
		return "/front/menu/add";
	}
	
	@RequestMapping("/save")
	@ResponseBody
	public String save(Menu data,HttpServletRequest request,HttpServletResponse response){
		int conId = 0;
		String strTemp = "";
		System.out.println("--------------------------------");
		
		int channelID = StrEdit.StrToInt(request.getParameter("channelID"));
		int parentId = StrEdit.StrToInt(request.getParameter("parentId"));
		int intValue1 = StrEdit.StrToInt(request.getParameter("intValue1"));
		int status = StrEdit.StrToInt(request.getParameter("status"));
		String className = StrEdit.StrChkNull(request.getParameter("className"));
		String text1 = StrEdit.StrChkNull(request.getParameter("text1"));
		String varValue5 = StrEdit.StrChkNull(request.getParameter("varValue5"));
		String varValue6 = StrEdit.StrChkNull(request.getParameter("varValue6"));
		String varValue7 = StrEdit.StrChkNull(request.getParameter("varValue7"));
		String varValue8 = StrEdit.StrChkNull(request.getParameter("varValue8"));
		String addTime = StrEdit.StrChkNull(request.getParameter("addTime"));
		System.out.println("channelID"+channelID);
		System.out.println("parentId"+parentId);
		System.out.println("className"+className);
		System.out.println("text1"+text1);
		MenuManage menus = new MenuManage();
		int flag = 0;
		try {
			flag = menus.createNew(channelID,parentId,className,varValue5,varValue6,varValue7,varValue8,text1,addTime,intValue1,status);
		} catch (Exception e) {
			System.out.println("添加部门异常" + e.getMessage());
			e.printStackTrace();
		}
		String strValue = menus.getStrValue1();
		if(flag==1000){
			
			strTemp = "{\"status\":\"1\",\"conId\":\""+Integer.parseInt(strValue)+"\"}";
		}else{
			strValue = "添加失败";
			strTemp = "{\"status\":\"0\",\"errors\":\""+strValue+"\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/update")
	@ResponseBody
	public String update(Menu data,HttpServletRequest request){
		int conId = data.getConId();
		String strTemp = "";
		//Date currTime = new Date();
		//data.setUpdateTime(currTime);
		boolean boolValue = service.update(data);
		if(boolValue){
			strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"更新失败\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/getById")
	public String getById(int id,HttpServletRequest request,Model model){
		Map<Long, String> typeMap = new HashMap<Long, String>();
		
		Menu data = service.findById(id);
		int channelID = data.getChannelID();
		request.setAttribute("channelID", channelID);
        
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
        
		return "/front/menu/add";
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public String delete(int id,HttpServletRequest request){
		String strTemp = "";
		
		MenuManage menus = new MenuManage();
		int flag = 0;
		try {
			flag = menus.delInfo(id);
		} catch (Exception e) {
			System.out.println("删除异常" + e.getMessage());
			e.printStackTrace();
		}
		String strValue = menus.getStrValue1();
		if(flag==1000){
			strTemp = "{\"status\":\"1\"}";
		}else{
			strValue = menus.getStrValue1();
			strTemp = "{\"status\":\"0\",\"errors\":\""+strValue+"\"}";
		}
		
		return strTemp;
	}
	
}
