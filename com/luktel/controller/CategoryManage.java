package com.luktel.controller;

import java.sql.*;

import com.util.common.StrEdit;
import com.util.common.DBConn2;

public class CategoryManage {
    private DBConn2 mdb = null;
    private ResultSet rs=null;
    
    private int userId = 0;
    private int userCat = 0;
    private String userName = "";
    
    private String strValue1 = "";
    
    public CategoryManage() {
    	mdb = new DBConn2();
    }
    
    public int getUserCat() {
		return userCat;
	}

	public void setUserCat(int userCat) {
		this.userCat = userCat;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getStrValue1() {
		return strValue1;
	}

	public void setStrValue1(String strValue1) {
		this.strValue1 = strValue1;
	}
	
    public int createNew(int ChannelID,int ParentID,String className,String varValue1,String varValue2,String varValue3,String varValue4,String varValue5,String varValue6,String varValue7,String varValue8,String text1,String addTime,int intValue1,int status) throws Exception {
    	
    	int flag = 0;
    	String strTemp = "未知";
    	
    	System.out.println("------------------------------开始添加------------------------------");
    	
    	if(ParentID<0 || className.equals("")){
    		flag = 1001;
    		strTemp = "参数错误";
    		System.out.println(strTemp);
    		this.setStrValue1(strTemp);
    		return flag;
    	}
    	
    	Connection conn=null;
		Statement state=null;
		conn = mdb.getNewConnection();
		conn.setAutoCommit(false);
		
		Connection con = null;
		Statement stmt = null;
		
		try{
			
			state = conn.createStatement();
			
			con = mdb.getNewConnection();
	    	stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
	    	//Statement stmt2 = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
	    	
	    	String sql = "", ParentName = "", ParentPath = "", arrChildID = "", ParentDir = "", ClassDir = "";
	    	int i = 0, MaxClassID = 0, MaxRootID = 0, Child = 0, PrevOrderID = 0, PrevID = 0, Depth = 0;
	    	int RootID = 0 , ParentDepth = 0;
	    	
	    	//判断同一频道,同一上级目录,是否重名
	    	sql = "select * from categoryconn where ChannelID="+ChannelID+" and ParentID="+ParentID+" AND ClassName='"+className+"'";
	    	rs = mdb.executeQuery(sql);
	    	if(rs.next()){
	    	  if(ParentID == 0){
	    		  flag = 1001;
		    	  strTemp = "已经存在一级栏目："+className+"";
		          System.out.println(strTemp);
		    	  this.setStrValue1(strTemp);
		    	  return flag;
	    	  }else{
	    		  flag = 1001;
		    	  strTemp = "已经存在子栏目："+className+"";
		          System.out.println(strTemp);
		    	  this.setStrValue1(strTemp);
		    	  return flag;
	    	  }
	    	}
	    	rs.close();
	    	
	    	//取得最大id
	    	int ClassID = 0;
	    	sql = "select max(ClassID) as maxClassID from categoryconn";
	    	//sql = "select max(ClassID) as maxClassID from categoryconn where ChannelID="+ChannelID+"";
	    	rs = mdb.executeQuery(sql);
	    	if(rs.next()){
	    	  MaxClassID = rs.getInt("maxClassID");
	    	  //if(MaxClassID == null)
	    	  //MaxClassID = 0;
	    	}
	    	ClassID = MaxClassID + 1;
	    	rs.close();
	    	
	    	//判断ID是否使用
	    	sql = "select * from categoryconn where ClassID="+ClassID+" and ChannelID="+ChannelID+"";
	    	rs = mdb.executeQuery(sql);
	    	if(rs.next()){
	    		flag = 1001;
	    		strTemp = "ID已经被使用："+ClassID+"";
	    		System.out.println(strTemp);
	    		this.setStrValue1(strTemp);
	    		return flag;
	    	}
	    	rs.close();
	    	
	    	//取得最大根目录id,用作排序,所有同一个栏目下的RootID相同
	    	sql = "select max(RootID) as maxRootID from categoryconn where ChannelID="+ChannelID+"";
	    	rs = mdb.executeQuery(sql);
	    	if(rs.next()){
	    	  MaxRootID = rs.getInt("maxRootID");
	    	  //if(MaxRootID == null)
	    	  //MaxRootID = 0;
	    	}

	    	RootID = MaxRootID + 1;
	    	rs.close();
	    	//System.out.println("最大根目录id:" + ClassID);
	    	
	    	//添加子栏目
	    	if(ParentID>0){
	    	    sql = "Select * from categoryconn Where ClassID="+ParentID+"";//判断上级目录是否存在
	    	    rs = mdb.executeQuery(sql);
	    		if(rs.next()){
	    		    RootID = rs.getInt("RootID");
	    	        ParentName = rs.getString("ClassName");
	    	        ParentDepth = rs.getInt("Depth");
	    	        ParentPath = rs.getString("ParentPath") + "," + rs.getInt("ClassID");    //得到此栏目的父级栏目路径
	    	        Child = rs.getInt("Child");
	    	        arrChildID = rs.getString("arrChildID") + "," + ClassID;
	    	        ClassDir = rs.getString("ClassDir");
	    	        ParentDir = rs.getString("ParentDir") + rs.getString("ClassDir") + "/";  //取出上级的父目录和上级目录作为当前栏目的父目录
	    			
	    			//更新本栏目的所有上级栏目的子栏目ID数组
	    			sql = "select ClassID,arrChildID from categoryconn where ClassID in (" + ParentPath + ")";
	    	        ResultSet rs2 = stmt.executeQuery(sql);
	    	        while(rs2.next()){
	    	        	sql = "update categoryconn set arrChildID='" + rs2.getString("arrChildID") + "," + ClassID + "' where ClassID=" + rs2.getInt("ClassID");
	    	        	state.execute(sql);
	    	        	//System.out.println(sql);
	    	        	//stmt2.executeUpdate("update categoryconn set arrChildID='" + rs2.getString("arrChildID") + "," + ClassID + "' where ClassID=" + rs2.getInt("ClassID"));
	    			}
	    			rs2.close();
	    			//stmt2.close();
	    			
	    			if(Child > 0){//存在子栏目,获取同栏目最大的序号
	    			
	    	            //得到父栏目的所有子栏目中最后一个栏目的OrderID,增加1作为序号id
	    				ResultSet rs3 = stmt.executeQuery("select max(OrderID) as maxOrderID from categoryconn where ClassID in ( " + arrChildID + ")");
	    				if(rs3.next()){
	    				    PrevOrderID = rs3.getInt("maxOrderID");
	    				}
	    	            rs3.close();
	    	            
	    	            //得到本栏目的上一个栏目ID
	    				rs3 = stmt.executeQuery("select ClassID from categoryconn where ChannelID=" + ChannelID + " and ParentID=" + ParentID + " order by OrderID desc limit 1");
	    				if(rs3.next()){
	    				    PrevID = rs3.getInt("ClassID");
	    				}
	    	            rs3.close();
	    				
	    			}else{//不存在子栏目
	    				
	    				PrevOrderID = rs.getInt("OrderID");
	    	            PrevID = 0;
	    	            
	    			}
	    			
	    		}else{
	    			flag = 1001;
	    			strTemp = "所属栏目已经被删除！";
	    			System.out.println(strTemp);
	        		this.setStrValue1(strTemp);
	    			return flag;
	    		}
	    		
	    	}else{//一级栏目
	    		    
	    			if(MaxRootID > 0){//如果存在最大根目录,则取其ClassID作为PrevID
	    				ResultSet rs4 = stmt.executeQuery("select ClassID from categoryconn where ChannelID=" + ChannelID + " and RootID=" + MaxRootID + " and Depth=0");
	    				if(rs4.next()){
	    				  PrevID = rs4.getInt("ClassID");
	    				}
	    				rs4.close();
	    			}else{
	    			    PrevID = 0;
	    			}
	    			
	    	        PrevOrderID = 0;
	    	        ParentPath = "0";
	    	        ParentDir = "";
	    	}

	    	if(ParentID > 0){
	    	    Depth = ParentDepth + 1;
	    	}else{
	    	    Depth = 0;
	    	}
	    	
	    	sql = "insert into categoryconn(ChannelID,ClassID,RootID,ParentID,Depth,ParentPath,OrderID,Child,PrevID,NextID,arrChildID,ClassName,ClassDir,ParentDir,VARVALUE1,VARVALUE2,VARVALUE3,VARVALUE4,VARVALUE5,VARVALUE6,VARVALUE7,VARVALUE8,INTVALUE1,TEXT1,ADDTIME,STATUS)";
	    	sql = sql + "values("+ChannelID+","+ClassID+","+RootID+","+ParentID+","+Depth+",'"+ParentPath+"',"+PrevOrderID+",0,"+PrevID+",0,'"+ClassID+"','"+className+"','"+ClassDir+"','"+ParentDir+"','"+varValue1+"','"+varValue2+"','"+varValue3+"','"+varValue4+"','"+varValue5+"','"+varValue6+"','"+varValue7+"','"+varValue8+"',"+intValue1+",'"+text1+"','"+addTime+"',"+status+")";
	    	state.execute(sql);
	    	//i = stmt.executeUpdate(sql);
	    	
	    	//更新与本栏目同一父栏目的上一个栏目的"NextID"字段值
	    	if(PrevID > 0){
	    		sql = "update categoryconn set NextID=" + ClassID + " where ClassID=" + PrevID;
	    	    stmt.executeUpdate(sql);
	    	}

	    	if(ParentID > 0){
	    	    //更新其父类的子栏目数
	    		sql = "update categoryconn set Child=Child+1 where ClassID=" + ParentID;
	    		state.execute(sql);
	    	    //stmt.executeUpdate(sql);
	    		//更新该栏目排序,以及大于本分类下的栏目排序序号,即上级栏目的同类栏目,将他们序号加大１,排到该子栏目后面
	    	    sql = "update categoryconn set OrderID=OrderID+1 where ChannelID=" + ChannelID + " and RootID=" + RootID + " and OrderID>" + PrevOrderID;
	    	    state.execute(sql);
	    		//stmt.executeUpdate(sql);
	    		sql = "update categoryconn set OrderID=" + PrevOrderID + "+1 where ClassID=" + ClassID;
	    		state.execute(sql);
	    		//stmt.executeUpdate(sql);
	    	}
	    	
	    	conn.commit();  //数据库操作最终提交给数据库
			conn.setAutoCommit(true);
	    	
	    	flag = 1000;
    		strTemp = "ok";
    		System.out.println("添加成功");
    		this.setStrValue1(String.valueOf(ClassID));
			
		}catch(Exception ex){
			flag = 1001;
    		strTemp = "添加失败，出现异常";
    		this.setStrValue1(strTemp);
			conn.rollback();
			System.out.println(strTemp);
			System.out.println(ex.getMessage());
		}finally{
			if(stmt!=null){stmt.close();}
			if(con!=null){con.close();}
			
			if(state!=null){state.close();}
			if(conn!=null){conn.close();}
			mdb.close();
		}
		
        return flag;
    }
    
    public String modifyInfo(int ClassID,String ClassName,int ClassType,String ClassDir,String LinkUrl,String ClassMemo) throws Exception {
    	
    	String reValue = "", sql = "";
    	int i = 0;
    	
    	if(ClassName.trim().equals("")){
    		reValue = "<li>栏目名称不能为空！</li>";
    		return reValue;
    	}
    	
    	sql = "Select * from memberconn Where ClassID="+ClassID+"";
    	rs = mdb.executeQuery(sql);
    	if(!rs.next()){
    		reValue = "<li>找不到指定的栏目！</li>";
    		return reValue;
    	}
    	rs.close();
    	
    	sql = "update memberconn set ClassName = '"+ClassName+"',ClassType = "+ClassType+",ClassDir = '"+ClassDir+"',LinkUrl = '"+LinkUrl+"',ClassMemo = '"+ClassMemo+"' where ClassID = "+ClassID;
    	i = mdb.executeUpdate(sql);
    	if(i > 0){
    		reValue = "ok";
    	}else{
    		reValue = "修改失败";
    	}
    	
    	mdb.close();
        
        return reValue;
    }
    
    public int delInfo(int ClassID) throws Exception {
    	
    	int flag = 0;
    	String strTemp = "未知";
    	
    	Connection conn=null;
		Statement state=null;
		conn = mdb.getNewConnection();
		conn.setAutoCommit(false);
		
		Connection con = null;
		Statement stmt = null;
		
		try{
			
			state = conn.createStatement();
			
			con = mdb.getNewConnection();
	    	stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
			
			String reValue = "", sql = "", ParentPath = "", arrChildID = "";
	    	int NextID = 0, i = 0, ParentID = 0, PrevID = 0, Depth = 0, OrderID = 0, RootID = 0;
	    	
	    	if(ClassID == 0){
	    		flag = 1001;
	    		strTemp = "参数错误";
	    		System.out.println(strTemp);
	    		this.setStrValue1(strTemp);
	    		return flag;
	    	}
	    	
	    	sql = "select * from categoryconn where ClassID="+ClassID+"";
	    	rs = mdb.executeQuery(sql);
	    	if(rs.next()){
	    		
	    		PrevID = rs.getInt("PrevID");
	    	    NextID = rs.getInt("NextID");
	    	    arrChildID = rs.getString("arrChildID");
	    	    RootID = rs.getInt("RootID");
	    	    OrderID = rs.getInt("OrderID");
	    	    Depth = rs.getInt("Depth");
	    	    ParentID = rs.getInt("ParentID");
	    	    ParentPath = rs.getString("ParentPath");
	    	    
	    	    if(Depth > 0){//如果删除的栏目深度大于0,即存在上级目录,则把上级目录的子目录数减1
	    	    	sql = "update categoryconn set Child=Child-1 where ClassID=" + ParentID;
	    	    	state.execute(sql);
		        	
	    	    	//stmt.executeUpdate("update categoryconn set Child=Child-1 where ClassID=" + ParentID);
	    	    	
	    	    	//更新此栏目的原来所有上级栏目的子栏目ID数组
	    			sql = "select ClassID,arrChildID from categoryconn where ClassID in (" + ParentPath + ")";
	    	        ResultSet rs2 = stmt.executeQuery(sql);
	    	        while(rs2.next()){
	    	        	sql = "update categoryconn set arrChildID='" + removeID(rs2.getString("arrChildID"), arrChildID) + "' where ClassID=" + rs2.getInt("ClassID");
	    	        	state.execute(sql);
	    	        	
	    	        	//stmt2.executeUpdate("update categoryconn set arrChildID='" + removeID(rs2.getString("arrChildID"), arrChildID) + "' where ClassID=" + rs2.getInt("ClassID"));
	    			}
	    			rs2.close();
	    			//stmt2.close();
	    			
	    			//更新与此栏目同根且排序在其之下的栏目 UBound(Split(arrChildID, ","))
	    			sql = "update categoryconn set OrderID=OrderID-" + (arrChildID.split("\\,").length - 1) + " + 1 where RootID=" + RootID + " and OrderID>" + OrderID;
		        	state.execute(sql);
		        	
	    			//stmt.executeUpdate("update categoryconn set OrderID=OrderID-" + (arrChildID.split("\\,").length - 1) + 1 + " where RootID=" + RootID + " and OrderID>" + OrderID);
	    	    }
	    	    
	    	    //修改上一栏目的NextID和下一栏目的PrevID
	    	    if(PrevID > 0){
	    	    	sql = "update categoryconn set NextID=" + NextID + " where ClassID=" + PrevID;
		    		state.execute(sql);
		    		
	    	    	//stmt.executeUpdate("update categoryconn set NextID=" + NextID + " where ClassID=" + PrevID);
	    	    }
	    	    if(NextID > 0){
	    	    	sql = "update categoryconn set PrevID=" + PrevID + " where ClassID=" + NextID;
		    		state.execute(sql);
		    		
	    	    	//stmt.executeUpdate("update categoryconn set PrevID=" + PrevID + " where ClassID=" + NextID);
	    	    }
	    	    
	    	    //删除本栏目（包括子栏目）
	    	    sql = "delete from categoryconn where ClassID in (" + arrChildID + ")";
	    		state.execute(sql);
	    		
	    		conn.commit();  //数据库操作最终提交给数据库
				conn.setAutoCommit(true);
		    	
		    	flag = 1000;
	    		strTemp = "删除成功";
	    		System.out.println(strTemp);
	    		this.setStrValue1(strTemp);
	    		
	    	    //i = stmt.executeUpdate("delete from categoryconn where ClassID in (" + arrChildID + ")");
	    		
	    	}else{
	    		flag = 1001;
	    		strTemp = "classId不存在";
	    		this.setStrValue1(strTemp);
	    		return flag;
	    	}
			
		}catch(Exception ex){
			flag = 1001;
    		strTemp = "删除失败，出现异常";
    		this.setStrValue1(strTemp);
			conn.rollback();
			System.out.println(strTemp);
			System.out.println(ex.getMessage());
		}finally{
			if(stmt!=null){stmt.close();}
			if(con!=null){con.close();}
			
			if(state!=null){state.close();}
			if(conn!=null){conn.close();}
			mdb.close();
		}
		
		return flag;
    }
    
    public void doPatch() throws Exception {
    	
    	Connection con = mdb.getNewConnection();
    	Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
    	
    	//更新一级栏目
    	int PrevID = 0, ClassID = 0 , ClassType = 0;
    	String ParentDir = "", ClassDir = "", sql = "";
    	sql = "select ClassID,RootID,OrderID,Depth,ParentID,ParentPath,Child,arrChildID,PrevID,NextID,ClassType,ParentDir,ClassDir ";
    	sql = sql + "from memberconn where ParentID=0 order by RootID";
    	ResultSet rs = mdb.executeQuery(sql);
    	while(rs.next()){
    		ClassID = rs.getInt("ClassID");
    		ClassType = rs.getInt("ClassType");
    		ClassDir = rs.getString("ClassDir");

    		if(PrevID != ClassID && PrevID > 0){
    			stmt.executeUpdate("update memberconn set NextID=" + ClassID + " where ClassID=" + PrevID + "");//更新上一条记录的NextID为当前ClassID
    		}

    		if(ClassType == 1){
        		ParentDir = "/";
        	}

    		String sql2 = "update memberconn set OrderID=0,Depth=0,ParentPath=0,PrevID="+PrevID+",NextID=0,arrChildID='"+ClassID+"',ParentDir='"+ParentDir+"' where ClassID=" + ClassID + "";
    		//System.out.println("更新一级栏目数据:"+sql2);
    		stmt.executeUpdate(sql2);

    		PrevID = ClassID;//当前ClassID作为下一次循环的PrevID使用

    		int iOrderID = 1;

    		UpdateClass(ClassID, 1, "0", "/" + ClassDir + "/", iOrderID);//当前栏目id作为ParentID,取出其子栏目

    	}
    	
    	stmt.close();
    	con.close();
    	mdb.close();

    	//System.out.println("修复栏目结构成功！");
    	
    }
    
    public int iOrderID = 1;
    public void UpdateClass(int iParentID, int iDepth, String sParentPath, String sParentDir, int mOrderID) throws Exception {

    	iOrderID = mOrderID;

    	//System.out.println("____________________________________");
    	//System.out.println("更新频道:"+ChannelID+",栏目:"+iParentID+"的下级栏目数据");
    	
    	Connection con = mdb.getNewConnection();
        Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
        Statement stmt2 = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
    	Statement stmtnew = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
    	
    	int PrevID = 0, ClassID = 0, ClassType = 0;
    	String ParentDir = "", ClassDir = "", ParentPath = "", arrChildID = "",sql = "";
    	ParentPath = sParentPath + "," + iParentID;
    	//System.out.println("TOP ParentPath:"+ParentPath);
    	//System.out.println("iOrderID:"+iOrderID);

    	int total = 0;
    	sql = "select count(*) as total from memberconn where ParentID=" + iParentID + "";
    	ResultSet rs = stmt.executeQuery(sql);
    	if(rs.next()){
    		total = rs.getInt("total");
    	}
    	rs.close();
    	//System.out.println(iParentID+"的下级栏目共有记录:"+total);
    	
    	if(total > 0){//查询是否有子栏目,更新上级栏目Child
    		stmt.executeUpdate("update memberconn set Child=" + total + " where ClassID=" + iParentID + "");
    	}else{
    		stmt.executeUpdate("update memberconn set Child=0 where ClassID=" + iParentID + "");

    	}
    	
    	PrevID = 0;
    	
    	sql = "select ClassID,RootID,OrderID,Depth,ParentID,ParentPath,Child,arrChildID,PrevID,NextID,ClassType,ParentDir,ClassDir ";
    	sql = sql + "from memberconn where ParentID=" + iParentID + " order by OrderID";
    	//System.out.println(sql);  1
    	ResultSet rs2 = stmtnew.executeQuery(sql);
    	while(rs2.next()){
    		//System.out.println(sql);  2  递归调用时,1的sql随着参数变化,2的的sql保持开始的

    		ClassID = rs2.getInt("ClassID");
    		ClassType = rs2.getInt("ClassType");
    		ClassDir = rs2.getString("ClassDir");

    		//System.out.println("ParentPath:"+ParentPath);
    		//在while里递归调用UpdateClass时,ParentPath,sql等还是保留最开始的值,相当于调用是另外一个线程
    		//在里面执行完递归调用后,返回到原先的继续执行,这里的ParentPath只保留开始传来的值
    		//System.out.println("栏目 ClassID:"+ClassID);
    		//System.out.println("iOrderID:"+iOrderID);
    		
    		//更新当前子栏目的所有上级栏目的arrChildID,stmt循环使用,在这里不能关闭
    		ResultSet rs3 = stmt.executeQuery("select ClassID,arrChildID from memberconn where ClassID in (" + ParentPath + ")");		
    		while(rs3.next()){
    			int ClassID3 = rs3.getInt("ClassID");
    			arrChildID = rs3.getString("arrChildID");
    			String newChildID = arrChildID+","+ClassID;
    			//System.out.println("更新上级栏目"+ClassID3+"的arrChildID:"+newChildID);
    			stmt2.executeUpdate("update memberconn set arrChildID='"+newChildID+"' where ClassID=" + ClassID3 + "");
    		}

    		if(PrevID != ClassID && PrevID > 0){
    			stmt2.executeUpdate("update memberconn set NextID=" + ClassID + " where ClassID=" + PrevID + "");//更新上一条记录的NextID为当前ClassID
    		}

    		if(ClassType == 1){
        		ParentDir = sParentDir;
        		//System.out.println("ParentDir:"+ParentDir);
        	}

    		String sql2 = "update memberconn set OrderID="+iOrderID+",Depth="+iDepth+",ParentPath='"+ParentPath+"',PrevID="+PrevID+",NextID=0,";
    		sql2 = sql2 + "arrChildID='"+ClassID+"',ParentDir='"+ParentDir+"' where ClassID=" + ClassID + "";		
    		stmt2.executeUpdate(sql2);
    		//System.out.println("更新栏目:"+ClassID);

    		PrevID = ClassID;//当前ClassID作为下一次循环的PrevID使用

    		iOrderID = iOrderID + 1;

    		UpdateClass(ClassID, iDepth + 1, ParentPath, sParentDir + ClassDir + "/", iOrderID);
    		//递归调用更新下一级目录,在继续更新同级目录的下一个记录
    		
    	}
    	
    	stmt.close();
		stmt2.close();
    	stmtnew.close();
    	con.close();

    }
    
    /*
     * 从字符串中去除另外一个字符串里的数据,以","为分割符
     */
    public String removeID(String classID, String removeId){
    	String arrClassID3 = "", reValue = "";
    	int i = 0, j = 0;
    	boolean bFound = false;
    	
    	if(classID == null){
    		reValue = "";
    	}else if(classID.trim().equals(removeId.trim())){
    		reValue = "";
    	}else{
    		String[] arrClassID = classID.split("\\,");//源数组
    		
    		if(removeId.indexOf(",") != -1){//包含,的
    			String[] arrClassID2 = removeId.split("\\,");//被去除数组
    			
    			for(i = 0; i < arrClassID.length; i++){
    				bFound = false;
    				
    				for(j = 0; j < arrClassID2.length; j++){
    					if(arrClassID[i].equals(arrClassID2[j])){
    						bFound = true;
    						break;
    					}
    				}
    				if(!bFound){
    					if(arrClassID3.equals(""))
    						arrClassID3 = arrClassID[i];
    					else
    						arrClassID3 = arrClassID3 + "," + arrClassID[i];
    				}
    				
    			}
    			
    		}else{
    			
    			for(i = 0; i < arrClassID.length; i++){
    				if(!arrClassID[i].equals(removeId)){
    					if(arrClassID3.equals(""))
    						arrClassID3 = arrClassID[i];
    					else
    						arrClassID3 = arrClassID3 + "," + arrClassID[i];
    				}
    			}
    			
    			
    		}
    		
    		reValue = arrClassID3;
    		
    		
    	}
    	
    	return reValue;
    }
    
    public String getNext(String current) throws Exception{
    	int postion = current.length() - 1;
    	byte []barr = current.getBytes();
    	barr[postion] += 1;
    	return new String(barr);
    }
    
    public int getDepthByID(int id) throws Exception{
        int i=0;
        String sql = "select Depth from memberconn where ClassID="+id+"";
        try{
            rs = mdb.executeQuery(sql);
            if(rs.next()){
            	i = rs.getInt("Depth");
            }
        }
        catch(Exception e){
            System.out.println("UserManage getDepthByID() " +e.getLocalizedMessage());
        }
        finally{
            rs.close();
            mdb.close();
        }
        return i;
    }
    
    public String order(int iChannelID, String basePath) throws Exception{
    	String ClassName = "";
    	int ClassID = 0, RootID = 0, i = 0, iCount = 0;
    	StringBuffer strTemp = new StringBuffer();
    	
    	strTemp.append("<table width='100%' border='0' align='center' cellpadding='0' cellspacing='0' class='table30'>\n");
    	strTemp.append("<tr>\n");
    	strTemp.append("<th height='22' colspan='4' class='thline_top' align='center'><strong>一 级 菜 单 排 序</strong></th>\n");
    	strTemp.append("</tr>\n");
    	
    	String sqlstr = "select count(*) as total from categoryconn where ChannelID="+iChannelID+" and ParentID=0 order by RootID,OrderID";
    	ResultSet rsstr = mdb.executeQuery(sqlstr);
    	if(rsstr.next()){
    		iCount = rsstr.getInt("total");
    	}
    	rsstr.close();
    	
    	String sql = "select * from categoryconn where ChannelID="+iChannelID+" and ParentID=0 order by RootID,OrderID";
    	ResultSet rs = mdb.executeQuery(sql);
    	i = 1;
    	while(rs.next()){
    		ClassID = rs.getInt("ClassID");
    		ClassName = rs.getString("ClassName");
    		RootID = rs.getInt("RootID");
    		
    		strTemp.append("<tr>\n");
    		strTemp.append("<td width='300' class='tdline'>\n");
    		
    		strTemp = strTemp.append(ClassName);
    		
    		if(i>1){
	    		
	    		strTemp.append("<form name='form' action='"+basePath+"menu/orderupdate' method='post'><td width='150' class='tdline'>");
	    		strTemp.append("<select name='MoveNum' size='1'><option value='0'>向上移动</option>");
	    		for(int j=1;j<=i-1;j++){
	    			strTemp.append("<option value='" + j + "'>" + j + "</option>");
	    		}
	    		strTemp.append("</select><input name='ChannelID' type='hidden' id='ChannelID' value='" + iChannelID + "'>");
	    		strTemp.append("<input name='ClassID' type='hidden' value='" + ClassID + "'>");
	    		strTemp.append("<input name='cRootID' type='hidden' value='" + RootID + "'>");
	    		strTemp.append("<input name='task' type='hidden' value='upOrder'>");
	    		strTemp.append("&nbsp;<input type='submit' name='Submit' value='修改'>");
	    		strTemp.append("</td></form>\n");
	    		
	    	}else{
	    		strTemp.append("<td width='150' class='tdline'>&nbsp;</td>\n");
	    	}
    		
	    	if(iCount>i){
	    		
	    		strTemp.append("<form name='form' action='"+basePath+"menu/orderupdate' method='post'><td width='150' class='tdline'>");
	    		strTemp.append("<select name='MoveNum' size='1'><option value='0'>向下移动</option>");
	    		for(int j=1;j<=iCount-i;j++){
	    			strTemp.append("<option value='" + j + "'>" + j + "</option>");
	    		}
	    		strTemp.append("</select><input name='ChannelID' type='hidden' id='ChannelID' value='" + iChannelID + "'>");
	    		strTemp.append("<input name='ClassID' type='hidden' value='" + ClassID + "'>");
	    		strTemp.append("<input name='cRootID' type='hidden' value='" + RootID + "'>");
	    		strTemp.append("<input name='task' type='hidden' value='downOrder'>");
	    		strTemp.append("&nbsp;<input type='submit' name='Submit' value='修改'>");
	    		strTemp.append("</td></form>\n");
	    		
	    	}else{
	    		strTemp.append("<td width='150' class='tdline'>&nbsp;</td>\n");
	    	}
	    	
	    	strTemp.append("<td class='tdline'>&nbsp;</td>\n");
    		strTemp.append("</tr>\n");
    		i = i + 1;
    	}
    	
    	strTemp.append("<tr><td class='tdline_topsingle' colspan='4' style='height:10px;'></td></tr>\n");
    	
    	strTemp.append("</table>\n");
    	rs.close();
    	mdb.close();
    	return strTemp.toString();
    	
    }
    
    public String orderN(int iChannelID, String basePath) throws Exception{
    	String ClassName = "";
    	int ClassID = 0, ClassType = 0, tmpDepth = 0, nextId = 0, ParentID = 0, Child = 0, i = 0;
    	int OrderID = 0, upMoveNum = 0, downMoveNum = 0;
    	StringBuffer strTemp = new StringBuffer();
    	
    	strTemp.append("<table width='100%' border='0' align='center' cellpadding='0' cellspacing='0' class='table30'>\n");
    	strTemp.append("<tr>\n");
    	strTemp.append("<th height='22' colspan='4' class='thline_top' align='center'><strong>N 级 菜 单 排 序</strong></th>\n");
    	strTemp.append("</tr>\n");
    	
    	String sql = "select * from categoryconn where ChannelID="+iChannelID+" order by RootID,OrderID";
    	ResultSet rs = mdb.executeQuery(sql);
    	while(rs.next()){
    		ClassID = rs.getInt("ClassID");
    		ClassName = rs.getString("ClassName");
    		ClassType = rs.getInt("ClassType");
    		tmpDepth = rs.getInt("Depth");
    		nextId = rs.getInt("NextID");
    		ParentID = rs.getInt("ParentID");
    		Child = rs.getInt("Child");
    		OrderID = rs.getInt("OrderID");
    		
    		strTemp.append("<tr>\n");
    		strTemp.append("<td width='300' class='tdline'>\n");
    		if(tmpDepth>0){
    			for(i = 0;i<=tmpDepth;i++){
    				strTemp = strTemp.append("&nbsp;&nbsp;&nbsp;");
    			}
    		}
    		
    		if(Child > 0)
    			strTemp = strTemp.append("<img src='"+basePath+"public/images/tree_folder4.gif' width='15' height='15' align='absmiddle'>\n");
    		else
    			strTemp = strTemp.append("<img src='"+basePath+"public/images/tree_folder3.gif' width='15' height='15' align='absmiddle'>\n");
    		
    		if(tmpDepth == 0)
    			strTemp = strTemp.append("<b>");
    		
    		strTemp = strTemp.append(ClassName);
    		
    		if(Child > 0)
    			strTemp = strTemp.append(" （" + Child + "）");
    		
    		strTemp.append("</td>\n");
    		
    		if(ParentID>0){
    			
    			//如果不是一级栏目，则算出相同深度的栏目数目，得到该栏目在相同深度的栏目中所处位置（之上或者之下的栏目数）
    			//所能提升最大幅度应为For i=1 to 该版之上的版面数
    			
    			String sql2 = "select count(ClassID) as totalNum from categoryconn where ParentID=" + ParentID + " and OrderID<" + OrderID + "";
    	    	ResultSet rs2 = mdb.executeQuery(sql2);
    	    	if(rs2.next()){
    	    		upMoveNum = rs2.getInt("totalNum");
    	    	}
    	    	
    	    	if(upMoveNum>0){
    	    		
    	    		strTemp.append("<form name='form' action='"+basePath+"menu/orderupdate' method='post'><td width='150' class='tdline'>");
    	    		strTemp.append("<select name='MoveNum' size='1'><option value='0'>向上移动</option>");
    	    		for(int j=1;j<=upMoveNum;j++){
    	    			strTemp.append("<option value='" + j + "'>" + j + "</option>");
    	    		}
    	    		strTemp.append("</select><input name='ChannelID' type='hidden' id='ChannelID' value='" + iChannelID + "'>");
    	    		strTemp.append("<input name='ClassID' type='hidden' value='" + ClassID + "'>");
    	    		strTemp.append("<input name='task' type='hidden' value='upOrderN'>");
    	    		strTemp.append("&nbsp;<input type='submit' name='Submit' value='修改'>");
    	    		strTemp.append("</td></form>\n");
    	    		
    	    	}else{
    	    		strTemp.append("<td width='150' class='tdline'>&nbsp;</td>\n");
    	    	}
    	    	rs2.close();
    	    	
    	    	//所能降低最大幅度应为For i=1 to 该版之下的版面数
    	    	String sql3 = "select count(ClassID) as totalNum from categoryconn where ParentID=" + ParentID + " and OrderID>" + OrderID + "";
    	    	ResultSet rs3 = mdb.executeQuery(sql3);
    	    	if(rs3.next()){
    	    		downMoveNum = rs3.getInt("totalNum");
    	    	}
    	    	
    	    	if(downMoveNum>0){
    	    		
    	    		strTemp.append("<form action='"+basePath+"menu/orderupdate' method='post'><td width='150' class='tdline'>");
    	    		strTemp.append("<select name='MoveNum' size='1'><option value='0'>向下移动</option>");
    	    		for(int j=1;j<=downMoveNum;j++){
    	    			strTemp.append("<option value='" + j + "'>" + j + "</option>");
    	    		}
    	    		strTemp.append("</select><input name='ChannelID' type='hidden' id='ChannelID' value='" + iChannelID + "'>");
    	    		strTemp.append("<input name='ClassID' type='hidden' value='" + ClassID + "'>");
    	    		strTemp.append("<input name='task' type='hidden' value='downOrderN'>");
    	    		strTemp.append("&nbsp;<input type='submit' name='Submit' value='修改'>");
    	    		strTemp.append("</td></form>\n");
    	    		
    	    	}else{
    	    		strTemp.append("<td width='150' class='tdline'>&nbsp;</td>\n");
    	    	}
    	    	rs3.close();
    	    	
    		}else{
    			strTemp.append("<td colspan='2' class='tdline'>&nbsp;</td>\n");
    		}
    		
    		strTemp.append("<td class='tdline'>&nbsp;</td>\n");
    		strTemp.append("</tr>\n");
    		
    		upMoveNum = 0;
    		downMoveNum = 0;
    	}
    	
    	strTemp.append("<tr><td class='tdline_topsingle' colspan='4' style='height:10px;'></td></tr>\n");
    	
    	strTemp.append("</table>\n");
    	rs.close();
    	mdb.close();
    	return strTemp.toString();
    	
    }
    
    public String upOrder(int ChannelID, int ClassID, int cRootID, int MoveNum) throws Exception{
    	String reValue = "";
    	
    	if(ClassID==0){
    		reValue = "<li>错误参数！</li>";
    		return reValue;
    	}
    	if(cRootID==0){
    		reValue = "<li>错误参数！</li>";
    		return reValue;
    	}
    	if(MoveNum==0){
    		reValue = "<li>请选择要提升的数字！</li>";
    		return reValue;
    	}
    	
    	//得到本栏目的PrevID,NextID
    	int tmpDepth = 0, PrevID = 0, NextID = 0, MaxRootID = 0, tRootID = 0, i = 0;
    	int tClassID = 0, tPrevID = 0;
    	String sql = "select PrevID,NextID from categoryconn where ClassID="+ClassID+"";
    	ResultSet rs = mdb.executeQuery(sql);
    	if(rs.next()){
    		PrevID = rs.getInt("PrevID");
    		NextID = rs.getInt("NextID");
    	}
    	rs.close();
    	
    	//先修改上一栏目的NextID和下一栏目的PrevID
    	if(PrevID>0){
    		mdb.executeUpdate("update categoryconn set NextID=" + NextID + " where ClassID=" + PrevID);
    	}
    	if(NextID>0){
    		mdb.executeUpdate("update categoryconn set PrevID=" + PrevID + " where ClassID=" + NextID);
    	}
    	
    	//得到本频道最大RootID值
    	String sql2 = "select max(RootID) as MaxRootID from categoryconn where ChannelID="+ChannelID+"";
    	ResultSet rs2 = mdb.executeQuery(sql2);
    	if(rs2.next()){
    		MaxRootID = rs2.getInt("MaxRootID") + 1;
    	}
    	rs2.close();
    	
    	//先将当前栏目移至最后，包括子栏目
    	mdb.executeUpdate("update categoryconn set RootID=" + MaxRootID + " where ChannelID=" + ChannelID + " and RootID=" + cRootID);
    	
    	//然后将位于当前栏目以上的栏目的RootID依次加一，范围为要提升的数字
    	String sql3 = "select * from categoryconn where ChannelID=" + ChannelID + " and ParentID=0 and RootID<" + cRootID + " order by RootID desc";
    	ResultSet rs3 = mdb.executeQuery(sql3);
    	i = 1;
    	while(rs3.next()){  //没有数据,如果当前栏目已经在最上面，则无需移动
    		tRootID = rs3.getInt("RootID");  //得到要提升位置的RootID，包括子栏目
    		
    		mdb.executeUpdate("update categoryconn set RootID=RootID+1 where ChannelID=" + ChannelID + " and RootID=" + tRootID);
    		i = i + 1;
    		
    		if(i > MoveNum){
    			tClassID = rs3.getInt("ClassID");
    			tPrevID = rs3.getInt("PrevID");
    			break;
    		}
    		
    	}
    	rs3.close();
    	
    	//更新移动后本栏目的的PrevID和NextID，以及上一栏目的NextID和下一栏目的PrevID
    	mdb.executeUpdate("update categoryconn set PrevID=" + tPrevID + " where ClassID=" + ClassID);
    	mdb.executeUpdate("update categoryconn set NextID=" + tClassID + " where ClassID=" + ClassID);
    	mdb.executeUpdate("update categoryconn set PrevID=" + ClassID + " where ClassID=" + tClassID);
    	
    	if(tPrevID > 0){
    		mdb.executeUpdate("update categoryconn set NextID=" + ClassID + " where ClassID=" + tPrevID);
    	}
    	
    	//然后再将当前栏目从最后移到相应位置，包括子栏目
    	mdb.executeUpdate("update categoryconn set RootID=" + tRootID + " where ChannelID=" + ChannelID + " and RootID=" + MaxRootID);
    	
    	mdb.close();
    	reValue = "ok";
    	return reValue;
    	
    }
    
    public String downOrder(int ChannelID, int ClassID, int cRootID, int MoveNum) throws Exception{
    	String reValue = "";
    	
    	if(ClassID==0){
    		reValue = "<li>错误参数！</li>";
    		return reValue;
    	}
    	if(cRootID==0){
    		reValue = "<li>错误参数！</li>";
    		return reValue;
    	}
    	if(MoveNum==0){
    		reValue = "<li>请选择要提升的数字！</li>";
    		return reValue;
    	}
    	
    	//得到本栏目的PrevID,NextID
    	int tmpDepth = 0, PrevID = 0, NextID = 0, MaxRootID = 0, tRootID = 0, i = 0;
    	int tClassID = 0, tPrevID = 0, tNextID = 0;
    	String sql = "select PrevID,NextID from categoryconn where ClassID="+ClassID+"";
    	ResultSet rs = mdb.executeQuery(sql);
    	if(rs.next()){
    		PrevID = rs.getInt("PrevID");
    		NextID = rs.getInt("NextID");
    	}
    	rs.close();
    	
    	//先修改上一栏目的NextID和下一栏目的PrevID
    	if(PrevID>0){
    		mdb.executeUpdate("update categoryconn set NextID=" + NextID + " where ClassID=" + PrevID);
    	}
    	if(NextID>0){
    		mdb.executeUpdate("update categoryconn set PrevID=" + PrevID + " where ClassID=" + NextID);
    	}
    	
    	//得到本频道最大RootID值
    	String sql2 = "select max(RootID) as MaxRootID from categoryconn where ChannelID="+ChannelID+"";
    	ResultSet rs2 = mdb.executeQuery(sql2);
    	if(rs2.next()){
    		MaxRootID = rs2.getInt("MaxRootID") + 1;
    	}
    	rs2.close();
    	
    	//先将当前栏目移至最后，包括子栏目
    	mdb.executeUpdate("update categoryconn set RootID=" + MaxRootID + " where ChannelID=" + ChannelID + " and RootID=" + cRootID);
    	
    	//然后将位于当前栏目以下的栏目的RootID依次减一，范围为要下降的数字
    	String sql3 = "select * from categoryconn where ChannelID=" + ChannelID + " and ParentID=0 and RootID>" + cRootID + " order by RootID";
    	ResultSet rs3 = mdb.executeQuery(sql3);
    	i = 1;
    	while(rs3.next()){
    		tRootID = rs3.getInt("RootID");  //得到要提升位置的RootID，包括子栏目
    		
    		mdb.executeUpdate("update categoryconn set RootID=RootID-1 where ChannelID=" + ChannelID + " and RootID=" + tRootID);
    		i = i + 1;
    		
    		if(i > MoveNum){
    			tClassID = rs3.getInt("ClassID");
    			tNextID = rs3.getInt("NextID");
    			break;
    		}
    		
    	}
    	rs3.close();
    	
    	//更新移动后本栏目的的PrevID和NextID，以及上一栏目的NextID和下一栏目的PrevID
    	mdb.executeUpdate("update categoryconn set PrevID=" + tClassID + " where ClassID=" + ClassID);
    	mdb.executeUpdate("update categoryconn set NextID=" + tNextID + " where ClassID=" + ClassID);
    	mdb.executeUpdate("update categoryconn set NextID=" + ClassID + " where ClassID=" + tClassID);
    	
    	if(tNextID > 0){
    		mdb.executeUpdate("update categoryconn set PrevID=" + ClassID + " where ClassID=" + tNextID);
    	}
    	
    	//然后再将当前栏目从最后移到相应位置，包括子栏目
    	mdb.executeUpdate("update categoryconn set RootID=" + tRootID + " where ChannelID=" + ChannelID + " and RootID=" + MaxRootID);
    	
    	mdb.close();
    	reValue = "ok";
    	return reValue;
    	
    }
    
    public String upOrderN(int ClassID, int MoveNum) throws Exception{
    	String reValue = "";
    	
    	if(ClassID==0){
    		reValue = "<li>错误参数！</li>";
    		return reValue;
    	}
    	if(MoveNum==0){
    		reValue = "<li>请选择要提升的数字！</li>";
    		return reValue;
    	}
    	
    	int tmpDepth = 0, PrevID = 0, NextID = 0, ParentID = 0, Child = 0, i = 0;
    	int OrderID = 0, AddOrderNum = 0, tClassID = 0, tOrderID = 0, tPrevID = 0, tChild = 0;
    	String ParentPath = "";
    	
    	//要移动的栏目信息
    	String sql = "select ParentID,OrderID,ParentPath,Child,PrevID,NextID from categoryconn where ClassID="+ClassID+"";
    	ResultSet rs = mdb.executeQuery(sql);
    	if(rs.next()){
    		ParentID = rs.getInt("ParentID");
    		OrderID = rs.getInt("OrderID");
    		ParentPath = rs.getString("ParentPath") + "," + ClassID;
    		Child = rs.getInt("Child");
    		PrevID = rs.getInt("PrevID");
    		NextID = rs.getInt("NextID");
    	}
    	rs.close();
    	
    	//获得要移动的栏目的所有子栏目数，然后加1（栏目本身），得到排序增加数（即其上栏目的OrderID增加数AddOrderNum）
    	if(Child>0){
    		String sql2 = "select count(*) as totalNum from categoryconn where ParentPath like '%" + ParentPath + "%'";
    		ResultSet rs2 = mdb.executeQuery(sql2);
    		if(rs2.next()){
    			AddOrderNum = rs2.getInt("totalNum") + 1;
    			
    		}
    		rs2.close();
    	}else{
    		AddOrderNum = 1;
    	}
    	
    	//先修改上一栏目的NextID和下一栏目的PrevID
    	if(PrevID>0){
    		mdb.executeUpdate("update categoryconn set NextID=" + NextID + " where ClassID=" + PrevID);
    	}
    	if(NextID>0){
    		mdb.executeUpdate("update categoryconn set PrevID=" + PrevID + " where ClassID=" + NextID);
    	}
    	
    	//和该栏目同级且排序在其之上的栏目------更新其排序，范围为要提升的数字AddOrderNum
    	String sqlstr = "select ClassID,OrderID,Child,ParentPath,PrevID,NextID from categoryconn where ParentID=" + ParentID + " and OrderID<" + OrderID + " order by OrderID desc";
    	ResultSet rstop = mdb.executeQuery(sqlstr);
    	i = 0;
    	while(rstop.next()){
    		tClassID = rstop.getInt("ClassID");
    		tOrderID = rstop.getInt("OrderID");
    		tChild = rstop.getInt("Child");
    		String tParentPath = rstop.getString("ParentPath");
    		mdb.executeUpdate("update categoryconn set OrderID=OrderID+" + AddOrderNum + " where ClassID=" + tClassID);
    		if(tChild>0){
    			String sql4 = "select ClassID,OrderID from categoryconn where ParentPath like '%" + tParentPath + "," + tClassID + "%' order by OrderID";
    			ResultSet rs4 = mdb.executeQuery(sql4);
    			while(rs4.next()){
    				mdb.executeUpdate("update categoryconn set OrderID=OrderID+" + AddOrderNum + " where ClassID=" + rs4.getInt("ClassID"));
    			}
    			rs4.close();
    		}
    		
    		i = i + 1;
    		if(i >= MoveNum){
    			//获得最后一个提升序号的同级栏目信息
    			tPrevID = rstop.getInt("PrevID");
    			break;
    		}
			
		}
    	rstop.close();
    	
    	mdb.executeUpdate("update categoryconn set PrevID=" + tPrevID + " where ClassID=" + ClassID);
    	mdb.executeUpdate("update categoryconn set NextID=" + tClassID + " where ClassID=" + ClassID);
    	mdb.executeUpdate("update categoryconn set PrevID=" + ClassID + " where ClassID=" + tClassID);
    	
    	if(tPrevID > 0){
    		mdb.executeUpdate("update categoryconn set NextID=" + ClassID + " where ClassID=" + tPrevID);
    	}
    	
    	//更新所要排序的栏目的序号
    	mdb.executeUpdate("update categoryconn set OrderID=" + tOrderID + " where ClassID=" + ClassID);
    	//如果有下属栏目，则更新其下属栏目排序
    	if(Child > 0){
    		i = 1;
    		String sql5 = "select ClassID from categoryconn where ParentPath like '%" + ParentPath + "%' order by OrderID";
    		ResultSet rs5 = mdb.executeQuery(sql5);
    		while(rs5.next()){
    			mdb.executeUpdate("update categoryconn set OrderID=" + (tOrderID + i) + " where ClassID=" + rs5.getInt("ClassID"));
    			i = i + 1;
    			
    		}
    		rs5.close();
    	}
    	
    	mdb.close();
    	reValue = "ok";
    	return reValue;
    	
    }
    
    public String downOrderN(int ClassID, int MoveNum) throws Exception{
    	String reValue = "";
    	
    	if(ClassID==0){
    		reValue = "<li>错误参数！</li>";
    		return reValue;
    	}
    	if(MoveNum==0){
    		reValue = "<li>请选择要下降的数字！</li>";
    		return reValue;
    	}
    	
    	int tmpDepth = 0, PrevID = 0, NextID = 0, ParentID = 0, Child = 0, i = 0, ii = 0;
    	int OrderID = 0, tClassID = 0, tOrderID = 0, tPrevID = 0, tNextID = 0, tChild = 0;
    	String ParentPath = "";
    	
    	//要移动的栏目信息
    	String sql = "select ParentID,OrderID,ParentPath,Child,PrevID,NextID from categoryconn where ClassID="+ClassID+"";
    	ResultSet rs = mdb.executeQuery(sql);
    	if(rs.next()){
    		ParentID = rs.getInt("ParentID");
    		OrderID = rs.getInt("OrderID");
    		ParentPath = rs.getString("ParentPath") + "," + ClassID;
    		Child = rs.getInt("Child");
    		PrevID = rs.getInt("PrevID");
    		NextID = rs.getInt("NextID");
    	}
    	rs.close();
    	
    	//先修改上一栏目的NextID和下一栏目的PrevID
    	if(PrevID>0){
    		mdb.executeUpdate("update categoryconn set NextID=" + NextID + " where ClassID=" + PrevID);
    	}
    	if(NextID>0){
    		mdb.executeUpdate("update categoryconn set PrevID=" + PrevID + " where ClassID=" + NextID);
    	}
    	
    	//和该栏目同级且排序在其之下的栏目------更新其排序，范围为要下降的数字
    	String sqlstr = "select ClassID,OrderID,Child,ParentPath,PrevID,NextID from categoryconn where ParentID=" + ParentID + " and OrderID>" + OrderID + " order by OrderID";
    	ResultSet rstop = mdb.executeQuery(sqlstr);
    	i = 0;    //同级栏目
    	ii = 0;   //同级栏目和子栏目
    	while(rstop.next()){
    		tClassID = rstop.getInt("ClassID");
    		tOrderID = rstop.getInt("OrderID");
    		tChild = rstop.getInt("Child");
    		String tParentPath = rstop.getString("ParentPath");
    		mdb.executeUpdate("update categoryconn set OrderID=" + (OrderID + ii) + " where ClassID=" + tClassID);
    		if(tChild>0){
    			String sql4 = "select ClassID,OrderID from categoryconn where ParentPath like '%" + tParentPath + "," + tClassID + "%' order by OrderID";
    			ResultSet rs4 = mdb.executeQuery(sql4);
    			while(rs4.next()){
    				ii = ii + 1;
    				mdb.executeUpdate("update categoryconn set OrderID=" + (OrderID + ii) + " where ClassID=" + rs4.getInt("ClassID"));
    			}
    			rs4.close();
    		}
    		
    		ii = ii + 1;
    		i = i + 1;
    		if(i >= MoveNum){
    			//获得移动后本栏目的上一栏目的信息
    			tNextID = rstop.getInt("NextID");
    			break;
    		}
			
		}
    	rstop.close();
    	
    	//更新移动后本栏目的的PrevID和NextID，以及上一栏目的NextID和下一栏目的PrevID
    	mdb.executeUpdate("update categoryconn set PrevID=" + tClassID + " where ClassID=" + ClassID);
    	mdb.executeUpdate("update categoryconn set NextID=" + tNextID + " where ClassID=" + ClassID);
    	mdb.executeUpdate("update categoryconn set NextID=" + ClassID + " where ClassID=" + tClassID);
    	
    	if(tNextID > 0){
    		mdb.executeUpdate("update categoryconn set PrevID=" + ClassID + " where ClassID=" + tNextID);
    	}
    	
    	//更新所要排序的栏目的序号
    	mdb.executeUpdate("update categoryconn set OrderID=" + (OrderID + ii) + " where ClassID=" + ClassID);
    	//如果有下属栏目，则更新其下属栏目排序
    	if(Child > 0){
    		i = 1;
    		String sql5 = "select ClassID from categoryconn where ParentPath like '%" + ParentPath + "%' order by OrderID";
    		ResultSet rs5 = mdb.executeQuery(sql5);
    		while(rs5.next()){
    			mdb.executeUpdate("update categoryconn set OrderID=" + (OrderID + ii + i) + " where ClassID=" + rs5.getInt("ClassID"));
    			i = i + 1;
    			
    		}
    		rs5.close();
    	}
    	
    	mdb.close();
    	reValue = "ok";
    	return reValue;
    }
    
    
    
}
