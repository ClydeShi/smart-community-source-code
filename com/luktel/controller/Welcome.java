package com.luktel.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;



import com.alibaba.fastjson.JSONObject;
import com.luktel.model.Menu;
import com.luktel.model.User;
import com.luktel.model.UserRole;
import com.luktel.service.DeptmentsService;
import com.luktel.service.MenuService;
import com.luktel.service.UserRoleService;
import com.luktel.service.UserService;
import com.util.common.Contants;
import com.util.common.PageModel;
import com.util.common.PageNew;
import com.util.common.StrEdit;

/*
 * @Controller是标记在类UserController上面的，所以类UserController就是一个SpringMVC Controller对象了
 * 使用@RequestMapping("/getAllUser")标记在Controller方法上，表示当请求/getAllUser的时候访问的是LoginController的getAllUser方法，
 * 该方法返回了一个包括Model和View的ModelAndView对象113323
 */

@Controller//@Controller注解标识一个控制器
public class Welcome{
	
	@Autowired
	private UserRoleService role;
	
	@Autowired
	private MenuService menu;
	
	@Autowired
	private DeptmentsService deptment;
	
	@RequestMapping("/")
	public String login() {
		System.out.println("------------------login------------------");
		//return "userList";//访问userList.jsp
		return "front/index/login";
	}
	
	@RequestMapping("/index")
	public String index(HttpServletRequest request) {
		System.out.println("------------------index------------------");
		int userId = 0, userRole = 0;
		User sessionUser = ((User)request.getSession().getAttribute(Contants.SESSION_USER));
		if(sessionUser!=null){
			userId = sessionUser.getConId();
			userRole = sessionUser.getIntValue4();
		}else{
			return "front/index/login";
		}
		UserRole data = role.findById(userRole);
		//request.setAttribute("data", data);
		String menuIds = data.getText1();
		if(menuIds.equals("")){
			menuIds = "0";
		}
		
		String ctx = request.getContextPath();
		StringBuffer strBuff = new StringBuffer("");
		int channelID = 1;
		
		String authority = "";//操作权限
		List<Menu> list = new ArrayList<Menu>();
		Map<String,Object> newMap = new HashMap<String,Object>();
		newMap.put("task", "getPriviledgeByIds");
		newMap.put("channelID", channelID);
		newMap.put("menuIds", menuIds);
		list = menu.getListByMap(newMap);
		for(int m=0; m<list.size(); m++) {
			Menu temp = list.get(m);
			String varValue5 = temp.getVarValue5();
			if(authority.equals("")){
				authority = varValue5;
			}else{
				authority = authority + "," + varValue5;
			}
		}
		request.getSession().setAttribute(Contants.SESSION_AUTHORITY, authority);
		
		List<Menu> list1 = new ArrayList<Menu>();
		Map<String,Object> newMap1 = new HashMap<String,Object>();
		newMap1.put("task", "getMenuByParentId");
        newMap1.put("channelID", channelID);
        newMap1.put("menuIds", menuIds);
        newMap1.put("parentId", 0);
        list1 = menu.getListByMap(newMap1);
		
		for(int m=0; m<list1.size(); m++) {//第一层菜单
			
			Menu temp = list1.get(m);
			int id1 = temp.getConId();
			String className1 = temp.getClassName();
			int parentId1 = temp.getParentId();
			int depth1 = temp.getDepth();
			int rootId1 = temp.getRootId();
			int child1 = temp.getChild();
			String link1 = ctx + temp.getVarValue6();
			String icon1 = temp.getVarValue7();
			
			strBuff.append("<li class=\"panel\"><a data-toggle=\"collapse\" data-parent=\"#1\" href=\"#menu"+id1+"\" class=\"collapsed\"><i class=\"fa "+icon1+"\"></i> ");
			strBuff.append("<span class=\"nav-label\">"+className1+"</span><span class=\"pull-right fa fa-angle-toggle\"></span></a>\n");
			strBuff.append("<ul id=\"menu"+id1+"\" class=\"nav collapse menu-item\">\n");//上面的href同这里的id
			strBuff.append("");
			strBuff.append("");
			
			List<Menu> list2 = new ArrayList<Menu>();
			Map<String,Object> newMap2 = new HashMap<String,Object>();
			newMap2.put("task", "getMenuByParentId");
	        newMap2.put("channelID", channelID);
	        newMap2.put("menuIds", menuIds);
	        newMap2.put("parentId", id1);
	        list2 = menu.getListByMap(newMap2);
	        
	        for(int m2=0; m2<list2.size(); m2++) {//第二层菜单
	        	
	        	Menu temp2 = list2.get(m2);
	        	int id2 = temp2.getConId();
				String className2 = temp2.getClassName();
				int parentId2 = temp2.getParentId();
				int depth2 = temp2.getDepth();
				int rootId2 = temp2.getRootId();
				int child2 = temp2.getChild();
				String link2 = ctx + temp2.getVarValue6();
				String icon2 = temp2.getVarValue7();
				
				if(child2>0){//如果有第三层菜单，则输出第三层菜单
					strBuff.append("<li class=\"item\"><a data-toggle=\"collapse\" data-parent=\"#menu"+id1+"\" href=\"#menu"+id2+"\" class=\"collapsed\"><i class=\"fa "+icon2+"\"></i> <span class=\"nav-label\">"+className2+"</span><span class=\"pull-right fa fa-angle-toggle\"></span></a>\n");
					strBuff.append("<ul id=\"menu"+id2+"\" class=\"nav collapse menu-item\">\n");//上面的href同这里的id
					
					List<Menu> list3 = new ArrayList<Menu>();
					Map<String,Object> newMap3 = new HashMap<String,Object>();
					newMap3.put("task", "getMenuByParentId");
			        newMap3.put("channelID", channelID);
			        newMap3.put("menuIds", menuIds);
			        newMap3.put("parentId", id2);
			        list3 = menu.getListByMap(newMap3);
			        
			        for(int m3=0; m3<list3.size(); m3++) {
			        	
			        	Menu temp3 = list3.get(m3);
			        	int id3 = temp3.getConId();
						String className3 = temp3.getClassName();
						int parentId3 = temp3.getParentId();
						int depth3 = temp3.getDepth();
						int rootId3 = temp3.getRootId();
						int child3 = temp3.getChild();
						String link3 = ctx + temp3.getVarValue6();
						String icon3 = temp3.getVarValue7();
						
						//判断是否有第四层菜单
						if(child3>0){//如果有第四层菜单，则输出第四层菜单
							strBuff.append("<li class=\"item\"><a data-toggle=\"collapse\" data-parent=\"#menu"+id2+"\" href=\"#menu"+id3+"\" class=\"collapsed\"><i class=\"fa "+icon3+"\"></i> <span class=\"nav-label\">"+className3+"</span><span class=\"pull-right fa fa-angle-toggle\"></span></a>\n");
							strBuff.append("<ul id=\"menu"+id3+"\" class=\"nav collapse menu-item\">\n");//上面的href同这里的id
							
							List<Menu> list4 = new ArrayList<Menu>();
							Map<String,Object> newMap4 = new HashMap<String,Object>();
							newMap4.put("task", "getMenuByParentId");
							newMap4.put("channelID", channelID);
							newMap4.put("menuIds", menuIds);
							newMap4.put("parentId", id3);
					        list4 = menu.getListByMap(newMap4);
					        
					        for(int m4=0; m4<list4.size(); m4++) {
					        	
					        	Menu temp4 = list4.get(m4);
					        	int id4 = temp4.getConId();
								String className4 = temp4.getClassName();
								int parentId4 = temp4.getParentId();
								int depth4 = temp4.getDepth();
								int rootId4 = temp4.getRootId();
								int child4 = temp4.getChild();
								String link4 = ctx + temp4.getVarValue6();
								String icon4 = temp4.getVarValue7();
								
								strBuff.append("<li class=\"item\"><a class=\"J_menuItem\" href=\""+link4+"\"><i class=\"fa "+icon4+"\"></i> <span class=\"nav-label\">"+className4+"</span></a></li>");
								
					        }
							
							strBuff.append("</ul>\n");
							strBuff.append("</li>\n");
						}else{//没有第四层菜单，则输出第三层菜单
							strBuff.append("<li class=\"item\"><a class=\"J_menuItem\" href=\""+link3+"\"><i class=\"fa "+icon3+"\"></i> <span class=\"nav-label\">"+className3+"</span></a></li>");
						}
						
			        }
					
					strBuff.append("</ul>\n");
					strBuff.append("</li>\n");
				}else{//没有第三层菜单，则输出第二层菜单
					strBuff.append("<li class=\"item\"><a class=\"J_menuItem\" href=\""+link2+"\" ><i class=\"fa "+icon2+"\"></i> <span class=\"nav-label\">"+className2+"</span></a></li>\n");
				}
				
				strBuff.append("");
				
	        }
	        
	        strBuff.append("</ul>\n");
	        
	        strBuff.append("</li>\n");
			
		}
		
		request.setAttribute("menu", strBuff.toString());
		
		return "front/index/index";
	}
	
	@RequestMapping("/desktop")
	public String desktop() {
		return "front/index/desktop";
	}
	
	@RequestMapping("/help")
	public String help() {
		return "front/index/help";
	}
	
	@RequestMapping("/deptmentselect")
	public String deptmentselect() {
		return "front/index/deptmentselect";
	}
	
}
