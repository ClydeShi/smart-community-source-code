package com.luktel.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.luktel.model.Attachment;
import com.luktel.model.Company;
import com.luktel.model.Decorate;
import com.luktel.model.Pay;
import com.luktel.model.PayDetail;
import com.luktel.model.User;
import com.luktel.service.AttachmentService;
import com.luktel.service.CompanyService;
import com.luktel.service.DecorateService;
import com.luktel.service.PayDetailService;
import com.luktel.service.PayService;
import com.luktel.service.RoomService;
import com.luktel.service.DictionaryService;
import com.util.common.Contants;
import com.util.common.PageModel;
import com.util.common.StrEdit;

/*
 * @Controller是标记在类UserController上面的，所以类UserController就是一个SpringMVC Controller对象了
 * 使用@RequestMapping("/getAllUser")标记在Controller方法上，表示当请求/getAllUser的时候访问的是LoginController的getAllUser方法，
 * 该方法返回了一个包括Model和View的ModelAndView对象113323
 */

@Controller//@Controller注解标识一个控制器
@RequestMapping("/pay")//如果@RequestMapping注解在类级别上，则表示相对路径，在方法级别上，则标记访问的路径
public class PayController{
	
	@Autowired
	private PayService service;
	
	@Autowired
	private PayDetailService paydetail;
	
	@Autowired
	private DecorateService decorate;
	
	@Autowired
	private CompanyService company;
	
	@Autowired
	private RoomService room;
	
	@Autowired
	private AttachmentService attach;
	
	@Autowired
	private DictionaryService dictionary;
	
	@RequestMapping("/list")
	public String list(HttpServletRequest request) {
		String modelName = "";
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		if(channelId==1){
			modelName = "装修押金缴纳";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelName", modelName);
		return "front/pay/list";
	}
	
	@RequestMapping("/getJsonById")
	@ResponseBody
	public String getJsonById(int id){
		String strTemp = "";
		Pay data = service.findById(id);
		strTemp = "{\"status\":\"1\",\"id\":\""+id+"\",\"name\":\""+data.getVarValue1()+"\"}";
		return strTemp;
	}
	
	@RequestMapping("/get")
	@ResponseBody
	public Map<String, Object> getAll(PageModel<Pay> pageModel, Pay data, HttpServletRequest request, Model model) {
		
		//System.out.println("------------------get------------------");
		String keywords = StrEdit.StrChkNull(data.getKeywords());
		
		Map<String,Object> newMap = new HashMap<String,Object>();
		
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		int classId = StrEdit.StrToInt(request.getParameter("classId"));
		int intValue2 = StrEdit.StrToInt(request.getParameter("intValue2"));
		String varValue1 = StrEdit.StrChkNull(request.getParameter("varValue1"));
		String varValue2 = StrEdit.StrChkNull(request.getParameter("varValue2"));
		String varValue3 = StrEdit.StrChkNull(request.getParameter("varValue3"));
		String varValue4 = StrEdit.StrChkNull(request.getParameter("varValue4"));
		String varValue8 = StrEdit.StrChkNull(request.getParameter("varValue8"));
		String varValue11 = StrEdit.StrChkNull(request.getParameter("varValue11"));
		String date = StrEdit.StrChkNull(request.getParameter("date"));
		String startDate = "";
		String endDate = "";
		if(!date.equals("")){
			startDate = date + " 00:00:00";
			endDate = date + " 23:59:59";
		}
		
		int pageNumber = StrEdit.StrToInt(request.getParameter("pageNumber"));
		int pageSize = StrEdit.StrToInt(request.getParameter("pageSize"));
		int startRow = 0;
        if(pageNumber!=0) {
        	startRow = (pageNumber - 1) * pageSize;
        }
        newMap.put("startRow", startRow);
        newMap.put("pageSize", pageSize);
		/*int pageStart = (pageNumber -1) * pageSize+1;//oracle分页
        int pageEnd = pageStart + pageSize -1;
        newMap.put("pageStart", pageStart);
        newMap.put("pageEnd", pageEnd);*/
        newMap.put("channelId", channelId);
        newMap.put("classId", classId);
        newMap.put("intValue2", intValue2);
        newMap.put("keywords", keywords);
        newMap.put("varValue1", varValue1);
        newMap.put("varValue2", varValue2);
        newMap.put("varValue3", varValue3);
        newMap.put("varValue4", varValue4);
        newMap.put("varValue8", varValue8);
        newMap.put("varValue11", varValue11);
        newMap.put("startDate", startDate);
        newMap.put("endDate", endDate);
		List<Pay> list = service.pageList(newMap);
		int totalRecords = service.totalRecord(newMap);
		pageModel.setTotal(totalRecords);
		
		Object jsonObject = JSONObject.toJSON(list);
		//System.out.println("jsonObject:"+jsonObject);
		//System.out.println("totalRecords1:"+totalRecords);
		
		int pn = 1;
		if (pageNumber != 0) {
			pn = new Integer(pageNumber);
		}
		pageModel.setPages(totalRecords, pn);//设置总记录数和当前页面
		Map<String, Object> map = new HashMap<>();
		map.put("page", pageModel);
		map.put("list", list);
		return map;
	}
	
	@RequestMapping("/add")
	public String toAdd(Pay data,HttpServletRequest request,Model model){
		String modelName = "";
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		if(channelId==1){
			modelName = "装修押金缴纳";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelName", modelName);
		
		List<String> dictionery5 = dictionary.selectDictionaryList(5);
		List<String> dictionery6 = dictionary.selectDictionaryList(6);
		model.addAttribute("dictionery5", dictionery5);
		model.addAttribute("dictionery6", dictionery6);
		
		Map<Long, String> typeMap = new HashMap<Long, String>();
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
		return "/front/pay/add";
	}
	
	@RequestMapping("/addfromdecorate")
	public String addfromdecorate(Pay data,HttpServletRequest request,Model model){
		String modelName = "";
		int decorateId = StrEdit.StrToInt(request.getParameter("decorateId"));
		int companyId = StrEdit.StrToInt(request.getParameter("companyId"));
		int channelId = StrEdit.StrToInt(request.getParameter("channelId"));
		if(channelId==1){
			modelName = "装修押金缴纳";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelName", modelName);
		
		List<String> dictionery5 = dictionary.selectDictionaryList(5);
		List<String> dictionery6 = dictionary.selectDictionaryList(6);
		model.addAttribute("dictionery5", dictionery5);
		model.addAttribute("dictionery6", dictionery6);
		
		Decorate decoratedata = decorate.findById(decorateId);
		model.addAttribute("decoratedata", decoratedata);
		
		//Company companydata = company.findById(companyId);
		//model.addAttribute("companydata", companydata);
		
		data.setConId(0);
		data.setVarValue1(decoratedata.getVarValue1());
		data.setVarValue2(decoratedata.getVarValue2());
		data.setVarValue3(decoratedata.getVarValue3());
		data.setVarValue4(decoratedata.getVarValue4());
		data.setVarValue9(decoratedata.getVarValue9());
		data.setIntValue2(decoratedata.getIntValue2());
		data.setDateValue1(decoratedata.getDateValue1());
		data.setDateValue2(decoratedata.getDateValue2());
		
		List<PayDetail> list1 = new ArrayList<PayDetail>();//把数组的值装入List
		String roomId = decoratedata.getVarValue10();
		String roomName = decoratedata.getVarValue11();
		String[] arrId = roomId.split("\\,");//如果不包含,则直接返回原字符的一维数组
		String[] arrName = roomName.split("\\,");
		if (arrId!=null){
			for(int i=0;i<arrId.length;i++){
				Map<String,Object> newMap1 = new HashMap<String,Object>();
				newMap1.put("task", "getRoomSquare");
				newMap1.put("conId", StrEdit.StrToInt(arrId[i]));
	    		String square = room.getStringValue(newMap1);
				PayDetail detail = new PayDetail();
				detail.setConId(0);
				detail.setVarValue1(arrName[i]);
				detail.setDecValue1(square);
				list1.add(detail);
			}
		}
        request.setAttribute("list1", list1);
        
		Map<Long, String> typeMap = new HashMap<Long, String>();
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
		return "/front/pay/add";
	}
	
	@RequestMapping("/save")
	@ResponseBody
	public String save(Pay data,HttpServletRequest request,HttpServletResponse response){
		int conId = 0;
		String strTemp = "";
		
		int userId = 0;
		String userName = "";
		User sessionUser = ((User)request.getSession().getAttribute(Contants.SESSION_USER));
		if(sessionUser!=null){
			userId = sessionUser.getConId();
			userName = sessionUser.getVarValue1();
		}
		
		String orderNo = StrEdit.getSN("7");
		data.setVarValue11(orderNo);
		
		int channelId = data.getChannelId();
		try{
			service.save(data);
			conId = data.getConId();
		}catch(Exception e){
			conId = 0;
			System.out.println("save插入数据失败" + e.getLocalizedMessage());
		}
		
		if(conId>0){
			
			String field_scope=StrEdit.StrChkNull(request.getParameter("field_scope"));
			String field_scope1=StrEdit.StrChkNull(request.getParameter("field_scope1"));
			String field_scope2=StrEdit.StrChkNull(request.getParameter("field_scope2"));
			String field_scope3=StrEdit.StrChkNull(request.getParameter("field_scope3"));
			String field_scope4=StrEdit.StrChkNull(request.getParameter("field_scope4"));
			String field_scope5=StrEdit.StrChkNull(request.getParameter("field_scope5"));
			String field_scope6=StrEdit.StrChkNull(request.getParameter("field_scope6"));
			String field_scope7=StrEdit.StrChkNull(request.getParameter("field_scope7"));
			this.updateDetail(conId,channelId,userName,field_scope,field_scope1,field_scope2,field_scope3,field_scope4,field_scope5,field_scope6,field_scope7);
			
			String attachIds=StrEdit.StrChkNull(request.getParameter("attachIds"));//附件
			if(!attachIds.equals("")){
				String[] arrAttachIds = attachIds.split("\\,");
				for(int m=0;m<arrAttachIds.length;m++){
					boolean boolValue = attach.update(StrEdit.StrToInt(arrAttachIds[m]),conId);
					if(!boolValue){
						System.out.println("附件信息ID更新失败："+arrAttachIds[m]);
					}
				}
			}
			
			strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"添加失败\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/update")
	@ResponseBody
	public String update(Pay data,HttpServletRequest request){
		int conId = data.getConId();
		String strTemp = "";
		
		int userId = 0;
		String userName = "";
		User sessionUser = ((User)request.getSession().getAttribute(Contants.SESSION_USER));
		if(sessionUser!=null){
			userId = sessionUser.getConId();
			userName = sessionUser.getVarValue1();
		}
		
		int channelId = data.getChannelId();
		boolean boolValue = service.update(data);
		if(boolValue){
			
			String field_scope=StrEdit.StrChkNull(request.getParameter("field_scope"));
			String field_scope1=StrEdit.StrChkNull(request.getParameter("field_scope1"));
			String field_scope2=StrEdit.StrChkNull(request.getParameter("field_scope2"));
			String field_scope3=StrEdit.StrChkNull(request.getParameter("field_scope3"));
			String field_scope4=StrEdit.StrChkNull(request.getParameter("field_scope4"));
			String field_scope5=StrEdit.StrChkNull(request.getParameter("field_scope5"));
			String field_scope6=StrEdit.StrChkNull(request.getParameter("field_scope6"));
			String field_scope7=StrEdit.StrChkNull(request.getParameter("field_scope7"));
			this.updateDetail(conId,channelId,userName,field_scope,field_scope1,field_scope2,field_scope3,field_scope4,field_scope5,field_scope6,field_scope7);
			
			strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"更新失败\"}";
		}
		
		return strTemp;
	}
	
	public String updateDetail(int id,int channelId,String userName,String field_scope,String field_scope1,String field_scope2,String field_scope3,String field_scope4,String field_scope5,String field_scope6,String field_scope7){
		
		String strTemp = "";
		Date currTime = new Date();
		
		if(!field_scope.equals("")){//删除已经去掉的id
			Map<String,Object> newMap1 = new HashMap<String,Object>();
			newMap1.put("task", "deleteByDetailIds");
	        newMap1.put("intValue2", id);
	        newMap1.put("detailIds", field_scope);
	        paydetail.deleteByMap(newMap1);
		}
		
		if(!field_scope1.equals("")){
			String[] arrField = field_scope.split("\\,");
			String[] arrField1 = field_scope1.split("\\,");
			String[] arrField2 = field_scope2.split("\\,");
			String[] arrField3 = field_scope3.split("\\,");
			String[] arrField4 = field_scope4.split("\\,");
			String[] arrField5 = field_scope5.split("\\,");
			String[] arrField6 = field_scope6.split("\\,");
			String[] arrField7 = field_scope7.split("\\,");
			for(int m=0;m<arrField1.length;m++){
				int detailId = StrEdit.StrToInt(arrField[m]);
				String strValue1 = arrField1[m];
				String strValue2 = arrField2[m];
				String strValue3 = arrField3[m];
				String strValue4 = arrField4[m];
				String strValue5 = arrField5[m];
				String strValue6 = arrField6[m];
				String strValue7 = arrField7[m];
				
				if(strValue1.equals("NOINPUT")){
					strValue1 = "";
				}
				if(strValue2.equals("NOVALUE")){
					strValue2 = "";
				}
				if(strValue3.equals("NOVALUE")){
					strValue3 = "";
				}
				if(strValue4.equals("NOVALUE")){//数字字段
					strValue4 = "0";
				}
				if(strValue5.equals("NOVALUE")){
					strValue5 = "0";
				}
				if(strValue6.equals("NOVALUE")){
					strValue6 = "0";
				}
				if(strValue7.equals("NOVALUE")){
					strValue7 = "";
				}
				
				if(detailId>0){//更新
					PayDetail data1 = new PayDetail();
					if(strValue1.equals("NOINPUT")){
						strValue1 = "";
					}
					data1.setConId(detailId);
					data1.setChannelId(channelId);
					data1.setVarValue1(strValue1);
					data1.setVarValue2("");
					data1.setVarValue3("");
					data1.setVarValue4("");
					data1.setVarValue40(userName);
					data1.setDecValue1(strValue4);
					data1.setDecValue2(strValue5);
					data1.setDecValue3(strValue6);
					data1.setIntValue1(m+1);
					data1.setIntValue2(id);
					data1.setIntValue3(0);
					data1.setIntValue4(0);
					data1.setText1(strValue7);
					data1.setAddTime(currTime);
					data1.setUpdateTime(currTime);
					data1.setDateValue1(StrEdit.strToDate(strValue2));
					data1.setDateValue2(StrEdit.strToDate(strValue3));
					paydetail.update(data1);
				}else{//新增
					PayDetail data1 = new PayDetail();
					if(!strValue1.equals("") && !strValue1.equals("NOINPUT")){
						data1.setChannelId(channelId);
						data1.setVarValue1(strValue1);
						data1.setVarValue2("");
						data1.setVarValue3("");
						data1.setVarValue4("");
						data1.setVarValue40(userName);
						data1.setDecValue1(strValue4);
						data1.setDecValue2(strValue5);
						data1.setDecValue3(strValue6);
						data1.setIntValue1(m+1);
						data1.setIntValue2(id);
						data1.setIntValue3(0);
						data1.setIntValue4(0);
						data1.setText1(strValue7);
						data1.setAddTime(currTime);
						data1.setUpdateTime(currTime);
						data1.setDateValue1(StrEdit.strToDate(strValue2));
						data1.setDateValue2(StrEdit.strToDate(strValue3));
						paydetail.save(data1);
						int conId = data1.getConId();
					}
				}
				
			}
		}
		
		return strTemp;
		
	}
	
	@RequestMapping("/getById")
	public String getById(int id,HttpServletRequest request,Model model){
		Map<Long, String> typeMap = new HashMap<Long, String>();
		Pay data = service.findById(id);
		String modelName = "";
		int channelId = data.getChannelId();
		if(channelId==1){
			modelName = "装修押金缴纳";
		}
		request.setAttribute("channelId", channelId);
		request.setAttribute("modelName", modelName);
		
		List<String> dictionery5 = dictionary.selectDictionaryList(5);
		List<String> dictionery6 = dictionary.selectDictionaryList(6);
		model.addAttribute("dictionery5", dictionery5);
		model.addAttribute("dictionery6", dictionery6);
		
		Map<String,Object> newMap1 = new HashMap<String,Object>();
		newMap1.put("task", "getDetailByIntValue2");
        newMap1.put("intValue2", id);
        List<PayDetail> list1 = paydetail.getListByMap(newMap1);
        request.setAttribute("list1", list1);
        request.setAttribute("roomnumber", list1.size());
        
        List<Attachment> attachList1001 = attach.list(1001,id);
        
        model.addAttribute("data", service.findById(id));
        model.addAttribute("typeMap", typeMap);
        model.addAttribute("attachList1001", attachList1001);
        
		return "/front/pay/add";
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public String delete(int id,HttpServletRequest request){
		String strTemp = "";
		boolean boolValue = service.delete(id);
		if(boolValue){
			
			Map<String,Object> newMap1 = new HashMap<String,Object>();
			newMap1.put("task", "deleteByPayId");
	        newMap1.put("intValue2", id);
	        paydetail.deleteByMap(newMap1);
	        
			strTemp = "{\"status\":\"1\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"删除失败\"}";
		}
		
		return strTemp;
	}
	
}
