package com.luktel.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.luktel.model.Attachment;
import com.luktel.model.Building;
import com.luktel.model.Contract;
import com.luktel.model.Room;
import com.luktel.model.User;
import com.luktel.service.AttachmentService;
import com.luktel.service.BuildingService;
import com.luktel.service.ContractService;
import com.luktel.service.DictionaryService;
import com.luktel.service.RoomService;
import com.util.common.Contants;
import com.util.common.PageModel;
import com.util.common.StrEdit;

/*
 * @Controller是标记在类UserController上面的，所以类UserController就是一个SpringMVC Controller对象了
 * 使用@RequestMapping("/getAllUser")标记在Controller方法上，表示当请求/getAllUser的时候访问的是LoginController的getAllUser方法，
 * 该方法返回了一个包括Model和View的ModelAndView对象113323
 */

@Controller//@Controller注解标识一个控制器
@RequestMapping("/room")//如果@RequestMapping注解在类级别上，则表示相对路径，在方法级别上，则标记访问的路径
public class RoomController{
	
	@Autowired
	private RoomService service;
	
	@Autowired
	private BuildingService building;
	
	@Autowired
	private ContractService contract;
	
	@Autowired
	private AttachmentService attach;
	
	@Autowired
	private DictionaryService dictionary;
	
	@RequestMapping("/list")
	public String list() {
		return "front/building/list";
	}
	
	@RequestMapping("/treeselect")
	public String treecheck(HttpServletRequest request) {
		int type = StrEdit.StrToInt(request.getParameter("type"));
		int conId = StrEdit.StrToInt(request.getParameter("conId"));
		String selectIds = StrEdit.StrChkNull(request.getParameter("selectIds"));
		request.setAttribute("type", type);
		request.setAttribute("conId", conId);
		request.setAttribute("selectIds", selectIds);
		return "front/room/treeselect";
	}
	
	@RequestMapping("/treeData")
	@ResponseBody
	public String treeData(HttpServletRequest request) {//返回json
		StringBuffer strBuff = new StringBuffer("");
		strBuff.append("[");
		
		int type = StrEdit.StrToInt(request.getParameter("type"));
		
		List<Building> list1 = new ArrayList<Building>();
		Map<String,Object> newMap1 = new HashMap<String,Object>();
		newMap1.put("task", "all");
        list1 = building.getListByMap(newMap1);
        for(int m=0; m<list1.size(); m++) {
        	if(m>0){strBuff.append(",");}
        	Building temp = list1.get(m);
        	strBuff.append("{");
        	if(type==3 || type==4){//人员多选才显示isParent，才会加载人员信息
        		strBuff.append("\"isParent\": \"true\",");
        	}
			strBuff.append("\"id\": \""+temp.getConId()+"\",");
			strBuff.append("\"pId\": \"0\",");
			strBuff.append("\"buildingId\": \"0\",");
			strBuff.append("\"floor\": \"0\",");
			strBuff.append("\"name\": \""+temp.getVarValue1()+"\"");
			strBuff.append("}");
			
			List<Room> list2 = new ArrayList<Room>();
			Map<String,Object> newMap2 = new HashMap<String,Object>();
			newMap2.put("task", "getFloor");
			newMap2.put("intValue1", temp.getConId());
			list2 = service.getListByMap(newMap2);
			if(list2.size()>0){
				strBuff.append(",");
			}
			for(int m2=0; m2<list2.size(); m2++) {
	        	if(m2>0){strBuff.append(",");}
	        	Room temp2 = list2.get(m2);
	        	strBuff.append("{");
	        	if(type==3 || type==4){//人员多选才显示isParent，才会加载人员信息
	        		strBuff.append("\"isParent\": \"true\",");
	        	}
				strBuff.append("\"id\": \""+temp2.getConId()+"\",");
				strBuff.append("\"pId\": \""+temp2.getIntValue1()+"\",");
				strBuff.append("\"buildingId\": \""+temp2.getIntValue1()+"\",");
				strBuff.append("\"floor\": \""+temp2.getIntValue2()+"\",");
				strBuff.append("\"name\": \"第"+temp2.getIntValue2()+"层\"");
				strBuff.append("}");
		        
	        }
			
        }
        strBuff.append("]");
        //System.out.println("list:" + strBuff.toString());
		
		return strBuff.toString();
	}
	
	@RequestMapping("/treeDataRoom")
	@ResponseBody
	public String treeDataRoom(HttpServletRequest request) {//返回json
		StringBuffer strBuff = new StringBuffer("");
		strBuff.append("[");
		
		int conId = StrEdit.StrToInt(request.getParameter("conId"));
		int buildingId = StrEdit.StrToInt(request.getParameter("buildingId"));
		int floor = StrEdit.StrToInt(request.getParameter("floor"));
		
		List<Room> list1 = new ArrayList<Room>();
		Map<String,Object> newMap1 = new HashMap<String,Object>();
		newMap1.put("task", "getRoom");
        newMap1.put("intValue1", buildingId);
        newMap1.put("intValue2", floor);
        list1 = service.getListByMap(newMap1);
        for(int m=0; m<list1.size(); m++) {
        	if(m>0){strBuff.append(",");}
        	Room temp = list1.get(m);
        	
        	String strRent = "";
        	Map<String,Object> newMap = new HashMap<String,Object>();
			newMap.put("task", "checkRentContract");
	        newMap.put("roomId", temp.getConId());
	        newMap.put("contractId", conId);
	        int isExist = contract.getIntValue(newMap);
	        if(isExist>0){
				strRent = "rent";
			}else{
				strRent = "notrent";
			}
	        
        	strBuff.append("{");
			strBuff.append("\"id\": \"u_"+temp.getConId()+"\",");
			strBuff.append("\"pId\": \""+temp.getIntValue2()+"\",");
			
			if(isExist>0){
				strBuff.append("\"nocheck\": \"true\",");
				strBuff.append("\"name\": \""+temp.getVarValue1()+"("+temp.getVarValue3()+"㎡ 已租)\"");
			}else{
				strBuff.append("\"name\": \""+temp.getVarValue1()+"("+temp.getVarValue3()+"㎡)\"");
			}
			
			strBuff.append("}");
        }
        strBuff.append("]");
		//System.out.println("list:" + strBuff.toString());
		
		return strBuff.toString();
	}
	
	@RequestMapping("/roomselectbycontract")
	public String roomselectbycontract(HttpServletRequest request) {
		int type = StrEdit.StrToInt(request.getParameter("type"));
		int companyId = StrEdit.StrToInt(request.getParameter("companyId"));
		String selectIds = StrEdit.StrChkNull(request.getParameter("selectIds"));
		request.setAttribute("type", type);
		request.setAttribute("companyId", companyId);
		request.setAttribute("selectIds", selectIds);
		return "front/room/roomselectbycontract";
	}
	
	@RequestMapping("/contractTreeData")
	@ResponseBody
	public String contractTreeData(HttpServletRequest request) {//返回json
		StringBuffer strBuff = new StringBuffer("");
		strBuff.append("[");
		
		int type = StrEdit.StrToInt(request.getParameter("type"));
		int companyId = StrEdit.StrToInt(request.getParameter("companyId"));
		
		List<Contract> list1 = new ArrayList<Contract>();
		Map<String,Object> newMap1 = new HashMap<String,Object>();
		newMap1.put("task", "all");
		newMap1.put("intValue2", companyId);
        list1 = contract.getListByMap(newMap1);
        for(int m=0; m<list1.size(); m++) {
        	if(m>0){strBuff.append(",");}
        	Contract temp = list1.get(m);
        	strBuff.append("{");
        	if(type==3 || type==4){//人员多选才显示isParent，才会加载人员信息
        		strBuff.append("\"isParent\": \"true\",");
        	}
			strBuff.append("\"id\": \""+temp.getConId()+"\",");
			strBuff.append("\"pId\": \"0\",");
			strBuff.append("\"roomIds\": \""+temp.getVarValue9()+"\",");
			strBuff.append("\"name\": \""+temp.getVarValue1()+"\"");
			strBuff.append("}");
			
        }
        strBuff.append("]");
        //System.out.println("list:" + strBuff.toString());
		
		return strBuff.toString();
	}
	
	@RequestMapping("/contractTreeDataRoom")
	@ResponseBody
	public String treeDataRoom1(HttpServletRequest request) {//返回json
		StringBuffer strBuff = new StringBuffer("");
		strBuff.append("[");
		
		int contractId = StrEdit.StrToInt(request.getParameter("contractId"));
		String pareroomIds = StrEdit.StrChkNull(request.getParameter("pareroomIds"));
		
		List<Room> list1 = new ArrayList<Room>();
		Map<String,Object> newMap1 = new HashMap<String,Object>();
		newMap1.put("task", "getRoomByRoomIds");
        newMap1.put("roomIds", pareroomIds);
        list1 = service.getListByMap(newMap1);
        for(int m=0; m<list1.size(); m++) {
        	if(m>0){strBuff.append(",");}
        	Room temp = list1.get(m);
        	int buildingId = temp.getIntValue1();
        	Map<String,Object> newMap2 = new HashMap<String,Object>();
    		newMap2.put("task", "buildingName");
    		newMap2.put("conId", buildingId);
    		String buildingName = building.getStringValue(newMap2);
	        
        	strBuff.append("{");
			strBuff.append("\"id\": \"u_"+temp.getConId()+"\",");
			strBuff.append("\"pId\": \""+contractId+"\",");
			strBuff.append("\"name\": \""+buildingName+""+temp.getVarValue1()+"\"");
			strBuff.append("}");
        }
        strBuff.append("]");
		//System.out.println("list:" + strBuff.toString());
		
		return strBuff.toString();
	}
	
	@RequestMapping("/all")
	public String all(HttpServletRequest request) {
		int buildingId = StrEdit.StrToInt(request.getParameter("buildingId"));
		request.setAttribute("buildingId", buildingId);
		
		String ctx = request.getContextPath();
		StringBuffer strBuff = new StringBuffer("");
		
		List<Room> list1 = new ArrayList<Room>();
		Map<String,Object> newMap1 = new HashMap<String,Object>();
		newMap1.put("task", "getFloor");
        newMap1.put("intValue1", buildingId);
        list1 = service.getListByMap(newMap1);
        request.setAttribute("floornumber", list1.size());
        for(int m=0; m<list1.size(); m++) {//楼层
        	Room temp = list1.get(m);
			int id1 = temp.getConId();
			int intValue1 = temp.getIntValue1();
			int intValue2 = temp.getIntValue2();
			String varValue1 = temp.getVarValue1();
			strBuff.append("<div class=\"floor\">\n");
			strBuff.append("<div class=\"floorname\">第"+intValue2+"层 <a href=\"#\" class=\"tan4\" value=\""+intValue2+"\">整层复制</a> <a href=\"#\" class=\"tan3\" value=\""+intValue2+"\">整层删除</a></div>\n");
			strBuff.append("<div class=\"roombox\">\n");
			strBuff.append("<ul>\n");
			
			List<Room> list2 = new ArrayList<Room>();
			Map<String,Object> newMap2 = new HashMap<String,Object>();
			newMap2.put("task", "getRoom");
			newMap2.put("intValue1", buildingId);
			newMap2.put("intValue2", intValue2);
			list2 = service.getListByMap(newMap2);
	        for(int m2=0; m2<list2.size(); m2++) {//房源
	        	Room temp2 = list2.get(m2);
				int mid1 = temp2.getConId();
				int mintValue1 = temp2.getIntValue1();
				int mintValue2 = temp2.getIntValue2();
				String mvarValue1 = temp2.getVarValue1();
				String mvarValue2 = temp2.getVarValue2();
				String mvarValue3 = temp2.getVarValue3();
				String mvarValue4 = temp2.getVarValue4();
				String strRent = "";
				Map<String,Object> newMap = new HashMap<String,Object>();
				newMap.put("task", "checkRent");
		        newMap.put("roomId", mid1);
		        int isExist = contract.getIntValue(newMap);
		        if(isExist>0){
					strRent = "rent";
				}else{
					strRent = "notrent";
				}
				/*Integer isExist = contract.getIntValue(newMap);//适用于getByMap的情况
				System.out.println("roomId"+mid1);
				System.out.println("isExist"+isExist);
				if(isExist!=null){
					if(isExist>0){
						strRent = "rent";
					}else{
						strRent = "notrent";
					}
				}*/
				
				strBuff.append("<li class=\""+strRent+"\">"+mvarValue1+"<br><span>建筑"+mvarValue3+"㎡/使用"+mvarValue4+"㎡ <a href=\""+ctx+"/room/getById?id="+mid1+"\"><img src=\""+ctx+"/public/images/edit.gif\" width=\"11\" height=\"11\"></a> <a href=\"#\" class=\"tan2\" value=\""+mid1+"\"><img src=\""+ctx+"/public/images/close.gif\"></a></span></li>\n");
	        }
	        
			strBuff.append("</ul>\n");
			strBuff.append("</div>\n");
			strBuff.append("</div>\n");
			strBuff.append("");
        }
        
        request.setAttribute("room", strBuff.toString());
        
		return "front/room/all";
	}
	
	@RequestMapping("/muchadd")
	public String muchadd(Room data,HttpServletRequest request,Model model){
		int buildingId = StrEdit.StrToInt(request.getParameter("buildingId"));
		request.setAttribute("buildingId", buildingId);
		
		Map<String,Object> newMap1 = new HashMap<String,Object>();
		newMap1.put("task", "getRoomByBuildingId");
        newMap1.put("intValue1", buildingId);
        List<Room> list1 = service.getListByMap(newMap1);
        request.setAttribute("list1", list1);
        request.setAttribute("roomnumber", list1.size());
		
		Map<Long, String> typeMap = new HashMap<Long, String>();
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
		return "/front/room/muchadd";
	}
	
	@RequestMapping("/muchsave")
	@ResponseBody
	public String muchsave(HttpServletRequest request,HttpServletResponse response){
		int conId = 0;
		String strTemp = "";
		
		int userId = 0;
		String userName = "";
		User sessionUser = ((User)request.getSession().getAttribute(Contants.SESSION_USER));
		if(sessionUser!=null){
			userId = sessionUser.getConId();
			userName = sessionUser.getVarValue1();
		}
		Date currTime = new Date();
		
		int buildingId = StrEdit.StrToInt(request.getParameter("buildingId"));
		String field_scope=StrEdit.StrChkNull(request.getParameter("field_scope"));
		String field_scope1=StrEdit.StrChkNull(request.getParameter("field_scope1"));
		String field_scope2=StrEdit.StrChkNull(request.getParameter("field_scope2"));
		String field_scope3=StrEdit.StrChkNull(request.getParameter("field_scope3"));
		String field_scope4=StrEdit.StrChkNull(request.getParameter("field_scope4"));
		
		if(!field_scope.equals("")){//删除已经去掉的id
			Map<String,Object> newMap1 = new HashMap<String,Object>();
			newMap1.put("task", "deleteByRoomIds");
	        newMap1.put("intValue1", buildingId);
	        newMap1.put("roomIds", field_scope);
			service.deleteByMap(newMap1);
		}
		
		if(!field_scope1.equals("")){
			String[] arrField = field_scope.split("\\,");
			String[] arrField1 = field_scope1.split("\\,");
			String[] arrField2 = field_scope2.split("\\,");
			String[] arrField3 = field_scope3.split("\\,");
			String[] arrField4 = field_scope4.split("\\,");
			for(int m=0;m<arrField1.length;m++){
				int roomId = StrEdit.StrToInt(arrField[m]);
				String roomName = arrField1[m];
				int floor = StrEdit.StrToInt(arrField2[m]);
				String buildingSquare = arrField3[m];
				String useSquare = arrField4[m];
				if(buildingSquare.equals("NOVALUE")){
					buildingSquare = "";
				}
				if(useSquare.equals("NOVALUE")){
					useSquare = "";
				}
				
				if(roomId>0){//更新
					Room data = new Room();
					if(roomName.equals("NOINPUT")){
						roomName = "";
					}
					data.setConId(roomId);
					data.setVarValue1(roomName);
					data.setVarValue2("");
					data.setVarValue3(buildingSquare);
					data.setVarValue4(useSquare);
					data.setVarValue40(userName);
					data.setIntValue1(buildingId);
					data.setIntValue2(floor);
					data.setIntValue3(2);
					data.setIntValue4(m+1);
					data.setAddTime(currTime);
					data.setUpdateTime(currTime);
					service.update(data);
				}else{//新增
					Room data = new Room();
					if(!roomName.equals("") && !roomName.equals("NOINPUT")){
						data.setVarValue1(roomName);
						data.setVarValue2("");
						data.setVarValue3(buildingSquare);
						data.setVarValue4(useSquare);
						data.setVarValue40(userName);
						data.setIntValue1(buildingId);
						data.setIntValue2(floor);
						data.setIntValue3(2);
						data.setIntValue4(m+1);
						data.setAddTime(currTime);
						data.setUpdateTime(currTime);
						service.save(data);
						conId = data.getConId();
					}
				}
				
			}
		}
		
		strTemp = "{\"status\":\"1\"}";
		return strTemp;
	}
	
	@RequestMapping("/copy")
	@ResponseBody
	public String copy(HttpServletRequest request,HttpServletResponse response){
		int conId = 0;
		String strTemp = "";
		
		int userId = 0;
		String userName = "";
		User sessionUser = ((User)request.getSession().getAttribute(Contants.SESSION_USER));
		if(sessionUser!=null){
			userId = sessionUser.getConId();
			userName = sessionUser.getVarValue1();
		}
		Date currTime = new Date();
		
		int buildingId = StrEdit.StrToInt(request.getParameter("buildingId"));
		int floorId = StrEdit.StrToInt(request.getParameter("floorId"));
		int targetFloor = StrEdit.StrToInt(request.getParameter("targetFloor"));
		int roomrule = StrEdit.StrToInt(request.getParameter("roomrule"));
		
		int total = 0;
		Map<String,Object> newMap1 = new HashMap<String,Object>();
		newMap1.put("task", "getRoomTotal");
		newMap1.put("intValue1", buildingId);
        Integer romNumber = service.getIntValue(newMap1);
        if(romNumber!=null){
        	total = romNumber.intValue();
        }
		
		List<Room> list2 = new ArrayList<Room>();
		Map<String,Object> newMap2 = new HashMap<String,Object>();
		newMap2.put("task", "getRoom");
		newMap2.put("intValue1", buildingId);
		newMap2.put("intValue2", floorId);
		list2 = service.getListByMap(newMap2);
        for(int m2=0; m2<list2.size(); m2++) {//房源
        	Room temp2 = list2.get(m2);
			int mid1 = temp2.getConId();
			int mintValue1 = temp2.getIntValue1();
			int mintValue2 = temp2.getIntValue2();
			String mvarValue1 = temp2.getVarValue1();
			String mvarValue2 = temp2.getVarValue2();
			String mvarValue3 = temp2.getVarValue3();
			String mvarValue4 = temp2.getVarValue4();
			
			Room data = new Room();
			String newRoomName = "";
			if(roomrule==1){
				newRoomName = mvarValue1;
			}else if(roomrule==2){
				if(mvarValue1.length()>=1){
					newRoomName = targetFloor + mvarValue1.substring(1,mvarValue1.length());
				}else{
					newRoomName = mvarValue1;
				}
			}else if(roomrule==3){
				if(mvarValue1.length()>=2){
					newRoomName = targetFloor + mvarValue1.substring(2,mvarValue1.length());
				}else{
					newRoomName = mvarValue1;
				}
			}
			data.setVarValue1(newRoomName);
			data.setVarValue2(mvarValue2);
			data.setVarValue3(mvarValue3);
			data.setVarValue4(mvarValue4);
			data.setVarValue40(userName);
			data.setIntValue1(buildingId);
			data.setIntValue2(targetFloor);
			data.setIntValue3(2);
			data.setIntValue4(total+m2+1);
			data.setAddTime(currTime);
			data.setUpdateTime(currTime);
			service.save(data);
			conId = data.getConId();
        }
        
        strTemp = "{\"status\":\"1\"}";
        
		return strTemp;
	}
	
	@RequestMapping("/deleteByFloorId")
	@ResponseBody
	public String deleteByFloorId(int id,HttpServletRequest request){
		String strTemp = "";
		int buildingId = StrEdit.StrToInt(request.getParameter("buildingId"));
		Map<String,Object> newMap1 = new HashMap<String,Object>();
		newMap1.put("task", "deleteByFloorId");
		newMap1.put("intValue1", buildingId);
		newMap1.put("intValue2", id);
		boolean boolValue = service.deleteByMap(newMap1);
		if(boolValue){
			strTemp = "{\"status\":\"1\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"删除失败\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/getJsonById")
	@ResponseBody
	public String getJsonById(int id){
		String strTemp = "";
		Room data = service.findById(id);
		strTemp = "{\"status\":\"1\",\"id\":\""+id+"\",\"name\":\""+data.getVarValue1()+"\"}";
		return strTemp;
	}
	
	@RequestMapping("/get")
	@ResponseBody
	public Map<String, Object> getAll(PageModel<Room> pageModel, Room data, HttpServletRequest request, Model model) {
		
		//System.out.println("------------------get------------------");
		String keywords = StrEdit.StrChkNull(data.getKeywords());
		
		Map<String,Object> newMap = new HashMap<String,Object>();
		
		String varValue1 = StrEdit.StrChkNull(request.getParameter("varValue1"));
		String varValue2 = StrEdit.StrChkNull(request.getParameter("varValue2"));
		
		int pageNumber = StrEdit.StrToInt(request.getParameter("pageNumber"));
		int pageSize = StrEdit.StrToInt(request.getParameter("pageSize"));
		int startRow = 0;
        if(pageNumber!=0) {
        	startRow = (pageNumber - 1) * pageSize;
        }
        newMap.put("startRow", startRow);
        newMap.put("pageSize", pageSize);
		/*int pageStart = (pageNumber -1) * pageSize+1;//oracle分页
        int pageEnd = pageStart + pageSize -1;
        newMap.put("pageStart", pageStart);
        newMap.put("pageEnd", pageEnd);*/
        newMap.put("keywords", keywords);
        newMap.put("varValue1", varValue1);
        newMap.put("varValue2", varValue2);
		List<Room> list = service.pageList(newMap);
		int totalRecords = service.totalRecord(newMap);
		pageModel.setTotal(totalRecords);
		
		Object jsonObject = JSONObject.toJSON(list);
		//System.out.println("jsonObject:"+jsonObject);
		//System.out.println("totalRecords1:"+totalRecords);
		
		int pn = 1;
		if (pageNumber != 0) {
			pn = new Integer(pageNumber);
		}
		pageModel.setPages(totalRecords, pn);//设置总记录数和当前页面
		Map<String, Object> map = new HashMap<>();
		map.put("page", pageModel);
		map.put("list", list);
		return map;
	}
	
	@RequestMapping("/add")
	public String toAdd(Room data,HttpServletRequest request,Model model){
		/*int channelID = StrEdit.StrToInt(request.getParameter("channelID"));
		int parentId = StrEdit.StrToInt(request.getParameter("parentId"));
		
		String parentName = "";
		if(parentId>0){
			Map<String,Object> newMap1 = new HashMap<String,Object>();
			newMap1.put("task", "className");
	        newMap1.put("conId", parentId);
	        parentName = service.getStringValue(newMap1);
		}
        
		request.setAttribute("channelID", channelID);
		request.setAttribute("parentId", parentId);
		request.setAttribute("parentName", parentName);
		
		data.setVarValue7("无");*/
		
		int buildingId = StrEdit.StrToInt(request.getParameter("buildingId"));
		request.setAttribute("buildingId", buildingId);
		
		List<String> dictionery12 = dictionary.selectDictionaryList(12);
		model.addAttribute("dictionery12", dictionery12);
		
		Map<Long, String> typeMap = new HashMap<Long, String>();
        model.addAttribute("data", data);
        model.addAttribute("typeMap", typeMap);
		return "/front/room/add";
	}
	
	@RequestMapping("/save")
	@ResponseBody
	public String save(Room data,HttpServletRequest request,HttpServletResponse response){
		int conId = 0;
		String strTemp = "";
		try{
			service.save(data);
			conId = data.getConId();
		}catch(Exception e){
			conId = 0;
			System.out.println("save插入数据失败" + e.getLocalizedMessage());
		}
		
		if(conId>0){
			
			String attachIds=StrEdit.StrChkNull(request.getParameter("attachIds"));//附件
			if(!attachIds.equals("")){
				String[] arrAttachIds = attachIds.split("\\,");
				for(int m=0;m<arrAttachIds.length;m++){
					boolean boolValue = attach.update(StrEdit.StrToInt(arrAttachIds[m]),conId);
					if(!boolValue){
						System.out.println("附件信息ID更新失败："+arrAttachIds[m]);
					}
				}
			}
			
			String attachId1002=StrEdit.StrChkNull(request.getParameter("attachId1002"));
			if(!attachId1002.equals("")){
				String[] arrAttachIds = attachId1002.split("\\,");
				for(int m=0;m<arrAttachIds.length;m++){
					boolean boolValue = attach.update(StrEdit.StrToInt(arrAttachIds[m]),conId);
					if(!boolValue){
						System.out.println("附件信息ID更新失败："+arrAttachIds[m]);
					}
				}
			}
			
			String attachId1003=StrEdit.StrChkNull(request.getParameter("attachId1003"));
			if(!attachId1003.equals("")){
				String[] arrAttachIds = attachId1003.split("\\,");
				for(int m=0;m<arrAttachIds.length;m++){
					boolean boolValue = attach.update(StrEdit.StrToInt(arrAttachIds[m]),conId);
					if(!boolValue){
						System.out.println("附件信息ID更新失败："+arrAttachIds[m]);
					}
				}
			}
			
			strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"添加失败\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/update")
	@ResponseBody
	public String update(Room data,HttpServletRequest request){
		int conId = data.getConId();
		String strTemp = "";
		//Date currTime = new Date();
		//data.setUpdateTime(currTime);
		boolean boolValue = service.update(data);
		if(boolValue){
			strTemp = "{\"status\":\"1\",\"conId\":\""+conId+"\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"更新失败\"}";
		}
		
		return strTemp;
	}
	
	@RequestMapping("/getById")
	public String getById(int id,Model model){
		Map<Long, String> typeMap = new HashMap<Long, String>();
        
		List<Attachment> attachList1 = attach.list(1321,id);
		List<Attachment> attachList2 = attach.list(1322,id);
		List<Attachment> attachList3 = attach.list(1323,id);
		model.addAttribute("attachList1", attachList1);
		model.addAttribute("attachList2", attachList2);
		model.addAttribute("attachList3", attachList3);
		
		List<String> dictionery12 = dictionary.selectDictionaryList(12);
		model.addAttribute("dictionery12", dictionery12);
        
        model.addAttribute("data", service.findById(id));
        model.addAttribute("typeMap", typeMap);
        
		return "/front/room/add";
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public String delete(int id,HttpServletRequest request){
		String strTemp = "";
		
		Map<String,Object> newMap = new HashMap<String,Object>();
		newMap.put("task", "checkRent");
        newMap.put("roomId", id);
        int isExist = contract.getIntValue(newMap);
        if(isExist>0){
        	strTemp = "{\"status\":\"0\",\"errors\":\"该房源正在出租，不能删除\"}";
        	return strTemp;
		}
        
		boolean boolValue = service.delete(id);
		if(boolValue){
			strTemp = "{\"status\":\"1\"}";
		}else{
			strTemp = "{\"status\":\"0\",\"errors\":\"删除失败\"}";
		}
		
		return strTemp;
	}
	
}
