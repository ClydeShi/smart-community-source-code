package com.server.model;

import java.io.Serializable;
import java.util.List;

public class SysPassValueForSerial  implements Serializable {
	private static final long serialVersionUID = -7318957951656012514L;
	private Long id=null;
	private String arg1=null;
	private String arg2=null;
	private String arg3=null;
	private String arg4=null;
	private String arg5=null;
	private String arg6=null;
	
	private List<String>  arglist1=null;
	private List<String>  arglist2=null;
	private List<String>  arglist3=null;
	
	private List<SysPassValue>  mullinechar=null;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	
	
	public List<SysPassValue> getMullinechar() {
		return mullinechar;
	}
	public void setMullinechar(List<SysPassValue> mullinechar) {
		this.mullinechar = mullinechar;
	}
	public List<String> getArglist3() {
		return arglist3;
	}
	public void setArglist3(List<String> arglist3) {
		this.arglist3 = arglist3;
	}
	public List<String> getArglist1() {
		return arglist1;
	}
	public void setArglist1(List<String> arglist1) {
		this.arglist1 = arglist1;
	}
	public List<String> getArglist2() {
		return arglist2;
	}
	public void setArglist2(List<String> arglist2) {
		this.arglist2 = arglist2;
	}
	public SysPassValueForSerial(Long id, String arg1) {
		super();
		this.id = id;
		this.arg1 = arg1;
	}
	
	
	
	
	public SysPassValueForSerial(Long id, String arg1, String arg2) {
		super();
		this.id = id;
		this.arg1 = arg1;
		this.arg2 = arg2;
	}
	
	
	
	
	
	
	public SysPassValueForSerial(Long id, String arg1, String arg2, String arg3,
			String arg4) {
		super();
		this.id = id;
		this.arg1 = arg1;
		this.arg2 = arg2;
		this.arg3 = arg3;
		this.arg4 = arg4;
	}
	public SysPassValueForSerial(Long id, String arg1, String arg2, String arg3) {
		super();
		this.id = id;
		this.arg1 = arg1;
		this.arg2 = arg2;
		this.arg3 = arg3;
	}
	public SysPassValueForSerial() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getArg1() {
		return arg1;
	}
	public void setArg1(String arg1) {
		this.arg1 = arg1;
	}
	public String getArg2() {
		return arg2;
	}
	public void setArg2(String arg2) {
		this.arg2 = arg2;
	}
	public String getArg3() {
		return arg3;
	}
	public void setArg3(String arg3) {
		this.arg3 = arg3;
	}
	public String getArg4() {
		return arg4;
	}
	public void setArg4(String arg4) {
		this.arg4 = arg4;
	}
	public String getArg5() {
		return arg5;
	}
	public void setArg5(String arg5) {
		this.arg5 = arg5;
	}
	public String getArg6() {
		return arg6;
	}
	public void setArg6(String arg6) {
		this.arg6 = arg6;
	}
	public String getReturn1() {
		return return1;
	}
	public void setReturn1(String return1) {
		this.return1 = return1;
	}
	public String getReturn2() {
		return return2;
	}
	public void setReturn2(String return2) {
		this.return2 = return2;
	}
	private String return1=null;  //返回值1
	private String return2=null;  //返回值2
	
	

	 
}
