package com.server.model;

/*  这里是保存 常用的 ，登入的时候 必须读出来的 参数 */
public class HhSysArgument {
	private Long id=null;
	private String name=null;
	private String descript=null;
	
	private String shouye_lunbo1_picurl = null; //首页活动轮播图1
	private String shouye_lunbo1_picurl_type = null; //活动轮播图1对应的链接 对应的链接  轮播图片的类型  广告分为 1. 店铺 2.店铺产品 3.店铺活动  4.店铺服务
	private String shouye_lunbo1_picurl_value = null; //活动轮播图1对应的值
	
	private String shouye_lunbo2_picurl = null; //活动轮播图1
	private String shouye_lunbo2_picurl_type = null; //活动轮播图1对应的链接  对应的链接  轮播图片的类型  广告分为 1. 店铺 2.店铺产品 3.店铺活动  4.店铺服务
	private String shouye_lunbo2_picurl_value = null; //活动轮播图1对应的值
	
	private String shouye_lunbo3_picurl = null; //活动轮播图3
	private String shouye_lunbo3_picurl_type = null; //活动轮播图3对应的链接 对应的链接  轮播图片的类型  广告分为 1. 店铺 2.店铺产品 3.店铺活动  4.店铺服务 
	private String shouye_lunbo3_picurl_value = null; //活动轮播图3对应的值	
	
	private String shouye_lunbo4_picurl = null; //活动轮播图3
	private String shouye_lunbo4_picurl_type = null; //活动轮播图3对应的链接 对应的链接  轮播图片的类型  广告分为 1. 店铺 2.店铺产品 3.店铺活动  4.店铺服务 
	private String shouye_lunbo4_picurl_value = null; //活动轮播图3对应的值
	
	private String shouye_lunbo5_picurl = null; //活动轮播图3
	private String shouye_lunbo5_picurl_type = null; //活动轮播图3对应的链接 对应的链接  轮播图片的类型  广告分为 1. 店铺 2.店铺产品 3.店铺活动  4.店铺服务 
	private String shouye_lunbo5_picurl_value = null; //活动轮播图3对应的值
	
	private String shangwubanname =null;
	private String xiawubanname=null;
	private String wanshangbanname = null;
	
	private double shangwubanci_startdt =0.00;
	private double xiawubanci_startdt =0.00;
	private double wanshangbanci_startdt =0.00;
	

	private int shopcount=0;
	private String bz1="";
	private String bz2="";
	private String bz3="";
	
	 
	public int getShopcount() {
		return shopcount;
	}

	public void setShopcount(int shopcount) {
		this.shopcount = shopcount;
	}

	public String getBz1() {
		return bz1;
	}

	public void setBz1(String bz1) {
		this.bz1 = bz1;
	}

	public String getBz2() {
		return bz2;
	}

	public void setBz2(String bz2) {
		this.bz2 = bz2;
	}

	public String getBz3() {
		return bz3;
	}

	public void setBz3(String bz3) {
		this.bz3 = bz3;
	}

	public HhSysArgument(){
		super();
	}
	 
	public HhSysArgument(Long id, String name, String descript,
			String shouye_lunbo1_picurl,String shouye_lunbo1_picurl_type,String shouye_lunbo1_picurl_value,
			String shouye_lunbo2_picurl,String shouye_lunbo2_picurl_type,String shouye_lunbo2_picurl_value,
			String shouye_lunbo3_picurl,String shouye_lunbo3_picurl_type,String shouye_lunbo3_picurl_value, 
			String shouye_lunbo4_picurl,String shouye_lunbo4_picurl_type,String shouye_lunbo4_picurl_value,
			String shouye_lunbo5_picurl,String shouye_lunbo5_picurl_type,String shouye_lunbo5_picurl_value,
			String shangwubanname,String xiawubanname,String wanshangbanname,
			double shangwubanci_startdt,double xiawubanci_startdt,double wanshangbanci_startdt 
			) {
		super();
		this.id = id;
		this.name = name;
		this.descript = descript;
		
		//首页的广告图片连接
		this.shouye_lunbo1_picurl       = shouye_lunbo1_picurl;           //首页活动轮播图1 
		this.shouye_lunbo1_picurl_type  = shouye_lunbo1_picurl_type;	  //活动轮播图1对应的链接(店铺,服务,活动，产品，动态，技师)	
		this.shouye_lunbo1_picurl_value = shouye_lunbo1_picurl_value;     //活动轮播图1对应的值 (上面不同类型对应的ID值)
		
		this.shouye_lunbo2_picurl       = shouye_lunbo2_picurl;           //首页活动轮播图2 
		this.shouye_lunbo2_picurl_type  = shouye_lunbo2_picurl_type;	  //活动轮播图2对应的链接(店铺,服务,活动)	
		this.shouye_lunbo2_picurl_value = shouye_lunbo2_picurl_value;     //活动轮播图2对应的值 (店铺,服务,活动)  
		
		this.shouye_lunbo3_picurl       = shouye_lunbo3_picurl;            //首页活动轮播图1 
		this.shouye_lunbo3_picurl_type  = shouye_lunbo3_picurl_type;	   //活动轮播图1对应的链接(店铺,服务,活动)	
		this.shouye_lunbo3_picurl_value = shouye_lunbo3_picurl_value;      //活动轮播图1对应的值
		
		
		this.shouye_lunbo4_picurl       = shouye_lunbo4_picurl;            //首页活动轮播图1 
		this.shouye_lunbo4_picurl_type  = shouye_lunbo4_picurl_type;	   //活动轮播图1对应的链接(店铺,服务,活动)	
		this.shouye_lunbo4_picurl_value = shouye_lunbo4_picurl_value;      //活动轮播图1对应的值		
		
		
		
		this.shouye_lunbo5_picurl       = shouye_lunbo5_picurl;            //首页活动轮播图1 
		this.shouye_lunbo5_picurl_type  = shouye_lunbo5_picurl_type;	   //活动轮播图1对应的链接(店铺,服务,活动)	
		this.shouye_lunbo5_picurl_value = shouye_lunbo5_picurl_value;      //活动轮播图1对应的值		
		
		 
		
		this.shangwubanname  =shangwubanname;
		this.xiawubanname    =xiawubanname;
		this.wanshangbanname = wanshangbanname;
		
		
		this.shangwubanci_startdt  =shangwubanci_startdt;
		this.xiawubanci_startdt    =xiawubanci_startdt;
		this.wanshangbanci_startdt =wanshangbanci_startdt;
		
		
	}
	
	
	
	
	
	   public String getShouye_lunbo1_picurl() {
		return shouye_lunbo1_picurl;
	}

	public void setShouye_lunbo1_picurl(String shouye_lunbo1_picurl) {
		this.shouye_lunbo1_picurl = shouye_lunbo1_picurl;
	}

	public String getShouye_lunbo1_picurl_type() {
		return shouye_lunbo1_picurl_type;
	}

	public void setShouye_lunbo1_picurl_type(String shouye_lunbo1_picurl_type) {
		this.shouye_lunbo1_picurl_type = shouye_lunbo1_picurl_type;
	}

	public String getShouye_lunbo1_picurl_value() {
		return shouye_lunbo1_picurl_value;
	}

	public void setShouye_lunbo1_picurl_value(String shouye_lunbo1_picurl_value) {
		this.shouye_lunbo1_picurl_value = shouye_lunbo1_picurl_value;
	}

	public String getShouye_lunbo2_picurl() {
		return shouye_lunbo2_picurl;
	}

	public void setShouye_lunbo2_picurl(String shouye_lunbo2_picurl) {
		this.shouye_lunbo2_picurl = shouye_lunbo2_picurl;
	}

	public String getShouye_lunbo2_picurl_type() {
		return shouye_lunbo2_picurl_type;
	}

	public void setShouye_lunbo2_picurl_type(String shouye_lunbo2_picurl_type) {
		this.shouye_lunbo2_picurl_type = shouye_lunbo2_picurl_type;
	}

	public String getShouye_lunbo2_picurl_value() {
		return shouye_lunbo2_picurl_value;
	}

	public void setShouye_lunbo2_picurl_value(String shouye_lunbo2_picurl_value) {
		this.shouye_lunbo2_picurl_value = shouye_lunbo2_picurl_value;
	}

	public String getShouye_lunbo3_picurl() {
		return shouye_lunbo3_picurl;
	}

	public void setShouye_lunbo3_picurl(String shouye_lunbo3_picurl) {
		this.shouye_lunbo3_picurl = shouye_lunbo3_picurl;
	}

	public String getShouye_lunbo3_picurl_type() {
		return shouye_lunbo3_picurl_type;
	}

	public void setShouye_lunbo3_picurl_type(String shouye_lunbo3_picurl_type) {
		this.shouye_lunbo3_picurl_type = shouye_lunbo3_picurl_type;
	}

	public String getShouye_lunbo3_picurl_value() {
		return shouye_lunbo3_picurl_value;
	}

	public void setShouye_lunbo3_picurl_value(String shouye_lunbo3_picurl_value) {
		this.shouye_lunbo3_picurl_value = shouye_lunbo3_picurl_value;
	}

	public String getShouye_lunbo4_picurl() {
		return shouye_lunbo4_picurl;
	}

	public void setShouye_lunbo4_picurl(String shouye_lunbo4_picurl) {
		this.shouye_lunbo4_picurl = shouye_lunbo4_picurl;
	}

	public String getShouye_lunbo4_picurl_type() {
		return shouye_lunbo4_picurl_type;
	}

	public void setShouye_lunbo4_picurl_type(String shouye_lunbo4_picurl_type) {
		this.shouye_lunbo4_picurl_type = shouye_lunbo4_picurl_type;
	}

	public String getShouye_lunbo4_picurl_value() {
		return shouye_lunbo4_picurl_value;
	}

	public void setShouye_lunbo4_picurl_value(String shouye_lunbo4_picurl_value) {
		this.shouye_lunbo4_picurl_value = shouye_lunbo4_picurl_value;
	}

	public String getShouye_lunbo5_picurl() {
		return shouye_lunbo5_picurl;
	}

	public void setShouye_lunbo5_picurl(String shouye_lunbo5_picurl) {
		this.shouye_lunbo5_picurl = shouye_lunbo5_picurl;
	}

	public String getShouye_lunbo5_picurl_type() {
		return shouye_lunbo5_picurl_type;
	}

	public void setShouye_lunbo5_picurl_type(String shouye_lunbo5_picurl_type) {
		this.shouye_lunbo5_picurl_type = shouye_lunbo5_picurl_type;
	}

	public String getShouye_lunbo5_picurl_value() {
		return shouye_lunbo5_picurl_value;
	}

	public void setShouye_lunbo5_picurl_value(String shouye_lunbo5_picurl_value) {
		this.shouye_lunbo5_picurl_value = shouye_lunbo5_picurl_value;
	}

	public String getShangwubanname() {
		return shangwubanname;
	}

	public void setShangwubanname(String shangwubanname) {
		this.shangwubanname = shangwubanname;
	}

	public String getXiawubanname() {
		return xiawubanname;
	}

	public void setXiawubanname(String xiawubanname) {
		this.xiawubanname = xiawubanname;
	}

	public String getWanshangbanname() {
		return wanshangbanname;
	}

	public void setWanshangbanname(String wanshangbanname) {
		this.wanshangbanname = wanshangbanname;
	}

	public double getShangwubanci_startdt() {
		return shangwubanci_startdt;
	}

	public void setShangwubanci_startdt(double shangwubanci_startdt) {
		this.shangwubanci_startdt = shangwubanci_startdt;
	}

	public double getXiawubanci_startdt() {
		return xiawubanci_startdt;
	}

	public void setXiawubanci_startdt(double xiawubanci_startdt) {
		this.xiawubanci_startdt = xiawubanci_startdt;
	}

	public double getWanshangbanci_startdt() {
		return wanshangbanci_startdt;
	}

	public void setWanshangbanci_startdt(double wanshangbanci_startdt) {
		this.wanshangbanci_startdt = wanshangbanci_startdt;
	}

		//班次 早上 开始时间 
		public double get_shangwubanci_startdt() {
			return shangwubanci_startdt;
		}
		public void set_shangwubanci_startdt(double  shangwubanci_startdt) {
			this.shangwubanci_startdt = shangwubanci_startdt;
		}
	
		//班次 下午 开始时间 
		public double get_xiawubanci_startdt() {
			return xiawubanci_startdt;
		}
		public void set_xiawubanci_startdt(double  xiawubanci_startdt) {
			this.xiawubanci_startdt = xiawubanci_startdt;
		}	
	
		//班次 晚上 开始时间 
		public double get_wanshangbanci_startdt() {
			return wanshangbanci_startdt;
		}
		public void set_wanshangbanci_startdt(double  wanshangbanci_startdt) {
			this.wanshangbanci_startdt = wanshangbanci_startdt;
		}		
	
	
	
	//班次名称 早班
	public String get_shangwubanname() {
		return shangwubanname;
	}

	public void set_shangwubanname(String shangwubanname) {
		this.shangwubanname = shangwubanname;
	}
	
	//班次名称 下午班
	public String get_xiawubanname() {
		return xiawubanname;
	}

	public void set_xiawubanname(String xiawubanname) {
		this.xiawubanname = xiawubanname;
	}
	
	//班次名称 晚班
	public String get_wanshangbanname() {
		return wanshangbanname;
	}

	public void set_wanshangbanname(String wanshangbanname) {
		this.wanshangbanname = wanshangbanname;
	}
	 
	
	//另外一个放法得到轮播图片的连接
	public String get_shouye_lunbo_pic(int i){
		if (i ==1 ){
			return shouye_lunbo1_picurl;
		}
		
		if (i ==2 ){
			return shouye_lunbo2_picurl;
		}	
		
		if (i ==3 ){
			return shouye_lunbo3_picurl;
		}	
 		
		
		return "";
	}
	
	
	//另外一个放法得到轮播图片的类型
	public String get_shouye_lunbo_pic_type(int i){
		if (i ==1 ){
			return shouye_lunbo1_picurl_type;
		}
		
		if (i ==2 ){
			return shouye_lunbo2_picurl_type;
		}	
		
		if (i ==3 ){
			return shouye_lunbo3_picurl_type;
		}	
 		
		
		return "";
	}
	 
	//huodong_lunbo1_picurl    --轮播图片 的图片 值
	public String get_shouye_lunbo_pic_id(int i ) {
		
		if (i ==1 ){
			return shouye_lunbo1_picurl_value;
		}
		
		if (i ==2 ){
			return shouye_lunbo2_picurl_value;
		}	
		
		if (i ==3 ){
			return shouye_lunbo3_picurl_value;
		}	
		  
		return "";
	}

 
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescript() {
		return descript;
	}

	public void setDescript(String descript) {
		this.descript = descript;
	}

	 
}
