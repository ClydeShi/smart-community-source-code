package com.server.data;

import java.sql.*;
import java.util.*;

import com.util.common.DBConn2;

public class ListBaseInfo {
	private DBConn2 mdb = null;
    private String sql = "";
    private ResultSet rs = null;
	private int i = 0;
	private boolean boolValue = false;

    public ListBaseInfo() {
    	mdb = new DBConn2();
    }
    
    public int sumRecords(String sql){
    	int sum=0;
    	
    	try{
        	ResultSet rs = mdb.executeQuery(sql);
            if(rs.next()){
            	sum = rs.getInt("total");
            }
            rs.close();
        }
        catch(Exception e){
            System.out.println("ListBaseInfo sumRecords()" +e.getLocalizedMessage());            
        }
        finally{
            mdb.close();
        }
        return sum;
    }
    
    public String getContactCategoryName(int mid){
    	String strValue = "";
    	ResultSet rs = null;
        try{
            sql = "select NAME from contact_category where CONID="+mid+" order by CONID asc";
            rs = mdb.executeQuery(sql);
            while(rs.next()){
            	strValue = rs.getString("NAME");
            }
            rs.close();
        }
        catch(Exception e){
        	System.out.println("ListBaseInfo getContactCategoryName()" + e.getLocalizedMessage());
        }
        finally{
            mdb.close();
        }
        return strValue;
    }
    
    public String getActiveCategoryName(int mid){
    	String strValue = "";
    	ResultSet rs = null;
        try{
            sql = "select NAME from active_category where CONID="+mid+" order by CONID asc";
            rs = mdb.executeQuery(sql);
            while(rs.next()){
            	strValue = rs.getString("NAME");
            }
            rs.close();
        }
        catch(Exception e){
        	System.out.println("ListBaseInfo getContactCategoryName()" + e.getLocalizedMessage());
        }
        finally{
            mdb.close();
        }
        return strValue;
    }



}
