package com.server.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.server.model.CommonList;
import com.server.model.HhSysArgument;
import com.server.model.SysPassList;
import com.server.model.SysPassValueForSerial;
import com.server.model.User;
import com.util.common.DBConn2;
import com.util.common.FileEdit;
import com.util.common.StrEdit;
import com.server.model.CommonData;
import com.server.model.SysPassValue;
import com.server.data.DataInterface;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * 这个用来 模拟数据从服务器返回过来，测试的时候直接用 数组来返回值给客户端
 */

public class DemoDataInterface implements DataInterface {
	private DBConn2 mdb = null;
	private ResultSet rs = null;
	private String sql = "";
	private String strTemp = "";
	private String weburl ="http://139.9.1.194:82/zhyq/";
	private String path ="C:/zhyq/";
	private int pageSize = 10;
	private static Logger log = LoggerFactory.getLogger(DemoDataInterface.class);
	
	public DemoDataInterface() {
		mdb = new DBConn2();
	}
	
	public User userLogin(User user) throws Throwable {
		
		sql="select * from userconn where (VARVALUE1='"+user.getCode()+"' or VARVALUE8='"+user.getCode()+"') and VARVALUE2='"+MD5.toMD5(user.getPassword())+"' and INTVALUE1=1";
		System.out.println(sql);
		try{
            rs = mdb.executeQuery(sql);
            if(rs.next()){
            	int userRole = rs.getInt("INTVALUE4");
            	int userType = rs.getInt("INTVALUE6");
            	user.setId(Long.valueOf(rs.getInt("CONID")));
            	user.setCode(rs.getString("VARVALUE1"));
    			user.setName(rs.getString("VARVALUE3"));
    			user.setUserType(String.valueOf(userType));//人员类型 园区方 1001 保洁领班 1011 维修人员 1021 企业人员 1100
    			System.out.println("登录成功："+user.getCode());
    			System.out.println("userType："+userType);
    			
    			String psql = "select VARVALUE4,TEXT1 from USERROLE where CONID="+userRole+"";
    			ResultSet rs2 = mdb.executeQuery(psql);
    			if(rs2.next()){
    				user.setUserRole(rs2.getString("VARVALUE4"));//功能权限，StrEdit.isInclude(purview,2001)
    			}
    			rs2.close();
            }else{
            	user = null;
            }
        }
        catch(Exception e){
        	user = null;
            System.out.println("userLogin()" +e.getLocalizedMessage());
        }
        finally{
            rs.close();
            mdb.close();
        }
		
		return user;
	}
	
	/*
	 * 用来读取详情，读取增加修改删除结果
	 */
	@Override
	public SysPassValue sendOrderToServerForValue(SysPassValue curObject) throws Throwable {
		
		//重要的是返回这个数据
		SysPassValue dbResult = new SysPassValue();

		// 函数名称  页码数量  其他参数根据不同的 功能名称 进行读取
		String funName = curObject.getFunctionName();
		String pageNumber = curObject.getPageNumber();
		
		System.out.println("详情:"+funName);
		
		if(funName.equals("getWorkDetail")){
			
			String id = curObject.getId();
			String userId = curObject.getUserId();
			
			sql="select * from CLEAN where CONID="+id+"";
			try{
	            rs = mdb.executeQuery(sql);
	            if(rs.next()){
	            	dbResult.setId(id);
	            	dbResult.setArg1(rs.getString("VARVALUE1"));
	            	dbResult.setArg2(rs.getString("VARVALUE2"));
	            	dbResult.setArg3(rs.getString("VARVALUE3"));
	            	dbResult.setArg4(rs.getString("VARVALUE4"));
	            	dbResult.setArg5(StrEdit.StringLeft(rs.getString("DATEVALUE1"), 19));
	            	dbResult.setArg6(rs.getString("VARVALUE11"));
	            	dbResult.setArg7(rs.getString("TEXT1"));
	            	dbResult.setArg8(rs.getString("TEXT2"));
	            	dbResult.setArg9(StrEdit.StringLeft(rs.getString("DATEVALUE2"), 19));
	            	dbResult.setArg10(String.valueOf(rs.getInt("INTVALUE4")));
	            	dbResult.setReturnResult("1");
	            }
	        }catch(Exception e){
	            System.out.println("getWorkDetail()" +e.getLocalizedMessage());
	        }
	        finally{
	            mdb.close();
	        }
			
			return dbResult;
			
		}
		
		if(funName.equals("getArticleText1")){
			
			int arg1 = StrEdit.StrToInt(curObject.getArg1());
			
			sql="select TEXT1 from ARTICLE where INTVALUE2="+arg1+"";
			try{
	            rs = mdb.executeQuery(sql);
	            if(rs.next()){
	            	dbResult.setArg1(String.valueOf(arg1));
	            	dbResult.setArg2(rs.getString("TEXT1"));
	            	dbResult.setReturnResult("1");
	            }
	        }catch(Exception e){
	            System.out.println("getArticleText1()" +e.getLocalizedMessage());
	        }
	        finally{
	            mdb.close();
	        }
			
			return dbResult;
			
		}
		
		if(funName.equals("getUserDetail")){
			
			int userId = StrEdit.StrToInt(curObject.getUserId());
			
			sql="select * from userconn where CONID="+userId+"";
			try{
	            rs = mdb.executeQuery(sql);
	            if(rs.next()){
	            	dbResult.setId(String.valueOf(userId));
	            	dbResult.setArg1(rs.getString("VARVALUE1"));//用户名
	            	dbResult.setArg2(rs.getString("VARVALUE3"));//姓名
	            	dbResult.setArg3(rs.getString("VARVALUE4"));//性别
	            	dbResult.setArg4(rs.getString("VARVALUE8"));//手机号码
	            	dbResult.setArg5(rs.getString("VARVALUE9"));//电话号码
	            	dbResult.setArg6(rs.getString("VARVALUE10"));//QQ号码
	            	dbResult.setArg7(rs.getString("VARVALUE11"));//微信号
	            	dbResult.setArg8(rs.getString("VARVALUE20"));//所属企业
	            	dbResult.setArg9(String.valueOf(rs.getInt("INTVALUE1")));//状态
	            	dbResult.setArg10(String.valueOf(rs.getInt("INTVALUE2")));//部门ID
	            	dbResult.setArg11(String.valueOf(rs.getInt("INTVALUE3")));
	            	dbResult.setArg12(String.valueOf(rs.getInt("INTVALUE4")));//角色ID
	            	dbResult.setArg13(String.valueOf(rs.getInt("INTVALUE5")));//企业ID
	            	dbResult.setArg14(String.valueOf(rs.getInt("INTVALUE6")));//人员类型
	            	dbResult.setArg15(rs.getString("VARVALUE19"));//地址
	            	dbResult.setArg30(rs.getString("TEXT1"));
	            	dbResult.setArg31(StrEdit.StringLeft(rs.getString("ADDTIME"), 19));
	            	dbResult.setReturnResult("1");
	            }
	        }catch(Exception e){
	            System.out.println("getUserDetail()" +e.getLocalizedMessage());
	        }
	        finally{
	            mdb.close();
	        }
			
			return dbResult;
			
		}
		
		if(funName.equals("getUserDetails")){
			
			int userId = StrEdit.StrToInt(curObject.getUserId());
			
			sql="select * from userconn where CONID="+userId+"";
			try{
	            rs = mdb.executeQuery(sql);
	            if(rs.next()){
	            	String picture = rs.getString("VARVALUE38");
	            	if(!picture.equals("")){
	            		picture = weburl + picture;
	            	}
	            	dbResult.setId(String.valueOf(userId));
	            	dbResult.setArg1(rs.getString("VARVALUE1"));//用户名
	            	dbResult.setArg2("");
	            	dbResult.setArg3(rs.getString("VARVALUE3"));//姓名
	            	dbResult.setArg4(rs.getString("VARVALUE4"));//性别
	            	dbResult.setArg5(rs.getString("VARVALUE5"));
	            	dbResult.setArg6(rs.getString("VARVALUE6"));
	            	dbResult.setArg7(rs.getString("VARVALUE7"));
	            	dbResult.setArg8(rs.getString("VARVALUE8"));//手机号码
	            	dbResult.setArg9(rs.getString("VARVALUE9"));//电话号码
	            	dbResult.setArg10(rs.getString("VARVALUE10"));//QQ号码
	            	dbResult.setArg11(rs.getString("VARVALUE11"));//微信号
	            	dbResult.setArg12(rs.getString("VARVALUE12"));
	            	dbResult.setArg13(rs.getString("VARVALUE13"));
	            	dbResult.setArg14(rs.getString("VARVALUE14"));
	            	dbResult.setArg15(rs.getString("VARVALUE15"));
	            	dbResult.setArg16(rs.getString("VARVALUE16"));
	            	dbResult.setArg17(rs.getString("VARVALUE17"));
	            	dbResult.setArg18(rs.getString("VARVALUE18"));
	            	dbResult.setArg19(rs.getString("VARVALUE19"));//地址
	            	dbResult.setArg20(rs.getString("VARVALUE20"));//所属企业
	            	dbResult.setArg38(picture);
	            	dbResult.setArg40(rs.getString("TEXT1"));
	            	dbResult.setArg41(StrEdit.StringLeft(rs.getString("ADDTIME"), 19));
	            	dbResult.setArg42(String.valueOf(rs.getInt("INTVALUE5")));//企业ID
	            	dbResult.setArg43(String.valueOf(rs.getInt("INTVALUE6")));//人员类型
	            	dbResult.setReturnResult("1");
	            }
	        }catch(Exception e){
	            System.out.println("getUserDetails()" +e.getLocalizedMessage());
	        }
	        finally{
	            mdb.close();
	        }
			
			return dbResult;
			
		}
		
		if(funName.equals("setWorkStatus")){
			int id = StrEdit.StrToInt(curObject.getId());
			int userId = StrEdit.StrToInt(curObject.getUserId());
			int arg1 = StrEdit.StrToInt(curObject.getArg1());
			
			try{
				if(arg1==2){
					sql = "update CLEAN set VARVALUE6 = '6', INTVALUE4 = "+arg1+" where CONID="+id+"";//已完成
				}else{
					sql = "update CLEAN set INTVALUE4 = "+arg1+" where CONID="+id+"";
				}
				//sql = "delete from activeconn where CONID="+id+"";
				System.out.println(sql);
				int i = mdb.executeUpdate(sql);
				if(i>0){
					dbResult.setReturnResult("1");
				}else{
					dbResult.setReturnResult("0");
				}
			}catch(Exception e){
        		dbResult.setReturnResult("0");
        		System.out.println("setWorkStatus()" + e.getLocalizedMessage());
            }
            finally{
                mdb.close();
            }
			
			return dbResult;
		}
		
		if(funName.equals("setCleanStatus")){
			int id = StrEdit.StrToInt(curObject.getId());
			int userId = StrEdit.StrToInt(curObject.getUserId());
			int arg1 = StrEdit.StrToInt(curObject.getArg1());
			
			try{
				if(arg1==6){
					sql = "update CLEAN set VARVALUE6 = '"+arg1+"', INTVALUE4 = '2' where CONID="+id+"";//已完成
				}else if(arg1==-1){
					sql = "update CLEAN set VARVALUE6 = '"+arg1+"', INTVALUE4 = "+arg1+" where CONID="+id+"";//取消
				}
				//sql = "delete from activeconn where CONID="+id+"";
				System.out.println(sql);
				int i = mdb.executeUpdate(sql);
				if(i>0){
					dbResult.setReturnResult("1");
				}else{
					dbResult.setReturnResult("0");
				}
			}catch(Exception e){
        		dbResult.setReturnResult("0");
        		System.out.println("setCleanStatus()" + e.getLocalizedMessage());
            }
            finally{
                mdb.close();
            }
			
			return dbResult;
		}
		
		if(funName.equals("setMeetingStatus")){
			int id = StrEdit.StrToInt(curObject.getId());
			int userId = StrEdit.StrToInt(curObject.getUserId());
			int arg1 = StrEdit.StrToInt(curObject.getArg1());
			
			try{
				if(arg1==6){
					sql = "update MEETING set VARVALUE6 = '"+arg1+"', INTVALUE4 = '2' where CONID="+id+"";//已完成
				}else if(arg1==-1){
					sql = "update MEETING set VARVALUE6 = '"+arg1+"', INTVALUE4 = "+arg1+" where CONID="+id+"";//取消
				}
				//sql = "delete from activeconn where CONID="+id+"";
				System.out.println(sql);
				int i = mdb.executeUpdate(sql);
				if(i>0){
					dbResult.setReturnResult("1");
				}else{
					dbResult.setReturnResult("0");
				}
			}catch(Exception e){
        		dbResult.setReturnResult("0");
        		System.out.println("setMeetingStatus()" + e.getLocalizedMessage());
            }
            finally{
                mdb.close();
            }
			
			return dbResult;
		}
		
		if(funName.equals("modifyNickName")){
			int userId = StrEdit.StrToInt(curObject.getUserId());
			String arg1 = curObject.getArg1();
			
			try{
				sql = "update userconn set VARVALUE5 = '"+arg1+"' where CONID="+userId+"";
				System.out.println(sql);
				int i = mdb.executeUpdate(sql);
				if(i>0){
					dbResult.setReturnResult("1");
				}else{
					dbResult.setReturnResult("0");
				}
			}catch(Exception e){
        		dbResult.setReturnResult("0");
        		System.out.println("modifyNickName()" + e.getLocalizedMessage());
            }
            finally{
                mdb.close();
            }
			
			return dbResult;
		}
		
		if(funName.equals("modifyGender")){
			int userId = StrEdit.StrToInt(curObject.getUserId());
			String arg1 = curObject.getArg1();
			
			try{
				sql = "update userconn set VARVALUE4 = '"+arg1+"' where CONID="+userId+"";
				System.out.println(sql);
				int i = mdb.executeUpdate(sql);
				if(i>0){
					dbResult.setReturnResult("1");
					dbResult.setArg1(arg1);
				}else{
					dbResult.setReturnResult("0");
				}
			}catch(Exception e){
        		dbResult.setReturnResult("0");
        		System.out.println("modifyGender()" + e.getLocalizedMessage());
            }
            finally{
                mdb.close();
            }
			
			return dbResult;
		}
		
		if(funName.equals("modifyAddress")){
			int userId = StrEdit.StrToInt(curObject.getUserId());
			String arg1 = curObject.getArg1();
			
			try{
				sql = "update userconn set VARVALUE19 = '"+arg1+"' where CONID="+userId+"";
				System.out.println(sql);
				int i = mdb.executeUpdate(sql);
				if(i>0){
					dbResult.setReturnResult("1");
					dbResult.setArg1(arg1);
				}else{
					dbResult.setReturnResult("0");
				}
			}catch(Exception e){
        		dbResult.setReturnResult("0");
        		System.out.println("modifyAddress()" + e.getLocalizedMessage());
            }
            finally{
                mdb.close();
            }
			
			return dbResult;
		}
		
		if(funName.equals("modifyPhoto")){
			int userId = StrEdit.StrToInt(curObject.getUserId());
			String arg1 = curObject.getArg1();
			String arg2 = curObject.getArg2();
			if(arg1==null){
				dbResult.setReturnResult("0");
				return dbResult;
			}
			if(arg1.equals("")){
				dbResult.setReturnResult("0");
				return dbResult;
			}
			
			try{
				String oldPicture = "";
				sql="select VARVALUE38 from userconn where CONID="+userId+"";
				rs = mdb.executeQuery(sql);
	            if(rs.next()){
	            	oldPicture = rs.getString("VARVALUE38");
				}
	            rs.close();
	            if(!oldPicture.equals("")){
	            	sql = "delete from ATTACHMENT where VARVALUE4='"+oldPicture+"'";
	            	mdb.executeUpdate(sql);
	            	FileEdit del = new FileEdit();
	            	del.deleteFile1(path + oldPicture);
	            }
	            
	            String addTime = StrEdit.getSimDateForDB();
	            sql = "insert into ATTACHMENT(VARVALUE1,VARVALUE2,VARVALUE3,VARVALUE4,VARVALUE5,VARVALUE6,VARVALUE7,VARVALUE8,ADDTIME)values";
	            sql = sql + "(90,"+userId+",'头像','"+arg1+"','.jpg','','','"+arg2+"','"+addTime+"')";
	        	mdb.executeUpdate(sql);
	            
				sql = "update userconn set VARVALUE38 = '"+arg1+"' where CONID="+userId+"";
				System.out.println(sql);
				int i = mdb.executeUpdate(sql);
				if(i>0){
					dbResult.setReturnResult("1");
					dbResult.setArg1(weburl + arg1);
				}else{
					dbResult.setReturnResult("0");
				}
			}catch(Exception e){
        		dbResult.setReturnResult("0");
        		System.out.println("modifyPhoto()" + e.getLocalizedMessage());
            }
            finally{
                mdb.close();
            }
			
			return dbResult;
		}
		
		if(funName.equals("verifyPassword")){
			int userId = StrEdit.StrToInt(curObject.getUserId());
			String arg1 = curObject.getArg1();
			
			try{
				sql="select * from userconn where VARVALUE2='"+MD5.toMD5(arg1)+"' and CONID="+userId+"";
				System.out.println(sql);
				rs = mdb.executeQuery(sql);
	            if(rs.next()){
					dbResult.setReturnResult("1");
				}else{
					dbResult.setReturnResult("0");
				}
			}catch(Exception e){
        		dbResult.setReturnResult("0");
        		System.out.println("verifyPassword()" + e.getLocalizedMessage());
            }
            finally{
                mdb.close();
            }
			
			return dbResult;
		}
		
		if(funName.equals("modifyPassword")){
			int userId = StrEdit.StrToInt(curObject.getUserId());
			String arg1 = curObject.getArg1();
			
			try{
				sql = "update userconn set VARVALUE2='"+MD5.toMD5(arg1)+"' where CONID="+userId+"";
				System.out.println(sql);
				int i = mdb.executeUpdate(sql);
				if(i>0){
					dbResult.setReturnResult("1");
				}else{
					dbResult.setReturnResult("0");
				}
			}catch(Exception e){
        		dbResult.setReturnResult("0");
        		System.out.println("modifyPassword()" + e.getLocalizedMessage());
            }
            finally{
                mdb.close();
            }
			
			return dbResult;
		}
		
		if(funName.equals("saveWorkFeedback")){
			
			String userId = curObject.getUserId();
			int id = StrEdit.StrToInt(curObject.getId());
			String content = curObject.getArg10();
			String nowDate = StrEdit.getSimDateForDB();
			
			try{
				
				sql = "update CLEAN set TEXT2='"+content+"',INTVALUE4=2,DATEVALUE2='"+nowDate+"' where CONID="+id+"";
				
				System.out.println(sql);
				
				int i = mdb.executeUpdateAutoCommit(sql);//调用事务更新sql
				if(i>0){
					
					dbResult.setReturnResult("1");
					
					/*if(!arg2.equals("")){
						sql = "update activeconn set VARVALUE5='"+arg2+"' where CONID="+taskId+"";
						int j = mdb.executeUpdateAutoCommit(sql);
						if(j>0){
							dbResult.setReturnResult("1");
						}else{
							dbResult.setReturnResult("0");
						}
					}else{
						dbResult.setReturnResult("1");
					}*/
					
				}else{
					dbResult.setReturnResult("0");
				}
			}catch(Exception e){
        		System.out.println("saveWorkFeedback()" + e.getLocalizedMessage());
            }
            finally{
                mdb.close();
            }
			
			return dbResult;
			
		}
		
		if(funName.equals("uploadPicture")){
			int userId = StrEdit.StrToInt(curObject.getUserId());
			String arg1 = curObject.getArg1();
			String arg2 = curObject.getArg2();
			String arg3 = curObject.getArg3();
			String arg4 = curObject.getArg4();
			if(arg1==null){
				dbResult.setReturnResult("0");
				return dbResult;
			}
			if(arg1.equals("")){
				dbResult.setReturnResult("0");
				return dbResult;
			}
			if(arg3==null){
				arg3 = "1311";
			}
			if(arg4==null){
				arg4 = "图片";
			}
			
			try{	            
	            String addTime = StrEdit.getSimDateForDB();
	            sql = "insert into ATTACHMENT(VARVALUE1,VARVALUE2,VARVALUE3,VARVALUE4,VARVALUE5,VARVALUE6,VARVALUE7,VARVALUE8,ADDTIME)values";
	            sql = sql + "('"+arg3+"',0,'"+arg4+"','"+arg1+"','.jpg','','','"+arg2+"','"+addTime+"')";
	            System.out.println(sql);
				int i = mdb.executeUpdate(sql);
				if(i>0){
					int conId = 0;
					sql="select CONID from ATTACHMENT where VARVALUE1='"+arg3+"' and VARVALUE4='"+arg1+"' order by CONID desc limit 1";
					ResultSet rs1 = mdb.executeQuery(sql);
		            if(rs1.next()){
		            	conId = rs1.getInt("CONID");
		            	dbResult.setReturnResult("1");
						dbResult.setArg1(weburl + arg1);
						dbResult.setArg2(String.valueOf(conId));
		            }else{
		            	dbResult.setReturnResult("0");
		            }
		            rs1.close();
				}else{
					dbResult.setReturnResult("0");
				}
			}catch(Exception e){
        		dbResult.setReturnResult("0");
        		System.out.println("uploadPicture()" + e.getLocalizedMessage());
            }
            finally{
                mdb.close();
            }
			
			return dbResult;
		}
		
		if(funName.equals("saveProblemAdd")){
			
			String userId = curObject.getUserId();
			int id = StrEdit.StrToInt(curObject.getId());
			String arg1 = curObject.getArg1();
			String arg2 = curObject.getArg2();
			String arg3 = curObject.getArg3();
			String arg4 = curObject.getArg4();
			String arg5 = curObject.getArg5();
			String arg6 = curObject.getArg6();
			String arg7 = curObject.getArg7();
			String arg8 = curObject.getArg8();
			String arg9 = curObject.getArg9();
			String arg12 = curObject.getArg12();
			String nowDate = StrEdit.getSimDateForDB();
			String orderNo = StrEdit.getSN("2");
			
			try{
				
				sql="select INTVALUE5 from userconn where CONID="+userId+"";
				rs = mdb.executeQuery(sql);
	            if(rs.next()){
	            	arg9 = String.valueOf(rs.getInt("INTVALUE5"));//企业ID
	            }
	            rs.close();
	            
				sql = "insert into CLEAN(ChannelID,VARVALUE1,VARVALUE2,VARVALUE6,VARVALUE13,VARVALUE14,VARVALUE15,VARVALUE16,VARVALUE17,VARVALUE18,INTVALUE2,VARVALUE40,TEXT1,ADDTIME)values(3,'','"+arg5+"','2','"+orderNo+"','"+arg4+"','"+arg6+"','"+arg7+"','"+arg1+"','"+userId+"','"+arg9+"','"+arg8+"','"+arg2+"','"+nowDate+"')";
				
				System.out.println(sql);
				
				int i = mdb.executeUpdateAutoCommit(sql);//调用事务更新sql
				if(i>0){
					
					int conId = 0;
					sql="select CONID from CLEAN where ChannelID=3 and VARVALUE17='"+arg1+"' and VARVALUE18='"+userId+"' order by CONID desc limit 1";
					ResultSet rs1 = mdb.executeQuery(sql);
		            if(rs1.next()){
		            	conId = rs1.getInt("CONID");
		            }
		            
					if(!arg12.equals("")){
						sql = "update ATTACHMENT set VARVALUE2='"+conId+"' where CONID in ("+arg12+")";
						System.out.println(sql);
						int j = mdb.executeUpdateAutoCommit(sql);
					}
					
					dbResult.setReturnResult("1");
					
				}else{
					dbResult.setReturnResult("0");
				}
			}catch(Exception e){
        		System.out.println("saveProblemAdd()" + e.getLocalizedMessage());
            }
            finally{
                mdb.close();
            }
			
			return dbResult;
			
		}
		
		if(funName.equals("saveComplainAdd")){
			
			String userId = curObject.getUserId();
			int id = StrEdit.StrToInt(curObject.getId());
			String arg1 = curObject.getArg1();
			String arg2 = curObject.getArg2();
			int arg3 = StrEdit.StrToInt(curObject.getArg3());
			String arg4 = curObject.getArg4();
			String arg5 = curObject.getArg5();
			String arg6 = curObject.getArg6();
			String arg7 = curObject.getArg7();
			String arg8 = curObject.getArg8();
			String arg9 = curObject.getArg9();
			String arg12 = curObject.getArg12();
			String nowDate = StrEdit.getSimDateForDB();
			String orderNo = StrEdit.getSN("2");
			
			try{
				
				sql="select INTVALUE5 from userconn where CONID="+userId+"";
				rs = mdb.executeQuery(sql);
	            if(rs.next()){
	            	arg9 = String.valueOf(rs.getInt("INTVALUE5"));//企业ID
	            }
	            rs.close();
	            
				sql = "insert into CLEAN(ChannelID,VARVALUE1,VARVALUE2,VARVALUE3,VARVALUE6,VARVALUE13,VARVALUE14,VARVALUE15,VARVALUE16,VARVALUE17,VARVALUE18,INTVALUE2,VARVALUE40,TEXT1,ADDTIME)values(4,'"+arg4+"','','"+arg5+"','2','"+orderNo+"','"+arg4+"','"+arg6+"','"+arg7+"','"+arg1+"','"+userId+"','"+arg3+"','"+arg8+"','"+arg2+"','"+nowDate+"')";
				
				System.out.println(sql);
				
				int i = mdb.executeUpdateAutoCommit(sql);//调用事务更新sql
				if(i>0){
					
					int conId = 0;
					sql="select CONID from CLEAN where ChannelID=4 and VARVALUE17='"+arg1+"' and VARVALUE18='"+userId+"' order by CONID desc limit 1";
					ResultSet rs1 = mdb.executeQuery(sql);
		            if(rs1.next()){
		            	conId = rs1.getInt("CONID");
		            }
		            
					if(!arg12.equals("")){
						sql = "update ATTACHMENT set VARVALUE2='"+conId+"' where CONID in ("+arg12+")";
						System.out.println(sql);
						int j = mdb.executeUpdateAutoCommit(sql);
					}
					
					dbResult.setReturnResult("1");
					
				}else{
					dbResult.setReturnResult("0");
				}
			}catch(Exception e){
        		System.out.println("saveComplainAdd()" + e.getLocalizedMessage());
            }
            finally{
                mdb.close();
            }
			
			return dbResult;
			
		}
		
		if(funName.equals("saveCleanAdd")){
			
			String userId = curObject.getUserId();
			int id = StrEdit.StrToInt(curObject.getId());
			String arg1 = curObject.getArg1();
			String arg2 = curObject.getArg2();
			String arg3 = curObject.getArg3();
			String arg4 = curObject.getArg4();
			String arg5 = curObject.getArg5();
			String arg6 = curObject.getArg6();
			String arg7 = curObject.getArg7();
			String arg8 = curObject.getArg8();
			String arg9 = curObject.getArg9();
			String nowDate = StrEdit.getSimDateForDB();
			int channelId = StrEdit.StrToInt(arg1);
			if(channelId==0){
				channelId = 1;
			}
			String orderNo = StrEdit.getSN("2");
			
			try{
				
				sql = "insert into CLEAN(ChannelID,VARVALUE1,VARVALUE2,VARVALUE3,VARVALUE4,VARVALUE6,VARVALUE13,VARVALUE40,INTVALUE2,TEXT1,ADDTIME,DATEVALUE1)values";
				sql = sql + "('"+channelId+"','"+arg2+"','"+arg3+"','"+arg4+"','"+arg5+"','2','"+orderNo+"','"+arg6+"','"+arg7+"','"+arg8+"','"+nowDate+"','"+arg9+"')";
				
				System.out.println(sql);
				
				int i = mdb.executeUpdateAutoCommit(sql);//调用事务更新sql
				if(i>0){
					
					dbResult.setReturnResult("1");
					
				}else{
					dbResult.setReturnResult("0");
				}
			}catch(Exception e){
        		System.out.println("saveCleanAdd()" + e.getLocalizedMessage());
            }
            finally{
                mdb.close();
            }
			
			return dbResult;
			
		}
		
		if(funName.equals("saveVisitorAdd")){
			
			String userId = curObject.getUserId();
			String userName = curObject.getUserName();
			int id = StrEdit.StrToInt(curObject.getId());
			String arg1 = curObject.getArg1();
			String arg2 = curObject.getArg2();
			String arg3 = curObject.getArg3();
			String arg4 = curObject.getArg4();
			String arg5 = curObject.getArg5();
			String arg6 = curObject.getArg6();
			String arg7 = curObject.getArg7();
			String arg8 = curObject.getArg8();
			String arg9 = curObject.getArg9();
			String arg10 = curObject.getArg10();
			String arg11 = curObject.getArg11();
			String arg12 = curObject.getArg12();
			String arg13 = curObject.getArg13();
			String arg14 = curObject.getArg14();
			String arg15 = curObject.getArg15();
			String arg16 = curObject.getArg16();
			String arg17 = curObject.getArg17();
			String arg18 = curObject.getArg18();
			String arg19 = curObject.getArg19();
			String nowDate = StrEdit.getSimDateForDB();
			arg17 = nowDate;
			int channelId = StrEdit.StrToInt(arg1);
			if(channelId==0){
				channelId = 1;
			}
			
			try{
				
				sql = "insert into VISITOR(ChannelID,VARVALUE1,VARVALUE2,VARVALUE3,VARVALUE4,VARVALUE5,VARVALUE6,VARVALUE7,VARVALUE21,VARVALUE22,VARVALUE25,VARVALUE26,VARVALUE27,VARVALUE39,VARVALUE40,INTVALUE2,TEXT1,ADDTIME,UPDATETIME,DATEVALUE1,DATEVALUE2,USERID,USERNAME,STATUS)values";
				sql = sql + "('"+channelId+"','"+arg2+"','"+arg3+"','"+arg4+"','"+arg5+"','"+arg6+"','"+arg7+"','"+arg8+"','"+arg9+"','"+arg10+"','"+arg11+"','"+arg12+"','"+arg13+"','1','"+arg14+"','"+arg15+"','"+arg16+"','"+arg17+"','"+nowDate+"','"+arg18+"','"+arg19+"','"+userId+"','"+userName+"',0)";
				
				System.out.println(sql);
				
				int i = mdb.executeUpdateAutoCommit(sql);//调用事务更新sql
				if(i>0){
					
					dbResult.setReturnResult("1");
					
				}else{
					dbResult.setReturnResult("0");
				}
			}catch(Exception e){
        		System.out.println("saveVisitorAdd()" + e.getLocalizedMessage());
            }
            finally{
                mdb.close();
            }
			
			return dbResult;
			
		}
		
		if(funName.equals("saveMeetingRoomApplyAdd")){
			
			String userId = curObject.getUserId();
			String userName = curObject.getUserName();
			int id = StrEdit.StrToInt(curObject.getId());
			String arg1 = curObject.getArg1();
			String arg2 = curObject.getArg2();
			String arg3 = curObject.getArg3();
			String arg4 = curObject.getArg4();
			String arg5 = curObject.getArg5();
			String arg6 = curObject.getArg6();
			String arg7 = curObject.getArg7();
			String arg8 = curObject.getArg8();
			String arg9 = curObject.getArg9();
			String arg10 = curObject.getArg10();
			String arg11 = curObject.getArg11();
			String arg12 = curObject.getArg12();
			String arg13 = curObject.getArg13();
			String arg14 = curObject.getArg14();
			String arg15 = curObject.getArg15();
			String arg16 = curObject.getArg16();
			String arg17 = curObject.getArg17();
			String arg18 = curObject.getArg18();
			String arg19 = curObject.getArg19();
			String arg20 = curObject.getArg20();
			String arg21 = curObject.getArg21();
			String arg22 = curObject.getArg22();
			String nowDate = StrEdit.getSimDateForDB();
			arg17 = nowDate;
			arg18 = nowDate;
			int channelId = StrEdit.StrToInt(arg1);
			if(channelId==0){
				channelId = 1;
			}
			String dateValue1 = arg19 + " " + arg20 + ":00";
            String dateValue2 = arg19 + " " + arg21 + ":00";
            String orderNo = StrEdit.getSN("1");
			
			try{
				
				sql="select * from MEETINGROOM where CONID="+id+"";
				rs = mdb.executeQuery(sql);
	            if(rs.next()){
	            	String hourminutes1 = rs.getString("VARVALUE11");
	            	String hourminutes2 = rs.getString("VARVALUE12");
	            	System.out.println(hourminutes1);
	            	System.out.println(arg20);
	            	if(!StrEdit.compareHM(arg20, hourminutes1, hourminutes2)){
	            		dbResult.setReturnResult("2");
	            		strTemp = "该时间段内不可用，可预定时间范围："+hourminutes1+"至"+hourminutes2;
	            		dbResult.setArg1(strTemp);
	            		return dbResult;
	        		}
	            	if(!StrEdit.compareHM(arg21, hourminutes1, hourminutes2)){
	            		dbResult.setReturnResult("2");
	            		strTemp = "该时间段内不可用，可预定时间范围："+hourminutes1+"至"+hourminutes2;
	            		dbResult.setArg1(strTemp);
	            		return dbResult;
	        		}
	            }
	            rs.close();
	            
	            if(StrEdit.compareDate(dateValue1, dateValue2)!=1){
	            	dbResult.setReturnResult("2");
            		strTemp = "开始时间必须比结束时间早";
            		dbResult.setArg1(strTemp);
            		return dbResult;
	            }
	            
	            Date mdateValue1 = StrEdit.getStrToDate(dateValue1, "yyyy-MM-dd HH:mm:ss");
            	Date mdateValue2 = StrEdit.getStrToDate(dateValue2, "yyyy-MM-dd HH:mm:ss");
            	System.out.println("dateValue1:"+dateValue1);
            	System.out.println("dateValue2:"+dateValue2);
            	sql="select DATEVALUE1,DATEVALUE2 from MEETING where (VARVALUE6='4' or VARVALUE6='5' or VARVALUE6='6') and VARVALUE2='"+id+"' order by DATEVALUE1 desc";
	            //sql="select DATEVALUE1,DATEVALUE2 from MEETING where (VARVALUE6='4' or VARVALUE6='5' or VARVALUE6='6') and find_in_set("+id+",VARVALUE2)>0 order by DATEVALUE1 desc";
	            System.out.println(sql);
	            rs = mdb.executeQuery(sql);
	            while(rs.next()){
	            	String DATEVALUE1 = rs.getString("DATEVALUE1");
	            	String DATEVALUE2 = rs.getString("DATEVALUE2");
	            	System.out.println("DATEVALUE1:"+DATEVALUE1);
	            	System.out.println("DATEVALUE2:"+DATEVALUE2);
	            	Date startDate = StrEdit.getStrToDate(DATEVALUE1, "yyyy-MM-dd HH:mm:ss");
	            	Date endDate = StrEdit.getStrToDate(DATEVALUE2, "yyyy-MM-dd HH:mm:ss");
	            	if(StrEdit.belongCalendarForStartDate(mdateValue1, startDate, endDate)){
	            		dbResult.setReturnResult("2");
	            		strTemp = "该时间段内已经被预定";
	            		System.out.println(strTemp);
	            		dbResult.setArg1(strTemp);
	            		return dbResult;
	        		}
	            	if(StrEdit.belongCalendarForEndDate(mdateValue2, startDate, endDate)){
	            		dbResult.setReturnResult("2");
	            		strTemp = "该时间段内已经被预定啦";
	            		System.out.println(strTemp);
	            		dbResult.setArg1(strTemp);
	            		return dbResult;
	        		}
	            }
	            rs.close();
	            
	            sql="select count(*) as total from MEETING where (VARVALUE6='4' or VARVALUE6='5' or VARVALUE6='6') and VARVALUE2='"+id+"' and (('"+dateValue1+"'>=DATEVALUE1 and '"+dateValue1+"'<DATEVALUE2) or ('"+dateValue2+"'>DATEVALUE1 and '"+dateValue2+"'<=DATEVALUE2) or ('"+dateValue1+"'<=DATEVALUE1 and '"+dateValue2+"'>=DATEVALUE2))";
	            //sql="select count(*) as total from MEETING where (VARVALUE6='4' or VARVALUE6='5' or VARVALUE6='6') and find_in_set("+id+",VARVALUE2)>0 and ((DATEVALUE1 >= '"+dateValue1+"' and DATEVALUE1 < '"+dateValue2+"') or (DATEVALUE2 > '"+dateValue1+"' and DATEVALUE2 <= '"+dateValue2+"'))";
	            System.out.println(sql);
	            rs = mdb.executeQuery(sql);
	            if(rs.next()){
	            	int total = rs.getInt("total");
	            	if(total>0){
	            		dbResult.setReturnResult("2");
	            		strTemp = "该时间段内已经被预定";
	            		dbResult.setArg1(strTemp);
	            		return dbResult;
	        		}
	            }
	            rs.close();
	            
				sql = "insert into MEETING(ChannelID,VARVALUE1,VARVALUE2,VARVALUE3,VARVALUE4,VARVALUE5,VARVALUE6,VARVALUE7,VARVALUE8,VARVALUE9,VARVALUE10,VARVALUE12,VARVALUE40,DECVALUE1,DECVALUE2,INTVALUE2,TEXT1,ADDTIME,UPDATETIME,DATEVALUE1,DATEVALUE2,USERID,USERNAME,STATUS,VARVALUE11)values";
				sql = sql + "('"+channelId+"','"+arg2+"','"+arg3+"','"+arg4+"','"+arg5+"','"+arg6+"','"+arg7+"','"+arg8+"','"+arg9+"','"+arg10+"','"+arg11+"','"+orderNo+"','"+arg12+"','"+arg13+"','"+arg14+"','"+arg15+"','"+arg16+"','"+arg17+"','"+arg18+"','"+dateValue1+"','"+dateValue2+"','"+userId+"','"+userName+"',0,'"+arg22+"')";
				System.out.println(sql);
				int i = mdb.executeUpdateAutoCommit(sql);//调用事务更新sql
				if(i>0){
					
					sql="select CONID,VARVALUE1,DECVALUE2 from MEETING where VARVALUE2='"+arg3+"' and ADDTIME='"+nowDate+"' order by CONID desc limit 1";
					ResultSet rs1 = mdb.executeQuery(sql);
		            if(rs1.next()){
		            	int conId = rs1.getInt("CONID");
		            	String name = rs1.getString("VARVALUE1");
		            	String money = rs1.getString("DECVALUE2");
		            	dbResult.setArg1(String.valueOf(conId));
		            	dbResult.setArg2(name);
		            	dbResult.setArg3(money);
		            	dbResult.setReturnResult("1");
		            }else{
		            	dbResult.setReturnResult("0");
		            }
		            rs1.close();
				}else{
					dbResult.setReturnResult("0");
				}
			}catch(Exception e){
        		System.out.println("saveMeetingRoomApplyAdd()" + e.getLocalizedMessage());
            }
            finally{
                mdb.close();
            }
			
			return dbResult;
			
		}
		
		if(funName.equals("saveSpaceApplyAdd")){
			
			String userId = curObject.getUserId();
			String userName = curObject.getUserName();
			int id = StrEdit.StrToInt(curObject.getId());
			String arg1 = curObject.getArg1();
			String arg2 = curObject.getArg2();
			String arg3 = curObject.getArg3();
			String arg4 = curObject.getArg4();
			String arg5 = curObject.getArg5();
			String arg6 = curObject.getArg6();
			String arg7 = curObject.getArg7();
			String arg8 = curObject.getArg8();
			String arg9 = curObject.getArg9();
			String arg10 = curObject.getArg10();
			String arg11 = curObject.getArg11();
			String arg12 = curObject.getArg12();
			String arg13 = curObject.getArg13();
			String arg14 = curObject.getArg14();
			String arg15 = curObject.getArg15();
			String arg16 = curObject.getArg16();
			String arg17 = curObject.getArg17();
			String arg18 = curObject.getArg18();
			String arg19 = curObject.getArg19();
			String arg20 = curObject.getArg20();
			String arg21 = curObject.getArg21();
			String nowDate = StrEdit.getSimDateForDB();
			arg17 = nowDate;
			arg18 = nowDate;
			int channelId = StrEdit.StrToInt(arg1);
			if(channelId==0){
				channelId = 1;
			}
			String dateValue1 = arg19 + " " + arg20 + ":00";
            String dateValue2 = arg19 + " " + arg21 + ":00";
            //System.out.println(dateValue1);
            //System.out.println(dateValue2);
			
			try{
				
				sql="select * from SPACE where CONID="+id+"";
				rs = mdb.executeQuery(sql);
	            if(rs.next()){
	            	String hourminutes1 = rs.getString("VARVALUE11");
	            	String hourminutes2 = rs.getString("VARVALUE12");
	            	System.out.println(hourminutes1);
	            	System.out.println(arg20);
	            	if(!StrEdit.compareHM(arg20, hourminutes1, hourminutes2)){
	            		dbResult.setReturnResult("2");
	            		strTemp = "该时间段内不可用，可预定时间范围："+hourminutes1+"至"+hourminutes2;
	            		dbResult.setArg1(strTemp);
	            		return dbResult;
	        		}
	            	if(!StrEdit.compareHM(arg21, hourminutes1, hourminutes2)){
	            		dbResult.setReturnResult("2");
	            		strTemp = "该时间段内不可用，可预定时间范围："+hourminutes1+"至"+hourminutes2;
	            		dbResult.setArg1(strTemp);
	            		return dbResult;
	        		}
	            }
	            rs.close();
	            
	            if(StrEdit.compareDate(dateValue1, dateValue2)!=1){
	            	dbResult.setReturnResult("2");
            		strTemp = "开始时间必须比结束时间早";
            		dbResult.setArg1(strTemp);
            		return dbResult;
	            }
	            
	            sql="select count(*) as total from SPACEAPPLY where VARVALUE6='4' and find_in_set("+id+",VARVALUE2)>0 and ((DATEVALUE1 >= '"+dateValue1+"' and DATEVALUE1 < '"+dateValue2+"') or (DATEVALUE2 > '"+dateValue1+"' and DATEVALUE2 <= '"+dateValue2+"'))";
	            rs = mdb.executeQuery(sql);
	            if(rs.next()){
	            	int total = rs.getInt("total");
	            	if(total>0){
	            		dbResult.setReturnResult("2");
	            		strTemp = "该时间段内不可用";
	            		dbResult.setArg1(strTemp);
	            		return dbResult;
	        		}
	            }
	            rs.close();
	            
				sql = "insert into SPACEAPPLY(ChannelID,VARVALUE1,VARVALUE2,VARVALUE3,VARVALUE4,VARVALUE5,VARVALUE6,VARVALUE7,VARVALUE8,VARVALUE9,VARVALUE10,VARVALUE40,DECVALUE1,DECVALUE2,INTVALUE2,TEXT1,ADDTIME,UPDATETIME,DATEVALUE1,DATEVALUE2,USERID,USERNAME,STATUS)values";
				sql = sql + "('"+channelId+"','"+arg2+"','"+arg3+"','"+arg4+"','"+arg5+"','"+arg6+"','"+arg7+"','"+arg8+"','"+arg9+"','"+arg10+"','"+arg11+"','"+arg12+"','"+arg13+"','"+arg14+"','"+arg15+"','"+arg16+"','"+arg17+"','"+arg18+"','"+dateValue1+"','"+dateValue2+"','"+userId+"','"+userName+"',0)";
				
				System.out.println(sql);
				
				int i = mdb.executeUpdateAutoCommit(sql);//调用事务更新sql
				if(i>0){
					
					sql="select CONID,VARVALUE1,DECVALUE2 from SPACEAPPLY where VARVALUE2='"+arg3+"' and ADDTIME='"+nowDate+"' order by CONID desc limit 1";
					ResultSet rs1 = mdb.executeQuery(sql);
		            if(rs1.next()){
		            	int conId = rs1.getInt("CONID");
		            	String name = rs1.getString("VARVALUE1");
		            	String money = rs1.getString("DECVALUE2");
		            	dbResult.setArg1(String.valueOf(conId));
		            	dbResult.setArg2(name);
		            	dbResult.setArg3(money);
		            	dbResult.setReturnResult("1");
		            }else{
		            	dbResult.setReturnResult("0");
		            }
		            rs1.close();
				}else{
					dbResult.setReturnResult("0");
				}
			}catch(Exception e){
        		System.out.println("saveSpaceApplyAdd()" + e.getLocalizedMessage());
            }
            finally{
                mdb.close();
            }
			
			return dbResult;
			
		}
		
		if(funName.equals("getNewsDetail")){
			
			String id = curObject.getId();
			String userId = curObject.getUserId();
			
			sql="select * from NEWS where CONID="+id+"";
			try{
	            rs = mdb.executeQuery(sql);
	            if(rs.next()){
	            	String picture = rs.getString("VARVALUE38");
	            	if(!picture.equals("")){
	            		picture = weburl + picture;
	            	}
	            	dbResult.setId(id);
	            	dbResult.setArg1(rs.getString("VARVALUE1"));
	            	dbResult.setArg2(rs.getString("VARVALUE22"));
	            	dbResult.setArg3(rs.getString("TEXT1"));
	            	dbResult.setArg4(picture);
	            	dbResult.setArg5(StrEdit.StringLeft(rs.getString("ADDTIME"), 19));
	            	dbResult.setReturnResult("1");
	            }
	        }catch(Exception e){
	            System.out.println("getNewsDetail()" +e.getLocalizedMessage());
	        }
	        finally{
	            mdb.close();
	        }
			
			return dbResult;
			
		}
		
		if(funName.equals("savePlan")){
			
			int planId = StrEdit.StrToInt(curObject.getId());
			String userId = curObject.getUserId();
			String title = curObject.getArg1();
			String categoryId = curObject.getArg2();
			String street = curObject.getArg3();
			String wangge = curObject.getArg4();
			String wanggeuser = curObject.getArg5();
			String checkuserId = curObject.getArg6();
			String checkuserName = curObject.getArg7();
			String startdate = curObject.getArg8();
			String enddate = curObject.getArg9();
			String content = curObject.getArg10();
			String shopId = curObject.getArg11();
			String nowDate = StrEdit.getSimDateForDB();
			startdate = startdate+" 00:00:00";
			enddate = enddate+" 23:59:59";
			
			int INTVALUE4 = 2;
			int i = 0;
			
			boolean boolValue = false;
			Connection conn = null;
			Statement state = null;
			conn = mdb.getNewConnection();
			conn.setAutoCommit(false);
			state = conn.createStatement();
			
	        if(planId>0){
	        	try{
	        		sql = "update activeconn set VARVALUE1 = '"+street+"',VARVALUE2 = '"+wangge+"',VARVALUE5 = '"+wanggeuser+"',VARVALUE6 = '"+checkuserId+"',VARVALUE8 = '"+checkuserName+"',";
		        	sql = sql + "VARVALUE9 = '"+title+"',INTVALUE1 = "+categoryId+",TEXT1 = '"+content+"',UPDATETIME = '"+nowDate+"',DATEVALUE1 = '"+startdate+"',DATEVALUE2 = '"+enddate+"' where CONID="+planId;
		        	System.out.println(sql);
		        	state.execute(sql);
		        	conn.commit();  //数据库操作最终提交给数据库
					conn.setAutoCommit(true);
					boolValue = true;
		        	/*
		    		i = mdb.executeUpdate(sql);
		    		System.out.println("更新计划 i "+i);
		    		if(i>0){
		    			dbResult.setReturnResult("1");
		    			System.out.println("更新计划成功"+dbResult.getReturnResult());//不知道为什么只要更新成功后第二次输入错误也会显示成功，所以使用事务
		    		}else{
		    			dbResult.setReturnResult("0");
		    			System.out.println("更新计划失败"+dbResult.getReturnResult());
		    		}
		    		*/
	        	}catch(Exception e){
	        		boolValue = false;
	        		conn.rollback();
	        		System.out.println("更新计划异常" + e.getMessage());
	            }
	            finally{
	            	if(state!=null){state.close();}
	            	if(conn!=null){conn.close();}
	            }
	        	
	        }else{
	        	
	        	int intValue = 0;
	        	ResultSet rs = null;
		        try{
		            sql = "select max(INTVALUE7) as sid from activeconn where ChannelID="+shopId+"";
		            rs = mdb.executeQuery(sql);
		            if(rs.next()){
		            	intValue = rs.getInt("sid") + 1;
		            }
		            rs.close();
		        }
		        catch(Exception e){
		        	System.out.println("savePlan()" + e.getLocalizedMessage());
		        }
		        
	        	try{
	        		sql = "insert into activeconn(ChannelID,VARVALUE1,VARVALUE2,VARVALUE5,VARVALUE6,VARVALUE8,VARVALUE9,INTVALUE1,INTVALUE4,INTVALUE7,TEXT1,ADDTIME,UPDATETIME,DATEVALUE1,DATEVALUE2,DATEVALUE3,DATEVALUE4,DATEVALUE5,DATEVALUE6,USERID)values";
					sql = sql + "('"+shopId+"','"+street+"','"+wangge+"','"+wanggeuser+"','"+checkuserId+"','"+checkuserName+"','"+title+"',"+categoryId+","+INTVALUE4+","+intValue+",'"+content+"','"+nowDate+"','"+nowDate+"','"+startdate+"','"+enddate+"','"+nowDate+"','"+nowDate+"','"+nowDate+"','"+nowDate+"',"+userId+")";
					System.out.println(sql);
					state.execute(sql);
		        	conn.commit();  //数据库操作最终提交给数据库
					conn.setAutoCommit(true);
					boolValue = true;
					/*
					i = mdb.executeUpdate(sql);
					if(i>0){
						dbResult.setReturnResult("1");
					}else{
						dbResult.setReturnResult("0");
					}
					*/
	        	}catch(Exception e){
	        		boolValue = false;
	        		conn.rollback();
	        		System.out.println("新增计划异常" + e.getMessage());
	            }
	            finally{
	                mdb.close();
	            	if(state!=null){state.close();}
	            	if(conn!=null){conn.close();}
	            }
	        }
	        
	        if(boolValue){
	        	dbResult.setReturnResult("1");
	        }else{
	        	dbResult.setReturnResult("0");
	        }
	        
			return dbResult;
			
		}
		
		if(funName.equals("startTask")){
			
			int id = StrEdit.StrToInt(curObject.getId());
			int userId = StrEdit.StrToInt(curObject.getUserId());
			String arg1 = curObject.getArg1();
			
			try{
				sql = "update activeconn set INTVALUE4="+arg1+" where CONID="+id+"";
				System.out.println(sql);
				int i = mdb.executeUpdate(sql);
				if(i>0){
					dbResult.setId(String.valueOf(id));
					dbResult.setReturnResult("1");
				}else{
					dbResult.setReturnResult("0");
				}
			}catch(Exception e){
        		dbResult.setReturnResult("0");
        		System.out.println("startTask()" + e.getLocalizedMessage());
            }
            finally{
                mdb.close();
            }
			
			return dbResult;
			
		}
		
		if(funName.equals("delFeedback")){
			
			int id = StrEdit.StrToInt(curObject.getId());
			int userId = StrEdit.StrToInt(curObject.getUserId());
			String arg1 = curObject.getArg1();
			
			try{
				sql = "delete from feedbackconn where CONID="+id+"";
				System.out.println(sql);
				int i = mdb.executeUpdate(sql);
				if(i>0){
					dbResult.setReturnResult("1");
				}else{
					dbResult.setReturnResult("0");
				}
			}catch(Exception e){
        		dbResult.setReturnResult("0");
        		System.out.println("delFeedback()" + e.getLocalizedMessage());
            }
            finally{
                mdb.close();
            }
			
			return dbResult;
			
		}
		
		if(funName.equals("getContactDetail")){
			
			String id = curObject.getId();
			String userId = curObject.getUserId();
			
			sql="select * from userconn where CONID="+id+"";
			try{
	            rs = mdb.executeQuery(sql);
	            if(rs.next()){
	            	String picture = rs.getString("VARVALUE38");
	            	if(!picture.equals("")){
	            		picture = weburl + picture;
	            	}
	            	dbResult.setId(id);
	            	dbResult.setArg1(picture);
	            	dbResult.setArg2(rs.getString("VARVALUE3"));
	            	dbResult.setArg3(rs.getString("VARVALUE8"));
	            	dbResult.setArg4(rs.getString("VARVALUE14"));
	            	dbResult.setArg5(rs.getString("VARVALUE9"));
	            	dbResult.setArg6(rs.getString("VARVALUE10"));
	            	dbResult.setArg7(rs.getString("VARVALUE11"));
	            	dbResult.setArg8(rs.getString("VARVALUE18"));
	            	dbResult.setReturnResult("1");
	            }
	        }catch(Exception e){
	            System.out.println("getContactDetail()" +e.getLocalizedMessage());
	        }
	        finally{
	            mdb.close();
	        }
			
			return dbResult;
			
		}
		
		if(funName.equals("getMeetingRoomDetail")){
			
			String id = curObject.getId();
			String userId = curObject.getUserId();
			StringBuffer deviceList = new StringBuffer("");
			
			sql = "select VARVALUE2 as varValue2,VARVALUE3 as varValue3,sum(INTVALUE4) as intValue4 from STORAGE where ChannelID=5 and VARVALUE22='"+id+"' and VARVALUE23='1' and VARVALUE39='1' group by VARVALUE2 order by intValue4 desc";
			ResultSet rs1 = mdb.executeQuery(sql);
            while(rs1.next()){
            	String name = rs1.getString("varValue2");
            	String unite = rs1.getString("varValue3");
            	int quantity = rs1.getInt("intValue4");
            	deviceList.append(name + "" + quantity + "" + unite);
            	deviceList.append("\n");
            }
            rs1.close();
            
			sql="select * from MEETINGROOM where CONID="+id+"";
			try{
	            rs = mdb.executeQuery(sql);
	            if(rs.next()){
	            	String picture = rs.getString("VARVALUE38");
	            	if(!picture.equals("")){
	            		picture = weburl + picture;
	            	}
	            	dbResult.setId(id);
	            	dbResult.setArg1(rs.getString("VARVALUE1"));
	            	dbResult.setArg2(rs.getString("VARVALUE2"));
	            	dbResult.setArg3(rs.getString("VARVALUE3"));
	            	dbResult.setArg4(rs.getString("VARVALUE4"));
	            	dbResult.setArg5(rs.getString("VARVALUE5"));
	            	dbResult.setArg6(rs.getString("VARVALUE6"));
	            	dbResult.setArg7(rs.getString("VARVALUE7"));
	            	dbResult.setArg8(rs.getString("VARVALUE8"));
	            	dbResult.setArg9(rs.getString("VARVALUE9"));
	            	dbResult.setArg10(rs.getString("VARVALUE10"));
	            	dbResult.setArg11(rs.getString("VARVALUE11"));
	            	dbResult.setArg12(rs.getString("VARVALUE12"));
	            	dbResult.setArg38(picture);
	            	dbResult.setArg39(rs.getString("VARVALUE39"));
	            	dbResult.setArg40(rs.getString("VARVALUE40"));
	            	dbResult.setArg41(rs.getString("DECVALUE1"));
	            	dbResult.setArg42(rs.getString("DECVALUE2"));
	            	dbResult.setArg43(rs.getString("TEXT1"));
	            	dbResult.setArg44(StrEdit.StringLeft(rs.getString("ADDTIME"), 19));
	            	dbResult.setArg45(StrEdit.StringLeft(rs.getString("DATEVALUE1"), 19));
	            	dbResult.setArg46(StrEdit.StringLeft(rs.getString("DATEVALUE2"), 19));
	            	dbResult.setArg50(deviceList.toString());
	            	dbResult.setReturnResult("1");
	            }
	        }catch(Exception e){
	            System.out.println("getMeetingRoomDetail()" +e.getLocalizedMessage());
	        }
	        finally{
	            mdb.close();
	        }
			
			return dbResult;
			
		}
		
		if(funName.equals("getPreOrderData")){
			int i = 0;
			String id = curObject.getId();
			String userId = curObject.getUserId();
			int channelId = StrEdit.StrToInt(curObject.getArg1());
			String arg2 = curObject.getArg2();
			String startDate = arg2 + " 00:00:00";
			String endDate = arg2 + " 23:59:59";
			StringBuffer deviceList = new StringBuffer("");
			try{
				sql = "select DATEVALUE1,DATEVALUE2 from MEETING where ChannelID="+channelId+" and VARVALUE2='"+id+"' and (VARVALUE6='4' or VARVALUE6='5' or VARVALUE6='6') and (DATEVALUE1>='"+startDate+"' and DATEVALUE2<='"+endDate+"') order by DATEVALUE1 asc";
				System.out.println(sql);
				//log.info("输出日志 msg:{}", sql);
				rs = mdb.executeQuery(sql);
	            while(rs.next()){
	            	i++;
	            	String dateValue1 = rs.getString("DATEVALUE1");
	            	String dateValue2 = rs.getString("DATEVALUE2");
	            	dateValue1 = dateValue1.substring(11, 16);
	            	dateValue2 = dateValue2.substring(11, 16);
	            	deviceList.append(dateValue1 + " - " + dateValue2 + " 已预定");
	            	deviceList.append("\n");
	            }
	            if(i>0){
	            	//System.out.println(deviceList.toString());
	            	dbResult.setArg1(deviceList.toString());
	            	dbResult.setReturnResult("1");
	            }else{
	            	dbResult.setReturnResult("0");
	            }
	            
	        }catch(Exception e){
	            System.out.println("getPreOrderData()" +e.getLocalizedMessage());
	        }finally{
	        	rs.close();
	            mdb.close();
	        }
			
			return dbResult;
			
		}
		
		if(funName.equals("getCleanDetail")){
			
			String id = curObject.getId();
			String userId = curObject.getUserId();
			/*StringBuffer deviceList = new StringBuffer("");
			
			sql = "select VARVALUE2 as varValue2,VARVALUE3 as varValue3,sum(INTVALUE4) as intValue4 from STORAGE where ChannelID=5 and VARVALUE22='"+id+"' and VARVALUE23='1' and VARVALUE39='1' group by VARVALUE2 order by intValue4 desc";
			ResultSet rs1 = mdb.executeQuery(sql);
            while(rs1.next()){
            	String name = rs1.getString("varValue2");
            	String unite = rs1.getString("varValue3");
            	int quantity = rs1.getInt("intValue4");
            	deviceList.append(name + "" + quantity + "" + unite);
            	deviceList.append("\n");
            }
            rs1.close();*/
            
			sql="select * from CLEAN where CONID="+id+"";
			try{
	            rs = mdb.executeQuery(sql);
	            if(rs.next()){
	            	String picture = rs.getString("VARVALUE38");
	            	if(!picture.equals("")){
	            		picture = weburl + picture;
	            	}
	            	dbResult.setId(id);
	            	dbResult.setArg1(rs.getString("VARVALUE1"));
	            	dbResult.setArg2(rs.getString("VARVALUE2"));
	            	dbResult.setArg3(rs.getString("VARVALUE3"));
	            	dbResult.setArg4(rs.getString("VARVALUE4"));
	            	dbResult.setArg5(rs.getString("VARVALUE5"));
	            	dbResult.setArg6(rs.getString("VARVALUE6"));
	            	dbResult.setArg7(rs.getString("VARVALUE7"));
	            	dbResult.setArg8(rs.getString("VARVALUE8"));
	            	dbResult.setArg9(rs.getString("VARVALUE9"));
	            	dbResult.setArg10(rs.getString("VARVALUE10"));
	            	dbResult.setArg11(rs.getString("VARVALUE11"));
	            	dbResult.setArg12(rs.getString("VARVALUE12"));
	            	dbResult.setArg13(rs.getString("VARVALUE13"));
	            	dbResult.setArg14(rs.getString("VARVALUE14"));
	            	dbResult.setArg15(rs.getString("VARVALUE15"));
	            	dbResult.setArg16(rs.getString("VARVALUE16"));
	            	dbResult.setArg17(rs.getString("VARVALUE17"));
	            	dbResult.setArg18(rs.getString("VARVALUE18"));
	            	dbResult.setArg19(rs.getString("VARVALUE19"));
	            	dbResult.setArg20(rs.getString("VARVALUE20"));
	            	dbResult.setArg38(picture);
	            	dbResult.setArg39(rs.getString("VARVALUE39"));
	            	dbResult.setArg40(rs.getString("VARVALUE40"));
	            	dbResult.setArg41(rs.getString("DECVALUE1"));
	            	dbResult.setArg42(rs.getString("DECVALUE2"));
	            	dbResult.setArg43(rs.getString("TEXT1"));
	            	dbResult.setArg44(StrEdit.StringLeft(rs.getString("ADDTIME"), 19));
	            	dbResult.setArg45(StrEdit.StringLeft(rs.getString("DATEVALUE1"), 19));
	            	dbResult.setArg46(StrEdit.StringLeft(rs.getString("DATEVALUE2"), 19));
	            	//dbResult.setArg50(deviceList.toString());
	            	dbResult.setReturnResult("1");
	            }
	        }catch(Exception e){
	            System.out.println("getCleanDetail()" +e.getLocalizedMessage());
	        }
	        finally{
	            mdb.close();
	        }
			
			return dbResult;
			
		}
		
		if(funName.equals("getMeetingDetail")){
			
			String id = curObject.getId();
			String userId = curObject.getUserId();
			/*StringBuffer deviceList = new StringBuffer("");
			
			sql = "select VARVALUE2 as varValue2,VARVALUE3 as varValue3,sum(INTVALUE4) as intValue4 from STORAGE where ChannelID=5 and VARVALUE22='"+id+"' and VARVALUE23='1' and VARVALUE39='1' group by VARVALUE2 order by intValue4 desc";
			ResultSet rs1 = mdb.executeQuery(sql);
            while(rs1.next()){
            	String name = rs1.getString("varValue2");
            	String unite = rs1.getString("varValue3");
            	int quantity = rs1.getInt("intValue4");
            	deviceList.append(name + "" + quantity + "" + unite);
            	deviceList.append("\n");
            }
            rs1.close();*/
            
			sql="select * from MEETING where CONID="+id+"";
			try{
	            rs = mdb.executeQuery(sql);
	            if(rs.next()){
	            	String picture = rs.getString("VARVALUE38");
	            	if(!picture.equals("")){
	            		picture = weburl + picture;
	            	}
	            	dbResult.setId(id);
	            	dbResult.setArg1(rs.getString("VARVALUE1"));
	            	dbResult.setArg2(rs.getString("VARVALUE2"));
	            	dbResult.setArg3(rs.getString("VARVALUE3"));
	            	dbResult.setArg4(rs.getString("VARVALUE4"));
	            	dbResult.setArg5(rs.getString("VARVALUE5"));
	            	dbResult.setArg6(rs.getString("VARVALUE6"));
	            	dbResult.setArg7(rs.getString("VARVALUE7"));
	            	dbResult.setArg8(rs.getString("VARVALUE8"));
	            	dbResult.setArg9(rs.getString("VARVALUE9"));
	            	dbResult.setArg10(rs.getString("VARVALUE10"));
	            	dbResult.setArg11(rs.getString("VARVALUE11"));
	            	dbResult.setArg12(rs.getString("VARVALUE12"));
	            	dbResult.setArg13(rs.getString("VARVALUE13"));
	            	dbResult.setArg14(rs.getString("VARVALUE14"));
	            	dbResult.setArg15(rs.getString("VARVALUE15"));
	            	dbResult.setArg16(rs.getString("VARVALUE16"));
	            	dbResult.setArg17(rs.getString("VARVALUE17"));
	            	dbResult.setArg18(rs.getString("VARVALUE18"));
	            	dbResult.setArg19(rs.getString("VARVALUE19"));
	            	dbResult.setArg20(rs.getString("VARVALUE20"));
	            	dbResult.setArg21(rs.getString("VARVALUE21"));
	            	dbResult.setArg22(rs.getString("VARVALUE22"));
	            	dbResult.setArg23(rs.getString("VARVALUE23"));
	            	dbResult.setArg24(rs.getString("VARVALUE24"));
	            	dbResult.setArg25(rs.getString("VARVALUE25"));
	            	dbResult.setArg26(rs.getString("VARVALUE26"));
	            	dbResult.setArg27(rs.getString("VARVALUE27"));
	            	dbResult.setArg28(rs.getString("VARVALUE28"));
	            	dbResult.setArg29(rs.getString("VARVALUE29"));
	            	dbResult.setArg30(rs.getString("VARVALUE30"));	
	            	dbResult.setArg38(picture);
	            	dbResult.setArg39(rs.getString("VARVALUE39"));
	            	dbResult.setArg40(rs.getString("VARVALUE40"));
	            	dbResult.setArg41(rs.getString("DECVALUE1"));
	            	dbResult.setArg42(rs.getString("DECVALUE2"));
	            	dbResult.setArg43(rs.getString("TEXT1"));
	            	dbResult.setArg44(StrEdit.StringLeft(rs.getString("ADDTIME"), 19));
	            	dbResult.setArg45(StrEdit.StringLeft(rs.getString("DATEVALUE1"), 16));
	            	dbResult.setArg46(StrEdit.StringLeft(rs.getString("DATEVALUE2"), 16));
	            	//dbResult.setArg50(deviceList.toString());
	            	dbResult.setReturnResult("1");
	            }
	        }catch(Exception e){
	            System.out.println("getMeetingDetail()" +e.getLocalizedMessage());
	        }
	        finally{
	            mdb.close();
	        }
			
			return dbResult;
			
		}

		if(funName.equals("getVisitorDetail")){
			
			String id = curObject.getId();
			String userId = curObject.getUserId();
            
			sql="select * from VISITOR where CONID="+id+"";
			try{
	            rs = mdb.executeQuery(sql);
	            if(rs.next()){
	            	String picture = rs.getString("VARVALUE38");
	            	if(!picture.equals("")){
	            		picture = weburl + picture;
	            	}
	            	dbResult.setId(id);
	            	dbResult.setArg1(rs.getString("VARVALUE1"));
	            	dbResult.setArg2(rs.getString("VARVALUE2"));
	            	dbResult.setArg3(rs.getString("VARVALUE3"));
	            	dbResult.setArg4(rs.getString("VARVALUE4"));
	            	dbResult.setArg5(rs.getString("VARVALUE5"));
	            	dbResult.setArg6(rs.getString("VARVALUE6"));
	            	dbResult.setArg7(rs.getString("VARVALUE7"));
	            	dbResult.setArg8(rs.getString("VARVALUE8"));
	            	dbResult.setArg9(rs.getString("VARVALUE9"));
	            	dbResult.setArg10(rs.getString("VARVALUE10"));
	            	dbResult.setArg11(rs.getString("VARVALUE11"));
	            	dbResult.setArg12(rs.getString("VARVALUE12"));
	            	dbResult.setArg13(rs.getString("VARVALUE13"));
	            	dbResult.setArg14(rs.getString("VARVALUE14"));
	            	dbResult.setArg15(rs.getString("VARVALUE15"));
	            	dbResult.setArg16(rs.getString("VARVALUE16"));
	            	dbResult.setArg17(rs.getString("VARVALUE17"));
	            	dbResult.setArg18(rs.getString("VARVALUE18"));
	            	dbResult.setArg19(rs.getString("VARVALUE19"));
	            	dbResult.setArg20(rs.getString("VARVALUE20"));	
	            	dbResult.setArg21(rs.getString("VARVALUE21"));
	            	dbResult.setArg22(rs.getString("VARVALUE22"));
	            	dbResult.setArg23(rs.getString("VARVALUE23"));
	            	dbResult.setArg24(rs.getString("VARVALUE24"));
	            	dbResult.setArg25(rs.getString("VARVALUE25"));
	            	dbResult.setArg26(rs.getString("VARVALUE26"));
	            	dbResult.setArg27(rs.getString("VARVALUE27"));
	            	dbResult.setArg28(rs.getString("VARVALUE28"));
	            	dbResult.setArg29(rs.getString("VARVALUE29"));
	            	dbResult.setArg30(rs.getString("VARVALUE30"));
	            	dbResult.setArg38(picture);
	            	dbResult.setArg39(rs.getString("VARVALUE39"));
	            	dbResult.setArg40(rs.getString("VARVALUE40"));
	            	dbResult.setArg41(rs.getString("DECVALUE1"));
	            	dbResult.setArg42(rs.getString("DECVALUE2"));
	            	dbResult.setArg43(rs.getString("TEXT1"));
	            	dbResult.setArg44(StrEdit.StringLeft(rs.getString("ADDTIME"), 16));
	            	dbResult.setArg45(StrEdit.StringLeft(rs.getString("DATEVALUE1"), 16));
	            	dbResult.setArg46(StrEdit.StringLeft(rs.getString("DATEVALUE2"), 16));
	            	//dbResult.setArg50(deviceList.toString());
	            	dbResult.setReturnResult("1");
	            }
	        }catch(Exception e){
	            System.out.println("getVisitorDetail()" +e.getLocalizedMessage());
	        }
	        finally{
	            mdb.close();
	        }
			
			return dbResult;
			
		}
		
		if(funName.equals("getFeedbackDetail")){
			
			String id = curObject.getId();
			String userId = curObject.getUserId();
			
			sql="select * from feedbackconn where CONID="+id+"";
			try{
	            rs = mdb.executeQuery(sql);
	            if(rs.next()){
	            	dbResult.setId(String.valueOf(rs.getInt("CONID")));
	            	dbResult.setArg1(rs.getString("VARVALUE21"));
	            	dbResult.setArg2(rs.getString("VARVALUE22"));
	            	dbResult.setArg3(rs.getString("VARVALUE23"));
	            	dbResult.setArg4(rs.getString("VARVALUE24"));
	            	dbResult.setArg5(rs.getString("VARVALUE25"));
	            	dbResult.setArg6(rs.getString("VARVALUE26"));
	            	dbResult.setArg7(rs.getString("VARVALUE27"));
	            	dbResult.setArg8(rs.getString("VARVALUE28"));
	            	dbResult.setArg9(rs.getString("VARVALUE29"));
	            	dbResult.setArg10(rs.getString("VARVALUE30"));
	            	dbResult.setArg11(rs.getString("VARVALUE5"));
					dbResult.setArg12(rs.getString("VARVALUE1"));
					dbResult.setArg13(rs.getString("VARVALUE2"));
					dbResult.setArg14(rs.getString("DATEVALUE1"));
					dbResult.setArg15(rs.getString("DATEVALUE2"));
	            	dbResult.setReturnResult("1");
	            }
	        }catch(Exception e){
	            System.out.println("getFeedbackDetail()" +e.getLocalizedMessage());
	        }
	        finally{
	            mdb.close();
	        }
			
			return dbResult;
			
		}
		
		if(funName.equals("loginById")){
			
			int userId = StrEdit.StrToInt(curObject.getUserId());
			
			if(userId>0){
				
				try{
					sql = "update userconn set LastLoginTime = LoginTime where CONID=" + userId + "";
					mdb.executeUpdate(sql);
					System.out.println(sql);
					
					sql = "update userconn set LoginTime='" + StrEdit.getSimDateForDB() + "',LoginTimes = LoginTimes + 1 where CONID=" + userId + "";
					int i = mdb.executeUpdate(sql);
					if(i>0){
						dbResult.setReturnResult("1");
					}else{
						dbResult.setReturnResult("0");
					}
				}catch(Exception e){
	        		dbResult.setReturnResult("0");
	        		System.out.println("delPlan()" + e.getLocalizedMessage());
	            }
	            finally{
	                mdb.close();
	            }
				
				return dbResult;
				
			}
			
		}
		
		
		if(funName.equals("")){
			
		}
		
		return null;
	}
	
	/*
	 * 用来接收数据结果集合
	 */
	@Override
	public List<SysPassValue> sendOrderToServerForList(SysPassValue curObject) throws Throwable {
		
		List<SysPassValue> list=new ArrayList<SysPassValue>();
		
	    // 函数名称  页码数量  其他参数根据不同的 功能名称 进行读取
		String funName = curObject.getFunctionName();    //函数名称
		int pageNumber = StrEdit.StrToInt(curObject.getPageNumber());     //页码数量
		
		System.out.println("funName:"+funName);
		System.out.println("pageNumber:"+pageNumber);
		
		if(funName.equals("getWorkList")){//我的任务列表，如果需要更改为只显示属于自己的任务，则需要使用VARVALUE6检查员id判断，(find_in_set('32',VARVALUE6)>0 or VARVALUE6='')

			//ListBaseInfo listBaseInfo = new ListBaseInfo();//32为登录账户id,VARVALUE6=''表示全部

			String type = curObject.getArg1();
			String userId = curObject.getUserId();

			if(type.equals("未接收")){
				sql="select * from CLEAN where VARVALUE6='4' and INTVALUE4=0 and (find_in_set('"+userId+"',VARVALUE10)>0 or VARVALUE10='') order by CONID desc limit "+(pageNumber-1)*pageSize+","+pageSize;
				System.out.println(sql);
				try{
					rs = mdb.executeQuery(sql);
					while(rs.next()){
						SysPassValue valueObject = new SysPassValue();
						valueObject.setId(String.valueOf(rs.getInt("CONID")));
						valueObject.setArg1(rs.getString("VARVALUE1"));
						valueObject.setArg2(rs.getString("TEXT1"));
						valueObject.setArg3(rs.getString("VARVALUE3"));
						//valueObject.setArg3(listBaseInfo.getActiveCategoryName(rs.getInt("INTVALUE1")));
						valueObject.setArg4(rs.getString("VARVALUE4"));
						valueObject.setArg5(String.valueOf(rs.getInt("INTVALUE4")));
						list.add(valueObject);
					}
				}
				catch(Exception e){
					System.out.println("getWorkList()" +e.getLocalizedMessage());
				}
				finally{
					rs.close();
					mdb.close();
				}
			}

			if(type.equals("未完成")){
				sql="select * from CLEAN where VARVALUE6='4' and INTVALUE4=1 and (find_in_set('"+userId+"',VARVALUE10)>0 or VARVALUE10='') order by CONID desc limit "+(pageNumber-1)*pageSize+","+pageSize;
				System.out.println(sql);
				try{
					rs = mdb.executeQuery(sql);
					while(rs.next()){
						SysPassValue valueObject = new SysPassValue();
						valueObject.setId(String.valueOf(rs.getInt("CONID")));
						valueObject.setArg1(rs.getString("VARVALUE1"));
						valueObject.setArg2(rs.getString("TEXT1"));
						valueObject.setArg3(rs.getString("VARVALUE3"));
						//valueObject.setArg3(listBaseInfo.getActiveCategoryName(rs.getInt("INTVALUE1")));
						valueObject.setArg4(rs.getString("VARVALUE4"));
						valueObject.setArg5(String.valueOf(rs.getInt("INTVALUE4")));
						list.add(valueObject);
					}
				}
				catch(Exception e){
					System.out.println("getWorkList()" +e.getLocalizedMessage());
				}
				finally{
					rs.close();
					mdb.close();
				}
			}

			if(type.equals("已完成")){
				sql="select * from CLEAN where VARVALUE6='4' and INTVALUE4=2 and (find_in_set('"+userId+"',VARVALUE10)>0 or VARVALUE10='') order by CONID desc limit "+(pageNumber-1)*pageSize+","+pageSize;
				System.out.println(sql);
				try{
					rs = mdb.executeQuery(sql);
					while(rs.next()){
						SysPassValue valueObject = new SysPassValue();
						valueObject.setId(String.valueOf(rs.getInt("CONID")));
						valueObject.setArg1(rs.getString("VARVALUE1"));
						valueObject.setArg2(rs.getString("TEXT1"));
						valueObject.setArg3(rs.getString("VARVALUE3"));
						//valueObject.setArg3(listBaseInfo.getActiveCategoryName(rs.getInt("INTVALUE1")));
						valueObject.setArg4(rs.getString("VARVALUE4"));
						valueObject.setArg5(String.valueOf(rs.getInt("INTVALUE4")));
						list.add(valueObject);
					}
				}
				catch(Exception e){
					System.out.println("getWorkList()" +e.getLocalizedMessage());
				}
				finally{
					rs.close();
					mdb.close();
				}
			}
			
			return list;
		}
		
		if(funName.equals("getMyCleanList")){
			String type = curObject.getArg1();
			int channelId = StrEdit.StrToInt(curObject.getArg2());
			int companyId = StrEdit.StrToInt(curObject.getArg3());
			int userId = StrEdit.StrToInt(curObject.getUserId());
			
			String status = "0";
			if(type.equals("全部")){
				status = "";
			}else if(type.equals("待审核")){
				status = "2";
			}else if(type.equals("待完成")){
				status = "4";
			}else if(type.equals("已完成")){
				status = "6";
			}
			
			try{
				if(companyId==0){
					sql="select INTVALUE5 from userconn where CONID="+userId+"";
					ResultSet rs1 = mdb.executeQuery(sql);
		            if(rs1.next()){
		            	companyId = rs1.getInt("INTVALUE5");
		            }
		            rs1.close();
				}
			}catch(Exception e){
				System.out.println(e.getLocalizedMessage());
			}
			
			try{
				sql="select * from CLEAN where ChannelID="+channelId+" and (VARVALUE6='"+status+"' or '"+status+"' = '') and INTVALUE2="+companyId+" order by CONID desc limit "+(pageNumber-1)*pageSize+","+pageSize;
				System.out.println(sql);
				rs = mdb.executeQuery(sql);
				while(rs.next()){
					SysPassValue valueObject = new SysPassValue();
	            	valueObject.setId(String.valueOf(rs.getInt("CONID")));
	            	valueObject.setArg1(StrEdit.StringLeft(rs.getString("VARVALUE1"), 20));
	            	valueObject.setArg2(rs.getString("VARVALUE2"));
	            	valueObject.setArg3(rs.getString("VARVALUE3"));
	            	valueObject.setArg4(rs.getString("VARVALUE4"));
	            	valueObject.setArg5(StrEdit.StringLeft(rs.getString("ADDTIME"), 19));
	            	valueObject.setArg6(StrEdit.StringLeft(rs.getString("DATEVALUE1"), 19));
	            	String text1 = rs.getString("TEXT1");
	            	if(text1.length()>25){
	            		text1 = StrEdit.StringLeft(rs.getString("TEXT1"), 25) + "...";
	            	}
	            	valueObject.setArg7(text1);
	            	String strStatus = "";
	            	String varValue6 = rs.getString("VARVALUE6");
	            	if(varValue6.equals("-1")){
	            		strStatus = "已取消";
	            	}else if(varValue6.equals("1")){
	            		strStatus = "待提交";
	            	}else if(varValue6.equals("2")){
	            		strStatus = "待审核";
	            	}else if(varValue6.equals("3")){
	            		strStatus = "已退回";
	            	}else if(varValue6.equals("4")){
	            		strStatus = "待完成";
	            	}else if(varValue6.equals("5")){
	            		strStatus = "已支付";
	            	}else if(varValue6.equals("6")){
	            		strStatus = "已完成";
	            	}
	            	valueObject.setArg8(strStatus);
					list.add(valueObject);
				}
			}
			catch(Exception e){
				System.out.println("getMyCleanList()" +e.getLocalizedMessage());
			}
			finally{
				rs.close();
				mdb.close();
			}
			
			return list;
		}
		
		if(funName.equals("getMyComplainList")){
			String type = curObject.getArg1();
			int channelId = StrEdit.StrToInt(curObject.getArg2());
			int companyId = StrEdit.StrToInt(curObject.getArg3());
			int userId = StrEdit.StrToInt(curObject.getUserId());
			
			String status = "0";
			if(type.equals("全部")){
				status = "";
			}else if(type.equals("未回复")){
				status = "2";
			}else if(type.equals("已回复")){
				status = "4";
			}
			
			try{
				if(companyId==0){
					sql="select INTVALUE5 from userconn where CONID="+userId+"";
					ResultSet rs1 = mdb.executeQuery(sql);
		            if(rs1.next()){
		            	companyId = rs1.getInt("INTVALUE5");
		            }
		            rs1.close();
				}
			}catch(Exception e){
				System.out.println(e.getLocalizedMessage());
			}
			
			try{
				sql="select * from CLEAN where ChannelID="+channelId+" and (VARVALUE6='"+status+"' or '"+status+"' = '') and INTVALUE2="+companyId+" order by CONID desc limit "+(pageNumber-1)*pageSize+","+pageSize;
				System.out.println(sql);
				rs = mdb.executeQuery(sql);
				while(rs.next()){
					SysPassValue valueObject = new SysPassValue();
	            	valueObject.setId(String.valueOf(rs.getInt("CONID")));
	            	valueObject.setArg1(StrEdit.StringLeft(rs.getString("VARVALUE1"), 20));
	            	valueObject.setArg2(rs.getString("VARVALUE2"));
	            	valueObject.setArg3(rs.getString("VARVALUE3"));
	            	valueObject.setArg4(rs.getString("VARVALUE4"));
	            	valueObject.setArg5(StrEdit.StringLeft(rs.getString("ADDTIME"), 19));
	            	valueObject.setArg6(rs.getString("VARVALUE17"));
	            	String text1 = rs.getString("TEXT1");
	            	if(text1.length()>25){
	            		text1 = StrEdit.StringLeft(rs.getString("TEXT1"), 25) + "...";
	            	}
	            	valueObject.setArg7(text1);
	            	String strStatus = "";
	            	String varValue6 = rs.getString("VARVALUE6");
	            	if(varValue6.equals("-1")){
	            		strStatus = "已取消";
	            	}else if(varValue6.equals("2")){
	            		strStatus = "未回复";
	            	}else if(varValue6.equals("4")){
	            		strStatus = "已回复";
	            	}
	            	valueObject.setArg8(strStatus);
					list.add(valueObject);
				}
			}
			catch(Exception e){
				System.out.println("getMyComplainList()" +e.getLocalizedMessage());
			}
			finally{
				rs.close();
				mdb.close();
			}
			
			return list;
		}
		
		if(funName.equals("getMyMeetingList")){
			String type = curObject.getArg1();
			int channelId = StrEdit.StrToInt(curObject.getArg2());
			int companyId = StrEdit.StrToInt(curObject.getArg3());
			int userId = StrEdit.StrToInt(curObject.getUserId());
			try{
				if(companyId==0){
					sql="select INTVALUE5 from userconn where CONID="+userId+"";
					ResultSet rs1 = mdb.executeQuery(sql);
		            if(rs1.next()){
		            	companyId = rs1.getInt("INTVALUE5");
		            }
		            rs1.close();
				}
			}catch(Exception e){
				System.out.println(e.getLocalizedMessage());
			}
			
			try{
				if(type.equals("全部")){
					sql="select * from MEETING where ChannelID="+channelId+" and INTVALUE2="+companyId+" order by CONID desc limit "+(pageNumber-1)*pageSize+","+pageSize;
				}else if(type.equals("待付款")){
					sql="select * from MEETING where ChannelID="+channelId+" and VARVALUE6='4' and INTVALUE2="+companyId+" order by CONID desc limit "+(pageNumber-1)*pageSize+","+pageSize;
				}else if(type.equals("已付款")){
					sql="select * from MEETING where ChannelID="+channelId+" and VARVALUE6='5' and INTVALUE2="+companyId+" order by CONID desc limit "+(pageNumber-1)*pageSize+","+pageSize;
				}else if(type.equals("已完成")){
					sql="select * from MEETING where ChannelID="+channelId+" and VARVALUE6='6' and INTVALUE2="+companyId+" order by CONID desc limit "+(pageNumber-1)*pageSize+","+pageSize;
				}
				System.out.println(sql);
				rs = mdb.executeQuery(sql);
				while(rs.next()){
					SysPassValue valueObject = new SysPassValue();
	            	valueObject.setId(String.valueOf(rs.getInt("CONID")));
	            	valueObject.setArg1(rs.getString("VARVALUE1"));
	            	valueObject.setArg2(rs.getString("VARVALUE4"));
	            	valueObject.setArg3(rs.getString("VARVALUE5"));
	            	//valueObject.setArg4(rs.getString("VARVALUE6"));
	            	valueObject.setArg5(rs.getString("VARVALUE8"));
	            	valueObject.setArg6(rs.getString("VARVALUE9"));
	            	valueObject.setArg7(rs.getString("DECVALUE1"));
	            	valueObject.setArg8(rs.getString("DECVALUE2"));
	            	valueObject.setArg9(StrEdit.StringLeft(rs.getString("ADDTIME"), 16));
	            	valueObject.setArg10(StrEdit.StringLeft(rs.getString("DATEVALUE1"), 16));
	            	valueObject.setArg11(StrEdit.StringLeft(rs.getString("DATEVALUE2"), 16));
	            	String text1 = rs.getString("TEXT1");
	            	if(text1.length()>25){
	            		text1 = StrEdit.StringLeft(rs.getString("TEXT1"), 25) + "...";
	            	}
	            	valueObject.setArg12(text1);
	            	String strStatus = "";
	            	String varValue6 = rs.getString("VARVALUE6");
	            	if(varValue6.equals("-1")){
	            		strStatus = "已取消";
	            	}else if(varValue6.equals("1")){
	            		strStatus = "待提交";
	            	}else if(varValue6.equals("2")){
	            		strStatus = "待审核";
	            	}else if(varValue6.equals("3")){
	            		strStatus = "已退回";
	            	}else if(varValue6.equals("4")){
	            		strStatus = "待付款";
	            	}else if(varValue6.equals("5")){
	            		strStatus = "已付款";
	            	}else if(varValue6.equals("6")){
	            		strStatus = "已完成";
	            	}
	            	valueObject.setArg4(strStatus);
					list.add(valueObject);
				}
			}
			catch(Exception e){
				System.out.println("getMyMeetingList()" +e.getLocalizedMessage());
			}
			finally{
				rs.close();
				mdb.close();
			}
			
			return list;
		}
		
		if(funName.equals("getImageList")){
			
			int userId = StrEdit.StrToInt(curObject.getUserId());
			String arg1 = curObject.getArg1();
			String arg2 = curObject.getArg2();
			
			sql="select * from ATTACHMENT where VARVALUE1='"+arg1+"' and VARVALUE2='"+arg2+"' order by CONID desc limit "+(pageNumber-1)*pageSize+","+pageSize;
			System.out.println(sql);
			try{
	            rs = mdb.executeQuery(sql);
	            while(rs.next()){
	            	String picture = rs.getString("VARVALUE4");
	            	if(!picture.equals("")){
	            		picture = weburl + picture;
	            	}
	            	SysPassValue valueObject = new SysPassValue();
	            	valueObject.setId(String.valueOf(rs.getInt("CONID")));System.out.println(picture);
	            	valueObject.setArg1(picture);
	            	valueObject.setArg2(StrEdit.StringLeft(rs.getString("ADDTIME"), 19));
					list.add(valueObject);
	            }
	        }
	        catch(Exception e){
	            System.out.println("getImageList()" +e.getLocalizedMessage());
	        }
	        finally{
	            rs.close();
	            mdb.close();
	        }
			
			return list;
		}
		
		if(funName.equals("getNewsList")){
			
			int userId = StrEdit.StrToInt(curObject.getUserId());
			String channelId = curObject.getArg1();
			String keywords = curObject.getArg2();
			
			if(channelId.equals("1")){
				sql="select * from NEWS where ChannelID="+channelId+" and (VARVALUE1 like '%"+keywords+"%' or '"+keywords+"' = '') and (VARVALUE3='1' or find_in_set('"+userId+"',VARVALUE26)>0) and VARVALUE39='4' order by CONID desc limit "+(pageNumber-1)*pageSize+","+pageSize;
			}else if(channelId.equals("2")){
				sql="select * from NEWS where ChannelID="+channelId+" and (VARVALUE1 like '%"+keywords+"%' or '"+keywords+"' = '') and VARVALUE39='4' order by CONID desc limit "+(pageNumber-1)*pageSize+","+pageSize;
			}
			System.out.println(sql);
			try{
	            rs = mdb.executeQuery(sql);
	            while(rs.next()){
	            	String picture = rs.getString("VARVALUE38");
	            	if(!picture.equals("")){
	            		picture = weburl + picture;
	            	}
	            	SysPassValue valueObject = new SysPassValue();
	            	valueObject.setId(String.valueOf(rs.getInt("CONID")));
	            	valueObject.setArg1(StrEdit.StringLeft(rs.getString("VARVALUE1"), 20));
	            	valueObject.setArg2(rs.getString("VARVALUE22"));
	            	valueObject.setArg3(StrEdit.StringLeft(rs.getString("TEXT1"), 40));
	            	valueObject.setArg4(picture);
	            	valueObject.setArg5(StrEdit.StringLeft(rs.getString("ADDTIME"), 19));
					list.add(valueObject);
	            }
	        }
	        catch(Exception e){
	            System.out.println("getNewsList()" +e.getLocalizedMessage());
	        }
	        finally{
	            rs.close();
	            mdb.close();
	        }
			
			return list;
		}
		
		if(funName.equals("getMeetingRoomList")){
			
			int userId = StrEdit.StrToInt(curObject.getUserId());
			String channelId = curObject.getArg1();
			String keywords = curObject.getArg2();
			
			sql="select * from MEETINGROOM where ChannelID="+channelId+" and (VARVALUE1 like '%"+keywords+"%' or '"+keywords+"' = '') and VARVALUE10='Y' order by CONID desc limit "+(pageNumber-1)*pageSize+","+pageSize;
			System.out.println(sql);
			try{
	            rs = mdb.executeQuery(sql);
	            while(rs.next()){
	            	String picture = rs.getString("VARVALUE38");
	            	if(!picture.equals("")){
	            		picture = weburl + picture;
	            	}
	            	SysPassValue valueObject = new SysPassValue();
	            	String priceShow = "";
	            	String minute = rs.getString("VARVALUE4");
	            	String isFree = rs.getString("VARVALUE7");
	            	if(isFree.equals("N")){
	            		priceShow = "/"+minute+"分钟";
	            	}
	            	valueObject.setId(String.valueOf(rs.getInt("CONID")));
	            	valueObject.setArg1(StrEdit.StringLeft(rs.getString("VARVALUE1"), 20));
	            	valueObject.setArg2(StrEdit.StringLeft(rs.getString("TEXT1"), 40));
	            	valueObject.setArg3(picture);
	            	valueObject.setArg4(rs.getString("DECVALUE1"));
	            	valueObject.setArg5(priceShow);
	            	valueObject.setArg6(StrEdit.StringLeft(rs.getString("ADDTIME"), 19));
					list.add(valueObject);
	            }
	        }
	        catch(Exception e){
	            System.out.println("getMeetingRoomList()" +e.getLocalizedMessage());
	        }
	        finally{
	            rs.close();
	            mdb.close();
	        }
			
			return list;
		}
		
		if(funName.equals("getContactList")){
			
			int userId = StrEdit.StrToInt(curObject.getUserId());
			String channelId = curObject.getArg1();
			String keywords = curObject.getArg2();
			
			if(channelId.equals("1")){
				sql="select * from userconn where INTVALUE6 in (1001,1011,1021) and (VARVALUE3 like '%"+keywords+"%' or '"+keywords+"' = '') and VARVALUE39='1' order by CONID desc limit "+(pageNumber-1)*pageSize+","+pageSize;
			}
			System.out.println(sql);
			try{
	            rs = mdb.executeQuery(sql);
	            while(rs.next()){
	            	String picture = rs.getString("VARVALUE38");
	            	if(!picture.equals("")){
	            		picture = weburl + picture;
	            	}
	            	SysPassValue valueObject = new SysPassValue();
	            	valueObject.setId(String.valueOf(rs.getInt("CONID")));
	            	valueObject.setArg1(StrEdit.StringLeft(rs.getString("VARVALUE3"), 20));
	            	valueObject.setArg2(rs.getString("VARVALUE8"));
	            	valueObject.setArg3(rs.getString("VARVALUE9"));
	            	valueObject.setArg4(picture);
	            	valueObject.setArg5(StrEdit.StringLeft(rs.getString("ADDTIME"), 19));
					list.add(valueObject);
	            }
	        }
	        catch(Exception e){
	            System.out.println("getContactList()" +e.getLocalizedMessage());
	        }
	        finally{
	            rs.close();
	            mdb.close();
	        }
			
			return list;
		}
		
		if(funName.equals("getBuildingList")){
			
			String type = curObject.getArg1();
			
			sql="select * from BUILDING order by CONID asc limit "+(pageNumber-1)*pageSize+","+pageSize;
			System.out.println(sql);
			try{
	            rs = mdb.executeQuery(sql);
	            while(rs.next()){
	            	SysPassValue valueObject = new SysPassValue();
	            	valueObject.setId(String.valueOf(rs.getInt("CONID")));
	            	valueObject.setArg1(StrEdit.StringLeft(rs.getString("VARVALUE1"), 20));
	            	valueObject.setArg2(rs.getString("VARVALUE2"));
	            	valueObject.setArg3(rs.getString("VARVALUE3"));
	            	valueObject.setArg4(rs.getString("VARVALUE4"));
	            	valueObject.setArg5(StrEdit.StringLeft(rs.getString("ADDTIME"), 19));
					list.add(valueObject);
	            }
	        }
	        catch(Exception e){
	            System.out.println("getBuildingList()" +e.getLocalizedMessage());
	        }
	        finally{
	            rs.close();
	            mdb.close();
	        }
			
			return list;
		}
		
		if(funName.equals("getUrgentList")){
			
			String arg1 = curObject.getArg1();
			String arg2 = curObject.getArg2();
			
			if(pageNumber==1){
				
				SysPassValue valueObject1 = new SysPassValue();
				valueObject1.setId("1");
				valueObject1.setArg1("一般");
				list.add(valueObject1);
				
				SysPassValue valueObject2 = new SysPassValue();
				valueObject2.setId("2");
				valueObject2.setArg1("比较紧急");
				list.add(valueObject2);
				
				SysPassValue valueObject3 = new SysPassValue();
				valueObject3.setId("3");
				valueObject3.setArg1("非常紧急");
				list.add(valueObject3);
			}
			
			return list;
		}
		
		if(funName.equals("getMyVisitorList")){
			
			int userId = StrEdit.StrToInt(curObject.getUserId());
			String channelId = curObject.getArg1();
			String keywords = curObject.getArg2();
			int companyId = StrEdit.StrToInt(curObject.getArg3());
			try{
				if(companyId==0){
					sql="select INTVALUE5 from userconn where CONID="+userId+"";
					ResultSet rs1 = mdb.executeQuery(sql);
		            if(rs1.next()){
		            	companyId = rs1.getInt("INTVALUE5");
		            }
		            rs1.close();
				}
				
	            sql="select * from VISITOR where ChannelID="+channelId+" and (VARVALUE1 like '%"+keywords+"%' or VARVALUE3 like '%"+keywords+"%' or '"+keywords+"' = '') and INTVALUE2="+companyId+" order by CONID desc limit "+(pageNumber-1)*pageSize+","+pageSize;
				System.out.println(sql);
	            rs = mdb.executeQuery(sql);
	            while(rs.next()){
	            	String picture = rs.getString("VARVALUE38");
	            	if(!picture.equals("")){
	            		picture = weburl + picture;
	            	}
	            	String text1 = rs.getString("TEXT1");
	            	if(text1.length()>25){
	            		text1 = StrEdit.StringLeft(rs.getString("TEXT1"), 25) + "...";
	            	}
	            	SysPassValue valueObject = new SysPassValue();
	            	valueObject.setId(String.valueOf(rs.getInt("CONID")));
	            	valueObject.setArg1(rs.getString("VARVALUE1"));
	            	valueObject.setArg2(rs.getString("VARVALUE3"));
	            	valueObject.setArg3(rs.getString("VARVALUE5"));
	            	valueObject.setArg4(rs.getString("VARVALUE22"));
	            	valueObject.setArg5(rs.getString("VARVALUE27"));
	            	valueObject.setArg6(StrEdit.StringLeft(rs.getString("ADDTIME"), 16));
	            	valueObject.setArg7(StrEdit.StringLeft(rs.getString("DATEVALUE1"), 16));
	            	valueObject.setArg8(text1);
					list.add(valueObject);
	            }
	        }
	        catch(Exception e){
	            System.out.println("getMyVisitorList()" +e.getLocalizedMessage());
	        }
	        finally{
	            rs.close();
	            mdb.close();
	        }
			
			return list;
		}
		
		
		return null;
	}
		
		
	/*
	 * 读取 系统参数*
	 */
	@Override
	public HhSysArgument getHhSysArgument() throws Throwable {
		
		//得到系统的图片
		HhSysArgument  DataObject=new HhSysArgument(1l,"sysname","",
				"http://test1.zb388.com/huahuajpg/selectshopflash.png","shop","1",
				"http://test1.zb388.com/huahuajpg/selectshopflash.png","shop","1",
				"http://test1.zb388.com/huahuajpg/serverflash1.png","shop","1", 
				"http://test1.zb388.com/huahuajpg/serverflash1.png","shop","1",
				"http://test1.zb388.com/huahuajpg/serverflash1.png","shop","1",
				"早班(自定)","中班","晚班",8.00,13.00,18.00);
		
		 return DataObject;
	}
	
	public List<String> getHhZhongShanAreaList() throws Throwable {
		
		List<String> list1=new ArrayList<String>();
		list1.add(new String("东区"));
		list1.add(new String("南区"));
		list1.add(new String("西区"));
		list1.add(new String("北区"));
		return list1;
	}
	
	public List<CommonList> getTaskList(String userId,String type, int pageNumber) throws Throwable {
		
		List<CommonList> list =new ArrayList<CommonList>();
		ListBaseInfo listbase = new ListBaseInfo();
		
		if(type.equals("未执行")){
			sql="select * from activeconn where ChannelID=1 and INTVALUE4=2 order by CONID desc limit "+(pageNumber-1)*pageSize+","+pageSize;
			System.out.println(sql);
			try{
	            rs = mdb.executeQuery(sql);
	            while(rs.next()){
	            	CommonList valueObject = new CommonList();
	            	valueObject.setId(Long.valueOf(rs.getInt("CONID")));
	            	valueObject.setVarValue1(rs.getString("VARVALUE9"));
	            	valueObject.setVarValue2(rs.getString("TEXT1"));
	            	valueObject.setVarValue3(listbase.getActiveCategoryName(rs.getInt("INTVALUE1")));
	            	valueObject.setVarValue4(rs.getString("VARVALUE1"));
	            	list.add(valueObject);
	            }
	        }
	        catch(Exception e){
	            System.out.println("getTaskList()" +e.getLocalizedMessage());
	        }
	        finally{
	            rs.close();
	            mdb.close();
	        }
		}
		
		if(type.equals("执行中")){
			sql="select * from activeconn where ChannelID=1 and INTVALUE4=5 order by CONID desc limit "+(pageNumber-1)*pageSize+","+pageSize;
			System.out.println(sql);
			try{
	            rs = mdb.executeQuery(sql);
	            while(rs.next()){
	            	list.add(new CommonList(Long.valueOf(rs.getInt("CONID")),rs.getString("VARVALUE9"),rs.getString("TEXT1"),listbase.getActiveCategoryName(rs.getInt("INTVALUE1")),rs.getString("VARVALUE1"),"","","","","",""));
	            }
	        }
	        catch(Exception e){
	            System.out.println("getTaskList()" +e.getLocalizedMessage());
	        }
	        finally{
	            rs.close();
	            mdb.close();
	        }
		}
		
		if(type.equals("已完成")){
			sql="select * from activeconn where ChannelID=1 and INTVALUE4=7 order by CONID desc limit "+(pageNumber-1)*pageSize+","+pageSize;
			System.out.println(sql);
			try{
	            rs = mdb.executeQuery(sql);
	            while(rs.next()){
	            	list.add(new CommonList(Long.valueOf(rs.getInt("CONID")),rs.getString("VARVALUE9"),rs.getString("TEXT1"),listbase.getActiveCategoryName(rs.getInt("INTVALUE1")),rs.getString("VARVALUE1"),"","","","","",""));
	            }
	        }
	        catch(Exception e){
	            System.out.println("getTaskList()" +e.getLocalizedMessage());
	        }
	        finally{
	            rs.close();
	            mdb.close();
	        }
		}
		
		return list;
	}
	
	public CommonData getTaskDetail(String taskid,String userid) throws Throwable {
		
		CommonData taskView = new CommonData();
		ListBaseInfo listbase = new ListBaseInfo();
		
		//构造函数的赋值方法，如果有构造函数，则可以使用此方法
		//CommonData taskView = null;
		//taskView = new CommonData(Long.valueOf(taskid),"","认真工作数据","龙华街道","F20","杨美华","曾先进","认真工作","认真","2017-03-10","2017-03-10","已检查","已检查","已检查","6","6","6","6","6","6","6");
		
		System.out.println("getTaskDetail:"+taskid);
		
		sql="select * from activeconn where CONID="+taskid+"";
		try{
            rs = mdb.executeQuery(sql);
            if(rs.next()){
            	taskView.setId(Long.valueOf(taskid));//taskView.setId(Long.parseLong(taskid));
        		taskView.setVarValue1("");
        		taskView.setVarValue2(rs.getString("VARVALUE9"));
        		taskView.setVarValue3(listbase.getActiveCategoryName(rs.getInt("INTVALUE1")));
        		taskView.setVarValue4(rs.getString("VARVALUE1"));//街道
        		taskView.setVarValue5(rs.getString("VARVALUE2"));
        		taskView.setVarValue6(rs.getString("VARVALUE5"));
        		taskView.setVarValue7(rs.getString("VARVALUE8"));
        		taskView.setVarValue8(rs.getString("TEXT1"));
        		taskView.setVarValue9(StrEdit.StringLeft(rs.getString("DATEVALUE1"),10));
        		taskView.setVarValue10(StrEdit.StringLeft(rs.getString("DATEVALUE2"),10));
        		taskView.setVarValue11(String.valueOf(rs.getInt("INTVALUE1")));
        		taskView.setVarValue12(rs.getString("VARVALUE6"));
        		taskView.setVarValue21(rs.getString("VARVALUE21"));
        		taskView.setVarValue22(rs.getString("VARVALUE22"));
        		taskView.setVarValue23(rs.getString("VARVALUE23"));
        		taskView.setVarValue24(rs.getString("VARVALUE24"));
        		taskView.setVarValue25(rs.getString("VARVALUE25"));
        		taskView.setVarValue26(rs.getString("VARVALUE26"));
        		taskView.setVarValue27(rs.getString("VARVALUE27"));
        		taskView.setVarValue28(rs.getString("VARVALUE28"));
        		taskView.setVarValue29(rs.getString("VARVALUE29"));
        		taskView.setVarValue30(rs.getString("VARVALUE30"));
            }
        }
        catch(Exception e){
            System.out.println("getTaskDetail()" +e.getLocalizedMessage());
        }
        finally{
            rs.close();
            mdb.close();
        }
		
		return taskView;
	}
	
	public void userLogout(long id) throws Throwable {
		// TODO Auto-generated method stub
		
	}
	
 
	
}
