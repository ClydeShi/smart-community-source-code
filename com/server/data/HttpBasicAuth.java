package com.server.data;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpBasicAuth {
	private static String userName = "eventWebApiUser";
	private static String userPassword = "lhcg2017api";
	
	public HttpBasicAuth() {
	}
	 
	public static String getHtml(String tempurl,String code){
		try{
			URL url = new URL(tempurl);
			HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			conn.connect();
			InputStream is = conn.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is,code));
			String line="";
			StringBuffer resultBuffer = new StringBuffer();
			while((line = br.readLine())!=null){
				resultBuffer.append(line);
			}
			br.close();
			is.close();
			conn.disconnect();
			return resultBuffer.toString();
		} catch (Exception e) {
            e.printStackTrace();
        }
		return null;
	}
	
	
	
}
