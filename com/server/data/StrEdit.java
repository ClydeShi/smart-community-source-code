package com.server.data;

import java.io.*;
import java.util.*;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.text.ParseException;

public class StrEdit {
    public static String htmlftbbs = "";
    public static String strValue = "";

    /**
     * 字符串替换，将 source 中的 oldString 全部换成 newString
     *
     * @param source 源字符串
     * @param oldString 老的字符串
     * @param newString 新的字符串
     * @return 替换后的字符串
     * 用于输入的表单字符串转化成HTML格式的文本
     */
    public static String Replace(String source, String oldString, String newString) {
        StringBuffer output = new StringBuffer();

        int lengthOfSource = source.length();   // 源字符串长度
        int lengthOfOld = oldString.length();   // 老字符串长度

        int posStart = 0;   // 开始搜索位置
        int pos;            // 搜索到老字符串的位置

        while ((pos = source.indexOf(oldString, posStart)) >= 0) {
            output.append(source.substring(posStart, pos));

            output.append(newString);
            posStart = pos + lengthOfOld;
        }

        if (posStart < lengthOfSource) {
            output.append(source.substring(posStart));
        }

        return output.toString();
    }
    
    /**
     * 将字符串格式化成 HTML 代码
     * 只转换特殊字符，适合于 HTML 中的表单区域
     *
     * @param str 要格式化的字符串
     * @return 格式化后的字符串
     */
    public static String toHtmlInput(String str) {
        if (str == null)    return null;

        String html = new String(str);

        //html = Replace(html, "&", "&amp;");
        html = Replace(html, "<", "&lt;");
        html = Replace(html, ">", "&gt;");

        html = Replace(html, "\"", "&quot;");
        html = Replace(html, "'", "&#039;");

        return html;
    }

    /**
     * 将字符串格式化成 HTML 代码输出
     * 除普通特殊字符外，还对空格、制表符和换行进行转换，
     * 以将内容格式化输出，
     * 适合于 HTML 中的显示输出
     *
     * @param str 要格式化的字符串
     * @return 格式化后的字符串
     */
    public static String toHtml(String str) {
        if (str == null)
            return null;

        String html = new String(str);

        html = toHtmlInput(html);
        //html = Replace(html, "&amp;", "&");
        html = Replace(html, "&lt;", "<");
        html = Replace(html, "&gt;", ">");

        html = Replace(html, "\r\n", "\n");
        html = Replace(html, "\t", "    ");

        html = Replace(html, "&quot;", "\"");
        html = Replace(html, "&#039;", "'");

        return html;
    }
    
    public static String textToHtmlInputComment(String str) {
        if (str == null)
        	return null;

        String html = new String(str);
        
        //html = Replace(html, "&", "&amp;");
        html = Replace(html, " ", "&nbsp;");//change here
        html = Replace(html, "<", "&lt;");
        html = Replace(html, ">", "&gt;");
        html = Replace(html, "\"", "&quot;");
        html = Replace(html, "\\", "\\\\");
        html = Replace(html, "[quote]", "<div class='yinyong'><span><dfn>引用</dfn></span>");
        html = Replace(html, "[/quote]", "</div>");
        html = Replace(html, "[br]", "<br>");
        html = Replace(html, "'", "&#039;");
        
        return html;
    }
    
    public static String toHtmlComment(String str) {
        if (str == null)
            return null;

        String html = new String(str);
        html = Replace(html, "\r\n", "\n");
        html = Replace(html, "\t", "    ");
        html = Replace(html, "&quot;", "\"");
        html = Replace(html, "&#039;", "'");
        html = Replace(html, "\n", "<br>");

        return html;
    }
    
    //replace one \ to two \\
    public static String toDB(String str) {
        if (str == null)    return null;

        String html = new String(str);
        html = Replace(html, "'", "&#039;");
        html = Replace(html, "\\", "\\\\");

        return html;
    }
    
    public static String toPage(String str) {
        if (str == null)
            return null;

        String html = new String(str);
        html = Replace(html, "&#039;", "'");

        return html;
    }
    
    public static String textareaToDB(String str) {//for mysql use textarea
        if (str == null)
        	return null;

        String html = new String(str);
        html = Replace(html, "&", "&amp;");//put in the first
        html = Replace(html, "'", "&#039;");
        html = Replace(html, "<", "&lt;");
        html = Replace(html, ">", "&gt;");
        //html = Replace(html, " ", "&nbsp;");
        html = Replace(html, "\\", "\\\\");
        html = Replace(html, "</textarea>", "[/textarea]");
        
        return html;
    }
    
    public static String templateToTextarea(String str) {//for textarea
        if (str == null)
        	return null;
        
        String html = new String(str);
        html = Replace(html, "&", "&amp;");//put in the first
        html = Replace(html, "'", "&#039;");
        html = Replace(html, "<", "&lt;");
        html = Replace(html, ">", "&gt;");
        //html = Replace(html, " ", "&nbsp;"); //keep input the &nbsp; can display
        html = Replace(html, "</textarea>", "[/textarea]");
        
        return html;
    }
    
    public static String toHtmlPage(String str) {
        if (str == null)
            return null;

        String html = new String(str);
        html = Replace(html, "&amp;", "&");
        html = Replace(html, "&lt;", "<");
        html = Replace(html, "&gt;", ">");
        html = Replace(html, "&#039;", "'");
        //html = Replace(html, "\n", "<br>"); //textarea中的换行为\n,显示时要将\n转换为br,会影响标签的默认换行
        html = Replace(html, "[/textarea]", "</textarea>");
        
        return html;
    }
    
    public static String textToPage(String str) {
        if (str == null)
            return null;
        
        String html = new String(str);
        html = Replace(html, "&#039;", "'");
        html = Replace(html, "\n", "<br>");

        return html;
    }
    
    public static String textToPageForComment(String str) {
        if (str == null)
            return null;
        
        String html = new String(str);
        html = Replace(html, "&#039;", "'");
        html = Replace(html, "\n", "<br>");
        html = Replace(html, "\r", "");//换行会影响Ajax的显示，去掉换行

        return html;
    }
    
    public static String htmlToDBbak(String ftbbs)
    {
        ftbbs = htmlz(ftbbs, "&lt;", "<");
        ftbbs = htmlz(ftbbs, "&gt;", ">");
        ftbbs = htmlz(ftbbs, "&#039;", "'");
        ftbbs = htmlz(ftbbs, "&nbsp;", " ");
        return ftbbs;
    }

    public static String DBToTextareabak(String ftbbs)
    {        
        ftbbs = htmlz(ftbbs, "<", "&lt;");
        ftbbs = htmlz(ftbbs, ">", "&gt;");
        ftbbs = htmlz(ftbbs, "'", "&#039;");
        ftbbs = htmlz(ftbbs, " ", "&nbsp;");
        return ftbbs;
    }
    
    public static String DBToHtmlbak(String ftbbs)
    {        
        ftbbs = htmlz(ftbbs, "<", "&lt;");
        ftbbs = htmlz(ftbbs, ">", "&gt;");
        ftbbs = htmlz(ftbbs, "<br>", "\n");//textarea中的换行为\n,显示时要将\n转换为br
        ftbbs = htmlz(ftbbs, "'", "&#039;");
        return ftbbs;
    }
    
    public static String textareaToDBbak(String ftbbs)        /*用于静态化功能，转换Html内容*/
    {
    	ftbbs = htmlz(ftbbs, "[/textarea]", "</textarea>");
    	ftbbs = htmlz(ftbbs, "&amp;nbsp;", "&nbsp;");//保留空格代码 2009-10-10
        ftbbs = htmlz(ftbbs, "&lt;", "<");
        ftbbs = htmlz(ftbbs, "&gt;", ">");
        ftbbs = htmlz(ftbbs, "&#039;", "'");
        //ftbbs = htmlz(ftbbs, "\\\\", "\\");
        //ftbbs = htmlz(ftbbs, "&nbsp;", " ");
        
        return ftbbs;
    }
    
    public static String DBTextareabak(String ftbbs)        /*用于静态化功能*/
    {
    	ftbbs = htmlz(ftbbs, "<", "&lt;");
        ftbbs = htmlz(ftbbs, ">", "&gt;");
        ftbbs = htmlz(ftbbs, "'", "&#039;");
        //ftbbs = htmlz(ftbbs, " ", "&nbsp;");
        
        return ftbbs;
    }
    
    public static String DBTextareatbak(String ftbbs)        /*用于静态化功能，取出Html内容转换*/
    {
    	ftbbs = htmlz(ftbbs, "</textarea>", "[/textarea]");
    	ftbbs = htmlz(ftbbs, "<", "&lt;");
        ftbbs = htmlz(ftbbs, ">", "&gt;");
        ftbbs = htmlz(ftbbs, "'", "&#039;");
        //ftbbs = htmlz(ftbbs, " ", "&nbsp;");
        ftbbs = htmlz(ftbbs, "&nbsp;", "&amp;nbsp;"); // to page 2009-10-10
        
        return ftbbs;
    }
    
    public static String templateToTextareabak(String ftbbs)
    {
    	ftbbs = htmlz(ftbbs, "[/textarea]", "</textarea>");
    	ftbbs = htmlz(ftbbs, "&amp;nbsp;", "&nbsp;");//保留空格代码 2009-10-10
        ftbbs = htmlz(ftbbs, "&lt;", "<");
        ftbbs = htmlz(ftbbs, "&gt;", ">");
        ftbbs = htmlz(ftbbs, "&#039;", "'");
        //ftbbs = htmlz(ftbbs, "&nbsp;", " ");
        return ftbbs;
    }
    
    public static String textareaToTemplatebak(String ftbbs)
    {
    	ftbbs = htmlz(ftbbs, "</textarea>", "[/textarea]");
    	ftbbs = htmlz(ftbbs, "<", "&lt;");
        ftbbs = htmlz(ftbbs, ">", "&gt;");
        ftbbs = htmlz(ftbbs, "'", "&#039;");
        //ftbbs = htmlz(ftbbs, " ", "&nbsp;"); //keep input the &nbsp; can display
        ftbbs = htmlz(ftbbs, "&nbsp;", "&amp;nbsp;");
        
        return ftbbs;
    }
    
    public static String htmlz(String str,String newstr,String oldstr)
    {
        int find = -1;
        int oldrep = oldstr.length();

        do {
            find = str.indexOf(oldstr);

            if (find != -1)
                str = str.substring(0, find) + newstr + str.substring(find + oldrep);

        } while (find != -1);

        return str;
    }

    /**
     * 将普通字符串格式化成数据库认可的字符串格式
     *
     * @param str 要格式化的字符串
     * @return 合法的数据库字符串
     */
    public static String toSql(String str) {
        String sql = new String(str);
        return Replace(sql, "'", "''");
    }

    public static String StrChkNull( String str)
    {
        String reValue = "";
        if(str == null || str.trim().equals("")){
            return reValue;
        }else{
            return str;
        }
    }
    
    public static String ISOToGB2312( String str )
    {
        String reValue = "";
        if(str==null){
            return reValue;
        }

        byte[] bytesStr = null;
        try {
            bytesStr = str.getBytes("ISO-8859-1");
            return new String( bytesStr, "gb2312" ) ;
        } catch (Exception ex) {
            return str ;
        }
    }

    public static String ISOToBig5( String str )
    {
        try {
            byte[] bytesStr = str.getBytes("ISO-8859-1");
            return new String(bytesStr, "big5");
        } catch (Exception ex) {
            return str;
        }
    }

    public static String UnicodeToUTF8(String strIn)
    {
        String strOut = "";
        if(strIn == null || strIn.trim().equals(""))
        {
            return strOut;
        }
        try
        {
            byte b[] = strIn.getBytes("ISO8859_1");
            strOut = new String(b, "utf-8");
        }
        catch(Exception e) { }
        return strOut;
    }
    
    public static String UnicodeToUTF8ChkNull(String strIn)
    {
    	String strOut = "";
        if(strIn == null || strIn.trim().equals(""))
        {
            return strOut;
        }
        try
        {
            byte b[] = strIn.getBytes("ISO8859_1");
            strOut = new String(b, "utf-8");
        }
        catch(Exception e) { }
        return strOut;
    }
    
    public static String UnicToUTF8Repl(String strIn)//转换编码和字符
    {
    	String strOut = "";
        if(strIn == null || strIn.trim().equals(""))
        {
            return strOut;
        }
        try
        {
            byte b[] = strIn.getBytes("ISO8859_1");
            strOut = new String(b, "utf-8");
        }
        catch(Exception e) { }
        
        strOut=strOut.replaceAll("'","''");
        strOut=strOut.replaceAll("&","&amp;");
        strOut=strOut.replaceAll("\"","&quot;");
        strOut=strOut.replaceAll("\\\\","\\\\\\\\");
        
        return strOut;
    }
    
    public static String replaceCode(String strIn)//插入数据库前转换字符
    {
    	String strOut = "";
    	if(strIn == null || strIn.trim().equals(""))
        {
            return strOut;
        }
        
    	strIn=strIn.replaceAll("'","''");
    	strIn=strIn.replaceAll("&","&amp;");
    	strIn=strIn.replaceAll("\"","&quot;");
    	strIn=strIn.replaceAll("\\\\","\\\\\\\\");
        
        return strIn;
    }
    
    /**
     * 将数组填充至指定大小,消除异常问题
     */
    public static String [] FillString(String[] StrForFill,int StrNeedSize){
        int n = 0;
        int StrSize = StrForFill.length;
        int StrFillPos = StrSize;
        String[] reStr = new String[StrNeedSize];//定义新数组
        for(n=0;n<StrSize;n++)
        {
            reStr[n] = StrForFill[n];//将原来的值填充数组,从0开始,如,0到6大小是6,
            //数据为reStr[0]到reStr[5]
        }

        for(n=0;n<StrNeedSize-StrSize;n++)
        {
            reStr[StrFillPos + n] = "0";//填充数组至指定大小,从StrSize开始
        }

        return reStr;
    }

    public static int StrToInt(String strtoint){
        try{
            int n;
            n = Integer.parseInt(strtoint);
            return n;
        }catch(NumberFormatException e){
            return 0;
        }
    }
    
    public static double StrToDouble(String strtodouble){
    	if(strtodouble == null)
    		strtodouble = "0";
        try{
            double n;
            n = Double.parseDouble(strtodouble);
            return n;
        }catch(NumberFormatException e){
            return 0;
        }
    }
    
    public static float StrToFloat(String strtofloat){
    	if(strtofloat == null)
    		strtofloat = "0";
        try{
            float n;
            n = Float.parseFloat(strtofloat);
            return n;
        }catch(NumberFormatException e){
            return 0;
        }
    }

    public static String getLocalTime(){
        java.text.DateFormat df=java.text.DateFormat.getInstance();//格式化日期06-12-6 下午10:12
        return df.format(new java.util.Date());
    }

    public static String getDataForDB(){
        java.util.Calendar cl = Calendar.getInstance();
        int smonth=cl.get(Calendar.MONTH)+1;
        String nowdate=cl.get(Calendar.YEAR) + "-" + smonth + "-" + cl.get(Calendar.DAY_OF_MONTH) +
                       " " +cl.get(Calendar.HOUR_OF_DAY) + ":" +cl.get(Calendar.MINUTE) + ":" +cl.get(Calendar.SECOND);

        return nowdate;//2007-1-9 16:49:6,跟北京时间一致
    }

    public static String getSimDateForDB(){
        Date currTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.US);//HH to hh 12小时制
        String nowdate = null;
        try {
            nowdate = new String(formatter.format(currTime).getBytes("iso-8859-1"));
        } catch (UnsupportedEncodingException ex) {
        }

        return nowdate;//2007-07-02 18:06:33,24小时制,北京时间
    }
    
    public static String getSimDateForDB2(){
        Date currTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd",Locale.US);//HH to hh 12小时制
        String nowdate = null;
        try {
            nowdate = new String(formatter.format(currTime).getBytes("iso-8859-1"));
        } catch (UnsupportedEncodingException ex) {
        }

        return nowdate;//2007-07-02
    }

    /*
     * 得到指定日期所在周的第一天和最后一天
     */
    public static String getFirstAndLastOfWeek(String dataStr, String type) throws ParseException {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(dataStr));

        int d = 0;
        if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            d = -6;
        } else {
            d = 2 - cal.get(Calendar.DAY_OF_WEEK);
        }
        cal.add(Calendar.DAY_OF_WEEK, d);
        // 所在周开始日期
        String data1 = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_WEEK, 6);
        // 所在周结束日期
        String data2 = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());

        if(type.equals("first")){
            return data1;
        }else{
            return data2;
        }
    }




    /**
     * 取得当前日期所在周的第一天
     *
     * @param date
     * @return
     */
    public static Date getFirstDayOfWeek(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.SUNDAY);
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_WEEK,
                calendar.getFirstDayOfWeek()); // Sunday
        return calendar.getTime();
    }

    /**
     * 取得当前日期所在周的最后一天
     *
     * @param date
     * @return
     */
    public static Date getLastDayOfWeek(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.SUNDAY);
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_WEEK,
                calendar.getFirstDayOfWeek() + 6); // Saturday
        return calendar.getTime();
    }

    /**
     * 返回指定日期的季的第一天
     *
     * @param date
     * @return
     */
    public static Date getFirstDayOfQuarter(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return getFirstDayOfQuarter(calendar.get(Calendar.YEAR),
                getQuarterOfYear(date));
    }

    public static String getFirstDayOfQuarter(String dataStr) throws ParseException {

        //Calendar cal = Calendar.getInstance();
        //cal.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(dataStr));

        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        Date date=sdf.parse(dataStr);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        Date mdate = getFirstDayOfQuarter(calendar.get(Calendar.YEAR), getQuarterOfYear(date));

        String dateStr = new SimpleDateFormat("yyyy-MM-dd").format(mdate);

        return dateStr;
    }

    /**
     * 返回指定年季的季的第一天
     *
     * @param year
     * @param quarter
     * @return
     */
    public static Date getFirstDayOfQuarter(Integer year, Integer quarter) {
        Calendar calendar = Calendar.getInstance();
        Integer month = new Integer(0);
        if (quarter == 1) {
            month = 1 - 1;
        } else if (quarter == 2) {
            month = 4 - 1;
        } else if (quarter == 3) {
            month = 7 - 1;
        } else if (quarter == 4) {
            month = 10 - 1;
        } else {
            month = calendar.get(Calendar.MONTH);
        }
        return getFirstDayOfMonth(year, month);
    }

    /**
     * 返回指定日期的月的第一天
     *
     * @param date
     * @return
     */
    public static Date getFirstDayOfMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH), 1);
        return calendar.getTime();
    }

    /**
     * 返回指定年月的月的第一天
     *
     * @param year
     * @param month
     * @return
     */
    public static Date getFirstDayOfMonth(Integer year, Integer month) {
        Calendar calendar = Calendar.getInstance();
        if (year == null) {
            year = calendar.get(Calendar.YEAR);
        }
        if (month == null) {
            month = calendar.get(Calendar.MONTH);
        }
        calendar.set(year, month, 1);
        return calendar.getTime();
    }

    /**
     * 返回指定日期的月的最后一天
     *
     * @param date
     * @return
     */
    public static Date getLastDayOfMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH), 1);
        calendar.roll(Calendar.DATE, -1);
        return calendar.getTime();
    }

    /**
     * 返回指定年月的月的最后一天
     *
     * @param year
     * @param month
     * @return
     */
    public static Date getLastDayOfMonth(Integer year, Integer month) {
        Calendar calendar = Calendar.getInstance();
        if (year == null) {
            year = calendar.get(Calendar.YEAR);
        }
        if (month == null) {
            month = calendar.get(Calendar.MONTH);
        }
        calendar.set(year, month, 1);
        calendar.roll(Calendar.DATE, -1);
        return calendar.getTime();
    }


    /**
     * 返回指定日期的季的最后一天
     *
     * @param date
     * @return
     */
    public static Date getLastDayOfQuarter(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return getLastDayOfQuarter(calendar.get(Calendar.YEAR),
                getQuarterOfYear(date));
    }

    /**
     * 返回指定年季的季的最后一天
     *
     * @param year
     * @param quarter
     * @return
     */
    public static Date getLastDayOfQuarter(Integer year, Integer quarter) {
        Calendar calendar = Calendar.getInstance();
        Integer month = new Integer(0);
        if (quarter == 1) {
            month = 3 - 1;
        } else if (quarter == 2) {
            month = 6 - 1;
        } else if (quarter == 3) {
            month = 9 - 1;
        } else if (quarter == 4) {
            month = 12 - 1;
        } else {
            month = calendar.get(Calendar.MONTH);
        }
        return getLastDayOfMonth(year, month);
    }

    /**
     * 返回指定日期的季度
     *
     * @param date
     * @return
     */
    public static int getQuarterOfYear(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.MONTH) / 3 + 1;
    }




    /*
     * 得到本月的第一天
     */
    public static String getMonthFirstDay() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, calendar
                .getActualMinimum(Calendar.DAY_OF_MONTH));
        //calendar.set(Calendar.DATE, 1);
        SimpleDateFormat simpleFormate = new SimpleDateFormat("yyyy-MM-dd");
        return simpleFormate.format(calendar.getTime());
    }
    
    /*
     * 得到本月的最后一天
     */
    public static String getMonthLastDay() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, calendar
                .getActualMaximum(Calendar.DAY_OF_MONTH));
        //calendar.set(Calendar.DATE, 1);
        //calendar.roll(Calendar.DATE, - 1);
        SimpleDateFormat simpleFormate = new SimpleDateFormat("yyyy-MM-dd");
        return simpleFormate.format(calendar.getTime());
    }
    
    public static String getFirstDay2(String someDate){//未测试
    	java.text.DateFormat df=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	java.util.Calendar c1=java.util.Calendar.getInstance();
    	Calendar   ca   =   Calendar.getInstance();
    	try{
    		ca.setTime(df.parse(someDate));                            //  someDate 为你要获取的那个月的时间
    	}catch(java.text.ParseException e){
    		System.out.println("格式不正确");
    	}
    	ca.set(Calendar.DAY_OF_MONTH,   1);
    	//第一天
    	Date   firstDate   =   ca.getTime();
    	ca.add(Calendar.MONTH,   1);
    	ca.add(Calendar.DAY_OF_MONTH,   -1);
    	//最后一天
    	Date   lastDate   =   ca.getTime();
    	
    	return someDate;
    }
    
    /*
     * 获取当年的第一天
     */
    public static String getCurrYearFirst(){
        Calendar currCal=Calendar.getInstance();
        int currentYear = currCal.get(Calendar.YEAR);
        
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd",Locale.US);
        
        String nowdate = null;
        try {
            nowdate = new String(formatter.format(getYearFirst(currentYear)).getBytes("iso-8859-1"));
        } catch (UnsupportedEncodingException ex) {
        }
        
        return nowdate;
    }
    
    /*
     * 获取某年第一天日期
     */
    public static Date getYearFirst(int year){
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.YEAR, year);
        Date currYearFirst = calendar.getTime();
        return currYearFirst;
    }
    
    /*
     * 获取当年的最后一天
     */
    public static Date getCurrYearLast(){  
        Calendar currCal=Calendar.getInstance();    
        int currentYear = currCal.get(Calendar.YEAR);  
        return getYearLast(currentYear);  
    }
    
    /*
     * 获取某年最后一天日期
     */
    public static Date getYearLast(int year){
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.YEAR, year);
        calendar.roll(Calendar.DAY_OF_YEAR, -1);
        Date currYearLast = calendar.getTime();
        
        return currYearLast;
    }
    
    public static int compareDate(String date1, String data2){
    	int status = 0;
    	
    	java.text.DateFormat df=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	java.util.Calendar c1=java.util.Calendar.getInstance();
    	java.util.Calendar c2=java.util.Calendar.getInstance();
    	
    	try{
    		c1.setTime(df.parse(date1));
    		c2.setTime(df.parse(data2));
    	}catch(java.text.ParseException e){
    		System.out.println("格式不正确");
    	}
    	
    	int result=c1.compareTo(c2);
    	if(result==0){
    		status = 0;
    		//System.out.println("date1相等data2");
    	}else if(result<0){
    		status = 1;
    		//System.out.println("date1小于data2");
    	}else if(result>0){
    		status = 2;
    		//System.out.println("date1大于data2");
    	}

        return status;
    }
    
    public static long getDateHours(String date1, String data2){  //获取时间间隔的小时
    	long h = 0;
    	
    	java.text.DateFormat df=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	java.util.Calendar c1=java.util.Calendar.getInstance();
    	java.util.Calendar c2=java.util.Calendar.getInstance();
    	
    	try{
    		c1.setTime(df.parse(date1));
    		c2.setTime(df.parse(data2));
    	}catch(java.text.ParseException e){
    		System.out.println("格式不正确");
    	}
    	
    	long l = c2.getTime().getTime() - c1.getTime().getTime();
    	
    	h = l/(3600*1000);

        return h;
    }
    
    public static String getDateForFileName(){
    	String nowdate = null;
    	Date currTime = new Date();
    	SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss",Locale.US);
        try {
            nowdate = new String(formatter.format(currTime).getBytes("iso-8859-1"));
        } catch (UnsupportedEncodingException ex) {
        }
        
        return nowdate;
    }
    
    public static String getDateForDirectory(int type){
    	String nowdate = null;
    	Date currTime = new Date();
    	SimpleDateFormat formatter = null;
    	
    	if(type==1){
    		formatter = new SimpleDateFormat("yyyy",Locale.US);
    	}else if(type==2){
    		formatter = new SimpleDateFormat("yyyyMM",Locale.US);
    	}else if(type==3){
    		formatter = new SimpleDateFormat("yyyyMMdd",Locale.US);
    	}
    	
        try {
            nowdate = new String(formatter.format(currTime).getBytes("iso-8859-1"));
        } catch (UnsupportedEncodingException ex) {
        }
        
        return nowdate;
    }
    
    public static String getDateForDirectory(){
    	String NewYear="",NewMonth="",NewDay="";
        int mDay=0;
        java.util.Calendar cal = Calendar.getInstance();
        NewYear = ""+(cal.get(cal.YEAR));
        NewMonth = ""+(cal.get(cal.MONTH)+1);
        mDay = cal.get(cal.DATE);
        NewDay = ""+mDay;

        //if(NewYear.length()<=2)
            //NewYear="20" + NewYear;
        if(NewMonth.length()<=1)
            NewMonth="0" + NewMonth;
        if(mDay<=9)
            NewDay="0" + mDay;
        
        
        String nowdate = NewYear + "" + NewMonth + "" + NewDay;

        return nowdate;
    }

    public static String getDateOne(String type){
        java.util.Calendar cl = Calendar.getInstance();
        String revalue="";
        if(type.equals("year")){
            revalue = cl.get(Calendar.YEAR)+"";
        }else if(type.equals("month")){
            revalue = cl.get(Calendar.MONTH)+1+"";
        }else if(type.equals("day")){
            revalue = cl.get(Calendar.DAY_OF_MONTH)+"";
        }
        
        return revalue;
    }

    public static int getYear(){
        java.util.Calendar cl = Calendar.getInstance();
        int nowYear=cl.get(Calendar.YEAR);

        return nowYear;//2007
    }
    
    public static String getYesterdayDate(){
    	java.util.Calendar cl = Calendar.getInstance();
    	cl.add(Calendar.DATE, -1);
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    	
        return sdf.format(cl.getTime());
    }
    
    public static String getTomorrowDate(){
    	java.util.Calendar cl = Calendar.getInstance();
    	cl.add(Calendar.DATE, 1);
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    	
        return sdf.format(cl.getTime());
    }
    
    public static String getNextDate(int day){
    	java.util.Calendar cl = Calendar.getInstance();
    	cl.add(Calendar.DATE, day);
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    	
        return sdf.format(cl.getTime());
    }
    
    public static String getNextDate(int day, String date) throws Exception{
    	java.util.Calendar cl = Calendar.getInstance();
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    	cl.setTime(sdf.parse(date));
    	cl.add(Calendar.DATE, day);
    	
        return sdf.format(cl.getTime());
    }
    
    public static String getNextDateWeek(int day, String date) throws Exception{
    	String[] weekOfDays = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
    	java.util.Calendar cl = Calendar.getInstance();
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    	cl.setTime(sdf.parse(date));
    	cl.add(Calendar.DATE, day);
    	int w = cl.get(Calendar.DAY_OF_WEEK) - 1;
    	if(w < 0)
    		w = 0;
    	
        return "<em>"+sdf.format(cl.getTime())+"</em><p>"+weekOfDays[w]+"</p>";
    }
    
    public static int daysBetween(String smdate,String bdate) throws Exception{
    	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
    	Calendar cal = Calendar.getInstance();
    	cal.setTime(sdf.parse(smdate));
    	long time1 = cal.getTimeInMillis();
    	cal.setTime(sdf.parse(bdate));
    	long time2 = cal.getTimeInMillis();
    	long between_days=(time2-time1)/(1000*3600*24);
    	
    	return Integer.parseInt(String.valueOf(between_days));
    }
    
    public static String getDateBefore(String mdate,int day) throws Exception{
    	String strDate = "";
    	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	Date d1=sdf.parse(mdate);
    	Calendar now=Calendar.getInstance();
    	now.setTime(d1);
    	now.set(Calendar.DATE,now.get(Calendar.DATE)-day);
    	strDate = new String(sdf.format(now.getTime()).getBytes("iso-8859-1"));
    	
    	return strDate;
    }
    
    public static String getDateAfter(String mdate,int day) throws Exception{
    	String strDate = "";
    	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	Date d1=sdf.parse(mdate);
    	Calendar now=Calendar.getInstance();
    	now.setTime(d1);
    	now.set(Calendar.DATE,now.get(Calendar.DATE)+day);
    	strDate = new String(sdf.format(now.getTime()).getBytes("iso-8859-1"));
    	
    	return strDate;
    }
    
    public static String StringLeft(String str, int len)
    {
        if (str == null) {
            str = "";
        } else if (str.length() > len) {
            try {
                str = str.substring(0, len);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        return str;
    }

    public static String StringLeftToRight(String str, int left, int right)
    {
        if (str == null) {
            str = "";
        } else if (str.length() > left) {
            try {
                str = str.substring(left, right);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        return str;
    }
    
    public static String StringLeftWithDot(String str, int len)
    {
        if (str == null) {
            str = "";
        } else if (str.length() > len) {
            try {
                str = str.substring(0, len) + "...";
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        return str;
    }
    
    /**
     * 取出Flash Code
     * @param flashName String
     * @param width int
     * @param height int
     * @return String
     * @throws Exception
     */
    public String getFlashCode(String flashName,int width,int height) throws Exception{
    	String reValue = "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0' width='"+width+"' height='"+height+"'>";
        reValue = reValue + "<param name='movie' value='"+flashName+"'>";
        reValue = reValue + "<param name='quality' value='high'>";
        reValue = reValue + "<embed src='"+flashName+"' quality='high' pluginspage='http://www.macromedia.com/go/getflashplayer' type='application/x-shockwave-flash' width='"+width+"' height='"+height+"'></embed></object>";

        return reValue;
    }
    
    public static boolean sqlInj(String str,int isCheck){
    	if(isCheck == 0){
    		return false;
    	}else{
    		String injectStr = "'|and|exec|insert|select|delete|update|drop|count|*|%|chr|mid|master|truncate|char|declare|;|or|-|+|,";
        	String injStr[] = injectStr.split("\\|");
        	for (int i = 0; i < injStr.length; i++)
        	{
        		if (str.indexOf(injStr[i])>=0)
                {
        			System.out.println("sql inject");
        			return true;
                }
        	}
        	
        	return false;
    	}
    	
    }
    
    public boolean isValidInput(String str){
    	if(str.matches("[A-Za-z0-9]+"))
    		return true;
    	else
    		return false;
    }
    
    public static boolean extAllowed(String ext){
    	boolean boolValue = false;
    	if(ext==null||ext.equals(""))
    		boolValue = false;
    	else if(!ext.equalsIgnoreCase("jpg")&&!ext.equalsIgnoreCase("jpeg")&&!ext.equalsIgnoreCase("gif")&&!ext.equalsIgnoreCase("bmp")&&!ext.equalsIgnoreCase("png"))
    		boolValue = true;
    	
    	return boolValue;
    }
    
    public static String getHtmlFileLocation(String str)
    {
		if (str.length() >= 10) {
			
			try {
            	str = str.substring(0, 4) + "/"+   str.substring(5, 7) + "" + str.substring(8, 10) + "/";
            } catch (Exception e) {
            	System.out.println("StrEdit getHtmlFileLocation:" + e.getLocalizedMessage());
            }
		}
        return str;
    }
    
    public static String getPage(String url) {
    	String targ = "";
        if(url.indexOf("61.154.104.70") == -1 && url.indexOf("http") == -1 && url.indexOf("localhost") == -1){
        	targ = "<script language='JavaScript'>window.location.href='http://www.u138.cn';</script>";
    	}
        
        return targ;
    }
    
    public static String hiddenLastIP(String ipaddress){
    	
    	if(ipaddress==null||ipaddress.equals("")){		
    		return "";
    	}
    	StringBuffer reValue = new StringBuffer("");
    	String s[] = ipaddress.split("\\.");
    	int slen = s.length - 1;
    	for(int i=0; i< slen; i++)
    	{
    		reValue.append(s[i]+".");
    	}
    	return reValue+"*";
    }
    
    public static String getNameWithoutExtension(String fileName) {
		return fileName.substring(0, fileName.lastIndexOf("."));
	}
    
    public static String getExtension(String fileName) {
    	return fileName.substring(fileName.lastIndexOf(".")+1);
    }
    
    public static String[] getStringDate(String date) throws ParseException{
    	String[] weeks = new String[7];//返回的这周的日期
    	String[] a = date.split("-");
    	int week = getDayOfWeek(a[0], a[1], a[2]);//获取周几
    	int minWeek = 0;
    	int maxWeek = 7;
    	String format = "yyyy-MM-dd";

    	if(week == 1){//如果是周日（老外是从周日开始算一周）
    		weeks[6] = date;
    		for(int i = 5; i >= 0; i--){
    			weeks[i] = getFormatDateAdd(getStrToDate(date, format), -1, format);
    			date = weeks[i];
    		}
    	}else{
    		int temp = week - 2;
    		weeks[temp] = date;
    		for(int i = temp - 1; i >= minWeek; i--){
    			weeks[i] = getFormatDateAdd(getStrToDate(date, format), -1, format);
    			date = weeks[i];
    		}
    		
    		date = weeks[temp];
    		for(int i = temp + 1; i < maxWeek; i++){
    			weeks[i] = getFormatDateAdd(getStrToDate(date, format), 1, format);
    			date = weeks[i];
    		}
    	}
    	
    	return weeks;
    }
    
    /**
    * 根据指定的年、月、日返回当前是星期几。1表示星期天、2表示星期一、7表示星期六。
    * 
    * @param year
    * @param month
    * month是从1开始的12结束
    * @param day
    * @return 返回一个代表当期日期是星期几的数字。1表示星期天、2表示星期一、7表示星期六。
    */
    public static int getDayOfWeek(String year, String month, String day) {
    	Calendar cal = new GregorianCalendar(new Integer(year).intValue(),
    	new Integer(month).intValue() - 1, new Integer(day).intValue());
    	return cal.get(Calendar.DAY_OF_WEEK);
    }
    
    /**
    * 取得给定日期加上一定天数后的日期对象.
    * 
    * @param date
    * 给定的日期对象
    * @param amount
    * 需要添加的天数，如果是向前的天数，使用负数就可以.
    * @param format
    * 输出格式.
    * @return Date 加上一定天数以后的Date对象.
    */
    public static String getFormatDateAdd(Date date, int amount, String format) {
    	Calendar cal = new GregorianCalendar();
    	cal.setTime(date);
    	cal.add(GregorianCalendar.DATE, amount);
    	return getFormatDateTime(cal.getTime(), format);
    }
    
    /**
    * 根据给定的格式与时间(Date类型的)，返回时间字符串。最为通用。
    * 
    * @param date
    * 指定的日期
    * @param format
    * 日期格式字符串
    * @return String 指定格式的日期字符串.
    */
    public static String getFormatDateTime(Date date, String format) {
    	SimpleDateFormat sdf = new SimpleDateFormat(format);
    	return sdf.format(date);
    }
    
    /**
    * 返回制定日期字符串的date格式
    * 
    * @param date
    * @param format
    * @return
    * @throws ParseException
    */
    public static Date getStrToDate(String date, String format) throws ParseException{
    	SimpleDateFormat sdf = new SimpleDateFormat(format);
    	return sdf.parse(date);
    }
    
    public static String formatFloat(double value) {
        DecimalFormat df = new DecimalFormat("0.0");
        return df.format(value);
    }
    
    public static String getListByArray(String[] arrClassID){
    	String strValue = "";
    	if (arrClassID!=null){
    		for(int i=0;i<arrClassID.length;i++){
    			if(strValue.equals(""))
    				strValue = strValue + StrEdit.UnicodeToUTF8(arrClassID[i]);
    			else
    				strValue = strValue + "," + StrEdit.UnicodeToUTF8(arrClassID[i]);
    		}
    	}
    	
    	return strValue;
    }
    
    public static boolean isInclude(String[] arrClassID,int ClassID){
    	boolean boolValue = false;
    	if (arrClassID!=null){
			for(int i=0;i<arrClassID.length;i++){
				if(StrEdit.StrToInt(arrClassID[i])==ClassID){
					boolValue = true;
					break;
				}
			}
		}
    	
    	return boolValue;
    }
    
    public static boolean isInclude(String strID,int id){
    	boolean boolValue = false;
    	if(!strID.equals("")){
    		String[] arrParameter = strID.split("\\,");
        	if (arrParameter!=null){
    			for(int i=0;i<arrParameter.length;i++){
    				if(StrEdit.StrToInt(arrParameter[i])==id){
    					boolValue = true;
    					break;
    				}
    			}
    		}
    	}
    	
    	return boolValue;
    }
    
    public static boolean isInclude(String[] arrClassID,String ClassName){
    	boolean boolValue = false;
    	if (arrClassID!=null){
			for(int i=0;i<arrClassID.length;i++){
				if(arrClassID[i].equals(ClassName)){
					boolValue = true;
					break;
				}
			}
		}
    	
    	return boolValue;
    }
    
    
    
}
