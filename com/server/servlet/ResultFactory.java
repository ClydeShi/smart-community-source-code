package com.server.servlet;

public class ResultFactory {
	private final static Result success=new Result(true,null,null);
	public static Result createSuccessResult() {
		return success;
	}
	public static Result createErrorResult(String error) {
		return new Result(false,error,null);
	}
}
