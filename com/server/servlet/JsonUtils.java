package com.server.servlet;

import java.lang.reflect.Type;

import com.google.gson.Gson;

public class JsonUtils {
	private final static Gson gson=new Gson();
	public static <T> T toObject(String json,Type t) {
		return gson.fromJson(json, t);
	}
	public static <T> T toObject(String json,Class<T> c) {
		return gson.fromJson(json, c);
	}
	public static String toJson(Object obj) {
		return gson.toJson(obj);
	}
}