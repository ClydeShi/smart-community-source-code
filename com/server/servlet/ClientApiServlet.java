package com.server.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 

import com.server.data.DataInterface;
import com.server.data.DemoDataInterface;
import com.server.data.StrEdit;
import com.server.model.CommonList;
import com.server.model.SysPassValue;
import com.server.model.User;
import com.server.model.SysPassValue;
import com.server.servlet.Result;

public class ClientApiServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private DataInterface api=null;
    public ClientApiServlet() {
        super();
        api=new DemoDataInterface();
    }
    
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		Result r = new Result();
		
		System.out.println("visit interface");
		
		/*
		 * 使用json数据格式 http://www.huahuadesign.net/clientapi?method=getServerList&funName=getMasterJianCaiList&pagenumber=1&userid=4&arg1=1&arg2=&arg3=&arg4=&arg5=&arg6=&arg7=&arg8=&arg9=&arg10=
		 * 直接把参数解析成数据 api.sendOrderToServerForList(dataObj) dataObj改为参数列表，DemoDataInterface那边直接接收参数
		 */
		
		try {
			String method=request.getParameter("method");
			System.out.println(method);
			
			if ("sendOrderToServerForList".equals(method)) {
				String json = request.getParameter("curObject");
				SysPassValue dataObj = JsonUtils.toObject(json, SysPassValue.class);//需要把json转成SysPassValue对象
				r.setData(api.sendOrderToServerForList(dataObj));
			} else if ("sendOrderToServerForValue".equals(method)) {
				String json = request.getParameter("curObject");
				SysPassValue dataObj = JsonUtils.toObject(json, SysPassValue.class);
				r.setData(api.sendOrderToServerForValue(dataObj));
			} else if ("getHhSysArgument".equals(method)) {   
				// 5个轮播图片，5个轮播类型，5个轮播值 
				r.setData(api.getHhSysArgument());
			} else if ("userLogin".equals(method)) {   
				//得到用户登入信息
				String json=request.getParameter("user");
				
				User temp=JsonUtils.toObject(json, User.class);
				
				//System.out.println();
				
				//User test = api.userLogin(temp);
				///if(test!=null){
					//System.out.println("user name:"+test.getName());
				//}
				r.setData(api.userLogin(temp));
			} else if ("getTaskList".equals(method)) {   
				//得到用户登入信息
				String userId=request.getParameter("userId");
				String shopType=request.getParameter("shopType");
				int curpaper=StrEdit.StrToInt(request.getParameter("curpaper"));
				System.out.println("shopType:"+shopType);
				
				//System.out.println();
				
				//User test = api.userLogin(temp);
				///if(test!=null){
					//System.out.println("user name:"+test.getName());
				//} 
				r.setData(api.getTaskList(userId, shopType, curpaper));
			} else if ("getTaskDetail".equals(method)) {   
				//得到用户登入信息
				String userId=request.getParameter("userId");
				String taskid=request.getParameter("taskid");
				System.out.println("taskid:"+taskid);
				
				//CommonList temp=JsonUtils.toObject(json, CommonList.class);
				
				//System.out.println();
				
				//User test = api.userLogin(temp);
				///if(test!=null){
					//System.out.println("user name:"+test.getName());
				//} 
				r.setData(api.getTaskDetail(taskid, userId));
			}
			} catch(Throwable t) {
				t.printStackTrace();
				r.setResult(false);
				r.setError("error !!");
		}
		 
		String result=JsonUtils.toJson(r);
		response.setContentType("text/json");
		response.getWriter().write(result);
		
	}
	
	
	
}
