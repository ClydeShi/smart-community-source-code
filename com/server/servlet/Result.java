package com.server.servlet;

public class Result<T> {
	private boolean result=true;
	private String error=null;
	private T data=null;
	public Result() {
		super();
	}
	public Result(boolean result, String error, T data) {
		super();
		this.result = result;
		this.error = error;
		this.data = data;
	}
	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
}
